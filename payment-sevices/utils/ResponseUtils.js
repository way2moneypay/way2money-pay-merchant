var ResponseUtils = module.exports

var LogsModel = require('../models/LogsModel')
var this_file_name = 'restapp/utils/ResponseUtils.js'

ResponseUtils.print = function (file_name, function_name) {
  let error_message = 'Error occured at ' + file_name + ' in ' + function_name + ' : '
  let log_message = 'Logging for ' + file_name + ' in ' + function_name + ' : '
  let exception_message = 'Exception occured at ' + file_name + ' in ' + function_name + ' : '

  this.error = function (message) {
    console.error(error_message)
    let error_object = {}
    error_object.type = 'ERROR'
    error_object.error_message = message
    error_object.file_name = file_name
    error_object.function_name = function_name
    console.error(error_object)
  }

  this.log = function (message) {
    console.log(log_message)
    let log_object = {}
    log_object.type = 'LOGGING'
    log_object.log_message = message
    log_object.file_name = file_name
    log_object.function_name = function_name
    console.log(log_object)
  }

  this.exception = function (message) {
    console.log(exception_message)
    let exception_object = {}
    exception_object.type = 'EXCEPTION'
    exception_object.log_message = message
    exception_object.file_name = file_name
    exception_object.function_name = function_name
    console.log(exception_object)
  }
}

var saveLogsData = function (data, code, message, type, file_name, function_name) {
  let logs_data = new LogsModel()

  logs_data.message = message
  logs_data.type = type
  logs_data.code = code
  logs_data.error_data = data
  logs_data.file_name = file_name
  logs_data.function_name = function_name

  logs_data.save(function (logs_stored_error, logs_stored) {
    if (logs_stored_error) {
      console.error('error while saving in ' + type, logs_data)
    };
  })
}

ResponseUtils.Logs = function (file_name, function_name) {
  this.error = function (data, code, message) {
    saveLogsData(data, code, message, 'ERROR', file_name, function_name)
  }

  this.log = function (data, code, message) {
    saveLogsData(data, code, message, 'LOG', file_name, function_name)
  }

  this.exception = function (data, code, message) {
    saveLogsData(data, code, message, 'EXCEPTION', file_name, function_name)
  }
}

ResponseUtils.PrintCommon = function () {
  // let error_message = "Error occured at " + file_name + " in " + function_name + " : ";
  // let log_message = "Logging for " + file_name + " in " + function_name + " : ";
  // let exception_message = "Exception occured at " + file_name + " in " + function_name + " : ";

  this.error = function (message, total_error) {
    let error_object = {}
    error_object.type = 'ERROR'
    error_object.error_message = message
    error_object.error_info = getErrorInfo(total_error)
    console.error(error_object)
  }

  this.log = function (message, total_error) {
    // console.log(log_message);
    let log_object = {}
    log_object.type = 'LOGGING'
    log_object.log_message = message
    log_object.log_info = getErrorInfo(total_error)

    console.log(log_object)
  }

  this.exception = function (message, total_error) {
    // console.log(exception_message);
    let exception_object = {}
    exception_object.type = 'EXCEPTION'
    exception_object.log_message = message
    exception_object.exception_info = getErrorInfo(total_error)

    console.log(exception_object)
  }
}

ResponseUtils.response = function (api_response) {
  let error_object = {}
  error_object.status_code = 400
  error_object.status = 'ERR'
  error_object.type = 'ERROR'

  let success_object = {}
  success_object.status_code = 200
  success_object.status = 'SUCCESS'

  this.error = function (message, status, status_code, data) {
    error_object.message = message
    error_object.status = status
    error_object.status_code = status_code || error_object.status_code
    data || data == 0 ? error_object.data = data : 'no data'

    api_response.status(200).send(error_object)
  }

  this.success = function (message, data, status) {
    success_object.message = message
    success_object.status = status || success_object.status
    data || data == 0 ? success_object.data = data : 'no data'

    api_response.status(200).send(success_object)
  }

  this.errorData = function (message, status, status_code) {
    error_object.message = message
    error_object.status = status
    error_object.status_code = status_code || error_object.status_code

    return error_object
  }

  this.successData = function (message, data, status) {
    success_object.message = message
    success_object.status = status || success_object.status
    data || date == 0 ? success_object.data = data : 'no data'
    return success_object
  }
}

var getErrorInfo = function (error) {
  // console.log("error_data :::::::::::::::::::::::::::::: ", error.stack.split("\n")[1].trim());
  return error.stack.split('\n')[1].trim()
}

var saveLogsDataWithInfo = function (message, code, data, type, error, total_error) {
  let logs_data = new LogsModel()

  logs_data.message = message
  logs_data.type = type
  logs_data.code = code
  logs_data.error_data = data
  logs_data.error_info = total_error ? error.stack : getErrorInfo(error)

  logs_data.save(function (logs_store_error, logs_stored) {
    if (logs_store_error) {
      console.error(logs_store_error)
      console.error('error while saving in ' + type, logs_data)
    };
  })
}

ResponseUtils.LogsCommon = function () {
  this.error = function (message, code, data, error, total_error) {
    saveLogsDataWithInfo(message, code, data, 'ERROR', error, total_error)
  }

  this.log = function (message, code, data, error, total_error) {
    saveLogsDataWithInfo(message, code, data, 'LOG', error)
  }

  this.exception = function (message, code, data, error, total_error) {
    saveLogsDataWithInfo(message, code, data, 'EXCEPTION', error, total_error)
  }
}
