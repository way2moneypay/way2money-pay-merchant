const ValidationUtils = module.exports

const moment = require('moment')

ValidationUtils.required = function (value, configuration) {
  let except_zero = configuration.except_zero
  if ((!value && !except_zero) || (except_zero && !value && value != 0)) return false
  return true
}

ValidationUtils.minimumValue = function (value, configuration) {
  let minimum = configuration['min']
  return value > minimum
}

ValidationUtils.maximumValue = function (value, configuration) {
  let maximum = configuration['max']
  return value < maximum
}

ValidationUtils.range = function (value, configuration) {
  let minimum = configuration['min']
  let maximum = configuration['max']

  return (value > minimum && value < maximum)
}

ValidationUtils.minimumLength = function (value, configuration) {
  let minimum = configuration['min']
  return value.length > minimum
}

ValidationUtils.maximumLength = function (value, configuration) {
  let maximum = configuration['max']
  return value.length < maximum
}

ValidationUtils.rangeLength = function (value, configuration) {
  let minimum = configuration['min']
  let maximum = configuration['max']

  return (value.length > minimum && value.length < maximum)
}

ValidationUtils.typeCheck = function (value, configuration) {
  let type = configuration['type']
  return typeof value === type
}

ValidationUtils.valueCheck = function (value, configuration) {
  let check_data = configuration['value']
  return value == check_data
}

ValidationUtils.inList = function (value, configuration) {
  let list = configuration['list']
  return list.indexOf(value) != -1
}

ValidationUtils.email = function (value) {
  let pattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
  let regex = new RegExp(pattern, ['g'])
  return regex.test(value)
}

ValidationUtils.pattern = function (value, configuration) {
  let pattern = configuration['pattern']
  let regex = new RegExp(pattern)
  return regex.test(value)
}

ValidationUtils.date = function (value, configuration) {
  let formats_list = configuration['formats_list']
  return moment(value, formats_list, true).isValid()
}

ValidationUtils.dateDiff = function (value, configuration, total_data) {
  let formats_list = configuration['formats_list']
  let second_value_type = configuration['second_value_type']
  let second_value = configuration['second_value']
  let minimum_difference = configuration['minimum_difference']
  let maximum_differencee = configuration['maximum_differencee']
  let cheking_type = configuration['cheking_type']
  let diff_type = configuration['diff_type'] ? configuration['diff_type'] : 'years'
  let diff_between = configuration['diff_between']

  // if (!ValidationUtils.date(value, formats_list)) {
  //   return false;
  // }

  console.log(value)
  let given_date = moment(value, formats_list)
  let another_date = moment()

  if (second_value_type == 'field') {
    if (!total_data || !second_value || !total_data[second_value]) {
      return false
    }

    let another_date = moment(total_data[second_value])
  }

  console.log(configuration)
  // return true;

  let diffrence = !diff_between ? another_date.diff(given_date, diff_type) : given_date.diff(another_date, diff_type)

  console.log(diffrence, cheking_type)

  if (cheking_type == 'max') {
    return diffrence < maximum_differencee
  }

  if (cheking_type == 'minimum') {
    return diffrence > minimum_difference
  }

  if (cheking_type == 'range') {
    return (diffrence > minimum_difference && diffrence < maximum_differencee)
  }
}

ValidationUtils.dependecyCheckOne = function (total_data, configuration, total_configuration) {
  let field_name = configuration['field_name']
  let field_valid = configuration['field_valid']
  let feild_validation = true
  let extra_validation = configuration['extra_validation']

  if (field_valid) {
    feild_validation = ValidationUtils.validateFeild(total_data[field_name], total_configuration[field_name], total_configuration, total_data)
    if (feild_validation.error) {
      return false
    }
  }

  if (extra_validation) {
    let valid = ValidationUtils.validateFeild(total_data, extra_validation, total_configuration, total_data)
    return valid
  }

  return true
}

ValidationUtils.dependecyCheckMultiple = function (type, total_configuration, fields_configuration, total_data) {
  for (var index = 0; index < fields_configuration; ++index) {
    let configuration = fields_configuration[index]
    let valid = ValidationUtils.dependecyCheckOne(total_data, configuration, total_configuration)

    if (type == 'or' && valid) {
      return true
    }

    if (type == 'and' && !valid) {
      return false
    }
  }

  if (type == 'or') {
    return false
  }

  if (type == 'and') {
    return true
  }
}

ValidationUtils.dependencyCheck = function (configuration, total_configuration, total_data) {
  for (var index = 0; index < configuration.length; ++index) {
    let type = configuration[index]['type']
    let fields_configuration = configuration[index]['config']

    if (!ValidationUtils.dependecyCheckMultiple(type, total_configuration, fields_configuration, total_data)) {
      return false
    }
  }

  return true
}

ValidationUtils.validateFeild = function (value, configuration, total_configuration, total_data) {
  if (configuration['dependendy']) {
    let dependency_validations = configuration['dependendy']['validations']
    let valid = ValidationUtils.dependecyCheck(dependency_validations, total_configuration, total_data)
    if (!valid) {
      return configuration['dependendy']['return_value']
    }
  }

  let feild_validations = configuration['validations']

  for (var index = 0; index < feild_validations.length; ++index) {
    let validation = feild_validations[index]
    let validation_name = validation['name']
    let validatioin_config = validation['config']

    if (!ValidationUtils[validation_name](value, validatioin_config, total_configuration)) {
      return {
        error: true,
        message: validatioin_config['error_message']
      }
    }
  }

  return {
    error: false
  }
}

ValidationUtils.validate = function (total_data, configuration) {
  let return_type = configuration['return_type']
  let validation_conf = configuration['configuration']
  let validation_feilds = Object.keys(validation_conf)
  let return_data = {
    validation_error: false,
    data: []
  }

  for (var index = 0; index < validation_feilds.length; ++index) {
    let field = validation_feilds[index]
    let value = total_data[field]
    let field_conf = validation_conf[field]

    let validation_data = ValidationUtils.validateFeild(value, field_conf, validation_conf, total_data)
    if (validation_data.error) {
      return_data.validation_error = true
      return_data.data.push({
        key: field,
        message: validation_data.message
      })

      if (return_type == 'single') {
        return return_data
      }
    }
  }

  return return_data
}
// Preparing configuration

ValidationUtils.Required = function (except_zero, error_message) {
  return {
    'name': 'required',
    'config': {
      'error_message': error_message,
      'except_zero': except_zero
    }
  }
}

ValidationUtils.TypeCheck = function (type, error_message) {
  return {
    'name': 'typeCheck',
    'config': {
      'type': type,
      'error_message': error_message
    }
  }
}

ValidationUtils.Pattern = function (pattern, error_message) {
  return {
    'name': 'pattern',
    'config': {
      'pattern': pattern,
      'error_message': error_message
    }
  }
}

ValidationUtils.Email = function (error_message) {
  return {
    'name': 'email',
    'config': {
      'error_message': error_message
    }
  }
}

ValidationUtils.Date = function (formats, error_message) {
  return {
    'name': 'date',
    'config': {
      'formats_list': typeof formats === 'string' ? [formats] : formats,
      'error_message': error_message
    }
  }
}

ValidationUtils.MinimumValue = function (minimum, error_message) {
  return {
    'name': 'minimumValue',
    'config': {
      'min': minimum,
      'error_message': error_message
    }
  }
}

ValidationUtils.MaximumValue = function (maximum, error_message) {
  return {
    'name': 'maximumValue',
    'config': {
      'max': maximum,
      'error_message': error_message
    }
  }
}

ValidationUtils.Range = function (minimum, maximum, error_message) {
  return {
    'name': 'range',
    'config': {
      'min': minimum,
      'max': maximum,
      'error_message': error_message
    }
  }
}

ValidationUtils.MaximumLength = function (maximum, error_message) {
  return {
    'name': 'maximumLength',
    'config': {
      'max': maximum,
      'error_message': error_message
    }
  }
}

ValidationUtils.MinimumLength = function (minimum, error_message) {
  return {
    'name': 'minimumLength',
    'config': {
      'min': minimum,
      'error_message': error_message
    }
  }
}

ValidationUtils.RangeLength = function (minimum, maximum, error_message) {
  return {
    'name': 'rangeLength',
    'config': {
      'min': minimum,
      'max': maximum,
      'error_message': error_message
    }
  }
}

ValidationUtils.InList = function (list, error_message) {
  return {
    'name': 'inList',
    'config': {
      'list': list,
      'error_message': error_message
    }
  }
}

ValidationUtils.ValueCheck = function (value, error_message) {
  return {
    'name': 'valueCheck',
    'config': {
      'value': value,
      'error_message': error_message
    }
  }
}

ValidationUtils.InList = function (list, error_message) {
  return {
    'name': 'inList',
    'config': {
      'list': list,
      'error_message': error_message
    }
  }
}

ValidationUtils.DateDiff = function (formats, c_type, d_type, max, min, error_message, second_type, second_value, d_between) {
  return {
    'name': 'dateDiff',
    'config': {
      'formats_list': typeof formats === 'string' ? [formats] : formats,
      'second_value_type': second_type,
      'second_value': second_value,
      'minimum_difference': min,
      'maximum_differencee': max,
      'cheking_type': c_type,
      'diff_type': d_type,
      'diff_between': d_between,
      'error_message': error_message
    }
  }
}

let configuration = {
  'return_type': 'single',
  'configuration': {
    'dob': {
      'validations': [
        ValidationUtils.Required('Please enter Date of Birth.'),
        ValidationUtils.Date(['DD/MM/YYYY', 'MM/DD/YYYY'], 'Please enter valid Date of Birth.')
      ]
    }
  }
}

let data = {
  'dob': '12/24/2011'
}
// console.log(ValidationUtils.validate(data, configuration));
// console.log(moment("12/24/2011", ["DD/MM/YYYY", "MM/DD/YYYY"], true).isValid())
