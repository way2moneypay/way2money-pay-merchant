// UTILS AND SERVICES AND CONFIG

const ResponseUtils = require('./ResponseUtils')

const SMSAlertsModel = require('./../models/SMSAlertsModel')
const EmailAlertsModel = require('./../models/EmailAlertsModel')
const EncDec = require('./EncDec')

// NODE MODULES

const AppConfig = require('config')
const MYSQL = require('mysql')
const Request = require('request')
const replaceall = require('replaceall')

// INTIALIZATIONS

var NotificationUtils = module.exports

var this_file_name = 'restapp/utils/NotificationUtils'

NotificationUtils.notification = function () {

}

NotificationUtils.sms = function (template_name, configuration, callback) {
  let this_function_name = 'NotificationUtils.sms'
  let Print = ResponseUtils.print(this_file_name, this_function_name)

  console.log('*******TEmplate Name at NotificationUtils.sms***************', template_name, configuration)
  try {
    SMSAlertsModel.findOne({
      alert_type: template_name,
      status: 'active'
    }, function (err, resp) {
      if (err) {
        console.error('error while getting sms template with name', templatename)
        callback(err)
      } else if (resp && configuration.mobile && configuration.mobile.toString().length === 10) {
        let body = resp.alert_content
        Object.keys(configuration.replace).forEach(function (obj) {
          body = body.replace('##' + obj + '##', configuration.replace[obj])
        })
        let con = MYSQL.createConnection({
          host: AppConfig.smsdb.hostname,
          user: AppConfig.smsdb.username,
          password: AppConfig.smsdb.password,
          database: AppConfig.smsdb.database
        })
        con.connect(function (err) {
          if (err) {
            console.log('__==RECHED TO FIRST ERR==___')
            console.error(err)
          }
          console.log('configuration :: ', configuration)
          let sender_id = configuration.sender_id ? configuration.sender_id : resp.sender_id

          let sql = 'insert into user_free_message_reg(custid,mobileno,message,senddate,sender,domain) values(0,?,?,CURRENT_TIMESTAMP,?,?)'
          con.query(sql, [configuration.mobile, body, sender_id, sender_id], function (err, result) {
            con.end()
            if (err) {
              console.log('__==RECHED TO SECOND ERR==___')
              console.error(err)
              return callback(err)
            }
            callback(null, 'sms sent successfully')
          })
        })
      } else {
        console.log(resp)
        console.log('invalid mobile number length')
        callback('invalid mobile number')
      }
    })
  } catch (error) {
    Print.exception(error)
    callback(error, null)
  }
}

NotificationUtils.email = function (template_name, configuration, callback) {
  let this_function_name = 'NotificationUtils.email'
  let Print = ResponseUtils.print(this_file_name, this_function_name)

  try {
    EmailAlertsModel.findOne({
      alert_name: template_name,
      status: 'active'
    }, function (err, resp) {
      if (err) {
        console.error('error while getting email template with name', templatename)
        callback(err)
      } else {
        // console.log("resp, ", resp);
        if (resp && resp.alert_route === 'api') {
          let emailmsg = {}
          emailmsg.em = configuration.email
          let body = resp.alert_content
          Object.keys(configuration.replace).forEach(function (obj) {
            body = body.replace('##' + obj + '##', configuration.replace[obj])
          })
          emailmsg.body = body
          emailmsg.subj = resp.alert_subject
          emailmsg.fromname = 'Way2Money'
          emailmsg.msgid = Date.now()
          emailmsg.fromdomain = 'way2moneymail.com'
          let encemail = EncDec.encrypt(JSON.stringify(emailmsg), 'truelittle@#$')
          encemail = replaceall('/', '<->', encemail)
          Request(resp.api_url + encemail, function (error, response, body) {
            console.log('error:', error) // Print the error if one occurred
            console.log('statusCode:', response && response.statusCode) // Print the response status code if a response was received
            console.log('body:', body) // Print the HTML for the Google homepage.
            callback(null, 'email sent successfully')
          })
        } else {
          console.log('invalid alert route')
          console.log(resp)
          callback('invalid alert route')
        }
      }
    })
  } catch (error) {
    Print.exception(error)
    callback(error, null)
  }
}
