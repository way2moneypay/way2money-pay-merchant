/* Mukesh */
/* PaymentService */
var paymentService = module.exports

const mongoose = require('mongoose')
/* requiring utils */
var ResponseUtils = require('./../utils/ResponseUtils')

const fileName = 'MerchantService'

/* requiring models */
const merchantModel = require('../models/MerchantModel')
const userModel = require('../models/UserDetailsModel')
const merchantCommissionModel = require('../models/MerchantCommissionsModel')
const userTransactionsModel = require('../models/UserTransactionsModel')

paymentService.getMerchantStatus = function (query, callback) {
  let default_error = 'Error while checking merchant active status'
  let findMerchant = {}
  console.log(query)
  let merchanFileds = { _id: true, merchant_status: true, brand_name: true, name: true }
  if (query.type == 'qrcode') {
    findMerchant['_id'] = query.merchant_id
  } else {
    findMerchant['mobile'] = query.merchant_mobile
  }
  merchantModel.findOne(findMerchant, merchanFileds,
    function (error, result) {
      if (error) {
        callback({ 'status': 'error', 'error': error, 'default_error': default_error, 'code': 'DB-ERROR' })
      } else {
        console.log('merchantStatus:: ', result)
        if (result.merchant_status) {
          if (query.type == 'mobile_number') {
            query['merchant_id'] = result._id
          }
          query['merchant_name'] = result.name
          query['brand_name'] = result.brand_name
          callback({ 'status': 'success', 'query': query })
        } else {
          callback({ 'status': 'failed', 'error': 'Merchant not authorized', code: 'MERCH-INACTIVE' })
        }
      }
    })
}

paymentService.getUserStatus = function (query, callback) {
  let default_error = 'Error while checking user status'
  let findUser = {}
  findUser['_id'] = query.user_id
  let userFields = {}
  userModel.findOne(findUser, function (error, result) {
    if (error) {
      callback({ 'status': 'error', 'error': error, 'default_error': default_error, 'code': 'DB-ERROR' })
    } else {
      var currentDate = (new Date()) / 1
      if (result.due_date != undefined && result.due_date != 0) {
        if (result.due_date > currentDate) {
          if (query.amount <= result.credit_available) {
            query['user_mobile'] = result.mobile_number
            query['con_fee'] = result.con_fee
            query['user_due_date'] = result.due_date
            callback({ 'status': 'success', 'query': query })
          } else {
            callback({ 'status': 'failed', 'error': 'Insufficient funds', code: 'INSUFFICIENTFUNDS' })
          }
        } else {
          callback({ 'status': 'failed', 'error': 'Due date time is exceeded', code: 'DUEDATE' })
        }
      } else {
        if (!result.due_date && query.transaction_type == 'confirm') {
          console.log('Enterssssssssss', result)
          var dueDate = new Date().setDate(new Date().getDate() + 15)
          userModel.update(findUser, { due_date: dueDate }, function (error, updateResult) {
            if (error) {
              callback({ 'status': 'error', 'error': error, 'default_error': default_error, 'code': 'DB-ERROR' })
            } else {
              // console.log("Result:: ", updateResult);
              query['user_mobile'] = result.mobile_number
              query['con_fee'] = result.con_fee
              query['user_due_date'] = dueDate
              if (query.amount <= result.credit_available) {
                callback({ 'status': 'success', 'query': query })
              } else {
                callback({ 'status': 'failed', 'error': 'Insufficient funds', code: 'INSUFFICIENTFUNDS' })
              }
            }
          })
        } else {
          if (query.amount <= result.credit_available) {
            callback({ 'status': 'success', 'query': query })
          } else {
            callback({ 'status': 'failed', 'error': 'Insufficient funds', code: 'INSUFFICIENTFUNDS' })
          }
        }
      }
    }
  })
}

paymentService.getMerchantCommission = function (query, callback) {
  let default_error = 'Error while getting merchant commission'
  let commissionFind = {
    'merchant_id': query.merchant_id,
    'min_amount': { $lt: query.amount },
    'max_amount': { $gte: query.amount }
  }
  let commissionFields = {
    commission_rate: true
  }
  merchantCommissionModel.findOne(commissionFind, commissionFields,
    function (error, result) {
      if (error) {
        callback({ 'status': 'error', 'error': error, 'default_error': default_error, 'code': 'DB-ERROR' })
      } else {
        query['commission_rate'] = result.commission_rate
        callback({ 'status': 'success', 'query': query })
      }
    })
}

paymentService.updateTransactionDetails = function (query, callback) {
  let default_error = 'Error while updating ' + query.transactionStatus + ' transaction details'
  if (query.transactionStatus == 'pending') {
    let transObject = {
      'user_id': query.user_id,
      'mobile_no': query.user_mobile,
      'merchant_id': query.merchant_id,
      'amount': query.amount,
      'reference_id': query.reference_id,
      'transaction_date': (new Date()) / 1,
      'merchant_commission': query.commission_rate,
      'con_fee': query.con_fee,
      'status': 0,
      'transaction_for': query.transaction_for,
      'transaction_status': 'pending'
    }
    var userTransactions = new userTransactionsModel(transObject)
    userTransactions.save(function (error, result) {
      if (error) {
        callback({ 'status': 'error', 'error': error, 'default_error': default_error, 'code': 'DB-ERROR' })
      } else {
        callback({ 'status': 'success', 'query': query })
      }
    })
  } else {
    userTransactionsModel.findOneAndUpdate(
      { reference_id: query.reference_id }, { status: 1, trans_details: [{ 'status': 1, 'date': (new Date() / 1) }] },
      function (error, result) {
        if (error) {
          callback({ 'status': 'error', 'error': error, 'default_error': default_error, 'code': 'DB-ERROR' })
        } else {
          callback({ 'status': 'success', 'query': query })
        }
      })
  }
}

paymentService.checkPayment = function (api_request, api_response) {
  const this_function_name = 'MerchantService.checkPayment'
  const default_error = 'Error while checking payment'
  let Logs = new ResponseUtils.Logs(fileName, this_function_name)
  let Print = new ResponseUtils.print(fileName, this_function_name)
  let Response = new ResponseUtils.response(api_response)
  let query = api_request.body.query
  paymentService.getMerchantStatus(query, function (result) {
    if (result.status == 'error') {
      Logs.error(result.error, 'DB-ERROR', default_error)
      return Response.error('Internal error', 'DB-ERROR', 400)
    } else if (result.status == 'failed') {
      Logs.error(result.error, result.code, result.default_error)
      return Response.error(result.error, result.code, 500)
    } else if (result.status == 'success') {
      paymentService.getUserStatus(result.query, function (userResult) {
        if (userResult.status == 'error') {
          Logs.error(userResult.error, 'DB-ERROR', userResult.default_error)
          return Response.error('Internal error', 'DB-ERROR', 400)
        } else if (userResult.status == 'failed') {
          Logs.error(userResult.error, userResult.code, default_error)
          return Response.error(userResult.error, userResult.code, 500)
        } else if (userResult.status == 'success') {
          paymentService.getMerchantCommission(userResult.query, function (commissionResult) {
            if (commissionResult.status == 'error') {
              Logs.error(commissionResult.error, 'DB-ERROR', commissionResult.default_error)
              return Response.error('Internal error', 'DB-ERROR', 400)
            } else if (commissionResult.status == 'failed') {
              Logs.error(commissionResult.error, commissionResult.code, default_error)
              return Response.error(commissionResult.error, commissionResult.code, 500)
            } else if (commissionResult.status == 'success') {
              if (query.transaction_type == 'confirm') {
                commissionResult.query['transactionStatus'] = 'confirm'
              } else {
                commissionResult.query['transactionStatus'] = 'pending'
              }
              paymentService.updateTransactionDetails(commissionResult.query, function (transResult) {
                if (transResult.status == 'error') {
                  Logs.error(transResult.error, 'DB-ERROR', transResult.default_error)
                  return Response.error('Internal error', 'DB-ERROR', 400)
                } else if (transResult.status == 'failed') {
                  Logs.error(transResult.error, transResult.code, default_error)
                  return Response.error(transResult.error, transResult.code, 500)
                } else if (transResult.status == 'success') {
                  if (query.transaction_type == 'checking') {
                    paymentService.buildObject(transResult.query, function (resultObject) {
                      if (resultObject) {
                        Response.success('Success', resultObject)
                      }
                    })
                  } else {
                    paymentService.updatePayment(transResult.query, function (updateResult) {
                      if (updateResult.status == 'error') {
                        Logs.error(updateResult.error, 'DB-ERROR', transResult.default_error)
                        return Response.error('Internal error', 'DB-ERROR', 400)
                      } else if (updateResult.status == 'failed') {
                        Logs.error(updateResult.error, updateResult.code, default_error)
                        return Response.error(updateResult.error, updateResult.code, 500)
                      } else if (updateResult.status == 'success') {
                        paymentService.buildObject(updateResult.query, function (resultObject) {
                          if (resultObject) {
                            // console.log(resultObject);
                            Response.success('Success', resultObject)
                          }
                        })
                      }
                    })
                  }
                }
              })
            }
          })
        }
      })
    }
  })
}

paymentService.updatePayment = function (query, callback) {
  let amount
  amount = query.amount + query.con_fee * query.amount
  // console.log("amount", query);
  let Logs = new ResponseUtils.Logs(fileName, 'merchantService.updatePayment')
  userModel.update({ _id: query.user_id },
    {
      $inc: { credit_due: amount, credit_available: -amount }
    },
    function (userError, userResult) {
      if (userError) {
        console.log(userError)
        Logs.error(userError, 'DB-ERROR', 'Error while updating user credit amount')
        callback(false)
      } else {
        amount = query.amount - (query.amount * (query.commission_rate / 100))
        merchantModel.update({ _id: query.merchant_id },
          {
            $inc: { pending_amt: amount }
          },
          function (merchantError, merchantResult) {
            // console.log(merchantResult);
            if (merchantError) {
              Logs.error(merchantError, 'DB-ERROR', 'Error while updating merchant pending amount')
              callback({ 'status': 'error', 'error': error, 'default_error': default_error, 'code': 'DB-ERROR' })
            } else {
              callback({ 'status': 'success', 'query': query })
            }
          }
        )
      }
    }
  )
}
paymentService.buildObject = function (query, callback) {
  result = {
    'type': query.type,
    'user_id': query.user_id,
    'user_mobile': query.user_mobile,
    'con_fee': query.con_fee,
    'user_due_date': query.user_due_date,
    'amount': query.amount,
    'merchant_id': query.merchant_id,
    'merchant_mobile': query.merchant_mobile,
    'merchant_name': query.merchant_name,
    'merchant_commission': query.commission_rate,
    'merchant_brand_name': query.brand_name,
    'reference_id': query.reference_id,
    'transaction_type': query.transaction_type
  }
  callback(result)
}
