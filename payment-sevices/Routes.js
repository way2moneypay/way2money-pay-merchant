'use strict'
var PaymentService = require('./services/PaymentService')
var AuthenticateUtils = require('./utils/AuthenticateUtils')
var EncDec = require('./utils/EncDec')

module.exports = function (app) {
  app.post('/api/v1/payment', EncDec.base64DecReq, AuthenticateUtils.userAuthenticationWithQuery, PaymentService.checkPayment)
}
