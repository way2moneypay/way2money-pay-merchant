'use strict'
const mongoose = require('mongoose')

const Schema = mongoose.Schema

var settings = new Schema({
  init_amount: {
    type: Number
  },
  con_fee: {
    type: Number
  },
  refund_period: {
    type: Number
  },
  due_period: {
    type: Number
  },
  penality_amount: {
    type: Number
  },
  payment_cycle: {
    type: Number
  },
  token_validity: {
    type: Number,
    default: 15
  },
  staging_payment_status: {
    type: String
  },
  live_payment_status: {
    type: String
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('payment_settings', settings)
