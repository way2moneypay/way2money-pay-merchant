var Mongoose = require('mongoose')

var Schema = Mongoose.Schema
var payment_gateway = new Schema({
  gateway_name: {
    type: String
  },
  status: {
    type: Number,
    default: 1,
    enum: [0, 1]
  }
}, {
  versionKey: false
})

module.exports = Mongoose.model('payment_gateway', payment_gateway)
