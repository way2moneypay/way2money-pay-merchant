const mongoose = require('mongoose')

const Schema = mongoose.Schema
const user_mobile_otp = new Schema({
  user_id: {
    type: Schema.Types.ObjectId,
    required: true
  },
  mobile_no: {
    type: String,
    required: true
  },
  merchant_id: {
    type: Schema.Types.ObjectId
  },
  otp: {
    type: Number,
    required: true
  },
  otp_date: {
    type: Number,
    required: true,
    default: Date.now
  },
  token: {
    type: String
  },
  token_expiry: {
    type: Number
  },
  otp_verified_date: {
    type: Number
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('user_mobile_otp', user_mobile_otp, 'user_mobile_otp')
