var mongoose = require('mongoose')

var Schema = mongoose.Schema
var user_payment_pending_transactions = new Schema({
  user_id: {
    type: Schema.Types.ObjectId,
    required: true
  },
  transaction_ref_id: {
    type: String
  },
  pending_transaction_ids: {
    type: Object
  },
  initiated_date: {
    type: Number,
    default: Date.now
  },
  amount_paying: {
    type: Number

  }
}, {
  versionKey: false
})

module.exports = mongoose.model('user_payment_pending_transactions', user_payment_pending_transactions)
