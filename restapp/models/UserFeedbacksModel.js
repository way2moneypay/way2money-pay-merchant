var mongoose = require('mongoose')

var Schema = mongoose.Schema
var user_feedbacks = new Schema({
  name: {
    type: String
  },
  email: {
    type: String,
    required: true
  },
  contacted_date: {
    type: Number,
    default: Date.now()
  },
  subject: {
    type: String
  },
  message: {
    type: String
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('user_feedbacks', user_feedbacks)
