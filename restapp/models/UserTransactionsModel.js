const mongoose = require('mongoose')

const Schema = mongoose.Schema

var user_transactions = new Schema({
  user_id: {
    type: Schema.Types.ObjectId,
    required: true
  },
  mobile_no: {
    type: String,
    required: true
  },
  merchant_id: {
    type: Schema.Types.ObjectId,
    required: true
  },
  amount: {
    type: Number,
    required: true
  },
  transaction_date: {
    type: Number,
    required: true
  },
  reference_id: {
    type: String,
    required: true
  },
  merchant_commission: {
    type: Number
  },
  status: {
    type: Number,
    required: true,
    enum: [0, 1, 2, 3, 4, 5, 6]
  },
  remarks: {
    type: String
  },
  transaction_status: {
    type: String,
    enum: ['pending', 'paid']
  },
  con_fee: {
    type: Number
  },
  transaction_for: {
    type: String
  },
  trans_amount: {
    type: Number
  },
  refund_status: {
    type: Boolean
  },
  trans_details: {
    type: Array
  },
  scan_id: {
    type: Schema.Types.ObjectId,
    required: true
  },
  trans_through: {
    type: String
  },
  ip_address: {
    type: String
  },
  os: {
    type: String
  },
  network_type: {
    type: String
  },
  credit_due: {
    type: Number
  },
  payment_due: {
    type: Number
  },
  merchant_brand: {
    type: String
  },
  m_status: {
    type: Number
  },
  locations: {
    type: Object
  },
  location_type: {
    type: String
  },
  created_by_parent: {
    type: Schema.Types.ObjectId
  },
  created_by: {
    type: Schema.Types.ObjectId
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('user_transactions', user_transactions)
