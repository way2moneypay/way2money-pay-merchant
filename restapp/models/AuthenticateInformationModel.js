var mongoose = require('mongoose')

var Schema = mongoose.Schema
var authenticate_information = new Schema({
  merchant_id: {
    type: Schema.Types.ObjectId
  },
  ip: {
    type: String
  },
  mobile_number: {
    type: String
  },
  date: {
    type: Number,
    default: Date.now
  },
  type: {
    type: String,
    enum: ['login', 'register', 'forgot-password', 'dashboard'] /* authenticate type */
  },
  status: {
    type: String,
    enum: ['success', 'failure'] /* login, register or forgot password status */
  },
  source: {
    type: String,
    enum: ['web', 'android', 'ios'] /* source of authentication */
  },
  device_details: {
    type: Object,
    default: {}
  },
  status_code: {
    type: String
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('authenticate_information', authenticate_information)
