var Mongoose = require('mongoose')

var Schema = Mongoose.Schema
var email_alerts = new Schema({
  alert_name: {
    type: String,
    required: true
  },
  alert_route: {
    type: String,
    enum: ['api', 'smtp'],
    required: true
  },
  alert_subject: {
    type: String,
    required: true
  },
  alert_content: {
    type: String,
    required: true
  },
  smtp_host: {
    type: String
  },
  smtp_port: {
    type: String
  },
  smtp_username: {
    type: String
  },
  smtp_password: {
    type: String
  },
  api_url: {
    type: String
  },
  status: {
    type: String,
    enum: ['active', 'inactive'],
    default: 'active'
  },
  create_date: {
    type: Number,
    default: Date.now
  }

}, { versionKey: false })

email_alerts.index({ alert_name: 1 }, { name: 'alertname' })
module.exports = Mongoose.model('email_alerts', email_alerts)
