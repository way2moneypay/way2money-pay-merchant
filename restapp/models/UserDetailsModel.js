var mongoose = require('mongoose')

var Schema = mongoose.Schema
bcrypt = require('bcrypt-nodejs')
var user_details = new Schema({
  name: {
    type: String
  },
  mobile_number: {
    type: String,
    required: true,
    unique: true
  },
  email: {
    type: String,
    required: true
  },
  register_date: {
    type: Number,
    required: true,
    default: Date.now()
  },
  register_merchant: {
    type: Schema.Types.ObjectId
  },
  password: {
    type: String
  },
  credit_available: {
    type: Number,
    default: 500
  },
  credit_due: {
    type: Number,
    default: 0
  },
  credit_limit: {
    type: Number,
    default: 500
  },
  user_status: {
    type: Number,
    default: 1
  },
  con_fee: {
    type: Number,
    default: 3.0
  },
  mobile_pin: {
    type: Number
  },
  due_date: {
    type: Number
  },
  is_mobile_verified: {
    type: Number,
    required: true,
    default: false
  },
  is_email_verified: {
    type: Number,
    required: true,
    default: false
  },
  user_fields_status: {
    enum: ['complete', 'incomplete'],
    type: String
  },
  is_temp_password: {
    type: Boolean,
    default: false
  },
  temp_password: {
    type: String
  },
  email_verification_count: {
    type: Number,
    default: 0
  },
  email_last_hit_date: {
    type: Date
  },
  last_trans_date: {
    type: Number
  }

}, {
  versionKey: false
})

user_details.methods.generateHash = function (password) {
  console.log('GENERATING HASH', password)
  let hash_password = bcrypt.hashSync(password, bcrypt.genSaltSync(8))

  // console.log("HASSHHH", bcrypt.hashSync(password, bcrypt.genSaltSync(8), null));
  return hash_password
}

user_details.methods.validPassword = function (password) {
  console.log('PASSWD CHECK', password, this.password)
  let validate = bcrypt.compareSync(password, this.password)
  return validate
}

module.exports = mongoose.model('user_details', user_details)
