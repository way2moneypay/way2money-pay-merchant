var Mongoose = require('mongoose')

var Schema = Mongoose.Schema
var sms_alerts = new Schema({
  alert_type: {
    type: String
  },
  alert_subject: {
    type: String
  },
  alert_content: {
    type: String
  },
  sender_id: {
    type: String
  },
  status: {
    type: String,
    enum: ['active', 'inactive']
  },
  create_date: {
    type: Number,
    default: Date.now
  }

}, {
  versionKey: false
})

sms_alerts.index({
  alert_type: 1
}, {
  name: 'alerttype'
})
module.exports = Mongoose.model('sms_alerts', sms_alerts)
