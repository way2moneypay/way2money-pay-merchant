var mongoose = require('mongoose')

var Schema = mongoose.Schema
var user_payments = new Schema({
  user_id: {
    type: Schema.Types.ObjectId,
    required: true
  },
  mobile_number: {
    type: String
  },
  payment_gateway_id: {
    type: Schema.Types.ObjectId
  },
  payment_ref_id: {
    type: String
  },
  payment_id: {
    type: String
  },
  transaction_ref_id: {
    type: String
  },
  payment_initiated_date: {
    type: Number,
    default: Date.now
  },
  paid_date: {
    type: Number
  },
  amount_paid: {
    type: Number,
    required: true
  },
  paid_status: {
    type: Number,
    default: 0
  },
  payment_mode: {
    type: Number
  },
  bank_name: {
    type: String
  },
  payment_remarks: {
    type: String
  },
  payment_type: {
    type: String
  },
  trans_details: {
    type: Object,
    default: []
  },
  gateway_response: {
    type: String
  },
  gateway_response_status: {
    type: String,
    default: 'pending',
    enum: ['pending', 'success']
  },
  source: {
    type: String,
    enum: ['ios', 'web', 'android'],
    default: 'ios'
  }

}, {
  versionKey: false
})

module.exports = mongoose.model('user_payments', user_payments)

// MODEL DOCUMENTATION

// paid_status {
//   0 - initiate
//   1 - success
//   2 - fail
//   3 - refund
// }
