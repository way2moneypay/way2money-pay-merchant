var mongoose = require('mongoose')

var Schema = mongoose.Schema
var merchant_mobile_otp = new Schema({
  mobile_number: {
    type: String,
    required: true
  },
  otp: {
    type: Number,
    required: true
  },
  otp_date: {
    type: Number,
    required: true,
    default: Date.now
  },
  otp_count: {
    type: Number,
    default: 1
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('merchant_otps', merchant_mobile_otp)
