const mongoose = require('mongoose')

const Schema = mongoose.Schema

var merchant_trans_cycle = new Schema({
  merchant_id: {
    type: Schema.Types.ObjectId
  },
  cycle_day: {
    type: String
  },
  amount: {
    type: Number
  },
  cycle_date: {
    type: Number
  },
  cycle_status: {
    type: String
  },
  transactions: {
    type: Number
  },
  start_date: {
    type: Number
  },
  end_date: {
    type: Number
  },
  accept_date: {
    type: Number
  },
  ref_id: {
    type: String
  },
  final_amount: {
    type: Number
  },
  commission_rate: {
    type: Number
  }

}, {
  versionKey: false
})
merchant_trans_cycle.index({ cycle_date: 1, merchant_id: 1 }, { unique: true })
module.exports = mongoose.model('merchant_trans_cycle', merchant_trans_cycle)
