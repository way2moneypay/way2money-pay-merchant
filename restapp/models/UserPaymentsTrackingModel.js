var mongoose = require('mongoose')

var Schema = mongoose.Schema
var user_payments_tracking = new Schema({
  payment_ref_id: {
    type: String
  },
  date: {
    type: Number,
    default: Date.now
  },
  message: {
    type: String
  },
  description: {
    type: String
  }

}, {
  versionKey: false
})

module.exports = mongoose.model('user_payments_tracking', user_payments_tracking)
