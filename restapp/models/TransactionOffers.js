var mongoose = require('mongoose')
var Schema = mongoose.Schema
var transactionsOffers = new Schema({
  name: {
    type: String
  },
  type: {
    type: String,
    required: true
  },
  offer_created_on: {
    type: Number
  },
  description: {
    type: String
  },
  offer_expires_on: {
    type: Number
  },
  status: {
    type: String,
    enum: ['Active', 'Inactive'],
    default: 'Active'
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('transactions_offers', transactionsOffers)
