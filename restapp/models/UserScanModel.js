'use strict'
const mongoose = require('mongoose')

const Schema = mongoose.Schema

var userScans = new Schema({
  merchant_id: {
    type: Schema.Types.ObjectId
  },
  user_id: {
    type: Schema.Types.ObjectId
  },
  date: {
    type: Number
  },
  reference_id: {
    type: String
  },
  status: {
    type: Number,
    default: 0
  },
  trans_id: {
    type: Schema.Types.ObjectId
  },
  os: {
    type: String
  },
  network_type: {
    type: String
  },
  mobile_no: {
    type: String
  },
  locations: {
    type: Object
  },
  location_type: {
    type: String
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('user_scans', userScans)
