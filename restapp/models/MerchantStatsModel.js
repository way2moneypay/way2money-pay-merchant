const mongoose = require('mongoose')

const Schema = mongoose.Schema

var merchant_trans_stats = new Schema({
  merchant_id: {
    type: Schema.Types.ObjectId
  },
  transactions: {
    type: Number
  },
  trans_amount: {
    type: Number
  },
  trans_date: {
    type: Number
  },
  users: { type: Number },
  amount: { type: Number }
}, {
  versionKey: false
})

module.exports = mongoose.model('merchant_trans_stats', merchant_trans_stats)
