const AppConfig = require('config')
const ResponseUtils = require('./ResponseUtils')
const JWT = require('jsonwebtoken')

var this_file_name = 'restapp/utils/AuthenticateUtils'

var AuthenticateUtils = module.exports

AuthenticateUtils.userAuthentication = function (api_request, api_response, next_service) {
  let this_function_name = 'AuthenticateUtils.userAuthentication'
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Response = new ResponseUtils.response(api_response)

  // check header or url parameters or post parameters for token
  var authentication_token = api_request.headers['x-access-token']
  // decode token
  if (authentication_token) {
    // verifies secret and checks exp
    JWT.verify(authentication_token, AppConfig.jwtsecret, function (token_verify_error, token_data) {
      console.log(token_verify_error)
      // console.log("TOKEN DATA IN AUTHENTICATION UTILS", token_data);
      if (token_verify_error) {
        // app.internalError('Invalid Authencation', 400, res);
        Print.error('Error while verifing user authentication token.')
        return Response.error('User athentication failed.', 'ERR')
      } else {
        // {
        //     // if everything is good, save to request for use in other routes
        //     const now = new Date().getTime();

        //     if(!token_data.a_expiry || token_data.a_expiry < now ){
        //         console.log('---------auth -------------',"verification authontication expired");
        //         const param = "Authenticaiton token expired.Please login again"
        //         // delete api_response.headers['x-access-token'];
        //         return Response.error(param,"AUTHTOKENERR",500);
        //     }else{
        // console.log(".....authontication failed");
        api_request.user = token_data
        next_service()
      }
      // }
    })
  } else {
    Print.error('Authentication token not found in request.')
    return Response.error('User athentication failed.', 'ERR')
  }
}

AuthenticateUtils.userAuthenticationWithQuery = function (api_request, api_response, next_service) {
  let this_function_name = 'AuthenticateUtils.userAuthenticationWithQuery'
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Response = new ResponseUtils.response(api_response)

  // check header or url parameters or post parameters for token
  var authentication_token = api_request.headers['x-access-token'] || api_request.query.token
  // decode token
  // console.log("authentication_token",authentication_token);
  if (authentication_token) {
    // verifies secret and checks exp
    JWT.verify(authentication_token, AppConfig.jwtsecret, function (token_verify_error, token_data) {
      if (token_verify_error) {
        // app.internalError('Invalid Authencation', 400, res);
        Print.error('Error while verifing user authentication token.')
        return Response.error('User athentication failed.', 'ERR')
      } else {
        // console.log('TOKEN DATA AT AUTHENTICATION', token_data);
        // if everything is good, save to request for use in other routes
        api_request.user = token_data
        next_service()
      }
    })
  } else {
    Print.error('Authentication token not found in request.')
    return Response.error('User athentication failed.', 'ERR')
  }
}
