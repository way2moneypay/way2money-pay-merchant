const moment = require('moment')
const rn = require('random-number')
const randomstring = require('randomstring')
const MerchantModel = require('./../models/MerchantModel')
const PaymentGatewayModel = require('./../models/PaymentGatewayModel')

var dateFormat = require('dateformat')
const AppConfig = require('config')
var JWT = require('jsonwebtoken')

const GeneralUtils = module.exports

let time_zone_types = {
  'ist': 11 * 30 * 60 * 1000,
  'gmt': 0
}

GeneralUtils.unixTimeDiff = function (time_one, time_two, diff_type) {
  // diff_type in [years, months, weeks, days, hours, minutes, seconds]
  time_one = moment(time_one)
  time_two = moment(time_two)
  // console.log("time_one",time_one);
  // console.log("time_two",time_two);

  let time_diff = time_two.diff(time_one, diff_type)
  return time_diff
}

GeneralUtils.isUnixSameDate = function (time_one, time_two, milles_one, milles_two) {
  time_one = time_one / milles_one
  time_two = time_two / milles_two

  time_one = moment.unix(time_one).format('MM/DD/YYYY')
  time_two = moment.unix(time_two).format('MM/DD/YYYY')
  return time_one == time_two
}

GeneralUtils.randomNumber = function (minimum, maximum) {
  var options = {
    min: minimum,
    max: maximum,
    integer: true
  }

  return rn(options)
}

GeneralUtils.randomString = function (string_length) {
  return randomstring.generate(string_length)
}

GeneralUtils.generateRandomDataForSendingSms = function (type) {
  let random_data
  if (type == 'verify_mobile') {
    random_data = GeneralUtils.randomNumber(1000, 9999)
  } else if (type == 'send_password') {
    random_data = GeneralUtils.randomString(6)
  }
  return random_data
}

GeneralUtils.checkAllowForSendingOtpOrPassword = function (message_sending_type, current_milliseconds, previous_milliseconds, messages_count) {
  let return_data = {}
  return_data = {
    success: true,
    error_code: null,
    error_message: null
  }

  // let maximum_duration_between_sending_sms = 45; // 45 seconds
  let checking_conditions = {}

  // checking sms count and last updated date
  if (message_sending_type == 'verify_mobile') {
    checking_conditions.time = 25
    checking_conditions.count = 100
  } else if (message_sending_type == 'send_password') {
    checking_conditions.time = 25
    checking_conditions.count = 100
  }

  // checking time between sending messages
  if (GeneralUtils.convertMillisecondsToStartOfTheDayMilliseconds(previous_milliseconds) != GeneralUtils.convertMillisecondsToStartOfTheDayMilliseconds(current_milliseconds)) {
    return_data.error_message = 'Update count to 0'
    return_data.error_code = 'COUNTRESET'
    return_data.success = true
  } else if (GeneralUtils.getDifferenceBetweenMillisecondsInSeconds(previous_milliseconds, current_milliseconds) < checking_conditions.time) {
    return_data.error_message = 'Maximum time between sending sms is 25 seconds. Please try again after sometime'
    return_data.error_code = 'TIMEEXCEED'
    return_data.success = false
    // checking two milliseconds are equal.. It convert current milliseconds to current day start milliseconds then compare two milliseconds
  } else if (GeneralUtils.convertMillisecondsToStartOfTheDayMilliseconds(previous_milliseconds) == GeneralUtils.convertMillisecondsToStartOfTheDayMilliseconds(current_milliseconds)) {
    // checking if user exhaust maximum count to send sms
    if (messages_count > checking_conditions.count) {
      return_data.error_message = 'Maximum count for sending messages per day is ' + checking_conditions.count + '. Please try later.'
      return_data.error_code = 'EXCEEDSMSCOUNT'
      return_data.success = false
    }
  }

  return return_data
}

GeneralUtils.convertMillisecondsToStartOfTheDayMilliseconds = function (input_milliseconds) {
  let temporary_date = new Date(input_milliseconds)
  let converted_date = temporary_date.getFullYear() + '-' + (temporary_date.getMonth() + 1) + '-' + temporary_date.getDate()
  return new Date(converted_date).getTime()
}

GeneralUtils.getDifferenceBetweenMillisecondsInSeconds = function (previous_milliseconds, current_milliseconds) {
  return ((current_milliseconds - previous_milliseconds) / 1000)
}

GeneralUtils.copyFeilds = function (object, second_object, copy_feilds) {
  second_object = second_object || {}

  let keys = copy_feilds || Object.keys(object)

  for (var index = 0; index < keys.length; ++index) {
    let key = keys[index]
    let value = object[key]
    second_object[key] = value
  }

  return second_object
}

GeneralUtils.copyFeildsWithAllTrue = function (object, second_object, copy_feilds) {
  second_object = second_object || {}

  let keys = copy_feilds || Object.keys(object)

  for (var index = 0; index < keys.length; ++index) {
    let key = keys[index]
    let value = true
    second_object[key] = value
  }

  return second_object
}

// calculate estimated loan amout to give for leads based on loan type and etc.,
GeneralUtils.estimatedLoanAmount = function (loan_data) {
  console.log('loan data new', loan_data)
  if (loan_data.loan_type == 'PERSONAL') {
    // let M = (loan_data.loan_amount * 11 * (( 12 ** loan_data.loan_tenure) / (12 ** loan_data.loan_tenure) - 1));
    // let M = (loan_data.loan_amount * 11 * (( Math.pow(12, loan_data.loan_tenure)) / (Math.pow(12,loan_data.loan_tenure)) - 1));
    let M = loan_data.loan_amount * 11 * ((12 ^ loan_data.loan_tenure) / ((12 ^ loan_data.loan_tenure) - 1))
    // let M = (loan_data.loan_amount * 11 * (( 12 * loan_data.loan_tenure) / (12*loan_data.loan_tenure) - 1));
    console.log('m value ', M)
    let X = (loan_data.salary * 0.5 - loan_data.current_emi) / (M * 100000 / loan_data.loan_amount)
    console.log('x value ', X)
    return X
  } else if (loan_type == 'HOME') {

  }
}

GeneralUtils.getIP = function (request) {
  var ip = request.headers['x-forwarded-for'] ||
    request.connection.remoteAddress ||
    request.socket.remoteAddress ||
    (request.connection.socket ? request.connection.socket.remoteAddress : null)

  return ip
}

// convert current date to specific timezone type (in terms of gmt) date and time. ex: 2018-03-02 converted as 2018-03-01T18:30:00.000Z if timezone is gmt
GeneralUtils.dateConversionFromCorrectDateToSpecificTimeZone = function (time_zone_type, input_date) { // converts correct date to utc date
  let new_date = dateFormat(input_date, 'isoUtcDateTime')
  console.log(new_date)
  new_date = new Date(new Date(new_date).getTime() + time_zone_types[time_zone_type]) // converting to indian standards
  return new_date
}

// convert current date to end of the day in specific time zone (in terms of gmt) date and time. ex: 2018-03-02 converted as 2018-03-02T18:29:59.999Z in case time zone type is gmt
GeneralUtils.dateConversionFromCorrectDateToSpecificTimeZoneEndOfTheDay = function (time_zone_type, input_date) { // it converts correct date to last minute of day in utc format
  let new_date = dateFormat(input_date, 'isoUtcDateTime')
  new_date = new Date(new Date(new_date).getTime() + time_zone_types[time_zone_type] + 86399999) // converting to indian standard time and make date to last second of the day
  return new_date
}

GeneralUtils.dateConversionFromCorrectToIst = function (input_date) { // converts correct date to utc date
  let new_date = dateFormat(input_date, 'isoUtcDateTime')
  new_date = new Date(new Date(new_date).getTime() + (11 * 30 * 60 * 1000)) // converting to indian standards
  return new_date
}

GeneralUtils.dateConversionFromCorrectToIstEndOfTheDay = function (input_date) { // it converts correct date to last minute of day in utc format
  let new_date = dateFormat(input_date, 'isoUtcDateTime')
  new_date = new Date(new Date(new_date).getTime() + (11 * 30 * 60 * 1000) + 86399999) // converting to indian standard time and make date to last second of the day
  return new_date
}

// Calculating FOIR percentage
GeneralUtils.calculateFOIR = function (salary, emi) {
  return ((emi / salary) * 100)
}

// Calculating Number years from given date
GeneralUtils.calculateYearsBetweenDate = function (date) {
  let new_date1 = moment(date, 'YYYY-MM-DD')
  let current_date = dateFormat(new Date(), 'isoUtcDateTime')
  let new_date2 = moment(current_date, 'YYYY-MM-DD')

  current_date = null

  return (new_date2.diff(new_date1, 'years'))
}

GeneralUtils.calculateProcessingFee = function (loan_amount, loan_name) {
  let newCalculatedProcessingFeeAmount = 0
  if (loan_name === 'FULLERTON') {
    /* checking loan amount to make processing fess */
    if (loan_amount < 100000) { /* <1L */
      newCalculatedProcessingFeeAmount = (loan_amount * (4 / 100))
    } else if (loan_amount < 200000 && loan_amount >= 100000) { /* 1L-2L */
      newCalculatedProcessingFeeAmount = (loan_amount * (3.5 / 100))
    } else if (loan_amount < 300000 && loan_amount >= 200000) { /* 2L-3L */
      newCalculatedProcessingFeeAmount = (loan_amount * (3.5 / 100))
    } else if (loan_amount < 500000 && loan_amount >= 300000) { /* 3L-5L */
      newCalculatedProcessingFeeAmount = (loan_amount * (3.0 / 100))
    } else if (loan_amount >= 500000) { /* >5L */
      newCalculatedProcessingFeeAmount = (loan_amount * (2.5 / 100))
    }
  }

  return newCalculatedProcessingFeeAmount
}

/* Ramu */
/* Checking post fior */
GeneralUtils.postFior = function (lead_details, partner_details) {
  /* calculating partner emi */
  let partner_emi = ConsumerLoanController2.calculateEmi(lead_details.loan_amount, lead_details.loan_tenure, partner_details.interest_rate_min)
  let updated_emi = lead_details.current_emi + partner_emi
  let new_monthly_income = lead_details.occupation == 'SALARIED' ? monthly_income : (monthly_income / 12)
  let fior_percentage = (updated_emi / new_monthly_income)
  console.log('partner_emi updated_emi new_monthly_income ', partner_emi, updated_emi, new_monthly_income)
  if (fior_percentage <= partner_details.min_borrow_requirements.fior) {
    return true
  } else {
    return false
  }
}

/* Ramu */
/* Calculates foir percentage for a lead */
GeneralUtils.calculateFoirPercentage = function (current_emi, monthly_income) {
  current_emi = current_emi || 0
  return ((current_emi * 100) / monthly_income)
}

/* Ramu */
/* Calculate emi for loan amount */
GeneralUtils.calculateEmi = function (loan_amount, loan_tenure, interest_rate_min) {
  let installments = loan_tenure < 12 ? loan_tenure : 12
  return GeneralUtils.getEmiAfterPreparingData(loan_amount, interest_rate_min, installments, loan_tenure)
}

/* Ramu */
/* getting emi from ptr formula */
GeneralUtils.getEmiAfterPreparingData = function (P, I, IN, N) {
  let R = I / IN
  R = R / 100
  let R1 = R + 1
  let part_1 = Math.pow(R1, N)
  let part_2 = Math.pow(R1, N) - 1
  let part_3 = part_1 / part_2

  return (P * (R * part_3)).toFixed(2)
}

/* Ramu */
/* Getting monthly based salaried and self employed */
/* for salaried monthly is monthly income */
/* for self employed monthly income(it is annual income field in our database) is monthly_income /12 */
GeneralUtils.getMonthlyIncome = function (monthly_income, occupation) {
  let new_monthly_income

  /* checking occupation type */
  if (occupation === 'SALARIED') {
    new_monthly_income = monthly_income
  } else {
    new_monthly_income = (monthly_income / 12)
  }

  return new_monthly_income
}

// Function for email verification
// check email validations
GeneralUtils.validateEmail = function (email) {
  if (!email) {
    return false
  }
  // const emailRegex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,63})?$/;
  const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
  return emailRegex.test(email)
}

GeneralUtils.decryptVerifyToken = function (token) {
  console.log('token called')
  let token_data = JWT.verify(token, AppConfig.jwtsecret, function (token_verify_error, token_data) {
    if (token_verify_error) {
      console.log('token error')
      return 'DECRYPTTOKENERR'
    } else {
      console.log('token data', token_data)
      return token_data
    }
  })
  return token_data
}

GeneralUtils.storeDefaultMerchants = function () {
  console.log('in store default merchants')
  MerchantModel.findOneAndUpdate({
    'name': 'Web'
  }, {
    $set: {
      'email': 'web@way2money.com',
      'mobile': '9999999999',
      'company': 'Way2online',
      'logo_url': 'https://apk4free.net/wp-content/uploads/2017/05/Way2SMS-Free-SMS.png',
      'merchant_category': 'aLL',
      'brand_name': 'Way2Online',
      'password': 'sha1$11611300$1$fb2c5098b902535d4fef4f2f169932a2de164cd8',
      'register_date': Date.now(),
      'merchant_status': 1,
      'token_validity': 5,
      'pending_amt': -1000,
      'approved_amt': 500
    }
  }, {
    new: true,
    upsert: true
  }, function (err, resp) {
    if (err) {
      console.error(err)
    }
  })
  MerchantModel.findOneAndUpdate({
    'name': 'Mobile'
  }, {
    $set: {
      'email': 'mobile@way2money.com',
      'mobile': '8888888888',
      'company': 'Way2online',
      'logo_url': 'https://apk4free.net/wp-content/uploads/2017/05/Way2SMS-Free-SMS.png',
      'merchant_category': 'aLL',
      'brand_name': 'Way2Online',
      'password': 'sha1$11611300$1$fb2c5098b902535d4fef4f2f169932a2de164cd8',
      'register_date': Date.now(),
      'merchant_status': 1,
      'token_validity': 5,
      'pending_amt': -1000,
      'approved_amt': 500
    }
  }, {
    new: true,
    upsert: true
  }, function (err, resp) {
    if (err) {
      console.error(err)
    }
  })
  PaymentGatewayModel.findOneAndUpdate({
    gateway_name: 'InstaMojo'
  }, {}, {
    new: true,
    upsert: true
  }, function (err, resp) {
    if (err) {
      console.error(err)
    }
  })
}
