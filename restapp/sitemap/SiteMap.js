'use strict'
const sm = require('sitemap')
const sitemap = sm.createSitemap({
  hostname: 'https://www.way2money.com',
  cacheTime: 600000, // 600 sec - cache purge period
  urls: [{
    url: '/personal-loan',
    changefreq: 'daily',
    priority: 1
  }, {
    url: '/emi-calculator',
    changefreq: 'daily',
    priority: 1
  },
  {
    url: '/about-us',
    changefreq: 'monthly',
    priority: 1
  },
  {
    url: '/tips',
    changefreq: 'monthly',
    priority: 1
  },
  {
    url: '/faqs',
    changefreq: 'monthly',
    priority: 1
  }
  ]
})

const SiteMap = module.exports

SiteMap.siteMap = function (request, response) {
  sitemap.toXML(function (err, xml) {
    if (err) {
      return response.status(500).end()
    }
    response.header('Content-Type', 'application/xml')
    response.send(xml)
  })
}
