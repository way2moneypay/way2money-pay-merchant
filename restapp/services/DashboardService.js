var DashboardService = module.exports
const async = require('async')
const mongoose = require('mongoose')

/* requiring required models */
const Transactions = require('../models/UserTransactionsModel')
const MerchantStatsModel = require('../models/MerchantStatsModel')
const MerchantModel = require('../models/MerchantModel')
/* requiring utils */
var ResponseUtils = require('./../utils/ResponseUtils')

let this_file_name = 'DashboardService.getUserDashboardDetails'

/* Ramu */
/* get user dashboard data */
DashboardService.getUserDashboardChartData = function (api_request, api_response) {
  let Response = new ResponseUtils.response(api_response)

  let id = mongoose.Types.ObjectId(api_request.user._id)
  let default_days = 30
  if (api_request.params.hasOwnProperty('days')) {
    default_days = api_request.params.days
  }
  async.parallel([
    function (callback) {
      var fromDay = new Date()
      fromDay.setHours(0, 0, 0, 0)
      fromDay.setDate(fromDay.getDate() - default_days)
      MerchantStatsModel.find({
        merchant_id: id,
        trans_date: {
          $gte: (fromDay.getTime()) - 19800000
        }
      },
      function (mongo_err, mongo_res) {
        if (mongo_err) {
          console.log('Error:::::::::::::', mongo_err)
          callback({
            error: 'Error in merchants report graph',
            error_message: mongo_res
          }, null)
        } else {
          // console.log("****************", mongo_res)
          var dates = []
          var counts = []
          // var inc = 0;
          async.eachSeries(mongo_res, function (i, next) {
            if (mongo_res.length > 0) {
              if (i.users != undefined) {
                // console.log(":::::::::::::::::::",i)
                dates.push(i.trans_date)
                counts.push(i.users)
                // console.log(":::::::::::::::::::counts",counts)
                // inc++;
                next()
              } else {
                dates.push(i.trans_date)
                counts.push(0)
                next()
              }
            }
          }, function () {
            callback(null, {
              dates: dates,
              counts: counts
            })
          })
        }
      }).sort({ trans_date: 1 })
    },
    function (callback) {
      var fromDay = new Date()
      fromDay.setHours(0, 0, 0, 0)
      fromDay.setDate(fromDay.getDate() - default_days)
      MerchantStatsModel.find({
        merchant_id: id,
        trans_date: {
          $gte: (fromDay.getTime()) - 19800000
        }
      },
      function (mongo_err, mongo_res) {
        if (mongo_err) {
          console.log('Error:::::::::::::', mongo_err)
          callback({
            error: 'Error in users report graph',
            error_message: mongo_res
          }, null)
        } else {
          var dates = []
          var counts = []
          // var inc = 0;
          async.eachSeries(mongo_res, function (i, next) {
            if (mongo_res.length > 0) {
              // console.log(":::::::::::::::::::", i.users)
              if (i.transactions != undefined) {
                dates.push(i.trans_date)
                counts.push(i.transactions)
                // console.log(":::::::::::::::::::",counts)
                next()
              } else {
                dates.push(i.trans_date)
                counts.push(0)
                next()
              }
            }
          }, function () {
            callback(null, {
              dates: dates,
              counts: counts
            })
          })
        }
      }).sort({ trans_date: 1 })
    }
  ],
  function (err, results) {
    if (err) {
      console.log('Error:::::::::::::', err)
      return Response.error(err)
    } else if (results) {
      // console.log(results)
      Array.prototype.unique = function () {
        var a = this.concat()
        for (var i = 0; i < a.length; ++i) {
          for (var j = i + 1; j < a.length; ++j) {
            if (a[i] === a[j]) { a.splice(j--, 1) }
          }
        }
        return a
      }
      const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Novr', 'Dec']
      var total_days = results[0].dates.concat(results[1].dates).unique()
      total_days = total_days.sort()
      var user_dates = []

      var transactions_dates = []
      var users_count = []

      var transactions_count = []
      var labels = []

      for (let i = 0; i < total_days.length; i++) {
        var day = new Date(total_days[i])
        labels.push(months[day.getMonth()] + ' ' + day.getDate())
        if ((results[0].dates.indexOf(total_days[i]) != -1)) {
          users_count.push(results[0].counts[results[0].dates.indexOf(total_days[i])])
        } else {
          users_count.push(0)
        }
        if ((results[1].dates.indexOf(total_days[i]) != -1)) {
          transactions_count.push(results[1].counts[results[1].dates.indexOf(total_days[i])])
        } else {
          transactions_count.push(0)
        }
      }

      return Response.success('Dashboard graph report data by days', {
        graph_report: {
          labels: labels,
          users_count: users_count,
          transactions_count: transactions_count
        }
      })
    }
  })
}
DashboardService.getUserDashboardDetails = function (api_request, api_response) {
  let Response = new ResponseUtils.response(api_response)

  let id = mongoose.Types.ObjectId(api_request.user._id)
  async.parallel([
    function (callback) {
      Transactions.distinct('user_id', {
        merchant_id: id
      }, function (mongo_err, mongo_res) {
        if (mongo_err) {
          console.log('Error:::::::::::::', mongo_err)
          callback({
            error: 'Error in total users count',
            error_message: mongo_res
          }, null)
        } else if (mongo_res) {
          callback(null, mongo_res.length)
        } else {
          callback(null, 0)
        }
      })
    },
    function (callback) {
      var today = new Date()
      today.setHours(0, 0, 0, 0)
      var yesterday = new Date()
      yesterday.setDate(yesterday.getDate() - 1)
      yesterday.setHours(0, 0, 0, 0)
      Transactions.distinct('user_id', {
        merchant_id: id,
        transaction_date: {
          $gte: (yesterday.getTime()) - 19800000,
          $lt: today.getTime()
        }
      }, function (mongo_err, mongo_res) {
        if (mongo_err) {
          console.log('Error:::::::::::::', mongo_err)
          callback({
            error: 'Error in yesterday users count',
            error_message: mongo_res
          }, null)
        } else if (mongo_res) {
          callback(null, mongo_res.length)
        } else {
          callback(null, 0)
        }
      })
    },
    function (callback) {
      var today = new Date()
      today.setHours(0, 0, 0, 0)
      Transactions.distinct('user_id', {
        merchant_id: id,
        transaction_date: {
          $gte: today.getTime()
        }
      }, function (mongo_err, mongo_res) {
        if (mongo_err) {
          console.log('Error:::::::::::::', mongo_err)
          callback({
            error: 'Error in today users count',
            error_message: mongo_res
          }, null)
        } else if (mongo_res) {
          callback(null, mongo_res.length)
        } else {
          callback(null, 0)
        }
      })
    },
    /* TransactionStats - MerchantStatsModel */
    function (callback) {
      MerchantStatsModel.aggregate([{ $match: { merchant_id: id } },
        { $group: { _id: null, transactions: { $sum: '$transactions' }, trans_amount: { $sum: '$trans_amount' } } }

      ],
      function (mongo_err, mongo_res) {
        if (mongo_err) {
          console.log('Error:::::::::::::', mongo_err)
          callback({
            error: 'Error in total merchant transactions count',
            error_message: mongo_res
          }, null)
        } else if (mongo_res) {
          callback(null, mongo_res)
        } else {
          callback(null, 0)
        }
      })
    },
    function (callback) {
      var today = new Date()
      today.setHours(0, 0, 0, 0)
      Transactions.aggregate([
        { $match: { merchant_id: id, transaction_date: { $gte: today.getTime() } } },
        { $group: { _id: null, transactions: { $sum: 1 }, trans_amount: { $sum: '$amount' } } }
      ],
      function (mongo_err, mongo_res) {
        if (mongo_err) {
          console.log('Error:::::::::::::', mongo_err)
          callback({
            error: 'Error in today merchant transactions count',
            error_message: mongo_res
          }, null)
        } else if (mongo_res) {
          callback(null, mongo_res)
        } else {
          callback(null, 0)
        }
      })
    },
    function (callback) {
      var today = new Date()
      today.setHours(0, 0, 0, 0)
      var yesterday = new Date()
      yesterday.setDate(yesterday.getDate() - 1)
      yesterday.setHours(0, 0, 0, 0)
      MerchantStatsModel.aggregate([
        {
          $match: {
            merchant_id: id,
            trans_date: {
              $gte: (yesterday.getTime()) - 19800000,
              $lt: today.getTime()
            }
          }
        },
        { $group: { _id: null, transactions: { $sum: '$transactions' }, trans_amount: { $sum: '$trans_amount' } } }
      ],
      function (mongo_err, mongo_res) {
        if (mongo_err) {
          console.log('Error:::::::::::::', mongo_err)
          callback({
            error: 'Error in yesterday merchant transactions count',
            error_message: mongo_res
          }, null)
        } else if (mongo_res) {
          callback(null, mongo_res)
        } else {
          callback(null, 0)
        }
      })
    },
    /* Merchants amount */
    function (callback) {
      MerchantModel.findOne({
        _id: id
      }, function (MerchantFindErr, MerchantFindRes) {
        if (MerchantFindErr) {
          console.log('Error:::::::::::::', MerchantFindErr)
          callback({
            error_message: 'MongoError: MerchantsAmount find',
            error: MerchantFindErr
          })
        } else if (MerchantFindRes) {
          callback(null, {
            pending_amt: MerchantFindRes.pending_amt ? MerchantFindRes.pending_amt : 0,
            approved_amt: MerchantFindRes.approved_amt ? MerchantFindRes.approved_amt : 0,
            claimed_amt: MerchantFindRes.claimed_amt ? MerchantFindRes.claimed_amt : 0
          })
        } else {
          callback(null, {
            pending_amt: 0,
            approved_amt: 0,
            claimed_amt: 0
          })
        }
      })
    }
  ],
  function (err, results) {
    if (err) {
      console.log('Error::::::::::::::::', err)
      return Response.error(err)
    } else if (results) {
      let total_transactions = 0; let total_transactions_amount = 0; let today_transactions_amount = 0; let today_transactions = 0; let yesterday_transactions = 0
      let yesterday_transactions_amount = 0
      // console.log(results)
      if (results[3].length > 0) {
        total_transactions = results[3][0].transactions
        total_transactions_amount = results[3][0].trans_amount
      }
      if (results[4].length > 0) {
        today_transactions = results[4][0].transactions
        today_transactions_amount = results[4][0].trans_amount
      }
      if (results[5].length > 0) {
        yesterday_transactions = results[5][0].transactions
        yesterday_transactions_amount = results[5][0].trans_amount
      }
      return Response.success('Dashboard data', {
        total_users: results[0],
        today_users: results[2],
        yesterday_users: results[1],
        total_transaction: total_transactions,
        today_transaction: today_transactions,
        yesterday_transaction: yesterday_transactions,
        total_transaction_amount: total_transactions_amount,
        today_transaction_amount: today_transactions_amount,
        yesterday_transaction_amount: yesterday_transactions_amount,
        pending_amt: results[6].pending_amt,
        approved_amt: results[6].approved_amt,
        claimed_amt: results[6].claimed_amt
      })
    }
  })
}
