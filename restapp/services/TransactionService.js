var TransactionService = module.exports
const mongoose = require('mongoose')
/* requiring utils */
var ResponseUtils = require('./../utils/ResponseUtils')
const async = require('async')
const this_file_name = 'TransactionService'

/* requiring models */
const UserTransactionsModel = require('./../models/UserTransactionsModel')
const SettingsModel = require('../models/PaymentSettingsModel')
const MerchantProfileService = require('../../app-services/v1/services/MerchantProfileService')

TransactionService.getTransactions = function (api_request, api_response) {
  var limits = {}

  var findObj = {}
  let this_function_name = 'TransactionService.getTransactions'
  // console.log("api_request",api_request.query);
  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Response = new ResponseUtils.response(api_response)
  let default_error = 'Error while getting transactions list'
  let default_error_code = 'GETTRANSACTIONSERROR'
  try {
    // let token_data, user_id;
    // // get decrypted user_id and query for count
    // // console.log('token data ', api_request.user);
    if (api_request.user && api_request.user._id) {
      token_data = api_request.user
      if (!api_request.query.skip && !api_request.query.limit) {
        if (api_request.query.type) {
          findObj = api_request.query
          findObj.transaction_date = {
            $gte: api_request.query.fromDate,
            $lt: api_request.query.toDate
          }
          delete findObj.fromDate
          delete findObj.toDate
          delete findObj.type
        } else {
          findObj = api_request.query
        }
        // console.log("search query", api_request.query);
        limits = {}
      } else {
        limits = {
          skip: Number(api_request.query.skip),
          limit: Number(api_request.query.limit)
        }
      }
      // var merchant_id = mongoose.Types.ObjectId("5b541dcaa5b277bfe0aa6e07");
      let merchant_id = mongoose.Types.ObjectId(api_request.user._id)
      MerchantProfileService.getMerchantDetails(merchant_id, (merResp, merchantObj) => {
        if (merResp) {
          if (merchantObj && merchantObj.is_admin_verified == true) {
            // user_id = mongoose.Types.ObjectId("5b543e64dd38e85725eae0c8");
            findObj.merchant_id = merchant_id
            UserTransactionsModel.find(findObj, {}, limits, function (err_trans_find, transactionData) {
              if (err_trans_find) {
                console.log('TRANSACIONS FIND ERROR', err_trans_find)
                Logs.error(err_trans_find + '', 'ERRORTRANSACTION FOUND', 'TRANSACTION DATA ERROR')
              } else {
                SettingsModel.findOne({}, {
                  refund_period: true
                }, function (settings_err, settings) {
                  var currDate = Date.now()
                  async.eachSeries(transactionData, function (i, next) {
                    var refundExpriryDate = new Date(i.transaction_date).setHours(24 * settings.refund_period)
                    if (refundExpriryDate > currDate) {
                      /**
                       *  0 Don't display refund button
                       *  1 Display refund button
                       *  2 Refund period exceeded
                       */
                      i.refund_status = 0
                      if (i.status == 1 || i.m_status == 6) {
                        i.refund_status = 1
                        next()
                      } else {
                        next()
                      }
                    } else {
                      i.refund_status = 2
                      next()
                    }
                  }, function (err) {
                    return Response.success('Success fetching transactions', transactionData)
                  })
                })
              }
              // console.log('transactionData ', transactionData);
            }).sort({
              transaction_date: -1
            })
          } else {
            return Response.success('Merchant Not Verified', 'MERCHANT VERIFY ERROR', null)
          }
        } else {
          return Response.error('Merchant Not Found', 'MERCHANT FOUND ERROR', 500)
        }
      })
    } else {
      Logs.error('Error! no user token data', default_error_code, default_error)
      return Response.error('Error! no user token data', 'DBERROR', 400)
    }
  } catch (exception) {
    Print.exception(exception + '')
    Logs.exception(exception + '', new Error(), default_error_code, default_error)
    return Response.error(default_error, default_error_code, 500)
  }
}

TransactionService.getTransactionsCount = function (api_request, api_response) {
  // console.log(" at transaction counts");

  let this_function_name = 'TransactionService.transactionsCount'
  // console.log("api_request",api_request.user)
  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Response = new ResponseUtils.response(api_response)
  let default_error = 'Error while getting transactions count'
  let default_error_code = 'GETTRANSACTIONSCOUNTERROR'
  try {
    // let token_data, user_id;
    // // get decrypted user_id and query for count
    // // console.log('token data ', api_request.user);
    if (api_request.user && api_request.user._id) {
      token_data = api_request.user
      // var merchant_id = mongoose.Types.ObjectId("5b541dcaa5b277bfe0aa6e07");
      let merchant_id = mongoose.Types.ObjectId(api_request.user._id)

      // user_id = mongoose.Types.ObjectId("5b543e64dd38e85725eae0c8");
      UserTransactionsModel.countDocuments({
        'merchant_id': merchant_id
      }, function (err_trans_find, transactionCount) {
        if (err_trans_find) {
          console.log('TRANSACIONS FIND ERROR', err_trans_find)
          Logs.error(err_trans_find + '', 'ERRORTRANSACTION FOUND', 'TRANSACTION DATA ERROR')
        }

        console.log('transactionCountData ', transactionCount)
        return Response.success('Success fetching transactions', transactionCount)
      })
    } else {
      Logs.error('Error! no user token data', default_error_code, default_error)
      return Response.error('Error! no user token data', 'DBERROR', 400)
    }
  } catch (exception) {
    Print.exception(exception + '')
    Logs.exception(exception + '', new Error(), default_error_code, default_error)
    return Response.error(default_error, default_error_code, 500)
  }
}
