/* requiring models */
var UserDetailsModel = require('./../models/UserDetailsModel')
const GeneralUtils = require('./../utils/GeneralUtils')
const NotificationUtils = require('./../utils/NotificationUtils')
const ResponseUtils = require('./../utils/ResponseUtils')
const AppConfig = require('config')
const ev = require('email-mx-validator')

var SharedService = module.exports
const this_file_name = 'SharedService'
SharedService.verifyEmail = function (request, response) {
  let this_function_name = 'ConsumerLoanController2.verifyEmail'
  let Response = new ResponseUtils.response(response)
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let default_error = 'Problem while verifying email. Please try again.'
  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  try {
    ev.validEmail(request.body.email, function (valid) {
      // returns true, because the syntax of the given email address is correct and the email address has at least one DNS MX record.
      if (valid) {
        return Response.success('Valid Email Address', {
          verification: valid
        })
      } else {
        return Response.success('Invalid Email Address', {
          verification: valid
        })
      }
    })
  } catch (exception) {
    console.error(exception)
    Print.exception(exception + '')
    Logs.exception('Problem while verifying email. Please try again.', 'EXCEPTIONEMAILVERIFY', exception + '', new Error())
    Response.error(default_error, 'EXCEPTION', 500)
  }
}
