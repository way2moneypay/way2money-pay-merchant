/** @rakeshvn */
var RefundService = module.exports
var async = require('async')
var mongoose = require('mongoose')
/* Requiring models */
var UserTransactionsModel = require('../models/UserTransactionsModel')
var MerchantTransactionsModel = require('../models/MerchantStatsModel')
var MerchantModel = require('../models/MerchantModel')
var UserDetailsModel = require('../models/UserDetailsModel')
var RefundTransactions = require('../models/RefundTransactions')
var PaymentSettingsModel = require('../models/PaymentSettingsModel')
const merchantStatsModel = require('../models/MerchantStatsModel')
const merchantTransCycleModel = require('../models/MerchantTransCycleModel')
const daywiseTransactionsModel = require('../models/DayWiseTransactionsModel')
const notificationUtils = require('../utils/NotificationUtils')
/* requiring utils */
var ResponseUtils = require('./../utils/ResponseUtils')
let this_file_name = 'RefundService'

RefundService.checkRefund = function (api_request, api_response) {
  default_error = 'Error while checking refund data'
  default_error_code = 'CHECKREFUNDERROR'

  let this_function_name = 'RefundService.checkRefund'
  let Response = new ResponseUtils.response(api_response)
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  try {
    default_error = 'Error while checking refund data'
    default_error_code = 'REFUNDDATAERROR'
    UserTransactionsModel.findById({
      _id: api_request.body.transaction_id
    }, function (TransactionsErr, TransactionsRes) {
      if (TransactionsErr) return Response.error(this_function_name, 'MongoError : UserTransactionsFind', 500, TransactionsErr)
      else if (TransactionsRes) {
        MerchantModel.findById({ _id: TransactionsRes.merchant_id }, function (merchant_error, merchant_data) {
          if (merchant_error) {
            console.log('Error while fetching merchantdetails', merchant_error)
          } else if (merchant_data) {
            // console.log(merchant_data)
            var merchant_commission_amount = (TransactionsRes.amount * TransactionsRes.merchant_commission) / 100
            var merchant_pending_amount = TransactionsRes.amount - merchant_commission_amount
            var con_fee_amount = (TransactionsRes.amount * TransactionsRes.con_fee) / 100
            var total_trans_amount = TransactionsRes.amount + con_fee_amount
            return Response.success('Refund data returned successfully', {
              merchant_pending_amount: merchant_pending_amount,
              merchant_commission_amount: merchant_commission_amount,
              merchant_commission: TransactionsRes.merchant_commission,
              total_trans_amount: total_trans_amount,
              con_fee_amount: con_fee_amount,
              con_fee: TransactionsRes.con_fee,
              original_amount: TransactionsRes.amount,
              merchant_id: TransactionsRes.merchant_id,
              user_id: TransactionsRes.user_id,
              transaction_date: TransactionsRes.transaction_date,
              transaction_id: TransactionsRes._id,
              merchant_mobile: merchant_data.mobile,
              merchant_name: merchant_data.name,
              reference_id: TransactionsRes.reference_id
            })
          }
        })
      } else {
        return Response.error(this_function_name, 'MongoError : UserTransactionsFind', 'UserTransactionNotFound')
      }
    })
  } catch (exception) {
    Print.exception(exception + '')
    Logs.exception(exception + '', default_error_code, default_error)
    return Response.error(default_error, default_error_code, 500, null)
  }
}
RefundService.refund = function (api_request, api_response) {
  // api_request.body = api_request.body.refund;
  default_error = 'Error while refunding'
  default_error_code = 'REFUNDERROR'

  let this_function_name = 'RefundService'
  let Response = new ResponseUtils.response(api_response)
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  // console.log(api_request.body)
  try {
    let transaction = {}
    if (api_request.body.refund != undefined) {
      transaction = api_request.body.refund
    } else {
      transaction = api_request.body
    }

    var pending_amt, creditAvailable, creditDue, user_name, user_mobile
    var transaction_date
    var merchant_commission_amount, merchant_pending_amount, con_fee_amount, total_trans_amount, merchant_id, user_id, original_amount

    let queries = []; let update = 0
    // console.log("checkrefund::::::::::", transaction, transaction.transaction_id)
    if (transaction.transaction_date != undefined) {
      var transaction_date = new Date(transaction.transaction_date)
      transaction_date = transaction_date.setHours(0, 0, 0, 0, 0)
      transaction_date = transaction_date / 1
    }
    if (transaction.merchant_commission != undefined) {
      var f_amount = transaction.original_amount - (transaction.original_amount * transaction.merchant_commission) / 100
    } else {
      var f_amount = transaction.original_amount
    }
    UserTransactionsModel.findOneAndUpdate({ _id: transaction.transaction_id }, {
      $set: {
        status: 4,
        transaction_status: 'refunded'
      },
      $addToSet: {
        trans_details: {
          status: 4,
          date: Date.now()
        }
      }
    }, {
      upsert: false, new: true
    }, function (updateErr, UpdateRes) {
      if (updateErr) {
        console.log('Error while updating userTransactions', updateErr)
        return Response.error({
          error_message: 'Error while updating userTransactions',
          error_details: updateErr
        }, 'REFUND-FAILED', 500)
      } else {
        var convenience = UpdateRes.amount + ((UpdateRes.amount * UpdateRes.con_fee) / 100)
        console.log('userTransactions updated successfully')
        UserDetailsModel.findOne({ _id: transaction.user_id }, function (err_get_user_details, user_details) {
          if (err_get_user_details) {
            console.log('Error while updating userDetails', err_get_user_details)
            return Response.error({
              error_message: 'Error while updating userDetails',
              error_details: updateErr
            }, 'REFUND-FAILED', 500)
          } else {
            user_details.credit_available = Number(user_details.credit_available) + Number(convenience)
            user_details.credit_due = Number(user_details.credit_due) - Number(convenience)

            if (user_details.credit_available > user_details.credit_limit) {
              user_details.credit_available = user_details.credit_limit
            }

            if (user_details.credit_due < 0) {
              user_details.credit_due = 0
              user_details.due_date = 0
            }
            user_name = user_details.name
            user_mobile = user_details.mobile_number
            if (user_details.credit_due == 0) {
              user_details.due_date = 0
            }
            creditAvailable = user_details.credit_available
            creditDue = user_details.credit_due
            user_details.save((err_save_user_details, user_detials_saved) => {
              if (err_save_user_details) {
                console.log('Error while saving userDetails', err_save_user_details)
                return Response.error({
                  error_message: 'Error while saving userDetails',
                  error_details: updateErr
                }, 'REFUND-FAILED', 500)
              } else {
                console.log('userDetails updated successfully in refund')
                merchantTransCycleModel.count({ cycle_date: { $gte: transaction_date } },
                  function (countError, countResult) {
                    if (countError) {
                      console.log(countError)
                      throw cb(countError)
                    } else {
                      if (countResult != 0) {
                        if (transaction.original_amount != undefined) {
                          //  transaction_date = transaction_date/1;
                          queries.push(function (cb) {
                            daywiseTransactionsModel.findOneAndUpdate({
                              trans_date: transaction_date
                            }, {
                              $inc: {
                                transactions: -1,
                                trans_amount: -transaction.original_amount,
                                amount: -f_amount
                              }
                            }, {
                              upsert: false
                            },
                            function (updateError, updateResult) {
                              // console.log(updateResult)
                              if (updateError) {
                                console.log(updateError)
                                throw cb(updateError)
                              } else {
                                cb(null, updateResult)
                              }
                            })
                          })
                        }
                        if (transaction.transaction_date != undefined && transaction.merchant_id != undefined && transaction.original_amount != undefined) {
                          queries.push(function (cb) {
                            merchantStatsModel.findOneAndUpdate({
                              merchant_id: transaction.merchant_id,
                              trans_date: transaction_date
                            }, {
                              $inc: {
                                transactions: -1,
                                trans_amount: -transaction.original_amount,
                                amount: -f_amount
                              }
                            }, {
                              upsert: false
                            },
                            function (updateError, updateResult) {
                              // console.log(updateResult)
                              if (updateError) {
                                console.log(updateError)
                                throw cb(updateError)
                              } else {
                                cb(null, updateResult)
                              }
                            })
                          })
                        }
                        queries.push(function (cb) {
                          // {cycle_status:{$ne:"pending"}}
                          merchantTransCycleModel.find({ merchant_id: transaction.merchant_id }).sort({ _id: -1 }).limit(1)

                            .exec(function (error, result) {
                              if (error) {
                                console.log(error)
                                throw cb(error)
                              } else {
                                if (Object.getOwnPropertyNames(result).length != 0) {
                                  if (result.cycle_status != 'pending') {
                                    MerchantModel.findOneAndUpdate({
                                      _id: transaction.merchant_id
                                    }, {
                                      $inc: {
                                        previous_cycle_paid_refund_amount: transaction.original_amount
                                      }
                                    }, {
                                      upsert: false
                                    },
                                    function (updateError, updateResult) {
                                      // console.log(updateResult)
                                      if (updateError) {
                                        console.log(updateError)
                                        throw cb(updateError)
                                      } else {
                                        cb(null, updateResult)
                                      }
                                    })
                                  } else {
                                    merchantTransCycleModel.findOneAndUpdate({ merchant_id: transaction.merchant_id }
                                      , {
                                        $inc: {
                                          transactions: -1,
                                          amount: -transaction.original_amount
                                        }
                                      }, {
                                        upsert: false
                                      }).sort({ _id: -1 }).limit(1)

                                      .exec(function (updateErr, UpdateRes) {
                                        if (updateErr) {
                                          console.log(updateErr)
                                          throw cb(updateErr)
                                        } else {
                                          cb(null, UpdateRes)
                                        }
                                      })
                                  }
                                } else {
                                  cb(null, 0)
                                }
                              }
                            })
                        })
                        async.parallel(queries, function (err, docs) {
                          if (err) {
                            console.log('async err', err)
                            return Response.error({
                              error_message: 'Error while updating transcycle',
                              error_details: err
                            }, 'REFUND-FAILED', 500)
                          } else {
                            RefundTransactionDetails = new RefundTransactions({
                              transaction_id: transaction.transaction_id,
                              merchant_id: transaction.merchant_id,
                              user_id: transaction.user_id,
                              con_fee: transaction.con_fee_amount,
                              merchant_commission: transaction.merchant_commission_amount,
                              amount: transaction.original_amount,
                              trans_amount: transaction.total_trans_amount,
                              credit_available: creditAvailable,
                              credit_due: creditDue,
                              refunded_date: Date.now(),
                              refund_by: 'Merchant'
                            })
                            RefundTransactionDetails.save(function (RefundTransactionSaveErr, RefundTransactionSaved) {
                              if (RefundTransactionSaveErr) {
                                console.log(RefundTransactionSaveErr)
                                return Response.error({
                                  error_message: 'Refund success. Saving redund transaction failed',
                                  error_details: RefundTransactionSaveErr
                                }, 'REFUND-FAILED', 500)
                              } else {
                                if (transaction.con_fee != undefined) {
                                  var convenience = transaction.original_amount + ((transaction.original_amount * transaction.con_fee) / 100)
                                } else {
                                  var convenience = transaction.original_amount
                                }
                                if (user_name != undefined && user_mobile != undefined) {
                                  let configuration = {}

                                  let obj = {
                                    amount: convenience,
                                    name: user_name
                                  }
                                  configuration.mobile = user_mobile
                                  configuration.replace = obj
                                  // console.log(configuration.emails);
                                  notificationUtils.sms('refund_sms', configuration, function (err, resp) {
                                    console.error(err)
                                    // console.log("in user");
                                  })
                                }
                                if (transaction.merchant_name != undefined && transaction.merchant_mobile) {
                                  let configuration = {}

                                  let obj = {
                                    amount: convenience,
                                    name: transaction.merchant_name,
                                    transaction: transaction.transaction_id
                                  }
                                  configuration.mobile = transaction.merchant_mobile
                                  configuration.replace = obj
                                  notificationUtils.sms('merchant_refund_sms', configuration, function (err, resp) {
                                    console.error(err)
                                    // console.log("In merchant");
                                  })
                                }
                                return Response.success('Refunded success', 'Refunded')
                              }
                            })
                          }
                        })
                      } else {
                        RefundTransactionDetails = new RefundTransactions({
                          transaction_id: transaction.transaction_id,
                          merchant_id: transaction.merchant_id,
                          user_id: transaction.user_id,
                          con_fee: con_fee_amount,
                          merchant_commission: transaction.merchant_commission_amount,
                          amount: transaction.original_amount,
                          trans_amount: transaction.total_trans_amount,
                          credit_available: creditAvailable,
                          credit_due: creditDue,
                          refunded_date: Date.now(),
                          refund_by: 'Merchant'
                        })
                        RefundTransactionDetails.save(function (RefundTransactionSaveErr, RefundTransactionSaved) {
                          if (RefundTransactionSaveErr) {
                            console.log(RefundTransactionSaveErr)
                            return Response.error({
                              error_message: 'Refund success. Saving redund transaction failed',
                              error_details: RefundTransactionSaveErr
                            }, 'REFUND-FAILED', 500)
                          } else {
                            if (transaction.con_fee != undefined) {
                              var convenience = transaction.original_amount + ((transaction.original_amount * transaction.con_fee) / 100)
                            } else {
                              var convenience = transaction.original_amount
                            }
                            if (user_name != undefined && user_mobile != undefined) {
                              let configuration = {}

                              let obj = {
                                amount: convenience,
                                name: user_name
                              }
                              configuration.mobile = user_mobile
                              configuration.replace = obj
                              // console.log(configuration.emails);
                              notificationUtils.sms('refund_sms', configuration, function (err, resp) {
                                console.error(err)
                                // console.log("in user");
                              })
                            }
                            console.log(transaction)
                            if (transaction.merchant_name != undefined && transaction.merchant_mobile) {
                              let configuration = {}

                              let obj = {
                                amount: convenience,
                                name: transaction.merchant_name,
                                transaction: transaction.reference_id
                              }
                              configuration.mobile = transaction.merchant_mobile
                              configuration.replace = obj
                              // console.log(configuration.emails);
                              notificationUtils.sms('merchant_refund_sms', configuration, function (err, resp) {
                                console.error(err)
                                // console.log("In merchant");
                              })
                            }
                            return Response.success('Refunded success', 'Refunded')
                          }
                        })
                      }
                    }
                  })
              }
            })
          }
        })
      }
    })

    /** Rollback function incase if refund fails */
    // function MerchantRefundRollback(error_msg, callback) {
    //   MerchantModel.findByIdAndUpdate({
    //     _id: merchant_id
    //   }, {
    //     $set: rollback_refund_merchant_amount
    //   }, function (MerchantUpdateErr, MerchantUpdated) {
    //     if (MerchantUpdateErr) MerchantRefundRollback(error_msg)
    //     else {
    //       console.log('Refund failed. Transaction rollbacked')
    //       return Response.error({
    //         error_message: "Refund failed",
    //         error_details: error_msg
    //       }, 'REFUND-FAILED', 500);
    //     }
    //   })
    // }
    /** Rollback function incase if refund fails */
    // function UserMerchantRefundRollback(error, callback) {
    //   MerchantModel.findByIdAndUpdate({
    //     _id: merchant_id
    //   }, {
    //     $set: {
    //       pending_amt: init_pending_amt
    //     }
    //   }, function (MerchantUpdateErr, MerchantUpdated) {
    //     if (MerchantUpdateErr) UserMerchantRefundRollback(error)
    //     else {
    //       UserDetailsModel.findByIdAndUpdate({
    //         _id: user_id
    //       }, {
    //         $set: {
    //           credit_available: init_credit_available,
    //           credit_due: init_credit_due
    //         }
    //       }, function (UserUpdateErr, UserUpdateUpdated) {
    //         if (UserUpdateErr) UserMerchantRefundRollback(error)
    //         else {
    //           return Response.error({
    //             error_msg: "Refund failed and transactions rollbacked.",
    //             error_details: error
    //           }, 'REFUND-FAILED', 500);
    //         }
    //       })
    //     }
    //   })
    // }
    /** Rollback function incase if refund fails */
    // function UserMerchantStatusRefundRollback(error, callback) {
    //   MerchantModel.findByIdAndUpdate({
    //     _id: merchant_id
    //   }, {
    //     $set: {
    //       pending_amt: init_pending_amt
    //     }
    //   }, function (MerchantUpdateErr, MerchantUpdated) {
    //     if (MerchantUpdateErr) UserMerchantStatusRefundRollback(error)
    //     else {
    //       UserDetailsModel.findByIdAndUpdate({
    //         _id: user_id
    //       }, {
    //         $set: {
    //           credit_available: init_credit_available,
    //           credit_due: init_credit_due
    //         }
    //       }, function (UserUpdateErr, UserUpdateUpdated) {
    //         if (UserUpdateErr) return Response.error({
    //           error_msg: "Rollback for merchant is successfull, rollback for user failed.",
    //           error_details: UserUpdateErr
    //         }, 'REFUND-FAILED', 500);
    //         else {
    //           UserTransactionsModel.findByIdAndUpdate({
    //             _id: api_request.body.transaction_id
    //           }, {
    //             $set: {
    //               status: init_status
    //             }
    //           }, function (err, res) {
    //             if (err) return Response.error({
    //               error_msg: 'Rollback for merchant and user is successfull, rollback for transaction status is failed reason :',
    //               error_details: err
    //             }, error, 500);
    //             else return Response.error({
    //               error_message: 'Refund failed and transactions rollbacked',
    //               error_details: error
    //             }, 'REFUND-FAILED', 500);
    //           })
    //         }
    //       })
    //     }
    //   })
    // }
    /** Refund of transactions starts */
    // UserTransactionsModel.findOne({
    //   _id: api_request.body.transaction_id
    // }, function (checkStatusErr, checkStatusRes) {
    //   if (checkStatusErr) return Response.error({
    //     error_msg: 'Error while checking transaction stats',
    //     error_type: 'MongoError: Transactiond find error',
    //     error_details: checkStatusErr
    //   }, 'REFUND-FAILED', 500)
    //   else if (checkStatusRes) {
    //     merchant_commission_amount = (checkStatusRes.amount * checkStatusRes.merchant_commission) / 100;
    //     merchant_pending_amount = checkStatusRes.amount - merchant_commission_amount;
    //     con_fee_amount = (checkStatusRes.amount * checkStatusRes.con_fee) / 100;
    //     total_trans_amount = checkStatusRes.amount + con_fee_amount;
    //     merchant_id = checkStatusRes.merchant_id;
    //     user_id = checkStatusRes.user_id;
    //     original_amount = checkStatusRes.amount;

    //     if (checkStatusRes.status != 4) {
    //       async.series([
    /** 1. Merchant refund */
    /** Checking whether merchants exists */
    // function (callback) {
    //   MerchantModel.findById({
    //     _id: merchant_id
    //   }, function (MerchantErr, MerchantRes) {
    //     if (MerchantErr) callback('MongoError : Merchants find error', null)
    //     else if (MerchantRes) {
    //       /** If merchant found refunding amount to merchant */
    //       let init_pending_amt = MerchantRes.pending_amt ? MerchantRes.pending_amt : 0;
    //       let init_approved_amt = MerchantRes.approved_amt ? MerchantRes.approved_amt : 0;
    //       /** Calculating refund amount */
    //       pending_amt = init_pending_amt - merchant_pending_amount
    //       approved_amt = init_approved_amt - merchant_pending_amount;

    //       refund_merchant_amount = {
    //         pending_amt: pending_amt
    //       }
    //       // Storing initial amount incase to rollback
    //       rollback_refund_merchant_amount = {
    //         pending_amt: init_pending_amt
    //       }
    //       /** Checking status */
    //       if (checkStatusRes.status == 6 || checkStatusRes.status == 7) {
    //         refund_merchant_amount = {
    //           approved_amt: approved_amt
    //         }
    //         // Storing initial amount incase to rollback
    //         rollback_refund_merchant_amount = {
    //           approved_amt: init_approved_amt
    //         }
    //       }

    //       MerchantModel.findByIdAndUpdate({
    //         _id: merchant_id
    //       }, {
    //         $set: refund_merchant_amount
    //       }, function (MerchantUpdateErr, MerchantUpdated) {
    //         /** If error occurs while refunding throwing error */
    //         /** It's the first step so rollback isn't necessary */
    //         if (MerchantUpdateErr) callback({
    //           error_name: 'MongoError : Merchants refund update failed',
    //           error: MerchantUpdateErr
    //         }, null)
    //         else if (MerchantUpdated) callback(null, null)
    //       })
    //     } else {
    //       callback('404 : Merchant not found', null)
    //     }
    //   })
    // },
    // /** 2. User's refund */
    // /** Checking whether user exists */
    // function (callback) {
    //   UserDetailsModel.findById({
    //     _id: user_id
    //   }, function (UserErr, UserRes) {
    //     /** If error occurs while checking user exists rollback merchants refund*/
    //     if (UserErr) {
    //       MerchantRefundRollback({
    //         error_msg: 'Refund failed and transactions rollbacked',
    //         error_type: "MongoError : User's find error",
    //         error_details: UserErr
    //       })
    //     } else if (UserRes) {
    //       /** Storing due_date */
    //       due_date = UserRes.due_date
    //       /** Storing intial amount if incase to rollback refund */
    //       init_credit_available = UserRes.credit_available
    //       init_credit_due = UserRes.credit_due
    //       /** Storing refund amount */
    //       credit_available = UserRes.credit_available + total_trans_amount
    //       credit_due = UserRes.credit_due - total_trans_amount
    //       UserDetailsModel.findByIdAndUpdate({
    //         _id: user_id
    //       }, {
    //         $set: {
    //           credit_available: credit_available,
    //           credit_due: credit_due
    //         }
    //       }, function (UserUpdateErr, UserUpdateUpdated) {
    //         if (UserUpdateErr) {
    //           MerchantRefundRollback({
    //             error_msg: 'Refund failed and transactions rollbacked',
    //             error_type: "MongoError : User's refund update failed",
    //             error_details: UserUpdateErr
    //           })
    //         } else if (UserUpdateUpdated) {
    //           callback(null, null)
    //         }
    //       })
    //     } else {
    //       MerchantRefundRollback({
    //         error_msg: 'Refund failed and transactions rollbacked',
    //         error_type: "404: User not found"
    //       })
    //     }
    //   })
    // },
    // /** 3. Updating redunded in user's transaction */
    // function (callback) {
    //   UserTransactionsModel.findByIdAndUpdate({
    //     _id: api_request.body.transaction_id
    //   }, {
    //     $set: {
    //       status: 4
    //     },
    //     $addToSet: {
    //       trans_details: {
    //         status: 4,
    //         date: Date.now()
    //       }
    //     }
    //   }, function (err, res) {
    //     if (err) {
    //       UserMerchantRefundRollback({
    //         error_msg: 'Refund failed and transactions rollbacked',
    //         error_type: "MongoError: Updating in user's transaction failed",
    //         error: error_details
    //       })
    //     } else {
    //       transaction_date = res.transaction_date
    //       init_status = res.status
    //       callback(null, null)
    //     }
    //   })
    // },
    // /** 4. Checking to updating due date */
    // function (callback) {
    //   /** Explanation of updating due date (Example) */
    //   /** Existing due_date = 15 Jul (Period : 15 days)
    //    *  List of transactions
    //    *  1 Jul - failed (clicked refund on this transaction )
    //    *  5 Jul - success
    //    *  8 Jul - success
    //    *  After refund
    //    *  (1 Jul) transaction is not valid anymore
    //    *  So due date should be updated to 20 Jul (5 Jul + 15 days)
    //    *  Case 1. Check whether transactions exists after refunded transaction
    //    *          if exists update due_date = transaction_date+due_period
    //    *  Case 2. If transactions not found update due_date = 0
    //    */

    //   /** Step 1. Check whether successfull transactions exists after refunded transaction */
    //   UserTransactionsModel.findOne({
    //     user_id: mongoose.Types.ObjectId(user_id),
    //     status: 1
    //   }, function (findErr, findRes) {
    //     if (findErr) UserMerchantStatusRefundRollback(findErr)
    //     /**
    //      * Case 1.
    //      * If transactions exits.
    //      * Check whether transactions are greater than refunded transaction if found
    //      * update due_date = transaction_date (results) + due_period
    //      */
    //     else if (findRes) {
    //       if (findRes.transaction_date >= transaction_date) {
    //         console.log(' | / Due date update / | Case 1: Updating due_date = transaction_date (results) + due_period')
    //         PaymentSettingsModel.findOne({}, function (findErr, findData) {
    //           if (findErr) UserMerchantStatusRefundRollback(findErr)
    //           else {
    //             updated_due_date = new Date(findRes.transaction_date)
    //             updated_due_date.setDate(updated_due_date.getDate() + findData.due_period)
    //             UserDetailsModel.findByIdAndUpdate({
    //               _id: user_id
    //             }, {
    //               $set: {
    //                 due_date: updated_due_date
    //               }
    //             }, function (updateErr, UpdateRes) {
    //               if (updateErr) UserMerchantStatusRefundRollback({
    //                 error_msg: "Refund failed and transactions rollbacked",
    //                 error_type: "MongoError : Due date update failed",
    //                 error_details: updateErr
    //               })
    //               else callback(null, null)
    //             })
    //           }
    //         })
    //       } else {
    //         callback(null, null)
    //       }
    //     } else {
    //       /**
    //        * Case 2.
    //        * If transactions not exists update due_date = 0
    //        */
    //       console.log(' | / Due date update / | Case 2: Updating due_date = 0')
    //       UserDetailsModel.findByIdAndUpdate({
    //         _id: user_id
    //       }, {
    //         $set: {
    //           due_date: 0
    //         }
    //       }, function (updateErr, UpdateRes) {
    //         if (updateErr) UserMerchantStatusRefundRollback({
    //           error_msg: "Refund failed and transactions rollbacked",
    //           error_type: "MongoError : Due date update failed",
    //           error_details: updateErr
    //         })
    //         else callback(null, null)
    //       })
    //     }
    //   })
    // },
    /** 5. Saving refund transaction */
    /** User's, Merchant's refund is successfull so storing refund transaction */
    //   function (callback) {
    //     RefundTransactionDetails = new RefundTransactions({
    //       transaction_id: api_request.body.transaction_id,
    //       merchant_id: merchant_id,
    //       user_id: user_id,
    //       con_fee: con_fee_amount,
    //       merchant_commission: merchant_commission_amount,
    //       amount: original_amount,
    //       trans_amount: total_trans_amount,
    //       pending_amt: pending_amt,
    //       credit_available: credit_available,
    //       credit_due: credit_due,
    //       refunded_date: Date.now(),
    //       refund_by: 'Merchant'
    //     })
    //     RefundTransactionDetails.save(function (RefundTransactionSaveErr, RefundTransactionSaved) {
    //       if (RefundTransactionSaveErr) callback({
    //         error_msg: "Refund success. Saving redund transaction failed",
    //         error_type: "MongoError : Due date update failed",
    //         error_details: RefundTransactionSaveErr
    //       })
    //       else callback(null, null)
    //     })
    //   },
    // ], function (err, results) {
    //   if (err) {
    //     return Response.error('Error while refunding', err, 500, '');
    //   } else {
    //     return Response.success("Refunded success", 'Refunded');
    //   }
    // })
    //     } else {
    //       return Response.success("Transaction is already refunded", 'Refunded');
    //     }
    //   } else return Response.success("Transaction is not found", 'Refund failed', 500);
    // })
  } catch (exception) {
    Print.exception(exception + '')
    Logs.exception(exception + '', default_error_code, default_error)
    return Response.error(default_error, default_error_code, 500, null)
  }
}
