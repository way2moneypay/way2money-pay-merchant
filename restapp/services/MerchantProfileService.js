const {
  isEmail
} = require('validator')
const ShortId = require('shortid')
const crypto = require('crypto')
const AppConfig = require('config')
const FileUploadUtils = require('./../utils/FileUtils')
const ResponseUtils = require('./../utils/ResponseUtils')
const MerchantModel = require('../models/MerchantModel')
const MerchantCommissionModel = require('../models/MerchantCommissionsModel')
const SettingsModel = require('../models/PaymentSettingsModel')
const async = require('async')
const fs = require('fs')
const homedir = require('os').homedir()
// const passwordHash = require('password-hash');
const MerchantProfileServices = module.exports

MerchantProfileServices.register = function (apiReq, apiRes) {
  let Response = new ResponseUtils.response(apiRes)
  var step = apiReq.params.step
  var date = new Date()
  var updated_on = date / 1
  var regMerchantObj = apiReq.body
  var token_data = apiReq.user
  var merchant = {}
  if (step == 1) {
    if (!regMerchantObj.name) {
      return Response.error('Name is required.', null)
    } else if (!regMerchantObj.email) {
      return Response.error('Email is required.', null)
    } else if (!regMerchantObj.brand_name) {
      return Response.error('Brand Name is required.', null)
    } else {
      var web_access_token = crypto.randomBytes(10).toString('hex')
      var web_secret_key = crypto.randomBytes(10).toString('hex')
      SettingsModel.findOne({}, (errFindSettings, settings) => {
        if (errFindSettings) {
          return Response.error('Error while finding settings', errFindSettings, null)
        } else {
          MerchantModel.findByIdAndUpdate({
            _id: token_data._id
          }, {
            $set: {
              name: regMerchantObj.name,
              email: regMerchantObj.email.toLowerCase(),
              brand_name: regMerchantObj.brand_name,
              token_validity: settings.token_validity,
              merchant_status: regMerchantObj.type == 0 ? 0 : 2,
              updated_on: updated_on,
              secret_key: ShortId.generate(),
              web_access_token: web_access_token,
              web_secret_key: web_secret_key,
              is_profile_verified: true
            }
          }, {
            new: true
          }, (errMerchant, merchant) => {
            if (errMerchant) {
              return Response.error('Something went wrong while registering merchant.', errMerchant, null)
            } else {
              return Response.success('Merchant profile successfully updated.', merchant)
            }
          })
        }
      })
    }
  } else if (step == 2) {
    var bank_details = 0
    var pan_details = 0
    var oldMerchantObj = {}
    async.waterfall([
      (done) => {
        MerchantModel.findById({
          _id: regMerchantObj._id
        }, (errFind, merchantFound) => {
          if (errFind) {
            console.log('Error while finding merchant details on step 2')
            done(null)
          } else {
            oldMerchantObj = merchantFound
            done(null)
          }
        })
      },
      (done) => {
        if (regMerchantObj.type == 0 && regMerchantObj.bank_name && regMerchantObj.account_no && regMerchantObj.name_on_bank && regMerchantObj.ifsc_code) {
          bank_details = 1
        }
        if (regMerchantObj.type == 1 && (regMerchantObj.bank_name != oldMerchantObj.bank_name || regMerchantObj.account_no != oldMerchantObj.account_no || regMerchantObj.name_on_bank != oldMerchantObj.name_on_bank || regMerchantObj.ifsc_code != oldMerchantObj.ifsc_code)) {
          bank_details = 1
        } else if (regMerchantObj.type == 1 && (regMerchantObj.bank_name == oldMerchantObj.bank_name && regMerchantObj.account_no == oldMerchantObj.account_no && regMerchantObj.name_on_bank == oldMerchantObj.name_on_bank && regMerchantObj.ifsc_code == oldMerchantObj.ifsc_code)) {
          bank_details = oldMerchantObj.bank_details
        }
        if (regMerchantObj.type == 1 && oldMerchantObj.pan_card && oldMerchantObj.pan_card != regMerchantObj.pan_card) {
          pan_details = 2
        } else if (regMerchantObj.type == 1 && oldMerchantObj.pan_card && oldMerchantObj.pan_card == regMerchantObj.pan_card) {
          if (regMerchantObj.pan_number != oldMerchantObj.pan_number || regMerchantObj.pan_name != oldMerchantObj.pan_name) {
            pan_details = 2
          } else {
            pan_details = oldMerchantObj.pan_details
          }
        } else if (regMerchantObj.type == 1 && regMerchantObj.pan_number && regMerchantObj.pan_name && !oldMerchantObj.pan_card) {
          pan_details = 1
        } else if (regMerchantObj.type == 0 && regMerchantObj.pan_number && regMerchantObj.pan_name) {
          pan_details = 1
        }
        MerchantModel.findByIdAndUpdate({
          _id: token_data._id
        }, {
          $set: {
            bank_name: regMerchantObj.bank_name,
            account_no: regMerchantObj.account_no,
            name_on_bank: regMerchantObj.name_on_bank,
            ifsc_code: regMerchantObj.ifsc_code,
            pan_number: regMerchantObj.pan_number,
            pan_name: regMerchantObj.pan_name,
            tin: regMerchantObj.tin,
            bank_details: bank_details,
            pan_details: pan_details,
            updated_on: updated_on
          }
        }, {
          new: true
        }, (errMerchant, merchant) => {
          if (errMerchant) {
            return Response.error('Something went wrong while updating bank details.', errMerchant, null)
          } else {
            return Response.success('Merchant bank details successfully updated.', merchant)
          }
        })
      }
    ])
  }
}

MerchantProfileServices.updateMerchant = function (apiReq, apiRes) {
  let Response = new ResponseUtils.response(apiRes)
  var merchantObj = apiReq.body
  merchantObj.brand_name = merchantObj.brand_name.trim()
  merchantObj.logo_url = merchantObj.logo_url.trim()
  // if (typeof (merchantObj.web_domains) == "string") {
  //   merchantObj.web_domains = merchantObj.web_domains.trim().split(',');
  // } else {
  //   merchantObj.web_domains = merchantObj.web_domains[0].trim().split(',');
  // }
  var merchantId = apiReq.user._id
  var updateMerchantProfile = {}
  if (merchantObj && !merchantObj.brand_name) {
    return Response.error('Brand name is required.', null)
  } else {
    MerchantModel.findByIdAndUpdate({
      _id: merchantId
    }, {
      $set: merchantObj
    }, (errUpdate, profileUpdated) => {
      if (errUpdate) {
        return Response.error('Error while updating merchant profile.', null)
      } else {
        return Response.success('Merchant details updated successfully.')
        // apiRes.send({
        //   success: true,
        //   message: 'Merchant details updated successfully.'
        // });
      };
    })
  }
}

var unlinkLeadFile = function (upload_root_path, name) {
  // console.log('Unlink ::', upload_root_path + '/' + name + '.jpeg');
  let file_name = upload_root_path + '/' + name + '.jpeg'
  if (fs.existsSync(file_name)) fs.unlink(file_name, () => {})
  file_name = upload_root_path + '/' + name + '.jpg'
  if (fs.existsSync(file_name)) fs.unlink(file_name, () => {})
  file_name = upload_root_path + '/' + name + '.png'
  if (fs.existsSync(file_name)) fs.unlink(file_name, () => {})
  file_name = upload_root_path + '/' + name + '.pdf'
  if (fs.existsSync(file_name)) fs.unlink(file_name, () => {})
}

MerchantProfileServices.uploadDoc = (apiReq, apiRes) => {
  let Response = new ResponseUtils.response(apiRes)
  var token_data = apiReq.user
  var file_extension = apiReq.query.file_extension
  var image_type = apiReq.query.image_type
  var file_name = apiReq.query.file_name + '_' + image_type + file_extension
  var upload_directory = __dirname + '../../../uploads'
  let upload_root_path = upload_directory + '/' + file_name
  unlinkLeadFile(upload_directory, apiReq.query.file_name + '_' + image_type)
  let FileUpload = new FileUploadUtils.upload(apiReq, apiRes, upload_root_path, upload_directory, file_name, 'file', null, true, 'single')
  FileUpload.exec(function (upload_error, upload_file_name) {
    if (upload_error) {
      return Response.error('Error while Uploading Image', null)
    } else {
      var imageUpload = {}
      if (image_type == 'pan_card') {
        imageUpload['pan_details'] = 2
      }
      imageUpload[image_type] = AppConfig.images_cdn + file_name + '?q=' + Math.random()
      MerchantModel.findByIdAndUpdate({
        _id: token_data._id
      }, {
        $set: imageUpload
      },
      (errUpdateLogoPath, logoPathUpdated) => {
        if (errUpdateLogoPath) {
          return Response.error('Error while updating file url in DB.', null)
        } else {
          return Response.success('File successfully uploaded.', file_name)
        }
      })
    }
  })
}

MerchantProfileServices.getDetails = function (apiReq, apiRes) {
  let Response = new ResponseUtils.response(apiRes)
  var token_data = apiReq.user
  // console.log('get details token data', token_data);

  MerchantModel.findById({
    _id: token_data._id
  }, {
    register_date: false,
    web_access_token: false,
    web_secret_key: false,
    secret_key: false,
    updated_on: false,
    register_by: false
  }, (errFind, merchant) => {
    if (errFind) {
      return Response.error('Error while fetching merchant details.', null)
    } else {
      return Response.success('Merchant details successfully fetched.', merchant)
    }
  })
}
