var UserDetailsModel = require('./../models/UserDetailsModel')
var UserFeedbacksModel = require('./../models/UserFeedbacksModel')
var MerchantModel = require('./../models/MerchantModel')
var MerchantOTPModel = require('./../models/MerchantOTPModel')
var PaymentService = require('./../services/TransactionService')
const mongoose = require('mongoose')
var JWT = require('jsonwebtoken')
var LoginService = module.exports
var UserDetailsModel = require('./../models/UserDetailsModel')
const GeneralUtils = require('./../utils/GeneralUtils')
const NotificationUtils = require('./../utils/NotificationUtils')
const ResponseUtils = require('./../utils/ResponseUtils')
const AppConfig = require('config')
const this_file_name = './LoginService.js'

LoginService.registrationOTP = function (api_request, api_response) {
  let this_file_name = './../services/LoginService.js'
  let this_function_name = 'LoginService.register'
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Response = new ResponseUtils.response(api_response)
  // console.log("API QUERY RECEIVED", api_request.body);
  let mobile = (api_request.body.mobile).toString()
  // console.log("mobile",mobile);
  let otp = SendOTP(mobile, null, function (otp_error, otp_msg, otp_code, data) {
    if (otp_error) {
      return Response.error(otp_msg, otp_code, otp, null)
    } else {
      console.log('OTP SEND SUCCESSFULLY')
      return Response.success('Successfully registered the user.', [])
    }
  })
}

LoginService.login = function (api_request, api_response) {
  // console.log("api_request", api_request.body)
  let Response = new ResponseUtils.response(api_response)
  try {
    if (!api_request.body.mobile || !api_request.body.otp) {
      console.error('Mobile (OR) OTP not sent.')
      return Response.error('Mobile (OR) OTP not sent.', 'BAD-PARAMS', 400)
    }

    let mobile_filter = {}
    mobile_filter.mobile = api_request.body.mobile.toString()

    let saveObj = {}
    saveObj.mobile = api_request.body.mobile
    saveObj.register_date = new Date() / 1
    VerifyOtp(api_request.body.mobile, api_request.body.otp, false, (otp_verify_error, verify_otp_code, verify_otp_message) => {
      // console.log("otp_verify_error", otp_verify_error);
      // console.log("verify_otp_code", verify_otp_code);
      if (otp_verify_error) {
        return Response.error(verify_otp_message, verify_otp_code, 500)
      }
      MerchantModel.findOne(mobile_filter, (merchant_find_error, merchant_data) => {
        // console.log("merchant_find_error",merchant_find_error);
        // console.log("merchant_data",merchant_data);
        if (merchant_find_error) {
          console.log('Error while finding the merchant details in mobile login.', merchant_find_error)
          return Response.error('Internal Server Error.', 'DB-ERROR', 500)
        } else if (!merchant_data) {
          new MerchantModel(saveObj).save((merchant_error, merchantObj) => {
            if (merchant_error) {
              console.log('MERCHANT SAVE ERROR', merchant_error)
              Logs.error(merchant_error + '', 'ERRO MERCHANT SAVE', 'MERCHANT SAVE ERROR')
            } else {
              let merchant = {}
              merchant._id = merchantObj._id
              merchant.mobile = merchantObj.mobile
              merchant.register_date = merchantObj.register_date
              // merchant.is_profile_verified = merchant_data.is_profile_verified;
              let token = JWT.sign(merchant, AppConfig.jwtsecret)
              let response = {}
              response.merchant_token = token
              // response.merchant_deta = merchant;
              return Response.success('SUCCESSFULLY LOGGED IN', response)
            }
          })
        } else {
          let merchant = {}
          merchant._id = merchant_data._id
          merchant.mobile = merchant_data.mobile
          merchant.register_date = merchant_data.register_date
          merchant.is_profile_verified = merchant_data.is_profile_verified
          let token = JWT.sign(merchant, AppConfig.jwtsecret)
          let response = {}
          response.merchant_token = token
          // response.merchant_deta = merchant;
          return Response.success('SUCCESSFULLY LOGGED IN', response)
        }
      })
    })
  } catch (exception) {
    Print.exception(exception + '', exception)
    Logs.exception('Exception occurred.', 'EXCEPTION', exception + '', exception)
    return Response.error('Internal Server Error', 'EXCEPTION', 500)
  }
}

var SendOTP = function (mobile, skip, callback) {
  if (skip) return callback(null)

  let otp_filter = {}
  otp_filter.mobile_number = mobile

  let otp_number = GeneralUtils.randomNumber(1000, 9999)

  let OTPData = new MerchantOTPModel()
  OTPData.mobile_number = mobile
  OTPData.otp = otp_number
  OTPData.otp_date = Date.now()

  MerchantOTPModel.findOne(otp_filter, (otp_find_error, otp_data) => {
    if (otp_find_error) {
      console.error('Error while finding the otp data.', otp_find_error)
      return callback(true, 'Internal server error.', 'DB-ERROR', null)
    }

    if (otp_data) {
      let otp_send_diff = GeneralUtils.unixTimeDiff(otp_data.otp_date, Date.now(), 'minutes')
      let otp_send_diff_seconds = GeneralUtils.unixTimeDiff(otp_data.otp_date, Date.now(), 'seconds')

      let is_same_day = GeneralUtils.isUnixSameDate(Date.now(), otp_data.otp_date, 1000, 1000)
      // console.log(otp_send_diff_seconds, otp_data.otp_date, Date.now());
      if (otp_send_diff_seconds < 30) {
        let error_data = {
          limit: 30
        }
        return callback(true, 'OTP-MULTIPLE-REQUESTS', 'Please wait for 30 seconds.', error_data)
      }

      if (is_same_day && otp_data.otp_count > 100) {
        let data = {}
        data.limit = 100
        return callback(true, 'OTP-COUNT-EXCEEDS', 'Otp count exceeds', data)
      }
      otp_data.otp_count = is_same_day ? otp_data.otp_count + 1 : 1
      // console.log("otp diff :: ", otp_send_diff);
      if (otp_send_diff_seconds >= 30 || otp_data.otp_verified) {
        otp_data.otp_date = Date.now()
        otp_data.otp = otp_number
        otp_data.otp_verified = false
      } else {
        otp_number = otp_data.otp
      }
    }

    let configuration = {
      mobile: mobile,
      replace: {
        code: otp_number
      }
    }

    NotificationUtils.sms('sms_mobile_verification', configuration, function (code_send_error, code_success) {
      if (code_send_error) {
        console.error('Error while sending verification code.', code_send_error)
        // return Response.error("Internal Server Error.", "INS-ERROR", null);
      }

      if (otp_data) {
        // console.log("otp update data :: ", otp_data);
        otp_data.save((otp_save_error, otp_data) => {
          if (otp_save_error) {
            console.log('Error while updating the otp details.', otp_save_error)
            return callback(true, 'OTP-SAVE-ERROR', 'Internal Server Error.', null)
          }

          callback(null, null, null, otp_number)
        })
      }

      if (!otp_data) {
        // console.log("otp save data :: ", OTPData);
        OTPData.save((otp_save_error, otp_data) => {
          if (otp_save_error) {
            console.log('Error while saving the otp details.', otp_save_error)
            return callback(true, 'OTP-SAVE-ERROR', 'Internal Server Error.', null)
          }

          callback(null, null, null, otp_number)
        })
      }
    })
  })
}

var VerifyOtp = function (mobile, otp, skip, callback) {
  if (skip) {
    return callback()
  }

  if (!otp || !mobile) {
    return callback(true, 'BAD-PARAMS', 'Mobile (OR) OTP not sent.', null)
  }

  let mobile_filter = {
    mobile_number: mobile,
    otp: otp
  }

  MerchantOTPModel.findOne(mobile_filter, (otp_find_error, otp_data) => {
    if (otp_find_error) {
      console.error('Error while finding mobile otp.', otp_find_error)
      return callback(true, 'DB-ERROR', 'Internal Server Error.', null)
    }

    if (!otp_data || !otp_data.otp) {
      console.log('No otp data found.', mobile, otp)
      return callback(true, 'INVALID', 'Invalid OTP.', null)
    }

    let otp_time = GeneralUtils.unixTimeDiff(otp_data.otp_date, Date.now(), 'minutes')

    if (otp_time > 2) {
      console.log('Mobile expires before ' + (otp_time - 2) + 'minutes')
      return callback(true, 'OTP-EXPIRY', 'Otp has been expired.', null)
    }

    otp_data.otp_verified = true
    otp_data.otp = 0

    otp_data.save((otp_date_save_error, otp_updated_data) => {
      if (otp_date_save_error) {
        Print.error(otp_date_save_error, new Error())
        return callback(true, 'DB-ERROR', 'Internal Server Error at otp details update.', null)
      }
      return callback(null, null, null, null)
    })
  })
}
