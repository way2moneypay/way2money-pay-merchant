var UserDetailsModel = require('./../models/UserDetailsModel')
var MerchantModel = require('./../models/MerchantModel')
var UserFeedbacksModel = require('./../models/UserFeedbacksModel')
var UserMobileOTPModel = require('./../models/UserMobileOTPModel')
var MobileOTPModel = require('./../models/MobileOTPModel')
var PaymentService = require('./../services/TransactionService')
const DateFormat = require('dateformat')
const mongoose = require('mongoose')
var JWT = require('jsonwebtoken')
var UserService = module.exports
/* requiring models */
var UserDetailsModel = require('./../models/UserDetailsModel')
const GeneralUtils = require('./../utils/GeneralUtils')
const NotificationUtils = require('./../utils/NotificationUtils')
const ResponseUtils = require('./../utils/ResponseUtils')
const AppConfig = require('config')
// var UserService = module.exports;
const this_file_name = './UserService.js'
// login related services
/* Web related login api */
UserService.login = function (api_request, api_response) {
  let this_file_name = './../services/UserService.js'
  let this_function_name = 'UserService.login'
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  let Response = new ResponseUtils.response(api_response)
  console.log('API QUERY RECEIVED', api_request.body)
  try {
    let mobile = api_request.body.mobile
    let password = api_request.body.password
    let name = api_request.body.fname
    // console.log("API REQUEST TOKEN DATA AT LOGIN FUNCTION", api_request.user);
    console.log('SENDING PASSWORD', password)
    /* FINDING MOBILE NUMBER */
    UserDetailsModel.findOne({
      mobile_number: mobile
    }, function (mobile_error, mobile_data) {
      if (mobile_error) {
        console.log('MOBILE FIND ERROR', mobile_error)
        Logs.error(mobile_error + '', 'ERRORMOBILE FOUND', 'MOBILE DATA ERROR')
      } else {
        if (mobile_data) {
          console.log('MODELLLLL', mobile_data)
          console.log('MOBILE BEFORE CHECKING', password)
          if (!mobile_data.is_temp_password && mobile_data.password) {
            if (mobile_data.validPassword(password)) {
              console.log('SUCCESSFULLY LOGGED IN')
              let user = {}
              user.name = name
              user._id = mobile_data._id
              user.mobile_number = mobile
              // user.email = email;
              user.register_date = mobile_data.register_date
              let token = JWT.sign(user, AppConfig.jwtsecret)
              let response = {}
              response.token = token
              response.user_details = mobile_data
              // console.log("SUCCESSFUILLY REGISTERED DATA");
              return Response.success('SUCCESSFULLY LOGGED IN', response)
            } else {
              return Response.error('PASSWORD FAILED', 'INVAU', 400, null)
            }
          } else {
            if (mobile_data.is_temp_password) {
              if (password === mobile_data.temp_password) {
                let user = {}
                user.name = name
                user._id = mobile_data._id
                user.mobile_number = mobile
                user.register_date = mobile_data.register_date
                let token = JWT.sign(user, AppConfig.jwtsecret)
                let response = {}
                response.token = token
                response.temp_password = mobile_data.is_temp_password
                console.log('SUCCESSFUILLY REGISTERED DATA')
                return Response.success('SUCCESSFULLY LOGGED IN', response)
              } else {
                console.log('PASSWORD WRONG')
                return Response.error('PASSWORD FAILED', 'INVAU', 400, null)
              }
            } else {
              return Response.error('PASSWORD FAILED', 'INVAU', 400, null)
            }
          }
        } else {
          console.log('MOBILE IS NOT EXIST')
          return Response.error('MOBILE NUMBER IS NOT EXIST', 'NOTEXISTMOBILE', 400, null)
        }
      }
    })
  } catch (exception) {
    Logs.exception(exception + '', default_error_code, default_error)
  }
}

UserService.isMobileCheck = function (api_request, api_response) {
  let this_file_name = './../services/UserService.js'
  let this_function_name = 'UserService.isMobileCheck'
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  let Response = new ResponseUtils.response(api_response)
  console.log('API QUERY RECEIVED AT ISMOBILECHECK', api_request.body)
  try {
    let mobile = api_request.body.mobile
    /* FINDING MOBILE NUMBER */
    UserDetailsModel.findOne({
      mobile_number: mobile
    }, function (mobile_error, mobile_data) {
      if (mobile_error) {
        Logs.error(mobile_error + '', 'ERRORMOBILE FOUND', 'MOBILE DATA ERROR')
      } else {
        if (mobile_data) {
          if (mobile_data.password && !mobile_data.is_temp_password) {
            return Response.success('USER DETAILS FOUNDED', null, 'FDPWD')
          } else {
            return Response.success('USER NOT SET PWD', null, 'STPWD')
          }
        } else {
          return Response.error('Your mobile number is not registered', 'NOTEXIST', 404, null)
        }
      }
    })
  } catch (exception) {
    Logs.exception(exception + '', default_error_code, default_error)
  }
}

/* Web related register api */

UserService.register = function (api_request, api_response) {
  console.log('______REACHED TO REGISTER FUNCTION____', AppConfig)
  let this_file_name = './../services/UserService.js'
  let this_function_name = 'UserService.register'
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Response = new ResponseUtils.response(api_response)
  try {
    console.log('API QUERY RECEIVED---REGISTER', api_request.body)
    let mobile = api_request.body.mobile
    let name = api_request.body.fname
    let password = api_request.body.password
    let email = api_request.body.email
    let otp = api_request.body.otp
    let credit_bal
    MobileOTPModel.findOne({
      mobile_number: mobile
    }, function (mobile_otp_error, otp_data) {
      if (mobile_otp_error) {
        console.log('MOBILE OTP DETAILS ERROR', mobile_otp_error)
      } else {
        console.log('OTP DATA', otp_data)
        if (otp_data) {
          if (otp_data.otp === Number(otp)) {
            PaymentService.getPaymentSettings(function (payment_setting_error, payment_setting_error_code, payment_setting_message, payment_setting_data) {
              if (payment_setting_error) {
                return Response.error('ERROR WHILE GETTING PAYMENT SETTING', 'DB-ERROR', 500, null)
              } else {
                if (payment_setting_data) {
                  console.log('MATCHED OTP')
                  let new_user_model = new UserDetailsModel({
                    'name': name,
                    'mobile_number': mobile.toString(),
                    'email': email,
                    'register_date': Date.now(),
                    'register_merchant': mongoose.Types.ObjectId(AppConfig.webMerchantId),
                    'credit_available': payment_setting_data.init_amount,
                    'credit_due': 0,
                    'credit_limit': payment_setting_data.init_amount,
                    'user_status': 1,
                    'con_fee': payment_setting_data.con_fee,
                    'mobile_pin': null,
                    'is_mobile_verified': 1,
                    'is_email_verified': 0
                  })
                  new_user_model.password = new_user_model.generateHash(password)
                  new_user_model.save(function (register_error, register_details) {
                    if (register_error) {
                      console.log('REGISTERED ERROR', register_error)
                      return Response.error('ERROR WHILE REGISTERING USER DETAILS', 'ERRREG', 400)
                    } else {
                      console.log('__________---')
                      if (register_details) {
                        // JWT.sign(user, AppConfig.jwtsecret)
                        let user = {}
                        user.name = name
                        user._id = register_details._id
                        user.mobile_number = mobile
                        user.email = email
                        user.register_date = register_details.register_date
                        let token = JWT.sign(user, AppConfig.jwtsecret)
                        let response = {}
                        response.token = token
                        response.user_details = register_details

                        console.log('SUCCESSFUILLY REGISTERED DATA')
                        return Response.success('SUCCESS', response, null)
                      }
                    }
                  })
                } else {
                  return Response.success('NO PAYMENT SETTINGS FOUNDE', 'NO DATA', 400, null)
                }
              }
            })
          } else {
            console.log('OTP NOT MATCHED')
            return Response.error('INVALID OTP', 'INVLDOTP', 400)
          }
        }
      }
    })
  } catch (e) {
    Print.exception(exception)
    Logs.exception(exception + '', 'REGISTERERR', 'Error occrred while registring your account', new Error())
    return Response.error('Error occrred while registring your account', 'REGISTERERR', 400)
  }
}
/* REGSITRATION OTP SENDING */

/* Web related register api resendOTP */
UserService.registrationOTP = function (api_request, api_response) {
  // console.log("______REACHED TO REGISTER FUNCTION____");
  let this_file_name = './../services/UserService.js'
  let this_function_name = 'UserService.register'
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Response = new ResponseUtils.response(api_response)
  /* password */
  // new CustomerModel().generateHash(api_request.body.password);
  console.log('API QUERY RECEIVED', api_request.body)
  let mobile = (api_request.body.mobile).toString()
  MerchantModel.findOne({
    mobile: mobile
  }, function (mobile_find_error, mobile_data) {
    if (mobile_find_error) {
      console.log('MOBILE FIND ERROR', mobile_find_error)
      return Response.error('ERROR WHILE FINDING MOBILE DETAILS', 'MBLNOTFOUND', 400)
    } else {
      console.log('REGISTRATION OTP DATA', mobile_data)
      if (!mobile_data) {
        let otp = SendOTP(mobile, null, function (otp_error, otp_msg, otp_code, data) {
          if (otp_error) {
            return Response.error(otp_msg, otp_code, otp, null)
          } else {
            console.log('OTP SEND SUCCESSFULLY')
            return Response.success('Successfully registered the user.', data)
          }
        })
      } else {
        return Response.error('mobile number already exist', 'MOBILALREDY', 400)
      }
    }
  })
}
/* FORGOT PASSWORD */
UserService.forgotpassword = function (api_request, api_response) {
  let this_file_name = './../services/UserService.js'
  let this_function_name = 'UserService.forgotpassword'
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Response = new ResponseUtils.response(api_response)
  let mobile = api_request.body.mobile
  console.log('____FORGOT PASSWORD____', mobile)
  try {
    /* FINDING MOBILE NUMBER */
    UserDetailsModel.findOne({
      mobile_number: mobile.toString()
    }, function (mobile_error, mobile_data) {
      if (mobile_error) {
        console.log('MOBILE FIND ERROR', mobile_error)
      } else {
        if (mobile_data) {
          let temp_password = GeneralUtils.randomNumber(100000, 999999)

          // console.log("TEMPORARY PASSWORD", temp_password, mobile);
          let configuration = {
            mobile: mobile.toString(),
            replace: {
              code: temp_password
            }
          }

          NotificationUtils.sms('temp_password', configuration, function (code_send_error, code_success) {
            if (code_send_error) {
              console.error('Error while sending verification code.', code_send_error)
              // return Response.error("Internal Server Error.", "INS-ERROR", null);
            } else {
              console.log('------------SEND SUCCESSFULLY', code_success)

              if (code_success) {
                let find_filter = {
                  'mobile_number': mobile
                }
                let set_filter = {
                  $set: {
                    'is_temp_password': true,
                    'temp_password': temp_password
                  }
                }
                UserDetailsModel.findOneAndUpdate(find_filter, set_filter, {
                  upsert: false
                }, function (update_error, updated_data) {
                  if (update_error) {
                    console.log('UPDATE ERROR WHILE FORGOTPASSWORD', update_error)
                  } else {
                    return Response.success('OTP SEND SUCCESSFULLY', null, 200)
                  }
                })
              }
            }
          })
        } else {
          console.log('MOBILE IS NOT EXIST')
          return Response.error('MOBILE NUMBER IS NOT EXIST', 'NOTEXISTMOBILE', 400, null)
        }
      }
    })
  } catch (e) {
    console.log(e)
  }
}
/* RESET PASSWORD */
UserService.resetPassword = function (api_request, api_response) {
  let this_file_name = './../services/UserService.js'
  let this_function_name = 'UserService.resetPassword'
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Response = new ResponseUtils.response(api_response)
  let mobile = api_request.body.mobile
  let password = api_request.body.password
  console.log('Updating password')
  /* FINDING MOBILE NUMBER */
  UserDetailsModel.findOne({
    mobile_number: mobile
  }, function (mobile_error, mobile_data) {
    if (mobile_error) {
      console.log('MOBILE FIND ERROR', mobile_error)
    } else {
      if (mobile_data) {
        let find_filter = {
          'mobile_number': mobile
        }
        let set_filter = {
          $set: {
            'is_temp_password': false,
            'password': new UserDetailsModel().generateHash(password)
          }
        }
        UserDetailsModel.findOneAndUpdate(find_filter, set_filter, {
          upsert: false
        }, function (update_error, updated_data) {
          if (update_error) {
            console.log('UPDATE ERROR WHILE FORGOTPASSWORD', update_error)
          } else {
            let response = {}
            let user = {}
            user.name = updated_data.name
            user._id = updated_data._id
            user.mobile_number = mobile
            user.register_date = updated_data.register_date
            let token = JWT.sign(user, AppConfig.jwtsecret)
            response.token = token
            response.name = updated_data.name
            response.mobile = updated_data.mobile
            return Response.success('PASSWORD UPDATED SUCCESSFULLY', response, 200)
          }
        })
      } else {
        console.log('MOBILE IS NOT EXIST')
        return Response.error('MOBILE NUMBER IS NOT EXIST', 'NOTEXISTMOBILE', 400, null)
      }
    }
  })
}
/* Mobile related login api */

UserService.mobileLogin = function (api_request, api_response) {
  let Response = new ResponseUtils.response(api_response)

  if (api_request.body.full_name) {
    return UserService.mobileRegistration(api_request, api_response)
  }

  if (!api_request.body.mobile_number || !api_request.body.otp) {
    console.error('Mobile (OR) OTP not sent.')
    return Response.error('Mobile (OR) OTP not sent.', 'BAD-PARAMS', 400)
  }

  let mobile_filter = {}
  mobile_filter.mobile_number = api_request.body.mobile_number

  VerifyOtp(api_request.body.mobile_number, api_request.body.otp, false, (otp_verify_error, verify_otp_code, verify_otp_message) => {
    if (otp_verify_error) {
      return Response.error(verify_otp_message, verify_otp_code, 500)
    }

    UserDetailsModel.findOne(mobile_filter, (user_find_error, user_data) => {
      if (user_find_error) {
        console.log('Error while finding the user details in mobile login.', user_find_error)
        return Response.error('Internal Server Error.', 'DB-ERROR', 500)
      }

      if (!user_data) {
        console.log('No User Details found. User may deleted by some one.', user_data)
        return Response.error('No details found in users.', 'USER-NOT-FOUND', 404)
      }

      let token_data = {}
      token_data.email = user_data.email
      token_data.mobile_number = user_data.mobile_number
      token_data._id = user_data._id

      let token = JWT.sign(token_data, AppConfig.jwtsecret)
      let return_data = {}
      return_data.user_data = user_data
      return_data.user_token = token

      return Response.success('Successfully registered the user.', return_data)
    })
  })
}

UserService.UserStatusWithOTP = function (api_request, api_response) {
  let mobile = api_request.body.mobile_number
  let Response = new ResponseUtils.response(api_response)
  if (!mobile) {
    console.log('No Mobile found in request', mobile)
    return Response.error('No Mobile number sent.', 'BAD-PARAMS', 400)
  }

  UserStatus(mobile, false, (status_error, status_code, status_message, status_data) => {
    if (status_error) {
      return Response.error(status_message, status_code, 500)
    }

    SendOTP(mobile, false, (send_otp_error, send_otp_code, send_otp_message, send_otp_data) => {
      if (send_otp_error) {
        return Response.error(send_otp_message, send_otp_code, 500)
      }

      return Response.success('Successfully sent otp.', status_data)
    })
  })
}

UserService.mobileRegistration = function (api_request, api_response) {
  let UserData = new UserDetailsModel()

  let Response = new ResponseUtils.response(api_response)

  if (!api_request.body.mobile_number || !api_request.body.otp || !api_request.body.full_name || !api_request.body.email) {
    console.error('Mobile (OR) OTP (OR) name (OR) email not sent.')
    return Response.error('Mobile (OR) OTP (OR) name (OR) email not sent.', 'BAD-PARAMS', 400)
  }

  UserData.mobile_number = api_request.body.mobile_number
  UserData.email = api_request.body.email
  if (api_request.body.password) {
    UserData.password = new UserDetailsModel().generateHash(password)
  };
  UserData.full_name = api_request.body.full_name
  UserData.is_mobile_verified = true
  UserData.user_fields_status = 'complete'
  UserData.register_merchant = '0'

  VerifyOtp(api_request.body.mobile_number, api_request.body.otp, false, (otp_error, otp_code, otp_message, otp_data) => {
    if (otp_error) {
      return Response.error(otp_message, otp_code, 500)
    }
    console.log('============================================================')
    console.log('came to user save')

    UserData.save((user_save_error, user_data) => {
      if (user_save_error) {
        console.error('Error while saving user data.', user_save_error)
        return Response.error('Internal Server error', 'USER-SAVE-ERROR', null)
      }
      let token_data = {}
      token_data.email = user_data.email
      token_data.mobile_number = user_data.mobile_number
      token_data._id = user_data._id

      let token = JWT.sign(token_data, AppConfig.jwtsecret)
      let return_data = {}
      return_data.user_data = user_data
      return_data.user_token = token

      return Response.success('Successfully registered the user.', return_data)
    })
  })
}

// profile related functions

UserService.profile = function (api_request, api_response) {
  console.log('______PROFILE INFORMATION____')
  let this_file_name = './../services/UserService.js'
  let this_function_name = 'UserService.register'
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Response = new ResponseUtils.response(api_response)
  let default_error_code = 'PROFILEDISPERR'
  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  let default_error = 'Error while getting profile information'
  try {
    let token_data = api_request.user
    let find_filter = {}
    find_filter._id = token_data._id
    let project_fields = {}
    project_fields.name = 1
    project_fields.email = 1
    project_fields.mobile_number = 1
    project_fields.is_email_verified = 1

    UserDetailsModel.findOne(find_filter, project_fields, (profile_error, profile_data) => {
      if (profile_error) {
        Logs.error(profile_error + '', 'DB-ERR', 'profile userdetails db error', new Error())
        console.log(profile_error)
        return Response.error('Internal Server Error.Please try again', 'DB-ERR', 500)
      }
      if (profile_data) {
        return Response.success('Successfully got profile information', profile_data)
      } else {
        // Logs.error('Profile Information not found', 'NOPROFILEDATAERR', 'Profile Information not found', new Error());
        return Response.error('Profile Information not found', 'NOPROFILEDATAERR', 500)
      }
    })
  } catch (exception) {
    Logs.exception(exception + '', default_error_code, default_error, new Error())
    return response.error(default_error, default_error_code, 400)
  }
}

UserService.changePassword = function (api_request, api_response) {
  console.log('______CHANGE PASSWORD FUNCTION____')
  let this_file_name = './../services/UserService.js'
  let this_function_name = 'UserService.changePassword'
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Response = new ResponseUtils.response(api_response)

  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  let default_error_code = 'CHPWDERR'
  let default_error = 'Error Occurred while changing password.'

  try {
    let token_data = api_request.user
    let password_info = api_request.body

    console.log(password_info)
    if (password_info.current_pwd && password_info.new_pwd && password_info.conform_pwd) {
      if (password_info.new_pwd === password_info.conform_pwd) {
        let find_filter = {}
        find_filter._id = token_data._id
        UserDetailsModel.findOne(find_filter, function (user_error, user_data) {
          if (user_error) {
            console.log(user_error)
            Logs.error(user_error + '', 'DB-ERR', 'changePassword db error.Please try again.', new Error())
            return Response.error('Internal Server Error.Please try again.', 'DB-ERR', 500)
          }
          if (user_data) {
            console.log('adfjasldkfjasldfk', user_data.validPassword(password_info.current_pwd))
            if (user_data.validPassword(password_info.current_pwd)) {
              user_data.password = user_data.generateHash(password_info.new_pwd)
              user_data.save()
                .then((data) => {
                  return Response.success('Successfully Updated Password')
                })
                .catch((err) => {
                  Logs.error(err + '', 'DB-ERR', 'changePassword db update error.Please try again.', new Error())
                  return Response.error('Internal Server Error.Please try again', 'DBERR', 500)
                })
            } else {
              return Response.error('Current password did not matched.Please reset password', 'PWDMISMATCHERR', 500)
            }
          } else {
            return Response.error('User Information Not Found.', 'NOTVALIDUSER', 500)
          }
        })
      } else {
        return Response.error('New Password and Conform Passwords did not match.Please Give Valid Details', 'UPWDNOMATCHERR', 500)
      }
    } else {
      return Response.error('Please give all fields', 'NOUDATAERR', 500)
    }
  } catch (exception) {
    console.log(exception)
    Logs.exception(exception + '', default_error_code, default_error, new Error())
    return Response.error(default_error, default_error_code, 400)
  }
}

UserService.updateProfile = function (api_request, api_response) {
  // console.log("______UPDATE PASSWORD FUNCTION____");
  let this_file_name = './../services/UserService.js'
  let this_function_name = 'UserService.updateProfile'
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Response = new ResponseUtils.response(api_response)

  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  let default_error_code = 'UPDATEPROFILEERR'
  let default_error = 'Error while updating profile'

  try {
    console.log('body :: ', api_request.body)
    let profile_info = api_request.body
    let token_data = api_request.user

    if (!profile_info.email && !profile_info.name) {
      return Response.error('No details found in request to update profile.', 'BAD-PARAMS', 400)
    }

    let email_filter = {}
    email_filter.email = profile_info.email

    let user_filter = {}
    user_filter._id = token_data._id

    UserDetailsModel.findOne(email_filter, (email_find_error, email_data) => {
      if (email_find_error) {
        Logs.error(email_find_error + '', 'DB-ERR', 'Error while getting the user details in profile update.', new Error())

        console.error('Error while getting the user details in profile update.', email_find_error)
        return Response.error('Internal Server Error.', 'DB-ERROR', 500)
      }

      if (email_data && profile_info.email && email_data._id != token_data._id) {
        return Response.error('Email Already Exists.', 'DUPLICATE-EMAIL', 400)
      }

      UserDetailsModel.findOne(user_filter, (user_find_error, user_data) => {
        if (user_find_error) {
          Logs.error(user_find_error + '', 'DB-ERROR', 'Error while getting the user details in profile update.', new Error())

          console.error('Error while getting the user details in profile update.', user_find_error)
          return Response.error('Internal Server Error.', 'DB-ERROR', 500)
        }

        if (!user_data) {
          return Response.error('No user details found.', 'USER-NOT-FOUND', 404)
        }

        let email_change = profile_info.email && profile_info.email != user_data.email
        console.log(email_change)
        user_data.email = profile_info.email
        user_data.is_email_verified = !email_change
        user_data.name = profile_info.name

        user_data.save((profile_update_error, profile_data) => {
          if (profile_update_error) {
            console.error('Error while updating user details :: .', profile_update_error)
            Logs.error(profile_update_error + '', 'DB-ERROR', 'Error while getting the user details in profile update.', new Error())
            return Response.error('Internal Server Error.', 'DB-ERROR', 500)
          }
          let response_data = {}
          response_data.name = profile_data.name
          response_data.email = profile_data.email
          response_data.mobile_number = profile_data.mobile_number
          response_data.is_email_verified = profile_data.is_email_verified

          let email_exist_data = {}
          email_exist_data.name = user_data.name
          email_exist_data.host = AppConfig.api_url
          email_exist_data.email = user_data.email
          // email_exist_data.token = mailTokenData(email_exist_data);
          emailSendFunction('email-verify', email_exist_data, !email_change, (error, code, message, data) => {
            if (error) {
              Logs.error(error + '', 'DB-ERROR', 'EMAIL_SEND:Error while getting the user details in profile update.', new Error())
              return Response.error(message, code, 500)
            }
            if (email_change) {
              return Response.success('Profile Updated successfully.Email verfication link sent to your email. ', response_data)
            } else {
              return Response.success('Successfully updated profile.', response_data)
            }
          })
        })
      })
    })
  } catch (exception) {
    console.log(exception)
    Logs.exception(exception + '', default_error_code, default_error, new Error())
    return Response.error(default_error, default_error_code, 400)
  }
}

UserService.profileMobile = function (api_request, api_response) {
  let this_function_name = 'UserService.profileMobile'
  let Response = new ResponseUtils.response(api_response)

  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)

  let user_filter = {}
  user_filter._id = api_request.user._id

  UserDetailsModel.findOne(user_filter, (user_find_error, user_data) => {
    if (user_find_error) {
      Logs.error(user_find_error + '', 'DB-ERROR', 'Error while finding user details.')
      console.error('Error while finding user details.', user_find_error)
      return Response.error('Internal Server Error.', 'DB-ERROR', 500)
    }

    if (!user_data) {
      console.log('No User Details found. User may deleted by some one.', user_data)
      return Response.error('No details found in users.', 'USER-NOT-FOUND', 404)
    }

    return Response.success('Successfully fetched the user data.', user_data)
  })
}

UserService.SMSSendOTP = function (api_request, api_response) {
  let this_file_name = './../services/UserService.js'
  let this_function_name = 'UserService.SMSSendOTP'
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Response = new ResponseUtils.response(api_response)

  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  let default_error_code = 'PROFILEOTPSENDERR'
  let default_error = 'Error occurred while sending profile OTP'

  try {
    let token_data = api_request.user._id

    let mobile = api_request.body.mobile_number
    if (mobile === token_data.mobile_number) {
      return Response.error('Mobile Did not changed.Please try with other Mobile')
    }
    let mobile_filter = {}
    mobile_filter.mobile_number = mobile
    UserDetailsModel.findOne(mobile_filter, function (mobile_error, mobile_data) {
      if (mobile_error) {
        Print.error(mobile_error)
        Logs.error(mobile_error + '', 'DB-ERR', 'SMS_SEND:PROFILE SMS SEND Error', new Error())
        return Response.error('Internal Server Error', 'DB-ERR', 500)
      }

      if (!mobile_data) {
        SendOTP(mobile, null, function (error, error_msg, error_code, otp_data) {
          if (error) {
            Logs.error(error + '', error_code, error_msg, new Error())
            return Response.error(error_msg, error_code, 500)
          }
          return Response.success('Successfully Sent SMS')
        })
      } else {
        return Response.error('Mobile Already Existed with Other user')
      }
    })

    // return Response.success("otp SENDING");
  } catch (exception) {
    console.log(exception)
    Logs.exception(exception + '', default_error_code, default_error, new Error())
    return Response.error(default_error, default_error_code, 400)
  }
}
UserService.profileVerifySMS = function (api_request, api_response) {
  let this_file_name = './../services/UserService.js'
  let this_function_name = 'UserService.profileVerifySMS'
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Response = new ResponseUtils.response(api_response)

  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  let default_error_code = 'PROFILEOTPVERIFYERR'
  let default_error = 'Error while verifying MOBILESMS'

  try {
    let user_data = api_request.body
    let token_data = api_request.user

    VerifyOtp(user_data.mobile, user_data.otp, null, function (error, error_code, error_msg, data) {
      if (error) {
        Logs.error(error + '', error_code, error_msg, new Error())
        return Response.error(error_msg, error_code, 500)
      }

      UserService.updateUser(token_data._id, user_data.mobile_number, (update_error, user_error_code, user_error_msg, update_data) => {
        if (update_error) {
          Logs.error(update_error + '', user_error_code, user_error_msg, new Error())
          return Response.error(user_error_msg, user_error_code, 500)
        }
        let response_data = {}
        response_data.mobile_number = update_data.mobile_number
        return Response.success('Successfully verified user mobile', response_data)
      })
    })
  } catch (exception) {
    console.log(exception)
    Logs.exception(exception + '', default_error_code, default_error, new Error())
    return Response.error(default_error, default_error_code, 400)
  }
}

UserService.updateUser = function (user_id, mobile, callback) {
  let user_filter = {}
  user_filter._id = user_id

  let update_data = {}
  update_data.mobile_number = mobile
  update_data.last_mobile_update_date = Date.now()

  let projection = {}
  projection.mobile_number = 1
  UserDetailsModel.findOneAndUpdate(user_filter, update_data, {
    projection: projection,
    new: true
  }, function (user_err, user_data) {
    if (user_err) {
      console.log(user_err)
      return callback(user_err, 'DB-ERR', 'Internal Server Error', null)
    }
    if (!user_data) {
      return callback(true, 'NO-USER-PROFILEOTPUPD', 'User details not found.Your details are not valid.', null)
    }
    return callback(null, null, null, user_data)
  })
}

UserService.UserStatus = function (api_request, api_response) {
  let Response = new ResponseUtils.response(api_response)
  let this_function_name = 'UserService.UserStatus'

  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  let default_error_code = 'USERSTATUSERR'
  let default_error = 'Error occurred while getting userStatus'

  try {
    if (!api_request.body.mobile) {
      console.log('No Mobile number found in request.')
      return Response.error('No Mobile number found in request.', 'BAD-PARAMS', 400)
    }

    let mobile_filter = {}
    mobile_filter.mobile_number = api_request.body.mobile

    UserDetailsModel.findOne(mobile_filter, (mobile_find_error, mobile_data) => {
      if (mobile_find_error) {
        Logs.error(mobile_find_error + '', 'DB-ERROR', 'Error while finding user status.', new Error())
        console.error('Error while finding user status.', mobile_find_error)
        return Response.error('Internal Server Error.', 'DB-ERROR', 500)
      }

      let return_data = {}
      return_data.exists = !!mobile_data
      return_data.user_fields_status = user_data.user_fields_status

      if (mobile) {
        return Response.success('User details found.', return_data)
      } else {
        return Response.success('User details not-found.', return_data)
      }
    })
  } catch (exception) {
    Logs.exception(exception + '', default_error_code, default_error, new Error())
    return Response.error(default_error, default_error_code, 400)
  }
}

var UserStatus = function (mobile, skip, callback) {
  if (skip) return callback(null)

  if (!mobile) {
    console.log('No Mobile number found in request.')
    return callback(true, 'BAD-PARAMS', 'No Mobile number found in request.', 400)
  }

  let mobile_filter = {}
  mobile_filter.mobile_number = mobile

  UserDetailsModel.findOne(mobile_filter, (mobile_find_error, user_data) => {
    if (mobile_find_error) {
      console.error('Error while finding user status.', mobile_find_error)
      return callback(mobile_find_error, 'DB-ERROR', 'Internal Server Error.', 500)
    }

    let return_data = {}
    return_data.exists = !!user_data
    return_data.user_fields_status = user_data ? user_data.user_fields_status : null

    if (mobile) {
      return callback(null, null, 'User details found.', return_data)
    } else {
      return callback(null, null, 'User details not-found.', return_data)
    }
  })
}

// userVerification related functions

UserService.sendOTPMobile = function () {

}

UserService.verifyOTPMobile = function () {

}

var SendOTP = function (mobile, skip, callback) {
  if (skip) return callback(null)

  let otp_filter = {}
  otp_filter.mobile_number = mobile

  let otp_number = GeneralUtils.randomNumber(1000, 9999)

  let OTPData = new MobileOTPModel()
  OTPData.mobile_number = mobile
  OTPData.otp = otp_number
  OTPData.otp_date = Date.now()

  MobileOTPModel.findOne(otp_filter, (otp_find_error, otp_data) => {
    if (otp_find_error) {
      console.error('Error while finding the otp data.', otp_find_error)
      return callback(otp_find_error, 'Internal server error.', 'DB-ERROR', null)
    }

    if (otp_data) {
      let otp_send_diff = GeneralUtils.unixTimeDiff(otp_data.otp_date, Date.now(), 'minutes')
      let is_same_day = GeneralUtils.isUnixSameDate(Date.now(), otp_data.otp_date, 1000, 1000)
      if (is_same_day && otp_data.otp_count > 100) {
        let data = {}
        data.limit = 100
        return callback(true, 'OTP-COUNT-EXCEEDS', 'Otp count exceeds', data)
      }
      otp_data.otp_count = is_same_day ? otp_data.otp_count + 1 : 1
      console.log('otp diff :: ', otp_send_diff)
      if (otp_send_diff > 2) {
        otp_data.otp_date = Date.now()
        otp_data.otp = otp_number
      } else {
        otp_number = otp_data.otp
      }
    }

    let configuration = {
      mobile: mobile,
      replace: {
        code: otp_number
      }
    }
    console.log('CONFIGURATION', configuration)
    NotificationUtils.sms('sms_mobile_verification', configuration, function (code_send_error, code_success) {
      if (code_send_error) {
        console.error('Error while sending verification code.', code_send_error)
        // return Response.error("Internal Server Error.", "INS-ERROR", null);
      }

      if (otp_data) {
        otp_data.save((otp_save_error, otp_data) => {
          if (otp_save_error) {
            console.log('Error while updating the otp details.', otp_save_error)
            return callback(true, 'OTP-SAVE-ERROR', 'Internal Server Error.', null)
          }

          callback(null)
        })
      }

      if (!otp_data) {
        OTPData.save((otp_save_error, otp_data) => {
          if (otp_save_error) {
            console.log('Error while saving the otp details.', otp_save_error)
            return callback(true, 'OTP-SAVE-ERROR', 'Internal Server Error.', null)
          }

          callback(null)
        })
      }
    })
  })
}
var VerifyOtp = function (mobile, otp, skip, callback) {
  if (skip) {
    return callback()
  }

  if (!otp || !mobile) {
    return callback(true, 'BAD-PARAMS', 'Mobile (OR) OTP not sent.', null)
  }

  let mobile_filter = {
    mobile_number: mobile,
    otp: otp
  }

  MobileOTPModel.findOne(mobile_filter, (otp_find_error, otp_data) => {
    if (otp_find_error) {
      console.error('Error while finding mobile otp.', otp_find_error)
      return callback(otp_find_error, 'DB-ERROR', 'Internal Server Error.', null)
    }
    SendOTP

    if (!otp_data) {
      console.log('No otp data found.', mobile, otp)
      return callback(true, 'INVALID', 'Invalid OTP.', null)
    }

    let otp_time = GeneralUtils.unixTimeDiff(otp_data.otp_date, Date.now(), 'minutes')

    if (otp_time > 2) {
      console.log('Mobile expires before ' + (otp_time - 2) + 'minutes')
      return callback(true, 'OTP-EXPIRY', 'Otp has been expired.', null)
    }

    return callback(null, null, null, null)
  })
}

/* Ramu */
/* User credit details */
/* gives credit due, credit limit, credit date and due_date */
UserService.creditDetails = function (user_id, callback) {
  let this_function_name = 'UserService.creditDetails'

  /* defining logs */
  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  let Print = new ResponseUtils.print(this_file_name, this_function_name)

  /* defining errors */
  let default_error = 'Error while getting credit details for dashboard.'
  let default_error_code = 'ERRORWHILEGETTINGCREDITDETAILS'

  try {
    /* preparing find filter */
    let find_filter = {}
    find_filter = {
      _id: user_id
    }

    /* preparing projection fileds */
    let projection_fields = {}
    projection_fields = {
      credit_due: 1,
      credit_limit: 1,
      credit_available: 1,
      due_date: 1
    }

    /* getting user details */
    UserService.getUserDetails(find_filter, projection_fields, function (error, error_code, message, data) {
      if (error) {
        return callback(error, error_code, message, data)
      }

      if (data) {
        if (!data.due_date) data.due_date = null
        if (!data.credit_due) data.credit_due = 0
      }

      return callback(error, error_code, message, data)
    })
  } catch (exception) {
    Print.exception(exception + '')
    Logs.exception(exception + '', default_error_code, default_error)

    return callback(exception + '', default_error_code.default_error)
  }
}

/* Ramu */
/* Get one user details based on mobile number */
UserService.getUserDetails = function (find_filter, projection_fields, callback) {
  let this_function_name = 'UserService.creditDetails'

  /* defining errors */
  let default_error = 'Error while getting user details.'
  let default_error_code = 'ERRORWHILEGETTINGUSERDETAILS'

  /* defining logs */
  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  let Print = new ResponseUtils.print(this_file_name, this_function_name)

  try {
    /* getting data from database */
    UserDetailsModel.findOne(find_filter, projection_fields, function (error, data) {
      if (error) {
        Print.error(error + '')
        Logs.error(error + '', default_error_code, default_error)
        return callback(error, 'DBERROR', 'Error while getting user details', null)
      }

      if (!data) {
        return callback(true, 'USER-NOT-FOUND', 'User details not found. Please login.', {
          code: 404
        })
      }

      return callback(error, 'USERDETAILSERROR', 'Error while getting user details', data)
    })
  } catch (exception) {
    Print.exception(exception + '')
    Logs.exception(exception + '', default_error_code, default_error)

    return callback(exception, default_error_code, 'Error while getting user details', data)
  }
}

UserService.emailSend = function (api_request, api_response) {
  let this_file_name = './../services/UserService.js'
  let this_function_name = 'UserService.emailSend'
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Response = new ResponseUtils.response(api_response)

  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  let default_error = 'EMAILSENDERR'
  let default_error_code = 'Error occured while sending Email.'

  try {
    let host = api_request.get('host')
    let template_name = 'email-verify'

    let user_data = api_request.body
    let find_filter = {}

    find_filter.email = user_data.email
    UserDetailsModel.findOne(find_filter, function (email_error, email_data) {
      if (email_error) {
        Log.error(email_error + '', 'DB-ERR', 'EMAIL_SEND:Errror at Email Sending', new Error())
        return Response.error('Internal Server Error', 'DB-ERR', 500)
      }
      console.log('-----------', email_data)

      let email_exist_data = {}
      if (!email_data) {
        email_exist_data.name = user_data.fname
        email_exist_data.host = host
        email_exist_data.email = user_data.email
        // email_exist_data.token = mailTokenData(email_exist_data);
        emailSendFunction(template_name, email_exist_data, null, function (error, error_code, error_msg, data) {
          let email_data_new = new UserDetailsModel()
          email_data_new.email_last_hit_date = Date.now()
          email_data_new.email_verification_count = 1
          email_data.save()
            .then((data) => {
              return Response.success('Successfully sent EMAIL')
            })
            .error((err) => {
              return Response.error('Internal Server Error', 'DB-ERR', 500)
            })
        })
      } else {
        if (!email_data.is_email_verified) {
          if (!GeneralUtils.validateEmail(email_data.email)) {
            return Response.error('Email Validation Failed', 'EMAILVALIDERR', 500)
          }
          let mail_date = DateFormat(email_data.email_last_hit_date, 'yyyy-mm-dd').toString()
          let current_date = DateFormat(new Date(), 'yyyy-mm-dd').toString()

          if (mail_date == current_date && email_data.email_verification_count >= 30) {
            return Response.error('Mail Limit Excceeded.', 'MLSENDCOUNTEXCEEERR', 500)
          }
          email_exist_data.name = email_data.name
          email_exist_data.host = host
          email_exist_data.email = email_data.email
          // email_exist_data.token = mailTokenData(email_exist_data);

          emailSendFunction(template_name, email_exist_data, null, function (error, error_code, error_msg, mail_sent_data) {
            if (error) {
              Logs.error(error + '', error_code, 'Error Occured at emailSend function', new Error())
              console.log(error_msg, error_code)
              return Response.error(error_msg, error_code, 500)
            }

            email_data.email_last_hit_date = Date.now()
            if (mail_date != current_date) {
              email_data.email_verification_count = 0
            } else {
              email_data.email_verification_count += 1
            }
            email_data.save()
              .then((data) => {
                console.log(data)
                return Response.success('Successfully sent EMAIL')
              })
              .error((err) => {
                Log.error(err + '', 'DB-ERR', 'EMAIL_SEND:Errror at Email Sending update', new Error())
                return Response.error('Internal Server Error', 'DB-ERR', 500)
              })
          })
        } else {
          return Response.error('Email Already Verified')
        }
      }
    })
  } catch (exception) {
    console.log(exception)
    Logs.exception(exception + '', default_error_code, default_error, new Error())
    return Response.error(default_error, default_error_code, 400)
  }
}

let mailTokenData = function (email_exist_data) {
  let user = {}
  user.name = email_exist_data.name
  user.host = email_exist_data.host
  if (email_exist_data.new_email) {
    user.email = email_exist_data.current_email
    user.new_email = email_exist_data.new_email
  } else {
    user.email = email_exist_data.email
  }
  user.date = new Date().getTime() + (24 * 60 * 60 * 1000)
  let token = JWT.sign(user, AppConfig.jwtsecret)
  console.log('.........token is ......', token)
  return token
}

let emailSendFunction = function (template_name, email_exist_data, skip, callback) {
  if (skip) return callback(null)
  let link = 'http://' + email_exist_data.host + '/verify?id=' + mailTokenData(email_exist_data)
  let configuration = {}
  configuration.email = email_exist_data.email
  configuration.replace = {
    name: email_exist_data.name,
    link: link
  }

  NotificationUtils.email(template_name, configuration, function (error, response) {
    if (error) {
      console.log(error)

      return callback(error, 'EMAILVERIFYERROR', 'Error while sending Email', null)
    } else {
      console.log('--------------adfasdf-----', response)
      return callback(null)
    }
  })
}

UserService.emailChangeSend = function (api_request, api_response) {
  let this_file_name = './../services/UserService.js'
  let this_function_name = 'UserService.emailChangeSend'
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Response = new ResponseUtils.response(api_response)

  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  let default_error = 'CHEMAILERR'
  let default_error_code = 'Error while sending update Emils'

  try {
    let host = api_request.get('host')
    let template_name = 'email-verify'

    let user_data = api_request.body
    let token_data = api_request.user

    let find_filter = {}
    find_filter.email = user_data.email
    UserDetailsModel.findOne(find_filter, function (email_error, email_data) {
      if (email_error) {
        Logs.error(email_error + '', 'DB-ERR', 'EMAIL_SEND:Error at Email change SEND', new Error())
        return Response.error('Internal Server Error', 'DB-ERR', 500)
      }
      if (!email_data) {
        let email_exist_data = {}
        let user_filter = {}
        user_filter._id = token_data._id

        UserDetailsModel.findOne(user_filter, function (user_err, db_user_data) {
          if (user_err) {
            Logs.error(user_err + '', 'DB-ERR', 'EMAIL_SEND:Error at Email change SEND', new Error())
            console.log(user_err)
            return Response.error('Internal Server Error', 'DB-ERR', 500)
          }
          if (db_user_data) {
            if (!GeneralUtils.validateEmail(user_data.email)) {
              return Response.error('Email Validation Failed', 'EMAILVALIDERR', 500)
            }
            let mail_date = DateFormat(db_user_data.email_last_hit_date, 'yyyy-mm-dd').toString()
            let current_date = DateFormat(new Date(), 'yyyy-mm-dd').toString()

            if (mail_date == current_date && db_user_data.email_verification_count >= 30) {
              return Response.error('Mail Limit Excceeded.', 'MLSENDCOUNTEXCEEERR', 500)
            }
            email_exist_data.name = db_user_data.name
            email_exist_data.host = host
            email_exist_data.email = user_data.email
            email_exist_data.current_email = db_user_data.email
            email_exist_data.new_email = user_data.email
            // email_exist_data.token = mailTokenData(email_exist_data);

            emailSendFunction(template_name, email_exist_data, null, function (error, error_code, error_msg, mail_sent_data) {
              if (error) {
                Logs.error(error + '', error_code, 'Error Occured at changeemailSEND', new Error())
                console.log(error_msg, error_code)

                return Response.error(error_msg, error_code, 500)
              }

              db_user_data.email_last_hit_date = Date.now()
              if (mail_date != current_date) {
                db_user_data.email_verification_count = 0
              } else {
                db_user_data.email_verification_count += 1
              }
              db_user_data.save()
                .then((data) => {
                  console.log(data)
                  return Response.success('Successfully sent EMAIL.Please check your INBOX for updating new Email')
                })
                .error((err) => {
                  Logs.error(err + '', 'DB-ERR', 'Error Occured at changeemailSEND', new Error())

                  return Response.error('Internal Server Error', 'DB-ERR', 500)
                })
            })
          }
        })
      } else {
        return Response.error('Email Already Existed with other user.Please try again.')
      }
    })
  } catch (exception) {
    console.log(exception)
    Logs.exception(exception + '', default_error_code, default_error, new Error())
    return Response.error(default_error, default_error_code, 400)
  }
}

UserService.emailVerify = function (req, res) {
  let this_function_name = 'UserService.emailVerify'
  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  try {
    console.log(req.protocol + '://' + req.get('host'))
    console.log(req.query.id)
    let mail_verify = GeneralUtils.decryptVerifyToken(req.query.id)
    if (mail_verify === 'DECRYPTTOKENERR') {
      return res.redirect(AppConfig.redirect_url + '/verification?message=' + 'Your Authenticaiton details not found . please try again.')
    }
    console.log(mail_verify)

    const date = new Date().getTime()
    console.log(mail_verify.date)
    if (date > mail_verify.date) {
      const param = 'Email Update link Expired.Please Contact Admin'
      return res.redirect(AppConfig.redirect_url + '/verification?message=' + param)
    }
    if ((req.protocol + '://' + req.get('host')) == ('http://' + mail_verify.host)) {
      console.log('Domain is matched. Information is from Authentic email')
      let find_filter = {}
      find_filter.email = mail_verify.email

      let update_field = {}
      update_field.is_email_verified = true
      if (mail_verify.new_email) {
        update_field.email = mail_verify.new_email
      }

      UserDetailsModel.findOneAndUpdate(find_filter, update_field, {
        upsert: true
      }, function (error, user_data) {
        if (error) {
          Logs.exception(error + '', 'DB-ERR', 'EMAIL verify DB ERROR', new Error())
          const param = 'Internal Server Error.Please Try again.'
          return res.redirect(AppConfig.redirect_url + '/verification?message=' + param)
        }
        if (mail_verify.new_email) {
          const param = 'Successfully verified Email'
          return res.redirect(AppConfig.redirect_url + '/verification?message=' + param + '&update_profile=success')
        }
        const param = 'Successfully verified Email'
        return res.redirect(AppConfig.redirect_url + '/verification?message=' + param)
      })
    } else {
      const param = 'Bad Request.Your Details did not match to verify Email'
      return res.redirect(AppConfig.redirect_url + '/verification?message=' + param)
    }
  } catch (exception) {
    Logs.exception(exception + '', 'VEFIFYERR', 'Error Occurred while verifying your email')
    const param = 'Error Occurred while verifying your email.Please try again'
    return res.redirect(AppConfig.redirect_url + '/verification?message=' + param)
  }
}

// User contactus code Start

UserService.contactus = function (api_request, api_response) {
  console.log('______REACHED TO REGISTER FUNCTION____')
  let this_file_name = './../services/UserService.js'
  let this_function_name = 'UserService.register'
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Response = new ResponseUtils.response(api_response)

  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  let default_error = 'CONTACTUSERR'
  let default_error_code = 'Error occured in  contactus service'

  try {
    // console.log(api_request.body);
    let new_user_feedbacks_model = new UserFeedbacksModel({
      'name': api_request.body.name,
      'email': api_request.body.email,
      'contacted_date': Date.now(),
      'subject': api_request.body.subject,
      'message': api_request.body.message
    })
    // console.log(new_user_feedbacks_model);
    new_user_feedbacks_model.save(function (register_error, register_details) {
      if (register_error) {
        Logs.error(register_error + '', 'DB-ERR', 'Error Occured in contactus db model')
        console.log(register_error)
        return Response.error('Error While Post Your Query Please Try Again', register_error)
      } else {
        // console.log("SuccessFully Inserted")
        // console.log(register_details);
        return Response.success('Successfully Posted Your Query Please Wait for Reply', register_details)
      }
    })
  } catch (e) {
    Logs.exception(e + '', default_error_code, default_error, new Error())
    return Response.error(default_error, default_error_code, 500)
    console.log('EXCEPTION FOUND', e)
  }
}

UserService.sendAppLink = function (api_request, api_response) {
  let this_file_name = './../services/UserService.js'
  let this_function_name = 'UserService.sendAppLink'
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Response = new ResponseUtils.response(api_response)
  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  let default_error_code = 'PROFILEOTPSENDERR'
  let default_error = 'Error occurred while sending app link'

  try {
    console.log(api_request.query)
    // let token_data = api_request.user._id;

    // let mobile = api_request.body.mobile_number;
    // if (mobile === token_data.mobile_number) {
    //     return Response.error('Mobile Did not changed.Please try with other Mobile');
    // }
    // let mobile_filter = {};
    // mobile_filter.mobile_number = mobile;
    // UserDetailsModel.findOne(mobile_filter, function (mobile_error, mobile_data) {
    //     if (mobile_error) {

    //         Print.error(mobile_error);
    //         Logs.error(mobile_error + '', "DB-ERR", 'SMS_SEND:PROFILE SMS SEND Error', new Error());
    //         return Response.error("Internal Server Error", "DB-ERR", 500);
    //     }

    //     if (!mobile_data) {
    //         SendOTP(mobile, null, function (error, error_msg, error_code, otp_data) {
    //             if (error) {
    //                 Logs.error(error + '', error_code, error_msg, new Error());
    //                 return Response.error(error_msg, error_code, 500);
    //             }
    //             return Response.success('Successfully Sent SMS');
    //         });
    //     } else {
    //         return Response.error('Mobile Already Existed with Other user');
    //     }
    // });

    // return Response.success("otp SENDING");
  } catch (exception) {
    console.log(exception)
    Logs.exception(exception + '', default_error_code, default_error, new Error())
    return Response.error(default_error, default_error_code, 400)
  }
}
