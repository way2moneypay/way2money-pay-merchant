var QrCodeGenerator = module.exports

var qr = require('qr-image')
const AESNODE = require('../utils/EncDecNode')
var fs = require('fs')
const AppConfig = require('config')

/* requiring utils */
var ResponseUtils = require('./../utils/ResponseUtils')
let this_file_name = 'QrCodeGenerator'

/** Requiring Models */
const MerchantsModel = require('../models/MerchantModel')

const MerchantProfileService = require('../services/MerchantProfileService')

QrCodeGenerator.generateQrCode = function (api_request, api_response) {
  let this_function_name = 'QrCodeGenerator.generateQrCode'
  let Response = new ResponseUtils.response(api_response)
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)

  try {
    default_error = 'Error while generating qr code'
    default_error_code = 'QRCODEERROR'
    let Qrdata = {}
    MerchantsModel.findOne({
      _id: api_request.user._id
    }, (errFind, MerchatObj) => {
      if (errFind) {
        console.log('Err while finding merchant ', errFind)
        Response.error('Err while finding merchant', 500)
      } else {
        if (MerchatObj.qrcode_path) {
          return Response.success(MerchatObj.qrcode_path)
        } else {
          Qrdata['merchant_name'] = MerchatObj.name
          Qrdata['display_name'] = MerchatObj.brand_name
          Qrdata['id'] = MerchatObj._id
          Qrdata['mobile'] = MerchatObj.mobile

          var encQrdata = AESNODE.encrypt(JSON.stringify(Qrdata), AppConfig.jwtsecret)
          // var decQrdata= AESNODE.decrypt(encQrdata,AppConfig.node_enc_key);
          var qr_svg = qr.image(encQrdata, {
            type: 'png'
          })
          var qr_svg = qr.image(JSON.stringify(Qrdata), {
            type: 'png'
          })
          var finish = function () {
            MerchantsModel.findOneAndUpdate({
              _id: Qrdata.id
            }, {
              $set: {
                qrcode_path: 'https://business.way2money.com/qr_codes/' + Qrdata.id + '.png'
              }
            }, function (err, res) {
              if (err) return Response.error('QRCode path save failed', 500)
              else {
                return Response.success('https://business.way2money.com/qr_codes/' + Qrdata.id + '.png')
              }
            })
          }
          dest = fs.createWriteStream(__dirname + '/../../../uploads/qr_codes/' + Qrdata.id + '.png')
          qr_svg.pipe(dest)
          dest.addListener('finish', finish)
        }
      }
    })
  } catch (exception) {
    Print.exception(exception + '')
    Logs.exception(exception + '', default_error_code, default_error)
    return Response.error(default_error, default_error_code, 500, null)
  }
}
QrCodeGenerator.download = function (api_request, api_response) {
  let this_function_name = 'QrCodeGenerator.generateQrCode'
  let Response = new ResponseUtils.response(api_response)
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)

  try {
    default_error = 'Error while generating qr code'
    default_error_code = 'QRCODEERROR'
    let Qrdata = {}

    MerchantsModel.findOne({
      _id: api_request.user._id
    }, (errFind, MerchatObj) => {
      if (errFind) {
        console.log('Err while finding merchant ', errFind)
        Response.error('Err while finding merchant', 500)
      } else {
        if (MerchatObj.qrcode_path) {
          api_response.download(__dirname + '/../../uploads/qr_codes/' + api_request.user._id + '.png')
        } else {
          Qrdata['merchant_name'] = MerchatObj.name
          Qrdata['display_name'] = MerchatObj.brand_name
          Qrdata['id'] = MerchatObj._id
          Qrdata['mobile'] = MerchatObj.mobile
          var encQrdata = AESNODE.encrypt(JSON.stringify(Qrdata), AppConfig.jwtsecret)
          // var decQrdata= AESNODE.decrypt(encQrdata,AppConfig.node_enc_key);
          var qr_svg = qr.image(encQrdata, {
            type: 'png'
          })
          var finish = function () {
            MerchantsModel.findOneAndUpdate({
              _id: Qrdata.id
            }, {
              $set: {
                qrcode_path: 'https://business.way2money.com/qr_codes/' + Qrdata.id + '.png'
              }
            }, function (err, res) {
              if (err) return Response.error('QRCode path save failed', 500)
              else {
                path = __dirname + '/../../uploads/qr_codes/' + Qrdata.id + '.png'
                api_response.download(path)
              }
            })
          }
          dest = fs.createWriteStream(__dirname + '/../../uploads/qr_codes/' + Qrdata.id + '.png')
          qr_svg.pipe(dest)
          dest.addListener('finish', finish)
        }
      }
    })
  } catch (exception) {
    Print.exception(exception + '')
    Logs.exception(exception + '', default_error_code, default_error)
    return Response.error(default_error, default_error_code, 500, null)
  }
}
