var MerchantClaimService = module.exports
var async = require('async')

/* Requiring models */
var MerchantClaimModel = require('../models/MerchantClaimsModel')
var MerchantsModel = require('../models/MerchantModel')

/* requiring utils */
var ResponseUtils = require('./../utils/ResponseUtils')
let this_file_name = 'RefundService'

MerchantClaimService.claimNow = function (api_request, api_response) {
  default_error = 'Error while merchants claim'
  default_error_code = 'CLAIM-ERROR'
  let this_function_name = 'MerchantClaimService.claimNow'
  let Response = new ResponseUtils.response(api_response)
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  try {
    MerchantClaimModel.findOne({
      merchant_id: api_request.user._id,
      claim_status: 0
    }, function (MerchantFindErr, MerchantFindRes) {
      if (MerchantFindErr) return Response.error('MongoError : Merchant find error', MerchantFindErr, 500)
      else if (MerchantFindRes) {
        return Response.success("Dear, Merchant you're last claimed amount is pending please be patient and try again", '', 500)
      } else {
        Claim = new MerchantClaimModel({
          merchant_id: api_request.user._id,
          amount: api_request.body.amount,
          request_date: Date.now(),
          claim_status: 0
        })
        Claim.save(function (ClaimErr, ClaimSaved) {
          if (ClaimErr) return Response.error('MongoError : MerchantsClaim save error', ClaimErr, 500)
          else return Response.success('Successfully claimed')
        })
      }
    })
  } catch (exception) {
    Print.exception(exception + '')
    Logs.exception(exception + '', default_error_code, default_error)
    return Response.error(default_error, default_error_code, 500, null)
  }
}
