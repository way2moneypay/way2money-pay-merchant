const UserTransactionsModel = require('../models/UserTransactionsModel')
const merchantStatsModel = require('../models/MerchantStatsModel')
const async = require('async')
var UserTransactions = module.exports
UserTransactions.getTransactions = function (apiReq, apiRes) {
  UserTransactionsModel.find({ $or: [{ status: 1 }, { status: 5 }] }, function (error, transactions) {
    if (error) {
      console.log(error)
    } else {
      async.forEach(transactions, (transaction, callbackForEach) => {
        if (transaction.transaction_date != undefined && transaction.merchant_id != undefined && transaction.amount != undefined) {
          var transaction_date = new Date(transaction.transaction_date)
          transaction_date = transaction_date.setHours(0, 0, 0, 0, 0)
          transaction_date = (new Date(transaction_date).getTime() - 19800000)
          //  transaction_date = transaction_date/1;
          if (transaction.commission_rate != undefined) {
            var f_amount = transaction.amount - (transaction.amount * transaction.commission_rate) / 100
          } else {
            var f_amount = transaction.amount
          }

          merchantStatsModel.findOneAndUpdate({
            merchant_id: transaction.merchant_id,
            trans_date: transaction_date
          }, {
            $inc: {
              transactions: 1,
              trans_amount: transaction.amount,
              amount: f_amount
            }
          }, {
            upsert: true
          },
          function (updateError, updateResult) {
            // console.log(updateResult)
            if (updateError) {
              console.log('Error while updating merchant trans stats', updateError, transaction)
              apiRes.send({
                'status': 'fail',
                'message': 'error while updating',
                'error': updateError
              })
            } else {
              // console.log("sucess")
              callbackForEach()
            }
          })
        } else {
          callbackForEach()
        }
      }, (success) => {
        console.log('Updated successfully')
        apiRes.send({
          'status': 'success',
          'message': 'successfully updated'
        })
      })
    }
  })
}
