var DashboardService = module.exports
const async = require('async')

/* requiring required models */
const Transactions = require('../models/UserTransactionsModel')
const MerchantStatsModel = require('../models/MerchantStatsModel')
const MerchantModel = require('../models/MerchantModel')
/* requiring utils */
var ResponseUtils = require('./../utils/ResponseUtils')

let this_file_name = 'DashboardService.getUserDashboardDetails'

/* Ramu */
/* get user dashboard data */
DashboardService.getUserDashboardDetails = function (api_request, api_response) {
  let Response = new ResponseUtils.response(api_response)

  let id = api_request.user._id
  let default_days = 30
  if (api_request.params.hasOwnProperty('days')) {
    async.parallel([
      function (callback) {
        var fromDay = new Date()
        fromDay.setHours(0, 0, 0, 0)
        fromDay.setDate(fromDay.getDate() - api_request.params.days)
        MerchantStatsModel.find({
          merchant_id: id,
          trans_date: {
            $gte: fromDay.getTime()
          }
        },
        function (mongo_err, mongo_res) {
          if (mongo_err) {
            callback({
              error: 'Error in merchants report graph',
              error_message: mongo_res
            }, null)
          } else {
            var dates = []
            var counts = []
            var inc = 0
            async.eachSeries(mongo_res, function (i, next) {
              if (dates.length == 0) {
                day = new Date(i.trans_date)
                day.setHours(0, 0, 0, 0)
                dates[inc] = day.getTime()
                counts[inc] = 1
                next()
              } else {
                async.eachOfSeries(dates, function (val, j, next_val) {
                  day = new Date(i.trans_date)
                  day.setHours(0, 0, 0, 0)
                  if (val == day.getTime()) {
                    counts[j] += 1
                    next()
                  } else {
                    next_val()
                  }
                }, function () {
                  inc++
                  day.setHours(0, 0, 0, 0)
                  dates[inc] = day.getTime()
                  counts[inc] = 1
                  next()
                })
              }
            }, function () {
              callback(null, {
                dates: dates,
                counts: counts
              })
            })
          }
        }).sort({
          trans_date: 1
        })
      },
      function (callback) {
        var fromDay = new Date()
        fromDay.setHours(0, 0, 0, 0)
        fromDay.setDate(fromDay.getDate() - api_request.params.days)
        Transactions.find({
          merchant_id: id,
          transaction_date: {
            $gte: fromDay.getTime()
          }
        },
        function (mongo_err, mongo_res) {
          if (mongo_err) {
            callback({
              error: 'Error in users report graph',
              error_message: mongo_res
            }, null)
          } else {
            var dates = []
            var counts = []
            var inc = 0
            async.eachSeries(mongo_res, function (i, next) {
              if (dates.length == 0) {
                day = new Date(i.transaction_date)
                day.setHours(0, 0, 0, 0)
                dates[inc] = day / 1
                counts[inc] = 1
                next()
              } else {
                async.eachOfSeries(dates, function (val, j, next_val) {
                  day = new Date(i.transaction_date)
                  day.setHours(0, 0, 0, 0)
                  if (val == day / 1) {
                    counts[j] += 1
                    next()
                  } else {
                    next_val()
                  }
                }, function () {
                  inc++
                  day.setHours(0, 0, 0, 0)
                  dates[inc] = day / 1
                  counts[inc] = 1
                  next()
                })
              }
            }, function () {
              callback(null, {
                dates: dates,
                counts: counts
              })
            })
          }
        }).sort({
          transaction_date: 1
        })
      }
    ],
    function (err, results) {
      if (err) {
        return Response.error(err)
      } else if (results) {
        Array.prototype.unique = function () {
          var a = this.concat()
          for (var i = 0; i < a.length; ++i) {
            for (var j = i + 1; j < a.length; ++j) {
              if (a[i] === a[j]) { a.splice(j--, 1) }
            }
          }
          return a
        }
        const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Novr', 'Dec']
        var total_days = results[0].dates.concat(results[1].dates).unique()
        total_days = total_days.sort()
        var user_dates = []

        var transactions_dates = []
        var users_count = []

        var transactions_count = []
        var labels = []

        for (let i = 0; i < total_days.length; i++) {
          var day = new Date(total_days[i])
          labels.push(months[day.getMonth()] + ' ' + day.getDate())
          if ((results[0].dates.indexOf(total_days[i]) != -1)) {
            users_count.push(results[0].counts[results[0].dates.indexOf(total_days[i])])
          } else {
            users_count.push(0)
          }
          if ((results[1].dates.indexOf(total_days[i]) != -1)) {
            transactions_count.push(results[1].counts[results[1].dates.indexOf(total_days[i])])
          } else {
            transactions_count.push(0)
          }
        }

        return Response.success('Dashboard graph report data by days', {
          graph_report: {
            labels: labels,
            users_count: users_count,
            transactions_count: transactions_count
          }
        })
      }
    })
  } else {
    async.parallel([
      function (callback) {
        Transactions.distinct('user_id', {
          merchant_id: id
        }, function (mongo_err, mongo_res) {
          if (mongo_err) {
            callback({
              error: 'Error in total users count',
              error_message: mongo_res
            }, null)
          } else if (mongo_res) {
            callback(null, mongo_res.length)
          } else {
            callback(null, 0)
          }
        })
      },
      function (callback) {
        var today = new Date()
        today.setHours(0, 0, 0, 0)
        var yesterday = new Date()
        yesterday.setDate(yesterday.getDate() - 1)
        yesterday.setHours(0, 0, 0, 0)
        Transactions.distinct('user_id', {
          merchant_id: id,
          register_date: {
            $gte: yesterday.getTime(),
            $lt: today.getTime()
          }
        }, function (mongo_err, mongo_res) {
          if (mongo_err) {
            callback({
              error: 'Error in yesterday users count',
              error_message: mongo_res
            }, null)
          } else if (mongo_res) {
            callback(null, mongo_res.length)
          } else {
            callback(null, 0)
          }
        })
      },
      function (callback) {
        var today = new Date()
        today.setHours(0, 0, 0, 0)
        Transactions.distinct('user_id', {
          merchant_id: id,
          register_date: {
            $gte: today.getTime()
          }
        }, function (mongo_err, mongo_res) {
          if (mongo_err) {
            callback({
              error: 'Error in today users count',
              error_message: mongo_res
            }, null)
          } else if (mongo_res) {
            callback(null, mongo_res.length)
          } else {
            callback(null, 0)
          }
        })
      },
      /* TransactionStats - MerchantStatsModel */
      function (callback) {
        MerchantStatsModel.countDocuments({
          merchant_id: id
        },
        function (mongo_err, mongo_res) {
          if (mongo_err) {
            callback({
              error: 'Error in total merchant transactions count',
              error_message: mongo_res
            }, null)
          } else if (mongo_res) {
            callback(null, mongo_res)
          } else {
            callback(null, 0)
          }
        })
      },
      function (callback) {
        var today = new Date()
        today.setHours(0, 0, 0, 0)
        MerchantStatsModel.countDocuments({
          merchant_id: id,
          trans_date: {
            $gte: today.getTime()
          }
        },
        function (mongo_err, mongo_res) {
          if (mongo_err) {
            callback({
              error: 'Error in today merchant transactions count',
              error_message: mongo_res
            }, null)
          } else if (mongo_res) {
            callback(null, mongo_res)
          } else {
            callback(null, 0)
          }
        })
      },
      function (callback) {
        var today = new Date()
        today.setHours(0, 0, 0, 0)
        var yesterday = new Date()
        yesterday.setDate(yesterday.getDate() - 1)
        yesterday.setHours(0, 0, 0, 0)
        MerchantStatsModel.countDocuments({
          merchant_id: id,
          trans_date: {
            $gte: yesterday.getTime(),
            $lt: today.getTime()
          }
        },
        function (mongo_err, mongo_res) {
          if (mongo_err) {
            callback({
              error: 'Error in yesterday merchant transactions count',
              error_message: mongo_res
            }, null)
          } else if (mongo_res) {
            callback(null, mongo_res)
          } else {
            callback(null, 0)
          }
        })
      },
      /* TransactionsAmount - MerchantStatsModel */
      function (callback) {
        MerchantStatsModel.find({
          merchant_id: id
        },
        function (mongo_err, mongo_res) {
          if (mongo_err) console.log('Error in total_transactions_count')
          else if (mongo_res.length > 0) {
            var amount = 0
            async.eachSeries(mongo_res, function (i, next) {
              amount += i.trans_amount
              next()
            }, function () {
              callback(null, amount)
            })
          } else {
            callback(null, 0)
          }
        })
      },
      function (callback) {
        var today = new Date()
        today.setHours(0, 0, 0, 0)
        MerchantStatsModel.find({
          merchant_id: id,
          trans_date: {
            $gte: today.getTime()
          }
        },
        function (mongo_err, mongo_res) {
          if (mongo_err) console.log('Error in today_transactions_count')
          else if (mongo_res.length > 0) {
            var amount = 0
            async.eachSeries(mongo_res, function (i, next) {
              amount += i.trans_amount
              next()
            }, function () {
              callback(null, amount)
            })
          } else {
            callback(null, 0)
          }
        })
      },
      function (callback) {
        var today = new Date()
        today.setHours(0, 0, 0, 0)
        var yesterday = new Date()
        yesterday.setDate(yesterday.getDate() - 1)
        yesterday.setHours(0, 0, 0, 0)
        MerchantStatsModel.find({
          merchant_id: id,
          trans_date: {
            $gte: yesterday.getTime(),
            $lt: today.getTime()
          }
        },
        function (mongo_err, mongo_res) {
          if (mongo_err) console.log('Error in yesterday_transactions_count')
          else if (mongo_res.length > 0) {
            var amount = 0
            async.eachSeries(mongo_res, function (i, next) {
              amount += i.trans_amount
              next()
            }, function () {
              callback(null, amount)
            })
          } else {
            callback(null, 0)
          }
        })
      },
      /* Monthly report graph */
      function (callback) {
        var fromDay = new Date()
        fromDay.setHours(0, 0, 0, 0)
        fromDay.setDate(fromDay.getDate() - default_days)
        MerchantStatsModel.find({
          merchant_id: id,
          trans_date: {
            $gte: fromDay.getTime()
          }
        },
        function (mongo_err, mongo_res) {
          if (mongo_err) {
            callback({
              error: 'Error in merchant report graph',
              error_message: mongo_res
            }, null)
          } else {
            var dates = []
            var counts = []
            var inc = 0
            async.eachSeries(mongo_res, function (i, next) {
              if (dates.length == 0) {
                day = new Date(i.trans_date)
                day.setHours(0, 0, 0, 0)
                dates[inc] = day.getTime()
                counts[inc] = 1
                next()
              } else {
                async.eachOfSeries(dates, function (val, j, next_val) {
                  day = new Date(i.trans_date)
                  day.setHours(0, 0, 0, 0)
                  if (val == day.getTime()) {
                    counts[j] += 1
                    next()
                  } else {
                    next_val()
                  }
                }, function () {
                  inc++
                  day.setHours(0, 0, 0, 0)
                  dates[inc] = day.getTime()
                  counts[inc] = 1
                  next()
                })
              }
            }, function () {
              callback(null, {
                dates: dates,
                counts: counts
              })
            })
          }
        }).sort({
          trans_date: 1
        })
      },
      function (callback) {
        var fromDay = new Date()
        fromDay.setHours(0, 0, 0, 0)
        fromDay.setDate(fromDay.getDate() - default_days)
        Transactions.find({
          merchant_id: id,
          transaction_date: {
            $gte: fromDay.getTime()
          }
        },
        function (mongo_err, mongo_res) {
          if (mongo_err) console.log('Error in users_month_count')
          else {
            var dates = []
            var counts = []
            var inc = 0
            async.eachSeries(mongo_res, function (i, next) {
              if (dates.length == 0) {
                day = new Date(i.transaction_date)
                day.setHours(0, 0, 0, 0)
                dates[inc] = day / 1
                counts[inc] = 1
                next()
              } else {
                async.eachOfSeries(dates, function (val, j, next_val) {
                  day = new Date(i.transaction_date)
                  day.setHours(0, 0, 0, 0)
                  if (val == day / 1) {
                    counts[j] += 1
                    next()
                  } else {
                    next_val()
                  }
                }, function () {
                  inc++
                  day.setHours(0, 0, 0, 0)
                  dates[inc] = day / 1
                  counts[inc] = 1
                  next()
                })
              }
            }, function () {
              callback(null, {
                dates: dates,
                counts: counts
              })
            })
          }
        }).sort({
          transaction_date: 1
        })
      },
      /* Merchants amount */
      function (callback) {
        MerchantModel.findOne({
          _id: id
        }, function (MerchantFindErr, MerchantFindRes) {
          if (MerchantFindErr) {
            callback({
              error_message: 'MongoError: MerchantsAmount find',
              error: MerchantFindErr
            })
          } else if (MerchantFindRes) {
            callback(null, {
              pending_amt: MerchantFindRes.pending_amt ? MerchantFindRes.pending_amt : 0,
              approved_amt: MerchantFindRes.approved_amt ? MerchantFindRes.approved_amt : 0,
              claimed_amt: MerchantFindRes.claimed_amt ? MerchantFindRes.claimed_amt : 0
            })
          } else {
            callback(null, {
              pending_amt: 0,
              approved_amt: 0,
              claimed_amt: 0
            })
          }
        })
      }
    ],
    function (err, results) {
      if (err) {
        return Response.error(err)
      } else if (results) {
        Array.prototype.unique = function () {
          var a = this.concat()
          for (var i = 0; i < a.length; ++i) {
            for (var j = i + 1; j < a.length; ++j) {
              if (a[i] === a[j]) { a.splice(j--, 1) }
            }
          }
          return a
        }
        const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Novr', 'Dec']
        var total_days = results[9].dates.concat(results[10].dates).unique()
        total_days = total_days.sort()
        var user_dates = []

        var transactions_dates = []
        var users_count = []

        var transactions_count = []
        var labels = []

        for (let i = 0; i < total_days.length; i++) {
          var day = new Date(total_days[i])
          labels.push(months[day.getMonth()] + ' ' + day.getDate())
          if ((results[9].dates.indexOf(total_days[i]) != -1)) {
            users_count.push(results[9].counts[results[9].dates.indexOf(total_days[i])])
          } else {
            users_count.push(0)
          }
          if ((results[10].dates.indexOf(total_days[i]) != -1)) {
            transactions_count.push(results[10].counts[results[10].dates.indexOf(total_days[i])])
          } else {
            transactions_count.push(0)
          }
        }

        return Response.success('Dashboard data', {
          total_users: results[0],
          today_users: results[1],
          yesterday_users: results[2],
          total_transaction: results[3],
          today_transaction: results[4],
          yesterday_transaction: results[5],
          total_transaction_amount: results[6],
          today_transaction_amount: results[7],
          yesterday_transaction_amount: results[8],
          graph_report: {
            labels: labels,
            users_count: users_count,
            transactions_count: transactions_count
          },
          pending_amt: results[11].pending_amt,
          approved_amt: results[11].approved_amt,
          claimed_amt: results[11].claimed_amt
        })
      }
    })
  }
}
