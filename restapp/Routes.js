'use strict'
var MerchantProfileService = require('./services/MerchantProfileService')
var LoginService = require('./services/LoginService')
var PaymentSerivice = require('./services/PaymentService')
var AuthenticateUtils = require('./utils/AuthenticateUtils')
var DashboardService = require('./services/DashboardService')
var TransactionService = require('./services/TransactionService')
var RefundService = require('./services/RefundService')
var MerchantClaimService = require('./services/MerchantClaimService')
var QRCodeGenerator = require('./services/QrCodeGenerator')
var updateMerchantTransStats = require('./services/updateMerchantTransStats')
// var UserService = require("./services/UserService");
// var MerchantService = require("./services/MerchantService");
// const GeneralUtils = require("./utils/GeneralUtils");
// const SharedService = require("./services/SharedService");

module.exports = function (app) {
  app.get('/api/dashboard-data', AuthenticateUtils.userAuthenticationWithQuery, DashboardService.getUserDashboardDetails)
  app.get('/api/dashboard-chartdata', AuthenticateUtils.userAuthenticationWithQuery, DashboardService.getUserDashboardChartData)
  app.get('/api/dashboard-chartdata/:days', AuthenticateUtils.userAuthenticationWithQuery, DashboardService.getUserDashboardChartData)
  // app.post("/api/merchant/updateTransactionDetails", AuthenticateUtils.userAuthenticationWithQuery, merchantServices.updateTransactionDetails);
  app.post('/api/merchant/payment', AuthenticateUtils.userAuthenticationWithQuery, PaymentSerivice.checkPayment)
  app.post('/api/login', LoginService.login)
  app.post('/api/registrationOTP', LoginService.registrationOTP)
  app.post('/api/merchant/register/:step', AuthenticateUtils.userAuthenticationWithQuery, MerchantProfileService.register)
  app.post('/api/uploadDoc', AuthenticateUtils.userAuthenticationWithQuery, MerchantProfileService.uploadDoc)
  app.get('/api/transactions', AuthenticateUtils.userAuthenticationWithQuery, TransactionService.getTransactions)
  // app.get("/api/transactions/:cpage", AuthenticateUtils.userAuthenticationWithQuery, PaymentService.getTransactions);
  app.get('/api/transactionsCount', AuthenticateUtils.userAuthenticationWithQuery, TransactionService.getTransactionsCount)
  // app.get("/api/merchant/edit", AuthenticateUtils.userAuthenticationWithQuery, MerchantProfileService.getMerchantDetails);
  app.post('/api/merchant/update', AuthenticateUtils.userAuthenticationWithQuery, MerchantProfileService.updateMerchant)
  app.get('/api/merchant/getDetails', AuthenticateUtils.userAuthenticationWithQuery, MerchantProfileService.getDetails)
  app.post('/api/merchant-check-refund', AuthenticateUtils.userAuthenticationWithQuery, RefundService.checkRefund)
  app.post('/api/merchant-refund', AuthenticateUtils.userAuthenticationWithQuery, RefundService.refund)
  app.post('/api/merchant-claim', AuthenticateUtils.userAuthenticationWithQuery, MerchantClaimService.claimNow)
  app.get('/api/merchant-generate-qrcode', AuthenticateUtils.userAuthenticationWithQuery, QRCodeGenerator.generateQrCode)
  app.get('/api/merchant-download-qrcode', AuthenticateUtils.userAuthenticationWithQuery, QRCodeGenerator.download)
  app.get('/api/update-merchant-trans-stats', updateMerchantTransStats.getTransactions)
}
