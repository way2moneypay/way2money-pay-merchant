'use strict'

var path = require('path')

module.exports = function (shipit) {
  require('shipit-local')(shipit)

  shipit.initConfig({
    default: {
      repositoryUrl: 'https://anudeep1829@bitbucket.org/way2_node/routemails.git',
      keepReleases: 2,
      deleteOnRollback: false,
      deployTo: '/home/sys1190/shipitdeploy/routemails',
      updateSubmodules: false
    }
  })

  shipit.currentPath = path.join(shipit.config.deployTo, 'current')

  shipit.on('deploy:fetched', function () {
    // After git fetch in local? Anything ?
    shipit.log('fetched called')
  })

  // npm install
  shipit.blTask('npm_install', function () {
    shipit.log('npm install')
    return shipit.local(
      'cd ' + shipit.localReleasePath + ' && ' +
      'npm install'
    )
  })

  shipit.blTask('ng_build', function () {
    shipit.log('ng build called')
    return shipit.local(
      'cd ' + shipit.localReleasePath + ' && ' +
      'npm run build'
    )
  })

  shipit.on('deploy:build', function () {
    shipit.log('updated called')
    shipit.start(
      'npm_install',
      'ng_build'
    )
  })

  // Restart Nginx, pm2 may be ?
  shipit.on('deploy:published', function () {
    shipit.start('restart_pm2')
  })

  shipit.blTask('restart_pm2', function () {
    try {
      return shipit.local('pm2 gracefulReload routemails')
    } catch (err) {
      shipit.log(err)
    }
  })
}
