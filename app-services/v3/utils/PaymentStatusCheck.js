'use strict'
const paymentService = module.exports
const settingsModel = require('../models/PaymentSettingsModel')
const fileName = 'PaymentStatusCheck'
var ResponseUtils = require('./../utils/ResponseUtils')

paymentService.checkLivePaymentStatus = function (apiReq, apiRes, next_service) {
  var this_function_name = 'checkLivePaymentStatus'
  let Logs = new ResponseUtils.Logs(fileName, this_function_name)
  let Print = new ResponseUtils.print(fileName, this_function_name)
  let Response = new ResponseUtils.response(apiRes)
  let default_error = 'error while checking live_payment_status'
  settingsModel.findOne({}, {
    live_payment_status: true,
    staging_payment_status: true
  }, function (error, result) {
    if (error) {
      Logs.error(result.error, 'DB-ERROR', default_error)
      return Response.error('Internal error in live paymentStatus', 'DB-ERROR', 400)
    } else {
      //    console.log("environment:: ",process.env.NODE_ENV)
      if (process.env.NODE_ENV === 'production') {
        if (result.live_payment_status == 'active') {
          next_service()
        } else if (result.live_payment_status == 'inactive') {
          return Response.error('Currently we are not accepting any payments please try again later', 'PAYMENT-ERROR', 500)
        } else {
          next_service()
        }
      } else if (process.env.NODE_ENV === 'staging') {
        if (result.staging_payment_status == 'active') {
          next_service()
        } else if (result.staging_payment_status == 'inactive') {
          return Response.error('Currently we are not accepting any payments please try again later', 'PAYMENT-ERROR', 500)
        } else {
          next_service()
        }
      } else {
        next_service()
      }
    }
  })
}
