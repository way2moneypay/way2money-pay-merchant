var AuthenticateUtils = module.exports

const AppConfig = require('config')
const ResponseUtils = require('./ResponseUtils')
const JWT = require('jsonwebtoken')

/* Requiring models */
const MerchantsModel = require('../models/MerchantModel')

var this_file_name = 'restapp/utils/AuthenticateUtils'

AuthenticateUtils.userAuthentication = function (api_request, api_response, next_service) {
  let this_function_name = 'AuthenticateUtils.userAuthentication'
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Response = new ResponseUtils.response(api_response)
  // check header or url parameters or post parameters for token
  // console.log("Headers :: " , api_request.headers);
  var authentication_token = api_request.headers['x-access-token']
  // decode token
  if (authentication_token) {
    // verifies secret and checks exp
    // console.log("Auth token found");
    JWT.verify(authentication_token, AppConfig.jwtsecret, function (token_verify_error, token_data) {
      // console.log("TOKEN DATA IN AUTHENTICATION UTILS", token_data);
      if (token_verify_error) {
        // app.internalError('Invalid Authencation', 400, res);
        Print.error('Error while verifing user authentication token.')
        return Response.error('User athentication failed.', 'TOKEN-ERROR', 401)
      } else {
        // if everything is good, save to request for use in other routes
        let merchant_id = token_data._id
        let user_filter = {
          _id: merchant_id
        }

        MerchantsModel.findOne(user_filter, (user_find_error, user_data) => {
          if (user_find_error) {
            Print.error(user_find_error)
            return Response.error('Internal Server error.', 'DB-ERROR', 500)
          }

          if (!user_data) {
            return Response.error('Invalid Authentication details.', 'TOKEN-ERROR', 401)
          }

          if (!user_data.mobile_token_id || user_data.mobile_token_id != authentication_token) {
            return Response.error('Your session expired. Please login again.', 'TOKEN-ERROR', 401)
          }
          // console.log("user_data :: ", user_data);
          api_request.user = user_data
          return next_service()
        })
      }
    })
  } else {
    Print.error('Authentication token not found in request.')
    return Response.error('User athentication failed.', 'ERR')
  }
}

AuthenticateUtils.userAuthenticationWithQuery = function (api_request, api_response, next_service) {
  let this_function_name = 'AuthenticateUtils.userAuthenticationWithQuery'
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Response = new ResponseUtils.response(api_response)
  // console.log("api_request ::: ", api_request.headers);
  // check header or url parameters or post parameters for token
  var authentication_token = api_request.headers['x-access-token'] || api_request.body['x-access-token']
  // decode token
  if (authentication_token) {
    // verifies secret and checks exp
    JWT.verify(authentication_token, AppConfig.jwtsecret, function (token_verify_error, token_data) {
      if (token_verify_error) {
        // app.internalError('Invalid Authencation', 400, res);
        Print.error('Error while verifing user authentication token.')
        return Response.error('User athentication failed.', 'ERR')
      } else {
        // if everything is good, save to request for use in other routes
        api_request.user = token_data
        next_service()
      }
    })
  } else {
    Print.error('Authentication token not found in request.')
    return Response.error('User athentication failed.', 'ERR')
  }
}
