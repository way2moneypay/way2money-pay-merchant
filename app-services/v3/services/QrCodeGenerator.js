var QrCodeGenerator = module.exports

var qr = require('qr-image')
const AESNODE = require('../utils/EncDecNode')
var fs = require('fs')
const AppConfig = require('config')

const MerchatProfileService = require('./MerchantProfileService')

/* requiring utils */
var ResponseUtils = require('./../utils/ResponseUtils')
let this_file_name = 'QrCodeGenerator'

/** Requiring Models */
const MerchantsModel = require('../models/MerchantModel')

QrCodeGenerator.generateQrCode = function (api_request, api_response) {
  let this_function_name = 'QrCodeGenerator.generateQrCode'
  let Response = new ResponseUtils.response(api_response)
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)

  try {
    default_error = 'Error while generating qr code'
    default_error_code = 'QRCODEERROR'
    let Qrdata = {}
    MerchantsModel.findOne({
      _id: api_request.user._id
    }, (errFind, MerchatObj) => {
      if (errFind) {
        console.log('Err while finding merchant ', errFind)
        Response.error('Err while finding merchant', 500)
      } else {
        console.log('MerchatObj', MerchatObj)
        if (MerchatObj && MerchatObj.is_admin_verified && MerchatObj.qrcode_path && MerchatObj.merchant_status != 2) {
          if (MerchatObj.merchant_status == 0) {
            return Response.success('ACCOUNT-INACTIVE', {
              'is_admin_verified': false,
              'message': 'your account has been suspended temporarily'
            })
          } else {
            return Response.success('QRCODE-GENERATED', {
              is_admin_verified: true,
              qr_path: MerchatObj.qrcode_path
            })
          }
        } else if (MerchatObj && MerchatObj.is_admin_verified == true && (!MerchatObj.qrcode_path || MerchatObj.merchant_status == 2)) {
          Qrdata['merchant_name'] = MerchatObj.brand_name
          Qrdata['display_name'] = MerchatObj.brand_name
          Qrdata['id'] = MerchatObj._id
          Qrdata['mobile'] = MerchatObj.mobile
          var encQrdata = AESNODE.encrypt(JSON.stringify(Qrdata), AppConfig.jwtsecret)
          // var decodedPbj=AESNODE.decrypt("wzhEx/3V0Os9eUOcC2gs7Otoa96mR3Rg5tA630nrzdxayCGf6gZBw6mJHZ3s6NU/2awCrVWSOQ5kGiRHyYqdo2ci2+k5gxYCieJnmmYmcgQzUngHWvjUrLCSoqJ76wdZcd+61jWHL4qjxuSnDDBtwbg+THmAA3pflRb8VIAF6zU=", AppConfig.jwtsecret);
          // console.log("decodedPbj",decodedPbj);
          var qr_svg = qr.image(encQrdata, {
            type: 'png'
          })
          let qrRandom = Math.random()
          var finish = function () {
            MerchantsModel.findOneAndUpdate({
              _id: Qrdata.id
            }, {
              $set: {
                qrcode_path: 'https://business.way2money.com/qr_codes/' + Qrdata.id + '.png?' + qrRandom,
                merchant_status: 1
              }
            }, function (err, res) {
              if (err) return Response.error('QRCode path save failed', 500)
              else {
                return Response.success('QRCODE-GENERATED', {
                  is_admin_verified: true,
                  qr_path: 'https://business.way2money.com/qr_codes/' + Qrdata.id + '.png?' + qrRandom
                })
              }
            })
          }
          dest = fs.createWriteStream(__dirname + '/../../../uploads/qr_codes/' + Qrdata.id + '.png')
          qr_svg.pipe(dest)
          dest.addListener('finish', finish)
        } else {
          return Response.success('QRCODE-GENERATED', {
            'is_admin_verified': false,
            'message': 'your profile under verification'
          })
        }
      }
    })
  } catch (exception) {
    Print.exception(exception + '')
    Logs.exception(exception + '', default_error_code, default_error)
    return Response.error(default_error, default_error_code, 500, null)
  }
}
