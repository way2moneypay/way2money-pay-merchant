/* Mukesh */
/* PaymentService */
var paymentService = module.exports
const mongoose = require('mongoose')
const moment = require('moment')
const async = require('async')
/* requiring utils */
var ResponseUtils = require('./../utils/ResponseUtils')
const AESNODE = require('./../utils/EncDecNode')
var JWT = require('jsonwebtoken')
const fileName = 'PaymentService'
const AppConfig = require('config')
const request = require('request')
/* requiring models */
const merchantModel = require('../models/MerchantModel')
const userModel = require('../models/UserDetailsModel')
const merchantCommissionModel = require('../models/MerchantCommissionsModel')
const userTransactionsModel = require('../models/UserTransactionsModel')
const userScansModel = require('../models/UserScanModel')
const merchantStatsModel = require('../models/MerchantStatsModel')
const NotificationUtils = require('../utils/NotificationUtils')

/* services */
paymentService.getMerchantStatus = function (query, callback) {
  let default_error = 'Error while checking merchant active status'
  let findMerchant = {}
  // console.log("query", query);
  let merchantFields = {
    _id: true,
    merchant_status: true,
    brand_name: true,
    name: true
  }
  if (query.type === 'qrcode') {
    findMerchant['_id'] = query.merchant_id
  } else {
    findMerchant['mobile'] = query.merchant_mobile
  }
  merchantModel.findOne(findMerchant,
    function (error, result) {
      if (error) {
        console.log('DBERROR::', error)
        callback({
          'status': 'error',
          'error': error,
          'default_error': default_error,
          'code': 'DB-ERROR'
        })
      } else if (result) {
        console.log('merchantStatus details :: ', result)
        if (result.merchant_status) {
          if (query.type === 'mobile_number') {
            query['merchant_id'] = result._id
          }
          query['merchant_mobile'] = result.mobile
          query['merchant_name'] = result.name
          query['merchant_email'] = result.email
          query['merchant_name'] = result.name
          query['brand_name'] = result.brand_name
          query['pending_amt'] = result.pending_amt
          query['approved_amt'] = result.approved_amt
          query['merchant_logo'] = result.logo_url
          query['merchant_address'] = result.formatted_address
          query['created_by_parent'] = result.created_by_parent
          query['created_by'] = result.created_by
          callback({
            'status': 'success',
            'query': query
          })
        } else {
          callback({
            'status': 'failed',
            'error': 'Merchant not authorized',
            code: 'MERCH-INACTIVE'
          })
        }
      } else {
        callback({
          'status': 'failed',
          'error': 'Merchant not found',
          'default_error': default_error,
          'code': 'NO-DATA-FOUND'
        })
      }
    })
}

paymentService.getUserStatus = function (query, callback) {
  let default_error = 'Error while checking user status'
  let findUser = {}
  query['user_auth_token'] = query.user_id
  let user = decryptVerifyToken(query.user_id)
  findUser['_id'] = user._id // query.user_id
  query.user_id = user._id
  let userFields = {}
  userModel.findOne(findUser, function (error, result) {
    if (error) {
      console.log('DBERROR:: ', error)
      callback({
        'status': 'error',
        'error': error,
        'default_error': default_error,
        'code': 'DB-ERROR'
      })
    } else if (result) {
      query['user_name'] = result.name
      query['user_email'] = result.email
      // query["con_fee"] = result.con_fee;
      query['con_fee'] = 0 // result.con_fee;
      query['credit_available'] = result.credit_available
      query['user_mobile'] = result.mobile_number
      query['credit_due'] = result.credit_due
      var currentDate = (new Date()) / 1
      var timeDiff = currentDate - result.last_trans_date
      // console.log("timeDiff:: ", timeDiff)
      if (timeDiff <= 5000 && result.last_trans_date) {
        callback({
          'status': 'failed',
          'error': 'We do not allow duplicate transactions',
          'default_error': default_error,
          'code': 'DUP-TRANS'
        })
      } else {
        if (result.due_date != undefined && result.due_date != 0) {
          if (result.due_date > currentDate) {
            query['user_due_date'] = result.due_date
            if (query.transaction_type == 'confirm' && query.amount <= result.credit_available) {
              var last_transaction_date = new Date() / 1
              userModel.update(findUser, {
                $set: {
                  last_trans_date: last_transaction_date
                }
              }, function (error, updateResult) {
                if (error) {
                  console.log('DBERROR:: ', error)
                  callback({
                    'status': 'error',
                    'error': error,
                    'default_error': default_error,
                    'code': 'DB-ERROR'
                  })
                } else {
                  console.log('user:: ', updateResult)
                  callback({
                    'status': 'success',
                    'query': query
                  })
                }
              })
            } else if (query.transaction_type == 'pending') {
              callback({
                'status': 'success',
                'query': query
              })
            } else {
              callback({
                'status': 'failed',
                'error': 'Insufficient funds',
                code: 'INSUFFICIENTFUNDS'
              })
            }
          } else {
            callback({
              'status': 'failed',
              'error': 'Due date time is exceeded',
              code: 'DUEDATE'
            })
          }
        } else {
          if (!result.due_date && query.transaction_type == 'confirm') {
            var dueDate = new Date()
            dueDate.setHours(23)
            dueDate.setMinutes(59)
            dueDate.setSeconds(59)
            dueDate.setMilliseconds(999)
            dueDate = dueDate.setDate(new Date().getDate() + 15)

            var last_transaction_date = new Date() / 1
            // console.log("Entersssss::::::::::::::::::::::::::::::::::::")

            userModel.update(findUser, {
              $set: {
                due_date: dueDate,
                last_trans_date: last_transaction_date
              }
            }, function (error, updateResult) {
              if (error) {
                console.log('DBERROR:: ', error)
                callback({
                  'status': 'error',
                  'error': error,
                  'default_error': default_error,
                  'code': 'DB-ERROR'
                })
              } else {
                // console.log("Result:: ", updateResult);
                query['user_due_date'] = dueDate
                if (query.amount <= result.credit_available) {
                  callback({
                    'status': 'success',
                    'query': query
                  })
                } else {
                  callback({
                    'status': 'failed',
                    'error': 'Insufficient funds',
                    code: 'INSUFFICIENTFUNDS'
                  })
                }
              }
            })
          } else {
            if (query.transaction_type == 'confirm') {
              if (query.amount <= result.credit_available) {
                callback({
                  'status': 'success',
                  'query': query
                })
              } else {
                callback({
                  'status': 'failed',
                  'error': 'Insufficient funds',
                  code: 'INSUFFICIENTFUNDS'
                })
              }
            } else {
              callback({
                'status': 'success',
                'query': query
              })
            }
          }
        }
      }
    } else {
      callback({
        'status': 'failed',
        'error': 'User not found',
        'default_error': default_error,
        'code': 'NO-DATA-FOUND'
      })
    }
  })
}

paymentService.getMerchantCommission = function (query, callback) {
  if (query.transaction_type == 'confirm') {
    let default_error = 'Error while getting merchant commission'
    let commissionFind = {
      'merchant_id': query.merchant_id,
      'min_amount': {
        $lte: query.amount
      },
      'max_amount': {
        $gte: query.amount
      }
    }
    let commissionFields = {
      commission_rate: true
    }
    merchantCommissionModel.findOne(commissionFind, commissionFields,
      function (error, result) {
        // console.log("result:: ", result);
        if (error) {
          console.log('DBERROR:: ', error)
          callback({
            'status': 'error',
            'error': error,
            'default_error': default_error,
            'code': 'DB-ERROR'
          })
        } else if (result) {
          if (result.commission_rate) {
            query['commission_rate'] = result.commission_rate
          } else {
            query['commission_rate'] = 0
          }
          callback({
            'status': 'success',
            'query': query
          })
        } else {
          callback({
            'status': 'failed',
            'error': 'Transaction amount is not in range of your merchant.',
            'default_error': default_error,
            'code': 'NO-DATA-FOUND'
          })
        }
      })
  } else {
    callback({
      'status': 'success',
      'query': query
    })
  }
}

paymentService.updateTransactionDetails = function (query, callback) {
  let default_error = 'Error while updating ' + query.transactionStatus + ' transaction details'
  // console.log("credit_due:: ",query.credit_due);
  let location = {}
  if (query && query.latitude && query.longitude) {
    location['type'] = 'Point'
    var coordinates = []
    coordinates.push(query.longitude != undefined ? query.longitude : '')
    coordinates.push(query.latitude != undefined ? query.latitude : '')
    location['coordinates'] = coordinates
  }
  if (query.transactionStatus == 'confirm') {
    let trans_amt = query.amount + (query.amount * query.con_fee) / 100
    trans_amt = Number(trans_amt.toFixed(2))
    let pay_due
    if (query.credit_due <= 0) {
      pay_due = query.credit_due + trans_amt
    } else {
      pay_due = trans_amt
    }
    pay_due = Number(pay_due.toFixed(2))
    let transObject = {
      'user_id': mongoose.Types.ObjectId(query.user_id),
      'mobile_no': query.user_mobile,
      'merchant_id': query.merchant_id,
      'amount': query.amount,
      'reference_id': query.reference_id,
      'transaction_date': (new Date()) / 1,
      'merchant_commission': query.commission_rate,
      'con_fee': query.con_fee,
      'status': 1,
      'transaction_for': query.transaction_for,
      'transaction_status': 'pending',
      'trans_details': [{
        'status': 1,
        'date': (new Date() / 1)
      }],
      'scan_id': query.scan_id,
      'trans_amount': trans_amt,
      'trans_through': query.type,
      'ip_address': query.ip,
      'os': query.os,
      'network_type': query.network_type,
      'credit_due': query.credit_due,
      'merchant_brand': query.brand_name,
      'payment_due': pay_due,
      'location': location,
      'location_type': query.location_type !== undefined ? query.location_type : '',
      'created_by_parent': query.created_by_parent,
      'created_by': query.created_by
    }
    var userTransactions = new userTransactionsModel(transObject)
    userTransactions.save(function (error, result) {
      if (error) {
        console.log('DBERROR:: ', error)
        callback({
          'status': 'error',
          'error': error,
          'default_error': default_error,
          'code': 'DB-ERROR'
        })
      } else {
        userScansModel.update({
          _id: query.scan_id
        }, {
          status: 1,
          trans_id: result._id
        }, function (updateError, updateResult) {
          if (updateError) {
            console.log('DBERROR:: ', error)
            callback({
              'status': 'error',
              'error': error,
              'default_error': default_error,
              'code': 'DB-ERROR'
            })
          } else {
            callback({
              'status': 'success',
              'query': query
            })
          }
        })
      }
    })
  } else if (query.transactionStatus == 'pending') {
    // console.log("query :: ", query);
    let reference_id = query.brand_name.substring(0, 4) + '' + (Date.now() + 19800000)
    reference_id = reference_id.replace(/\s/g, '')
    query['reference_id'] = reference_id
    var userScanObject = {
      'reference_id': reference_id,
      'date': (new Date() / 1),
      'merchant_id': query.merchant_id,
      'user_id': query.user_id,
      'os': query.os,
      'network_type': query.network_type,
      'mobile_no': query.user_mobile,
      'location': location,
      'location_type': query.location_type != undefined ? query.location_type : ''
    }
    var userScanmodel = new userScansModel(userScanObject)
    userScanmodel.save(function (error, result) {
      // console.log("result11:: ", result);
      if (error) {
        console.log('DBERROR:: ', error)

        callback({
          'status': 'error',
          'error': error,
          'default_error': default_error,
          'code': 'DB-ERROR'
        })
      } else {
        // console.log("result:: ", result);
        query['scan_id'] = result._id
        callback({
          'status': 'success',
          'query': query
        })
      }
    })
  } else if (query.transactionStatus == 'failure') {
    userTransactionsModel.findOneAndUpdate({
      reference_id: query.reference_id
    }, {
      status: 2,
      trans_details: [{
        'status': 2,
        'date': (new Date() / 1)
      }]
    },
    function (error, result) {
      if (error) {
        console.log('DBERROR:: ', error)

        callback({
          'status': 'error',
          'error': error,
          'default_error': default_error,
          'code': 'DB-ERROR'
        })
      } else {
        callback({
          'status': 'success',
          'query': query
        })
      }
    })
  }
}

paymentService.getQRDetails = function (query, callback) {
  let default_error = 'Error while getting merchant details'

  try {
    // console.log(api_request.body);
    let token = query.qrcode_data

    var decodedObj = AESNODE.decrypt(token, AppConfig.jwtsecret)
    let tokenData = JSON.parse(decodedObj)
    console.log('token_data :: ', tokenData)
    merchantModel.findOne({
      _id: tokenData.id
    }, (errFindMerchant, merchant) => {
      if (errFindMerchant) {
        console.log('MERCHANT DETAILS FIND ERROR', errFindMerchant)
        return callback(true, errFindMerchant)
      } else {
        console.log('merchant on qr scan find', merchant)
        var merchant_details = {}
        merchant_details['id'] = merchant._id
        merchant_details['merchant_name'] = merchant.name
        merchant_details['display_name'] = merchant.brand_name
        merchant_details['mobile'] = merchant.mobile
        merchant_details['merchant_address'] = merchant.formatted_address
        let imgCdn = AppConfig.pay_image_cdn
        if (merchant.logo_url && merchant.logo_url.split('/').pop()) {
          merchant_details['merchant_logo'] = imgCdn + merchant.logo_url.split('/').pop()
        } else {
          merchant_details['merchant_logo'] = 'https://business.way2money.com/9000000001_logo_url.png'
        }
        return callback(false, merchant_details)
      }
    })
  } catch (exception) {
    console.log(exception + '', new Error(), default_error)
    return callback(true, default_error)
  }
}

paymentService.checkPayment = function (api_request, api_response) {
  const this_function_name = 'MerchantService.checkPayment'
  const default_error = 'Error while checking payment'
  let Logs = new ResponseUtils.Logs(fileName, this_function_name)
  let Print = new ResponseUtils.print(fileName, this_function_name)
  let Response = new ResponseUtils.response(api_response)
  let query = api_request.body
  if (query.amount && query.amount < 10 && query.transaction_type === 'confirm') {
    Logs.error('', 'LESS-AMOUNT', 'Amount is Less than 10')
    return Response.error('Amount should be greater than Rs 10', 'LESS-AMOUNT', 400)
  } else {
    if (query.amount) {
      query['amount'] = Number(query.amount)
    }
    var ip = api_request.headers['x-forwarded-for'] ||
      api_request.connection.remoteAddress ||
      api_request.socket.remoteAddress ||
      (api_request.connection.socket ? api_request.connection.socket.remoteAddress : null)
    // console.log("Ip address:: ", ip);
    query['ip'] = ip

    async.waterfall([
      (done) => {
        if (query.type === 'qrcode') {
          paymentService.getQRDetails(query, (get_qr_err, qrResult) => {
            if (get_qr_err) {
              Logs.error(get_qr_err, 'DB-ERROR', 'Error while getting merchant details')
              return Response.error('Merchant details not found. Please try again!', 'DB-ERROR', 500)
            } else {
              query['merchant_details'] = qrResult
              query['merchant_id'] = qrResult.id
              query['merchant_mobile'] = qrResult.mobile
              done()
            }
          })
        } else {
          done()
        }
      },
      (done) => {
        paymentService.getMerchantStatus(query, function (result) {
          if (result.status === 'error') {
            Logs.error(result.error, 'DB-ERROR', default_error)
            return Response.error('Internal error in merchant status', 'DB-ERROR', 400)
          } else if (result.status === 'failed') {
            Logs.error(result.error, result.code, result.default_error)
            return Response.error(result.error, result.code, 500)
          } else if (result.status === 'success') {
            paymentService.getUserStatus(result.query, function (userResult) {
              // console.log("userResult:: ", userResult)
              if (userResult.status === 'error') {
                Logs.error(userResult.error, 'DB-ERROR', userResult.default_error)
                return Response.error('Internal error in userStatus', 'DB-ERROR', 400)
              } else if (userResult.status == 'failed') {
                Logs.error(userResult.error, userResult.code, default_error)
                return Response.error(userResult.error, userResult.code, 500)
              } else if (userResult.status == 'success') {
                paymentService.getMerchantCommission(userResult.query, function (commissionResult) {
                  if (commissionResult.status == 'error') {
                    Logs.error(commissionResult.error, 'DB-ERROR', commissionResult.default_error)
                    return Response.error('Internal error in merchantCommission', 'DB-ERROR', 400)
                  } else if (commissionResult.status == 'failed') {
                    Logs.error(commissionResult.error, commissionResult.code, default_error)
                    return Response.error(commissionResult.error, commissionResult.code, 500)
                  } else if (commissionResult.status == 'success') {
                    if (query.transaction_type == 'confirm') {
                      commissionResult.query['transactionStatus'] = 'confirm'
                    } else {
                      commissionResult.query['transactionStatus'] = 'pending'
                    }
                    paymentService.updateTransactionDetails(commissionResult.query, function (transResult) {
                      if (transResult.status == 'error') {
                        Logs.error(transResult.error, 'DB-ERROR', transResult.default_error)
                        return Response.error('Internal error in updateTransactions', 'DB-ERROR', 400)
                      } else if (transResult.status == 'failed') {
                        Logs.error(transResult.error, transResult.code, default_error)
                        return Response.error(transResult.error, transResult.code, 500)
                      } else if (transResult.status == 'success') {
                        if (query.transaction_type == 'pending') {
                          // here we need to check offers
                          console.log('query response on pending :: ', query)
                          Response.success('Success', {
                            'con_fee': transResult.query.con_fee,
                            'credit_available': query.credit_available,
                            'scan_id': query.scan_id,
                            'reference_id': query.reference_id,
                            'merchant_address': transResult.query.merchant_address,
                            'merchant_details': query.merchant_details,
                            'merchant_logo': transResult.query.merchant_logo
                          })
                          // });
                        } else {
                          paymentService.updatePayment(transResult.query, function (updateResult) {
                            if (updateResult.status == 'error') {
                              Logs.error(updateResult.error, 'DB-ERROR', transResult.default_error)
                              return Response.error('Internal error updatePayment', 'DB-ERROR', 400)
                            } else if (updateResult.status == 'failed') {
                              Logs.error(updateResult.error, updateResult.code, default_error)
                              return Response.error(updateResult.error, updateResult.code, 500)
                            } else if (updateResult.status == 'success') {
                              let time = new Date()
                              time = time.getTime()
                              time = time + 19800000
                              Response.success('Your payment has been done successfully', {
                                'reference_id': updateResult.query.reference_id,
                                'merchant_details': updateResult.query.merchant_details,
                                'amount_paid': updateResult.query.amount,
                                'brand_name': updateResult.query.brand_name,
                                'paid_date': moment(Date.now()).format('Do MMM YYYY'),
                                'paid_time': moment(time).format('hh:mm A')
                              })
                              paymentService.sendSms(query)
                              if (query.user_email) {
                                paymentService.sendEmail(query)
                              }

                              // paymentService.updateMerchantStats(updateResult.query, function (statsResult) {
                              //   if (statsResult.status == "error") {
                              //     return Response.error("Internal error merchantstats update", "DB-ERROR", 400);
                              //   } else if (statsResult.status == "success") {
                              //     console.log("Update result on success", updateResult.query);
                              //     let time = new Date();
                              //     time = time.getTime();
                              //     time = time + 19800000;
                              //     Response.success("Your payment has been done successfully", {
                              //       "reference_id": updateResult.query.reference_id,
                              //       "amount_paid": updateResult.query.amount,
                              //       "brand_name": updateResult.query.brand_name,
                              //       "paid_date": moment(Date.now()).format("Do MMM YYYY"),
                              //       "paid_time": moment(time).format("hh:mm A")
                              //     });
                              //     paymentService.sendSms(query);
                              //     if (query.user_email) {
                              //       paymentService.sendEmail(query);
                              //     }
                              //   }
                              // })
                            }
                          })
                        }
                      }
                    })
                  }
                })
              }
            })
          }
        })
      }
    ])
  }
}

paymentService.updatePayment = function (query, callback) {
  let amount
  amount = query.amount + (query.con_fee / 100) * query.amount
  // console.log("amount:: ", amount);
  let Logs = new ResponseUtils.Logs(fileName, 'merchantService.updatePayment')
  userModel.update({
    _id: query.user_id
  }, {
    $inc: {
      credit_due: amount,
      credit_available: -amount
    }
  },
  function (userError, userResult) {
    if (userError) {
      query.transactionStatus = 'failure'
      paymentService.updateTransactionDetails(query, function (trnasResult) {
        if (trnasResult) {
          console.log(userError)
          Logs.error(userError, 'DB-ERROR', 'Error while updating user credit amount')
          callback({
            'status': 'error',
            'error': error,
            'default_error': default_error,
            'code': 'DB-ERROR'
          })
        }
      })
    } else {
      amount = query.amount - (query.amount * (query.commission_rate / 100))
      query['merchant_pending_amt'] = amount
      merchantModel.update({
        _id: query.merchant_id
      }, {
        $inc: {
          pending_amt: amount
        }
      },
      function (merchantError, merchantResult) {
        // console.log(merchantResult);
        if (merchantError) {
          amount = query.amount + (query.con_fee / 100) * query.amount
          userModel.update({
            _id: query.user_id
          }, {
            $inc: {
              credit_due: -amount,
              credit_available: amount
            }
          },
          function (err, result) {
            if (result) {
              query.transactionStatus = 'failure'
              paymentService.updateTransactionDetails(query, function (trnasResult) {
                if (trnasResult) {
                  Logs.error(merchantError, 'DB-ERROR', 'Error while updating merchant pending amount')
                  callback({
                    'status': 'error',
                    'error': error,
                    'default_error': default_error,
                    'code': 'DB-ERROR'
                  })
                }
              })
            }
          })
        } else {
          callback({
            'status': 'success',
            'query': query
          })
        }
      }
      )
    }
  }
  )
}
var decryptVerifyToken = function (token) {
  // console.log("token called");
  let token_data = JWT.verify(token, AppConfig.jwtsecret, function (token_verify_error, token_data) {
    if (token_verify_error) {
      console.log('token error')
      return 'DECRYPTTOKENERR'
    } else {
      // console.log("token data", token_data);
      return token_data
    }
  })
  return token_data
}
paymentService.updateMerchantStats = function (query, callback) {
  // console.log("Entree")
  var currentDate = new Date()
  currentDate.setHours(0)
  currentDate.setMinutes(0)
  currentDate.setSeconds(0)
  currentDate.setMilliseconds(0)
  var amount = query.amount - (query.amount * query.commission_rate) / 100
  // console.log("amount11", amount)
  merchantStatsModel.findOneAndUpdate({
    merchant_id: query.merchant_id,
    trans_date: currentDate
  }, {
    $inc: {
      transactions: 1,
      trans_amount: amount
    }
  }, {
    upsert: true
  },
  function (updateError, updateResult) {
    // console.log(updateResult)
    if (updateError) {
      callback({
        'status': 'error',
        'error': error,
        'default_error': default_error,
        'code': 'DB-ERROR'
      })
    } else {
      // console.log("sucess")
      callback({
        'status': 'success',
        'query': query
      })
    }
  })
}
paymentService.sendSms = function (query) {
  // var userMessage = (Number(query.amount) + Number((query.con_fee / 100) * query.amount)).toFixed(2) + "(Rs." + (parseFloat(query.amount)).toFixed(2) + " CON fee Rs. " + (parseFloat(query.con_fee / 100) * query.amount).toFixed(2) + " )";

  let long_url = AppConfig.long_url_domain + query.user_auth_token

  request(AppConfig.short_url + long_url, (error_short_url, short_url_response) => {
    if (error_short_url) {
      console.log('Error while sending sms to user')
    } else {
      console.log('Response from short URL DOMAIN :: ', short_url_response.body)
      // if(typeof(short_url_response) == 'string') {
      // }
      let response = JSON.parse(short_url_response.body)
      var userAmount = (Number(query.amount) + Number((query.con_fee / 100) * query.amount)).toFixed(2)

      var userConfiguration = {
        mobile: query.user_mobile,
        replace: {
          merchant: query.brand_name.substring(0, 10),
          amount: userAmount,
          url: response.data.short_url
        }
      }
      // console.log("userconfig:: ", userConfiguration);
      NotificationUtils.sms('sdk-payment', userConfiguration, function (userError, userResult) {
        if (userError) {
          console.log('Error while sending message to user:: ', userError)
        } else {
          console.log('message has been successfully sent to user')
        }
      })
    }
  })

  var pending_amt = query.pending_amt != undefined && query.pending_amt != null && query.pending_amt != '' ? query.pending_amt : 0
  var approved_amt = query.approved_amt != undefined && query.approved_amt != null && query.approved_amt != '' ? query.approved_amt : 0
  var merchantMessage = (query.amount) - (parseFloat(((query.amount * query.commission_rate) / 100).toFixed(2))).toFixed(2) + ' (Rs.' + (parseFloat((query.amount).toFixed(2))).toFixed(2) + ' - W2M charges Rs.' + (parseFloat(((query.amount * query.commission_rate) / 100).toFixed(2))).toFixed(2) + ') '
  var merchantConfiguration = {
    mobile: query.merchant_mobile,
    replace: {
      mobile: query.user_mobile,
      amount: merchantMessage,
      avlBalance: (parseFloat(((Number(pending_amt) + Number(approved_amt) + Number(query.merchant_pending_amt))).toFixed(2))).toFixed(2)
    }
  }
  // console.log("merchant config:: ", merchantConfiguration);
  NotificationUtils.sms('merchant-payment', merchantConfiguration, function (userError, userResult) {
    if (userError) {
      console.log('Error while sending message to merchant:: ', userError)
    } else {
      console.log('message has been successfully sent to merchant')
    }
  })
}

paymentService.sendEmail = function (query) {
  // let link = "https://www.way2money.com";
  let configuration = {}
  console.log(query.user_email)
  configuration.email = query.user_email
  var userAmount = (Number(query.amount) + Number((query.con_fee / 100) * query.amount)).toFixed(2)
  // name: query.user_name,
  configuration.replace = {
    merchant: query.brand_name,
    amount: userAmount,
    date: moment(Date.now()).format('Do MMM YYYY'),
    reference_id: query.reference_id
  }
  console.log('emailConfig:: ', configuration)
  NotificationUtils.email('sdk-payment', configuration, function (userError, userResult) {
    if (userError) {
      // console.log("Error while sending mail to user:: ", userError);
    } else {
      console.log('Mail has been successfully sent to user')
    }
  })
}

// paymentService.buildObject = function (query, callback) {
//   var result;
//   if (query.transaction_type == "confirm")
//     result = {
//       "type": query.type,
//       "user_id": query.user_id,
//       "user_mobile": query.user_mobile,
//       "con_fee": query.con_fee,
//       "user_due_date": query.user_due_date,
//       "amount": query.amount,
//       "merchant_id": query.merchant_id,
//       "merchant_mobile": query.merchant_mobile,
//       "merchant_name": query.merchant_name,
//       "merchant_commission": query.commission_rate,
//       "merchant_brand_name": query.brand_name,
//       "reference_id": query.reference_id,
//       "transaction_type": query.transaction_type
//     }
//   callback(result);
// }
