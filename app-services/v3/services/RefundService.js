var RefundService = module.exports
var async = require('async')
var mongoose = require('mongoose')

/* Requiring models */
var UserTransactionsModel = require('../models/UserTransactionsModel')
var MerchantTransactionsModel = require('../models/MerchantStatsModel')
var MerchantModel = require('../models/MerchantModel')
var UserDetailsModel = require('../models/UserDetailsModel')
var RefundTransactions = require('../models/RefundTransactions')
var PaymentSettingsModel = require('../models/PaymentSettingsModel')

/* requiring utils */
var ResponseUtils = require('./../utils/ResponseUtils')
let this_file_name = 'RefundService'

RefundService.checkRefund = function (api_request, api_response) {
  default_error = 'Error while checking refund data'
  default_error_code = 'CHECKREFUNDERROR'

  let this_function_name = 'RefundService.checkRefund'
  let Response = new ResponseUtils.response(api_response)
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  try {
    default_error = 'Error while checking refund data'
    default_error_code = 'REFUNDDATAERROR'
    UserTransactionsModel.findById({
      _id: api_request.body.transaction_id
    }, function (TransactionsErr, TransactionsRes) {
      if (TransactionsErr) return Response.error(this_function_name, 'MongoError : UserTransactionsFind', 500, TransactionsErr)
      else if (TransactionsRes) {
        var merchant_commission_amount = Number((TransactionsRes.amount * TransactionsRes.merchant_commission) / 100).toFixed(2)
        var merchant_pending_amount = TransactionsRes.amount - merchant_commission_amount
        merchant_pending_amount = Number(merchant_pending_amount.toFixed(2))
        var con_fee_amount = (TransactionsRes.amount * TransactionsRes.con_fee) / 100
        var total_trans_amount = TransactionsRes.amount + con_fee_amount
        return Response.success('Refund data returned successfully', {
          merchant_pending_amount: merchant_pending_amount,
          merchant_commission_amount: merchant_commission_amount,
          merchant_commission: TransactionsRes.merchant_commission,
          total_trans_amount: total_trans_amount,
          con_fee_amount: con_fee_amount,
          con_fee: TransactionsRes.con_fee,
          original_amount: Number(TransactionsRes.amount.toFixed(2)),
          merchant_id: TransactionsRes.merchant_id,
          user_id: TransactionsRes.user_id,
          transaction_id: api_request.body.transaction_id
        })
      } else {
        return Response.error(this_function_name, 'MongoError : UserTransactionsFind', 'UserTransactionNotFound')
      }
    })
  } catch (exception) {
    Print.exception(exception + '')
    Logs.exception(exception + '', default_error_code, default_error)
    return Response.error(default_error, default_error_code, 500, null)
  }
}
RefundService.refund = function (api_request, api_response) {
  // api_request.body = api_request.body.refund;
  default_error = 'Error while refunding'
  default_error_code = 'REFUNDERROR'

  let this_function_name = 'RefundService'
  let Response = new ResponseUtils.response(api_response)
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  try {
    var pending_amt, credit_available, credit_due
    var rollback_refund_merchant_amount, init_pending_amt, init_credit_available, init_credit_due, init_approved_amt
    var due_date, transaction_date, init_status
    var merchant_commission_amount, merchant_pending_amount, con_fee_amount, total_trans_amount, merchant_id, user_id, original_amount

    /** Rollback function incase if refund fails */
    function MerchantRefundRollback (error_msg, callback) {
      MerchantModel.findByIdAndUpdate({
        _id: merchant_id
      }, {
        $set: rollback_refund_merchant_amount
      }, function (MerchantUpdateErr, MerchantUpdated) {
        if (MerchantUpdateErr) MerchantRefundRollback(error_msg)
        else {
          return Response.error({
            error_message: 'Refund failed',
            error_details: error_msg
          }, 500)
        }
      })
    }
    /** Rollback function incase if refund fails */
    function UserMerchantRefundRollback (error, callback) {
      MerchantModel.findByIdAndUpdate({
        _id: merchant_id
      }, {
        $set: {
          pending_amt: init_pending_amt
        }
      }, function (MerchantUpdateErr, MerchantUpdated) {
        if (MerchantUpdateErr) UserMerchantRefundRollback(error)
        else {
          UserDetailsModel.findByIdAndUpdate({
            _id: user_id
          }, {
            $set: {
              credit_available: init_credit_available,
              credit_due: init_credit_due
            }
          }, function (UserUpdateErr, UserUpdateUpdated) {
            if (UserUpdateErr) UserMerchantRefundRollback(error)
            else {
              return Response.error({
                error_msg: 'Refund failed and transactions rollbacked.',
                error_details: error
              }, error, 500)
            }
          })
        }
      })
    }
    /** Rollback function incase if refund fails */
    function UserMerchantStatusRefundRollback (error, callback) {
      MerchantModel.findByIdAndUpdate({
        _id: merchant_id
      }, {
        $set: {
          pending_amt: init_pending_amt
        }
      }, function (MerchantUpdateErr, MerchantUpdated) {
        if (MerchantUpdateErr) UserMerchantStatusRefundRollback(error)
        else {
          UserDetailsModel.findByIdAndUpdate({
            _id: user_id
          }, {
            $set: {
              credit_available: init_credit_available,
              credit_due: init_credit_due
            }
          }, function (UserUpdateErr, UserUpdateUpdated) {
            if (UserUpdateErr) {
              return Response.error({
                error_msg: 'Rollback for merchant is successfull, rollback for user failed.',
                error_details: UserUpdateErr
              }, error, 500)
            } else {
              UserTransactionsModel.findByIdAndUpdate({
                _id: api_request.body.transaction_id
              }, {
                $set: {
                  status: init_status
                }
              }, function (err, res) {
                if (err) {
                  return Response.error({
                    error_msg: 'Rollback for merchant and user is successfull, rollback for transaction status is failed reason :',
                    error_details: err
                  }, error, 500)
                } else {
                  return Response.error({
                    error_message: 'Refund failed and transactions rollbacked',
                    error_details: error
                  }, 500)
                }
              })
            }
          })
        }
      })
    }
    /** Refund of transactions starts */
    UserTransactionsModel.findOne({
      _id: api_request.body.transaction_id
    }, function (checkStatusErr, checkStatusRes) {
      if (checkStatusErr) {
        return Response.error({
          error_msg: 'Error while checking transaction stats',
          error_type: 'MongoError: Transactiond find error',
          error_details: checkStatusErr
        }, 500)
      } else if (checkStatusRes) {
        merchant_commission_amount = (checkStatusRes.amount * checkStatusRes.merchant_commission) / 100
        merchant_pending_amount = checkStatusRes.amount - merchant_commission_amount
        con_fee_amount = (checkStatusRes.amount * checkStatusRes.con_fee) / 100
        total_trans_amount = checkStatusRes.amount + con_fee_amount
        merchant_id = checkStatusRes.merchant_id
        user_id = checkStatusRes.user_id
        original_amount = checkStatusRes.amount
        // if status 6 or 7 minus amount from approved
        if (checkStatusRes.status != 4) {
          async.series([
            /** 1. Merchant refund */
            /** Checking whether merchants exists */
            function (callback) {
              MerchantModel.findById({
                _id: merchant_id
              }, function (MerchantErr, MerchantRes) {
                if (MerchantErr) callback('MongoError : Merchants find error', null)
                else if (MerchantRes) {
                  /** If merchant found refunding amount to merchant */
                  let init_pending_amt = MerchantRes.pending_amt ? MerchantRes.pending_amt : 0
                  let init_approved_amt = MerchantRes.approved_amt ? MerchantRes.approved_amt : 0
                  /** Calculating refund amount */
                  pending_amt = init_pending_amt - merchant_pending_amount
                  approved_amt = init_approved_amt - merchant_pending_amount

                  refund_merchant_amount = {
                    pending_amt: pending_amt
                  }
                  // Storing initial amount incase to rollback
                  rollback_refund_merchant_amount = {
                    pending_amt: init_pending_amt
                  }
                  /** Checking status */
                  if (checkStatusRes.m_status == 6) {
                    refund_merchant_amount = {
                      approved_amt: approved_amt
                    }
                    // Storing initial amount incase to rollback
                    rollback_refund_merchant_amount = {
                      approved_amt: init_approved_amt
                    }
                  }

                  MerchantModel.findByIdAndUpdate({
                    _id: merchant_id
                  }, {
                    $set: refund_merchant_amount
                  }, function (MerchantUpdateErr, MerchantUpdated) {
                    /** If error occurs while refunding throwing error */
                    /** It's the first step so rollback isn't necessary */
                    if (MerchantUpdateErr) {
                      callback({
                        error_name: 'MongoError : Merchants refund update failed',
                        error: MerchantUpdateErr
                      }, null)
                    } else if (MerchantUpdated) callback(null, null)
                  })
                } else {
                  callback('404 : Merchant not found', null)
                }
              })
            },
            /** 2. User's refund */
            /** Checking whether user exists */
            function (callback) {
              UserDetailsModel.findById({
                _id: user_id
              }, function (UserErr, UserRes) {
                /** If error occurs while checking user exists rollback merchants refund */
                if (UserErr) {
                  MerchantRefundRollback({
                    error_msg: 'Refund failed and transactions rollbacked',
                    error_type: "MongoError : User's find error",
                    error_details: UserErr
                  })
                } else if (UserRes) {
                  /** Storing due_date */
                  due_date = UserRes.due_date
                  /** Storing intial amount if incase to rollback refund */
                  init_credit_available = UserRes.credit_available
                  init_credit_due = UserRes.credit_due
                  /** Storing refund amount */
                  credit_available = UserRes.credit_available + total_trans_amount
                  credit_due = UserRes.credit_due - total_trans_amount
                  UserDetailsModel.findByIdAndUpdate({
                    _id: user_id
                  }, {
                    $set: {
                      credit_available: credit_available,
                      credit_due: credit_due
                    }
                  }, function (UserUpdateErr, UserUpdateUpdated) {
                    if (UserUpdateErr) {
                      MerchantRefundRollback({
                        error_msg: 'Refund failed and transactions rollbacked',
                        error_type: "MongoError : User's refund update failed",
                        error_details: UserUpdateErr
                      })
                    } else if (UserUpdateUpdated) {
                      callback(null, null)
                    }
                  })
                } else {
                  MerchantRefundRollback({
                    error_msg: 'Refund failed and transactions rollbacked',
                    error_type: '404: User not found'
                  })
                }
              })
            },
            /** 3. Updating redunded in user's transaction */
            function (callback) {
              UserTransactionsModel.findByIdAndUpdate({
                _id: api_request.body.transaction_id
              }, {
                $set: {
                  status: 4
                },
                $addToSet: {
                  trans_details: {
                    status: 4,
                    date: Date.now()
                  }
                }
              }, function (err, res) {
                if (err) {
                  UserMerchantRefundRollback({
                    error_msg: 'Refund failed and transactions rollbacked',
                    error_type: "MongoError: Updating in user's transaction failed",
                    error: error_details
                  })
                } else {
                  transaction_date = res.transaction_date
                  init_status = res.status
                  callback(null, null)
                }
              })
            },
            /** 4. Checking to updating due date */
            function (callback) {
              /** Explanation of updating due date (Example) */
              /** Existing due_date = 15 Jul (Period : 15 days)
               *  List of transactions
               *  1 Jul - failed (clicked refund on this transaction )
               *  5 Jul - success
               *  8 Jul - success
               *  After refund
               *  (1 Jul) transaction is not valid anymore
               *  So due date should be updated to 20 Jul (5 Jul + 15 days)
               *  Case 1. Check whether transactions exists after refunded transaction
               *          if exists update due_date = transaction_date+due_period
               *  Case 2. If transactions not found update due_date = 0
               */

              /** Step 1. Check whether successfull transactions exists after refunded transaction */
              UserTransactionsModel.findOne({
                user_id: mongoose.Types.ObjectId(user_id),
                status: 1
              }, function (findErr, findRes) {
                if (findErr) UserMerchantStatusRefundRollback(findErr)
                /**
                 * Case 1.
                 * If transactions exits.
                 * Check whether transactions are greater than refunded transaction if found
                 * update due_date = transaction_date (results) + due_period
                 */
                else if (findRes) {
                  if (findRes.transaction_date >= transaction_date) {
                    console.log(' | / Due date update / | Case 1: Updating due_date = transaction_date (results) + due_period')
                    PaymentSettingsModel.findOne({}, function (findErr, findData) {
                      if (findErr) UserMerchantStatusRefundRollback(findErr)
                      else {
                        updated_due_date = new Date(findRes.transaction_date)
                        updated_due_date.setDate(updated_due_date.getDate() + findData.due_period)
                        UserDetailsModel.findByIdAndUpdate({
                          _id: user_id
                        }, {
                          $set: {
                            due_date: updated_due_date
                          }
                        }, function (updateErr, UpdateRes) {
                          if (updateErr) {
                            UserMerchantStatusRefundRollback({
                              error_msg: 'Refund failed and transactions rollbacked',
                              error_type: 'MongoError : Due date update failed',
                              error_details: updateErr
                            })
                          } else callback(null, null)
                        })
                      }
                    })
                  } else {
                    callback(null, null)
                  }
                } else {
                  /**
                   * Case 2.
                   * If transactions not exists update due_date = 0
                   */
                  console.log(' | / Due date update / | Case 2: Updating due_date = 0')
                  UserDetailsModel.findByIdAndUpdate({
                    _id: user_id
                  }, {
                    $set: {
                      due_date: 0
                    }
                  }, function (updateErr, UpdateRes) {
                    if (updateErr) {
                      UserMerchantStatusRefundRollback({
                        error_msg: 'Refund failed and transactions rollbacked',
                        error_type: 'MongoError : Due date update failed',
                        error_details: updateErr
                      })
                    } else callback(null, null)
                  })
                }
              })
            },
            /** 5. Saving refund transaction */
            /** User's, Merchant's refund is successfull so storing refund transaction */
            function (callback) {
              RefundTransactionDetails = new RefundTransactions({
                transaction_id: api_request.body.transaction_id,
                merchant_id: merchant_id,
                user_id: user_id,
                con_fee: con_fee_amount,
                merchant_commission: merchant_commission_amount,
                amount: original_amount,
                trans_amount: total_trans_amount,
                pending_amt: pending_amt,
                credit_available: credit_available,
                credit_due: credit_due,
                refunded_date: Date.now(),
                refund_by: 'Merchant'
              })
              RefundTransactionDetails.save(function (RefundTransactionSaveErr, RefundTransactionSaved) {
                if (RefundTransactionSaveErr) {
                  callback({
                    error_msg: 'Refund success. Saving redund transaction failed',
                    error_type: 'MongoError : Due date update failed',
                    error_details: RefundTransactionSaveErr
                  })
                } else callback(null, null)
              })
            }
          ], function (err, results) {
            if (err) {
              return Response.error('Error while refunding', err, 500, '')
            } else {
              return Response.success('Refunded success', 'Refunded')
            }
          })
        } else {
          return Response.success('Transaction is already refunded', 'Refunded')
        }
      } else return Response.success('Transaction is not found', 'Refund failed', 500)
    })
  } catch (exception) {
    Print.exception(exception + '')
    Logs.exception(exception + '', default_error_code, default_error)
    return Response.error(default_error, default_error_code, 500, null)
  }
}
