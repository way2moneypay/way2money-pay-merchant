var RequestUtils = require('./../utils/RequestUtils')
var ResponseUtils = require('./../utils/ResponseUtils')
var GateWaysConfig = require('config')['GateWaysConfig']
var UserDetailsModel = require('./../models/UserDetailsModel')
// var ResponseUtils = require("./../utils/ResponseUtils");
var PaymentService = require('./../services/PaymentService')
var Request = require('request')
var InstaMojoServices = module.exports
var qs = require('querystring')
var http = require('https')
var UserPaymentsModel = require('./../models/UserPaymentsModel')

var Logs = new ResponseUtils.LogsCommon()
var Print = new ResponseUtils.PrintCommon()

InstaMojoServices.InstaMojoCreatePayment = function (purpose, amount, mobile, buyer_name, email, domain, skip, callback) {
  if (skip) return callback()
  let headers = GateWaysConfig.InstaMojo[domain].keys
  let url = GateWaysConfig.InstaMojo[domain].urls.create_payment

  let payload = {
    purpose: purpose,
    amount: amount,
    phone: mobile,
    buyer_name: buyer_name,
    redirect_url: GateWaysConfig.InstaMojo[domain].redirect_url.create_payment,
    webhook: GateWaysConfig.InstaMojo[domain].webhook_urls.create_payment,
    send_email: false,
    send_sms: false,
    email: email,
    allow_repeated_payments: false
  }

  let RequestOptions = RequestUtils.MakeRequestOptions(url, 'post', payload, 'form', headers)
  Request(RequestOptions, (error, response, body) => {
    console.log(body)
    if (!body.success) {
      return callback(true, 'INS-PAYMENT-ERROR', body.message, null)
    }
    // if (body.success) {
    //   console.log("Error while doing payment :: ", error);
    //   return callback(true, "INS-PAYMENT-ERROR", "Internal Server Error.", null);
    // }
    return callback(null, null, null, body)
  })
}

InstaMojoServices.InstaMojoRefund = function (payment_id, type, body, callback) {
  if (skip) return callback()
  let headers = GateWaysConfig.InstaMojo.keys
  let url = GateWaysConfig.InstaMojo.urls.refund_payment

  let payload = {
    payment_id: payment_id,
    type: type,
    body: body
  }

  let RequestOptions = RequestUtils.MakeRequestOptions(url, 'post', payload, 'form', headers)
  Request(RequestOptions, (error, response, body) => {
    console.log(body)
  })
}

// InstaMojoServices.PaymentRequestCheck = function(){

// }

InstaMojoServices.CreatePaymentRequest = function (api_request, api_response) {
  let Response = new ResponseUtils.response(api_response)
  let user_id = api_request.user._id
  let source = api_request.body.source

  let user_filter = {
    _id: user_id
  }

  console.log(user_id)

  UserDetailsModel.findOne(user_filter, (user_find_error, user_data) => {
    if (user_find_error) {
      console.log('Error while finding user details in InstaMojoCreatePaymentRequest.', user_find_error)
      return Response.error('Internal Server Error.', 'DB-ERROR', 500)
    }

    if (!user_data) {
      return Response.error('No user details found.', 'USER-NOT-FOUND', 404)
    }

    let purpose = 'WAY2MONEY DUE AMOUNT PAYMENT'
    let mobile = Number(user_data.mobile_number)
    let email = user_data.email
    let buyer_name = user_data.name
    let amount = Number(Number(user_data.credit_due).toFixed(2))
    let domain = GateWaysConfig['InstaMojo']['working_domain']

    let user_id = user_data._id

    InstaMojoServices.InstaMojoCreatePayment(purpose, amount, mobile, buyer_name, email, domain, false, (error, code, message, data) => {
      if (error) {
        return Response.error(message, code, 500)
      }

      // console.log("Instamojo Data ::: ", data);
      PaymentService.initiatePaymentOfUser(data.payment_request.id, amount, user_id, source, (initiation_error, initiation_code, initiation_message, initiation_data) => {
        if (initiation_error) {
          return Response.error(initiation_message, initiation_code, 500, initiation_data)
        }

        return Response.success('Successfully created payment. Find url in data.', data)
      })
    })
  })
}

InstaMojoServices.paymentCompleteRedirect = function (api_request, api_response) {
  let Response = new ResponseUtils.response(api_response)

  try {
    let payment_id = api_request.body.payment_id
    let payment_request_id = api_request.body.payment_request_id
    let user_id = api_request.user._id

    if (!payment_id || !payment_request_id) {
      return Response.error('payment_id (OR) payment_request_id not sent in request.', 'BAD-PARAMS', 400)
    }

    let payment_filter = {
      transaction_ref_id: payment_request_id,
      user_id: user_id
    }

    Print.log(api_request.body, new Error())

    UserPaymentsModel.findOne(payment_filter, (payment_error, payment_data) => {
      if (payment_error) {
        Print.error(payment_error, new Error())
        Logs.error('Error while getting payments data.', 'DB-ERROR', payment_error + '', new Error())
        Response.error('Internal Server Error.', 'DB-ERROR', 500)
      }

      if (!payment_data) {
        return Response.error('No payment data found.', 'INVALID-TRANSACTION', 400)
      }

      payment_data.gateway_response_status = 'success'

      payment_data.save((payment_update_error, payment_update_data) => {
        if (payment_update_error) {
          Print.error(payment_update_error, new Error())
          Logs.error('Error while updating payments data.', 'DB-ERROR', 'Payment Error', new Error())
          Response.error('Internal Server Error.', 'DB-ERROR', 500)
        }

        if (payment_update_data.paid_status == 0) {
          return Response.success('Your Payment Still in progress', payment_update_data, 'PENDING')
        }

        if (payment_update_data.paid_status == 2) {
          return Response.success('Your Payment was failed.', payment_update_data, 'FAILURE')
        }

        return Response.success('Your Payment Still in progress', payment_update_data, 'SUCCESS')
      })
    })
  } catch (exception) {
    Print.exception(exception + '', exception)
    Logs.exception('Exception occurred.', 'PAYMENT-REDIRECT-EXCEPTION', null, exception)
    Response.error('Internal Server Error.', 'EXCEPTION', 500)
  }
}

InstaMojoServices.paymentCompleteRequest = function (api_request, api_response) {
  try {
    let payment_details = api_request.body
    payment_details.transaction_ref_id = payment_details.payment_request_id
    payment_details.credit_due = Number(payment_details.amount)
    payment_details.paid_status = payment_details.status == 'Credit' ? 'success' : 'failure'

    Print.log(payment_details, new Error())
    // Logs.log("Payment completion logs.", "PAYMENT-DATA", JSON.S, new Error());

    PaymentService.completionOfPaymentOfUserWeb(payment_details, (payment_update_error, update_code, update_message, update_data) => {
      if (payment_update_error) {
        Print.error(payment_update_error, new Error())
        Logs.error(update_message, update_code, update_data, new Error())
        return api_response.status(200).send('some ')
      }
    })
  } catch (exception) {
    Print.exception(exception + '', exception)
    Logs.exception('Exception occurred.', 'PAYMENT-COMPLETE-EXCEPTION', null, exception)
    api_response.status(200).send('exception occurred')
  }
}

// ################################################################################################################
// DOMAIN CHECK REQUESTS

InstaMojoServices.InstaMojoDomainCheck = function (client_id, client_secret, callback) {
  try {
    var options = {
      'method': 'POST',
      'hostname': 'www.instamojo.com',
      'port': null,
      'path': '/oauth2/token/',
      'headers': {
        'content-type': 'application/x-www-form-urlencoded',
        'cache-control': 'no-cache'
      }
    }

    var req = http.request(options, function (res) {
      var chunks = []

      res.on('data', function (chunk) {
        chunks.push(chunk)
      })

      res.on('end', function () {
        var body = Buffer.concat(chunks)
        console.log('InstaMojo response ::: ', body.toString())
        let access_token = 'production' + JSON.parse(body.toString()).access_token
        // console.log(JSON.parse(body.toString()));
        // access_token = "production" + access_token;
        return callback(null, access_token)
      })
    })

    req.write(qs.stringify({
      grant_type: 'client_credentials',
      client_id: client_id,
      client_secret: client_secret
    }))
    req.end()
  } catch (exception) {
    console.log(exception)
    return callback(true, null)
  }
}

InstaMojoServices.Way2PropertyDomainCheckAndroid = function (api_request, api_response) {
  try {
    let client_id = GateWaysConfig['InstaMojo']['Way2Property']['android-credentials']['client-id']
    let client_secret = GateWaysConfig['InstaMojo']['Way2Property']['android-credentials']['client-secret']

    InstaMojoServices.InstaMojoDomainCheck(client_id, client_secret, (error, access_token) => {
      if (error) {
        Print.error('Instamojo payment initiation android request failed.', new Error())
        Logs.error('Instamojo payment initiation android request failed.', 'INSTAMOJO-WAY2PROP-ANDROID', error, new Error())
        return api_response.status(200).send('null')
      };

      return api_response.status(200).send(access_token)
    })
  } catch (exception) {
    console.log(exception)
    api_response.status(200).send('null')
  }
}

InstaMojoServices.Way2PropertyDomainCheckIOS = function (api_request, api_response) {
  try {
    let client_id = GateWaysConfig['InstaMojo']['Way2Property']['ios-credentials']['client-id']
    let client_secret = GateWaysConfig['InstaMojo']['Way2Property']['ios-credentials']['client-secret']

    InstaMojoServices.InstaMojoDomainCheck(client_id, client_secret, (error, access_token) => {
      if (error) {
        Print.error('Instamojo payment initiation ios request failed.', new Error())
        Logs.error('Instamojo payment initiation ios request failed.', 'INSTAMOJO-WAY2PROP-IOS', error, new Error())
        return api_response.status(200).send('null')
      };
      return api_response.status(200).send(access_token)
    })
  } catch (exception) {
    console.log(exception)
    api_response.status(200).send('null')
  }
}

InstaMojoServices.Way2MoneyDomainCheckAndroid = function (api_request, api_response) {
  try {
    let client_id = GateWaysConfig['InstaMojo']['Way2Money']['android-credentials']['client-id']
    let client_secret = GateWaysConfig['InstaMojo']['Way2Money']['android-credentials']['client-secret']
    console.log(client_id, client_secret)
    InstaMojoServices.InstaMojoDomainCheck(client_id, client_secret, (error, access_token) => {
      if (error) {
        Print.error('Instamojo payment initiation ios request failed.', new Error())
        Logs.error('Instamojo payment initiation ios request failed.', 'INSTAMOJO-WAY2MONEY-IOS', error, new Error())
        return api_response.status(200).send('null')
      };
      return api_response.status(200).send(access_token)
    })
  } catch (exception) {
    console.log(exception)
    api_response.status(200).send('null')
  }
}

InstaMojoServices.Way2MoneyDomainCheckIOS = function (api_request, api_response) {
  try {
    let client_id = GateWaysConfig['InstaMojo']['Way2Money']['ios-credentials']['client-id']
    let client_secret = GateWaysConfig['InstaMojo']['Way2Money']['ios-credentials']['client-secret']

    InstaMojoServices.InstaMojoDomainCheck(client_id, client_secret, (error, access_token) => {
      if (error) {
        Print.error('Instamojo payment initiation ios request failed.', new Error())
        Logs.error('Instamojo payment initiation ios request failed.', 'INSTAMOJO-WAY2MONEY-IOS', error, new Error())
        return api_response.status(200).send('null')
      };
      return api_response.status(200).send(access_token)
    })
  } catch (exception) {
    console.log(exception)
    api_response.status(200).send('null')
  }
}

// DOMAIN CHECK REQUESTS END
// ################################################################################################################
