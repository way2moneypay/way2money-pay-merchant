var TransactionService = module.exports
const mongoose = require('mongoose')
/* requiring utils */
var ResponseUtils = require('./../utils/ResponseUtils')
const AESNODE = require('./../utils/EncDecNode')

const this_file_name = 'TransactionService'

const MerchatProfileService = require('./MerchantProfileService')

/* requiring models */
const UserTransactionsModel = require('./../models/UserTransactionsModel')
const MerchantsModel = require('./../models/MerchantModel')
const SettingsModel = require('./../models/PaymentSettingsModel')

const async = require('async')

const AppConfig = require('config')

TransactionService.getTransactions = function (findObj, limits, callback) {
  let this_function_name = 'TransactionService.getTransactions'
  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Response = new ResponseUtils.response(callback)
  let default_error = 'Error while getting transactions list'
  let default_error_code = 'GET TRANSACTIONS ERROR'
  try {
    UserTransactionsModel.find(findObj, {}, limits, function (err_trans_find, transactionData) {
      if (err_trans_find) {
        console.log('TRANSACIONS FIND ERROR', err_trans_find)
        Logs.error(err_trans_find + '', 'ERROR TRANSACTION FOUND', 'TRANSACTION DATA ERROR')
        return callback(err_trans_find, null)
      }
      return callback(null, transactionData)
    }).sort({
      transaction_date: -1
    })
  } catch (exception) {
    Print.exception(exception + '')
    Logs.exception(exception + '', new Error(), default_error_code, default_error)
    return Response.error(default_error, default_error_code, 500)
  }
}

TransactionService.getTransactionsCount = function (api_request, api_response) {
  console.log('we are at transaction counts')
  let this_function_name = 'TransactionService.transactionsCount'
  console.log('api_request', api_request.body)
  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Response = new ResponseUtils.response(api_response)
  let default_error = 'Error while getting transactions count'
  let default_error_code = 'GET TRANSACTIONS COUNT ERROR'
  try {
    if (api_request.user && api_request.user._id) {
      var no_more_pages; var resultArray = []
      let merchant_id = mongoose.Types.ObjectId(api_request.user._id)
      let findObj = {
        'merchant_id': merchant_id
      }
      if (api_request.body.hasOwnProperty('transaction_id')) {
        findObj['_id'] = mongoose.Types.ObjectId(api_request.body.transaction_id)
      }
      if (api_request.body.hasOwnProperty('from_date') && api_request.body.hasOwnProperty('to_date')) {
        var from_date = new Date(api_request.body.from_date)
        var to_date = new Date(api_request.body.to_date)
        findObj['transaction_date'] = {
          $gte: from_date.setHours(0, 0, 0, 0),
          $lte: to_date.setHours(23, 59, 59, 59)
        }
      } else if (api_request.body.hasOwnProperty('from_date')) {
        var from_date = new Date(api_request.body.from_date).getTime()
        findObj['transaction_date'] = {
          $gte: from_date
        }
      }
      let responseTransObj = {}
      MerchatProfileService.getMerchantDetails(merchant_id, (merResp, merchatObj) => {
        if (merResp, merchatObj) {
          if (merchatObj.is_admin_verified) {
            UserTransactionsModel.countDocuments(findObj,
              function (err_trans_find, transactionCount) {
                // console.log('transactionCount', transactionCount)
                if (err_trans_find) {
                  console.log('TRANSACIONS FIND ERROR', err_trans_find)
                  Logs.error(err_trans_find + '', 'ERROR TRANSACTION FOUND', 'TRANSACTION DATA ERROR')
                } else if (transactionCount <= 0) {
                  responseTransObj.no_more_pages = true
                  responseTransObj.is_admin_verified = true
                  responseTransObj.message = 'No Transactions Found'
                  return Response.success('Success fetching transactions', responseTransObj)
                } else {
                  if (api_request.body.hasOwnProperty('page')) {
                    let page = Number(api_request.body.page)
                    let limit = 10
                    let totalCount = Number(transactionCount)
                    let skip = limit * (page - 1)
                    let limits = {
                      'skip': skip,
                      'limit': limit
                    }
                    module.exports.getTransactions(findObj, limits, function (transact_err, transact_result) {
                      if (transact_result && Object.keys(transact_result)) {
                        transact_result = JSON.parse(JSON.stringify(transact_result))
                        // if ((page * limit) < totalCount) {
                        SettingsModel.findOne({}, {
                          refund_period: true
                        }, function (settings_err, settings) {
                          const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Novr', 'Dec']
                          var currDate = Date.now()
                          async.eachSeries(transact_result, function (i, next) {
                            var trans_date = new Date(i.transaction_date)
                            let transStatus = i.status
                            let mStatus = i.m_status
                            transaction_date = new Date(i.transaction_date)
                            i.status = AppConfig.trans_status[i.status]
                            i.transaction_date = 'Transaction done on ' + transaction_date.getDate() + ' ' + months[transaction_date.getMonth()] + ' ' + transaction_date.getFullYear()
                            i['merchant_commission_amount'] = Number((((i.amount ? i.amount : 0) * i.merchant_commission) / 100).toFixed(2))
                            i['original_amount'] = Number(i.amount.toFixed(2))
                            if (i.trans_details) {
                              for (let j = 0; j < i.trans_details.length; j++) {
                                if (i.trans_details[j].status == 4) {
                                  let refund_date = new Date(i.trans_details[j].date)
                                  let refund_dt = refund_date.getDate() + ' ' + months[refund_date.getMonth()] + ' ' + refund_date.getFullYear()
                                  i['trans_msg'] = 'Refunded on ' + refund_dt
                                  break
                                } else if (i.trans_details[j].m_status == 8 || i.trans_details[j].m_status == 9) {
                                  let claimed_date = new Date(i.trans_details[j].date)
                                  let claimed_dt = claimed_date.getDate() + ' ' + months[claimed_date.getMonth()] + ' ' + claimed_date.getFullYear()
                                  i['trans_msg'] = 'Claimed on ' + claimed_dt
                                  break
                                } else i['trans_msg'] = 'RS. ' + i.original_amount + ' - RS.' + i.merchant_commission_amount + ' (W2M charges)'
                              }
                            } else i['trans_msg'] = 'RS. ' + i.original_amount + ' - RS.' + i.merchant_commission_amount + ' (W2M charges)'
                            i.amount -= i.merchant_commission_amount
                            i.amount = Number(i.amount.toFixed(2))
                            delete i.merchant_commission, delete i.user_id, delete i.remarks
                            delete i.con_fee, delete i.trans_amount, delete i.trans_details
                            delete i.merchant_id, delete i.transaction_for, delete i.transaction_status
                            var refundExpriryDate = trans_date.setHours(24 * settings.refund_period)
                            if (merchatObj.merchant_status == 1 || merchatObj.merchant_status == 2) {
                              if (refundExpriryDate > currDate) {
                                /**
                                 *  0 Don't display refund button
                                 *  1 Display refund button
                                 *  2 Refund period exceeded
                                 */
                                if (transStatus == 1 || mStatus == 6) {
                                  i.refund_status = 1
                                  next()
                                } else {
                                  i.refund_status = 0
                                  next()
                                }
                              } else {
                                i.refund_status = 0
                                next()
                              }
                            } else {
                              i.refund_status = 0
                              next()
                            }
                          }, function (err) {
                            MerchantsModel.findOne({
                              _id: api_request.user._id
                            }, function (err, data) {
                              if (err) {
                                Response.error({
                                  error_message: 'Merchants find error',
                                  error: err
                                }, 500)
                              } else {
                                if ((page * limit) < totalCount) {
                                  responseTransObj.no_more_pages = false
                                } else {
                                  responseTransObj.no_more_pages = true
                                }
                                pending_amt = data.pending_amt ? data.pending_amt : 0
                                approved_amt = data.approved_amt ? data.approved_amt : 0
                                responseTransObj.total_balance = Number((pending_amt + approved_amt).toFixed(2))
                                responseTransObj.transactions = transact_result
                                responseTransObj.is_admin_verified = true
                                // console.log("responseTransObj",responseTransObj);
                                return Response.success('Success fetching transactions', responseTransObj)
                              }
                            })
                          })
                        })
                      } else if (transact_err) {
                        console.log('TRANSACIONS FIND ERROR', transact_err)
                        Logs.error(transact_err + '', 'ERROR TRANSACTION FOUND', 'TRANSACTION DATA ERROR')
                      } else {
                        responseTransObj.no_more_pages = true
                        responseTransObj.is_admin_verified = true
                        responseTransObj.transactions = []
                        responseTransObj.message = 'No Transactions found'
                        return Response.success('Success fetching transactions', responseTransObj)
                      }
                    })
                  } else {
                    responseTransObj.no_more_pages = true
                    responseTransObj.is_admin_verified = true
                    responseTransObj.transactions = []
                    responseTransObj.message = 'No Transactions found'
                    console.log('responseTransObj', responseTransObj)
                    return Response.success('Success fetcing transactions', responseTransObj)
                  }
                }
              })
          } else {
            return Response.success('Merchant transaction data Success', {
              'is_admin_verified': false,
              'no_more_pages': true,
              'message': 'No Transactions Found'
            })
          }
        } else {
          return Response.error('error while checking admin verified status for merchant', 'ADMINVERIFYCHECK', 500, null)
        }
      })
    } else {
      Logs.error('Error! no user token data', default_error_code, default_error)
      return Response.error('Error! no user token data', 'DBERROR', 400)
    }
  } catch (exception) {
    Print.exception(exception + '')
    Logs.exception(exception + '', new Error(), default_error_code, default_error)
    return Response.error(default_error, default_error_code, 500)
  }
}

TransactionService.getMerchantDetails = (api_request, api_response) => {
  let this_function_name = 'TransactionService.getMerchantDetails'
  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Response = new ResponseUtils.response(api_response)
  let default_error = 'Error while getting merchant details'
  let default_error_code = 'GET MERCHANT DETAILS ERROR'

  try {
    // console.log(api_request.body);
    let token = api_request.body.token
    // console.log("merchant_id :: ", merchant_id);
    var decodedPbj = AESNODE.decrypt(token, AppConfig.jwtsecret)
    let token_data = JSON.parse(decodedPbj)
    // console.log(token_data);
    MerchantsModel.findOne({
      _id: token_data.id
    }, (err_find_merchant, merchant) => {
      if (err_find_merchant) {
        console.log('MERCHANT DETAILS FIND ERROR', err_find_merchant)
        Logs.error(err_find_merchant + '', 'ERROR MERCHANT FOUND', 'MERCHANT DATA ERROR')
        return Response.error(err_find_merchant, null)
      } else {
        // console.log(merchant);
        var merchant_details = {}
        merchant_details['id'] = merchant._id
        merchant_details['merchant_name'] = merchant.name
        merchant_details['display_name'] = merchant.brand_name
        merchant_details['mobile'] = merchant.mobile
        return Response.success(null, merchant_details)
      }
    })
  } catch (exception) {
    Print.exception(exception + '')
    Logs.exception(exception + '', new Error(), default_error_code, default_error)
    return Response.error(default_error, default_error_code, 500)
  }
}
