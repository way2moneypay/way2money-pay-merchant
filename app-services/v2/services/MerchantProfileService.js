const {
  isEmail
} = require('validator')
const ShortId = require('shortid')
const crypto = require('crypto')
const AppConfig = require('config')
const FileUploadUtils = require('./../utils/FileUtils')
const ResponseUtils = require('./../utils/ResponseUtils')
const MerchantModel = require('../models/MerchantModel')
const MerchantCommissionModel = require('../models/MerchantCommissionsModel')
const async = require('async')
const fs = require('fs')
const homedir = require('os').homedir()
const passwordHash = require('password-hash')
var image_url = 'https://business.way2money.com/'
var banks = ['Allahabad Bank', 'Andhra Bank', 'Axis Bank', 'Bank of Bahrain and Kuwait', 'Bank of Baroda - Corporate Banking', 'Bank of Baroda - Retail Banking', 'Bank of India', 'Bank of Maharashtra', 'Canara Bank', 'Central Bank of India', 'City Union Bank', 'Corporation Bank', 'Deutsche Bank', 'Development Credit Bank', 'Dhanlaxmi Bank', 'Federal Bank', 'ICICI Bank', 'IDBI Bank', 'Indian Bank', 'Indian Overseas Bank', 'IndusInd Bank', 'ING Vysya Bank', 'Jammu and Kashmir Bank', 'Karnataka Bank Ltd', 'Karur Vysya Bank', 'Kotak Bank', 'Laxmi Vilas Bank', 'Oriental Bank of Commerce', 'Punjab National Bank - Corporate Banking', 'Punjab National Bank - Retail Banking', 'Punjab & Sind Bank', 'Shamrao Vitthal Co-operative Bank', 'South Indian Bank', 'State Bank of Bikaner & Jaipur', 'State Bank of Hyderabad', 'State Bank of India', 'State Bank of Mysore', 'State Bank of Patiala', 'State Bank of Travancore', 'Syndicate Bank', 'Tamilnad Mercantile Bank Ltd.', 'UCO Bank', 'Union Bank of India', 'United Bank of India', 'Vijaya Bank', 'Yes Bank Ltd']
const MerchantProfileServices = module.exports

MerchantProfileServices.register = function (apiReq, apiRes) {
  let Response = new ResponseUtils.response(apiRes)
  var date = new Date()
  var updated_on = date / 1
  var regMerchantObj = apiReq.body
  var token_data = apiReq.user
  var merchantSetObj = {}
  let oldMerchant = {}
  // var hashedPassword = passwordHash.generate(regMerchantObj.password);
  var web_access_token = crypto.randomBytes(10).toString('hex')
  var web_secret_key = crypto.randomBytes(10).toString('hex')
  // var web_domains_array = regMerchantObj.web_domains.trim().split(',');
  if (regMerchantObj.step == 1) {
    merchantSetObj = regMerchantObj
    merchantSetObj.secret_key = ShortId.generate()
    merchantSetObj.web_access_token = web_access_token
    merchantSetObj.is_profile_verified = true
    merchantSetObj.web_secret_key = web_secret_key
    merchantSetObj.register_date = Date.now()
    merchantSetObj.brand_name = regMerchantObj.company
    MerchantModel.findOneAndUpdate({
      _id: token_data._id
    }, {
      $set: merchantSetObj
    }, {
      new: true
    }, (errMerchant, merchantObj) => {
      // console.log("merchant", merchantObj);
      if (errMerchant) {
        return Response.error('Something went wrong while registering merchant.', errMerchant, null)
      } else if (merchantObj) {
        merchantObj = JSON.parse(JSON.stringify(merchantObj))
        if (merchantSetObj.type == 1 && merchantObj.is_admin_verified == true && merchantObj.merchant_status != 0) {
          MerchantModel.findOneAndUpdate({
            _id: token_data._id
          }, { $set: { merchant_status: 2 } }, function (errupdateSatus, merchStaUpObj) {
            if (errupdateSatus) {
              console.log('Error while updating merchant staus 2')
            }
          })
        }
        merchantObj.banks = banks
        delete merchantObj.web_access_token
        delete merchantObj.secret_key
        delete merchantObj.web_secret_key
        delete merchantObj.token_validity
        delete merchantObj.merchant_status
        delete merchantObj.web_domains
        return Response.success('Merchant profile successfully updated.', merchantObj)
      } else {
        return Response.error('Merchant not found.', 'MERNOTFOUND', null)
      }
    })
  } else if (regMerchantObj.step == 2) {
    async.waterfall([
      (done) => {
        merchantSetObj = regMerchantObj
        merchantSetObj.ifsc_code = merchantSetObj.ifsc_code != undefined && merchantSetObj.ifsc_code != undefined ? merchantSetObj.ifsc_code.toUpperCase() : ''
        merchantSetObj.pan_name = merchantSetObj.pan_name != undefined && merchantSetObj.pan_name != undefined ? merchantSetObj.pan_name.toUpperCase() : ''
        merchantSetObj.tin = merchantSetObj.tin != undefined && merchantSetObj.tin != undefined ? merchantSetObj.tin.toUpperCase() : ''
        merchantSetObj.account_no = merchantSetObj.account_no != undefined && merchantSetObj.account_no != undefined ? merchantSetObj.account_no.toUpperCase() : ''
        module.exports.getMerchantDetails(token_data._id, (merResp, merchantObj) => {
          if (merResp) {
            if (merchantObj) {
              oldMerchant = merchantObj
              done(null)
            } else {
              return Response.error('merchant not found', 'MERNFINDERROR', null)
            }
          } else {
            return Response.error('err while finding merchant', 'MERNFINDERROR', null)
          }
        })
      },
      (done) => {
        merchantSetObj.pan_number = merchantSetObj.pan_number != null && merchantSetObj.pan_number != undefined ? merchantSetObj.pan_number.toUpperCase() : ''
        if (merchantSetObj.bank_name != oldMerchant.bank_name ||
          merchantSetObj.account_no != oldMerchant.account_no ||
          merchantSetObj.ifsc_code != oldMerchant.ifsc_code ||
          merchantSetObj.name_on_bank != oldMerchant.name_on_bank
        ) {
          merchantSetObj.bank_details = 1
        }
        if (merchantSetObj.pan_number != null && merchantSetObj.pan_number != undefined &&
          merchantSetObj.pan_name != null && merchantSetObj.pan_name != undefined && regMerchantObj.type == 0) {
          merchantSetObj.pan_detials = 1
        }
        MerchantModel.findOneAndUpdate({
          _id: token_data._id
        }, {
          $set: merchantSetObj
        }, {
          new: true
        }, (errMerchant, merchantObj) => {
          // console.log("merchant", merchantObj);
          if (errMerchant) {
            return Response.error('Something went wrong while registering merchant.', errMerchant, null)
          } else {
            merchantObj = JSON.parse(JSON.stringify(merchantObj))
            merchantObj.banks = banks
            delete merchantObj.web_access_token
            delete merchantObj.secret_key
            delete merchantObj.web_secret_key
            delete merchantObj.token_validity
            delete merchantObj.merchant_status
            delete merchantObj.web_domains
            return Response.success('Merchant profile successfully updated.', merchantObj)
          }
        })
      }
    ])
  } else {
    console.log('not step provied in request')
  }
}

MerchantProfileServices.getMerchantDetails = function (merchant_id, merchantCallback) {
  var merchantId = merchant_id
  MerchantModel.findById({
    _id: merchantId
  }, {
    password: false
  }, (errFind, merchantFound) => {
    // console.log("merchantFound",merchantFound);
    // console.log("errFind",errFind);
    if (errFind) {
      merchantCallback(0, null)
    } else {
      merchantCallback(1, merchantFound)
    }
  })
}

MerchantProfileServices.updateMerchant = function (apiReq, apiRes) {
  let Response = new ResponseUtils.response(apiRes)
  var merchantObj = apiReq.body
  var merchantId = apiReq.user._id
  // console.log("merchatObj", apiReq.body);
  module.exports.getMerchantDetails(merchantId, (merResp, merchantExistObj) => {
    if (merResp && merchantExistObj) {
      // if (merchatObj.is_admin_verified) {
      merchantObj.brand_name = merchantObj.brand_name.trim()
      // merchantObj.logo_url = merchantObj.logo_url.trim();
      if (typeof (merchantObj.web_domains) === 'string') {
        merchantObj.web_domains = merchantObj.web_domains.trim().split(',')
      } else {
        merchantObj.web_domains = merchantObj.web_domains[0].trim().split(',')
      }
      var updateMerchantProfile = {}
      if (merchantObj && !merchantObj.brand_name) {
        return Response.error('Brand name is required.', null)
      } else {
        MerchantModel.findOneAndUpdate({
          _id: merchantId
        }, {
          $set: merchantObj
        }, {
          new: true
        }, (errUpdate, profileUpdated) => {
          // console.log("profileUpdated", profileUpdated);
          // console.log("errUpdate",errUpdate);
          if (errUpdate) {
            return Response.error('Error while updating merchant profile.', null)
          } else {
            let responceObj = {}
            responceObj.merchant_profile = profileUpdated
            return Response.success('Merchant details updated successfully.', responceObj)
            // apiRes.send({
            //   success: true,
            //   message: 'Merchant details updated successfully.'
            // });
          };
        })
      }

      // } else {
      //   return Response.success("Merchant transaction data Success", { "is_admin_verified": false });
      // }
    } else {
      return Response.error('error while checking admin verified status for merchant', 'ADMINVERIFYCHECK', 500, null)
    }
  })
}

var unlinkLeadFile = function (upload_root_path, name) {
  // console.log('Unlink ::', upload_root_path + '/' + name + '.jpeg');
  let file_name = upload_root_path + '/' + name + '.jpeg'
  if (fs.existsSync(file_name)) fs.unlink(file_name, () => {})
  file_name = upload_root_path + '/' + name + '.jpg'
  if (fs.existsSync(file_name)) fs.unlink(file_name, () => {})
  file_name = upload_root_path + '/' + name + '.png'
  if (fs.existsSync(file_name)) fs.unlink(file_name, () => {})
  file_name = upload_root_path + '/' + name + '.pdf'
  if (fs.existsSync(file_name)) fs.unlink(file_name, () => {})
}

MerchantProfileServices.uploadDoc = (apiReq, apiRes) => {
  // console.log("apiReq", apiReq.query);
  let Response = new ResponseUtils.response(apiRes)
  var token_data = apiReq.user
  module.exports.getMerchantDetails(token_data._id, (merResp, merchatObj) => {
    if (merResp && merchatObj) {
      // if (merchatObj.is_admin_verified) {
      var file_extension = apiReq.query.file_extension
      var image_type = apiReq.query.image_type
      var reqType = apiReq.query.type
      var file_name = apiReq.query.file_name + '_' + image_type + file_extension
      // console.log("req file_name", file_name);
      var upload_directory = __dirname + '../../../../uploads'
      let upload_root_path = upload_directory + '/' + file_name
      unlinkLeadFile(upload_directory, apiReq.query.file_name + '_' + image_type)
      let FileUpload = new FileUploadUtils.upload(apiReq, apiRes, upload_root_path, upload_directory, file_name, 'file', null, true, 'single')
      FileUpload.exec(function (upload_error, upload_file_name) {
        if (upload_error) {
          return Response.error('Error while Uploading Image', null)
        } else {
          var imageUpload = {}
          var imageUrl = image_url + file_name
          var randomNumber = Math.random()
          imageUpload[image_type] = imageUrl + '?q=' + randomNumber
          // console.log("image_url", image_url);
          if (image_type == 'pan_card') {
            imageUpload.pan_details = 2
          }
          MerchantModel.update({
            _id: token_data._id
          }, {
            $set: imageUpload
          },
          (errUpdateLogoPath, logoPathUpdated) => {
            if (errUpdateLogoPath) {
              return Response.error('Error while updating file url in DB.', null)
            } else {
              var resObj = {}
              resObj.image_url = imageUrl + '?q=' + randomNumber
              resObj.is_admin_verified = merchatObj.is_admin_verified
              // console.log("resObj", resObj);
              return Response.success('File successfully uploaded.', resObj)
            }
          })
        }
      })
      // } else {
      //   return Response.success("Merchant transaction data Success", { "is_admin_verified": false });
      // }
    } else {
      return Response.error('error while checking admin verified status for merchant or merchant not found', 'ADMINVERIFYCHECK', 500, null)
    }
  })
}

MerchantProfileServices.getDetails = function (apiReq, apiRes) {
  let Response = new ResponseUtils.response(apiRes)
  var token_data = apiReq.user
  // console.log('get details token data', token_data);
  MerchantModel.findById({
    _id: token_data._id
  }, {
    register_date: false,
    web_access_token: false,
    web_secret_key: false,
    secret_key: false,
    pending_amt: false,
    approved_amt: false,
    claimed_amt: false,
    updated_on: false,
    register_by: false,
    password: false
  }, (errFind, merchant) => {
    if (errFind) {
      return Response.error('Error while fetching merchant details.', null)
    } else {
      let merchantObj = JSON.parse(JSON.stringify(merchant))
      check_fields = ['bank_name', 'name_on_bank', 'pan_number', 'pan_name', 'account_no', 'tin', 'ifsc_code']
      for (var i in check_fields) {
        if (!merchantObj[check_fields[i]]) { delete merchantObj[check_fields[i]] }
      }
      // merchantObj.web_domains = merchant.web_domains.toString();
      merchantObj.company = merchantObj.brand_name
      merchantObj.banks = banks
      return Response.success('Merchant details successfully fetched.', merchantObj)
    }
  })
}
