var DashboardService = module.exports
const async = require('async')

const this_function_name = 'DashboardService'
const mongoose = require('mongoose')
const config = require('config')

const MerchatProfileService = require('./MerchantProfileService')

/* requiring required models */
const Transactions = require('../models/UserTransactionsModel')
const MerchantModel = require('../models/MerchantModel')
const SettingsModel = require('../models/PaymentSettingsModel')
const MerchantStatsModel = require('../models/MerchantStatsModel')

/* requiring utils */
var ResponseUtils = require('./../utils/ResponseUtils')
let this_file_name = 'DashboardService.getUserDashboardDetails'

/* Ramu */
/* get user dashboard data */
DashboardService.getUserDashboardDetails = function (api_request, api_response) {
  let this_function_name = 'DashboardService.getUserDashboardDetails'
  let Response = new ResponseUtils.response(api_response)
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)

  let default_error = 'Error while getting user dash board details.'
  let default_error_code = 'DASHBOARDERROR'

  try {
    let id = api_request.user._id
    // let id = "5b5370cf2754ea451292b496";
    MerchatProfileService.getMerchantDetails(id, (merResp, merchatObj) => {
      // console.log("merchatObj",merchatObj.is_admin_verfied)
      if (merResp && merchatObj) {
        if (merchatObj.is_admin_verified) {
          let default_days = 30
          if (api_request.body.hasOwnProperty('days')) {
            async.parallel([
              function (callback) {
                var fromDay = new Date()
                fromDay.setHours(0, 0, 0, 0)
                fromDay.setDate(fromDay.getDate() - api_request.body.days)
                MerchantStatsModel.find({
                  merchant_id: id,
                  trans_date: {
                    $gte: fromDay.getTime()
                  }
                },
                function (mongo_err, mongo_res) {
                  if (mongo_err) {
                    callback({
                      error: 'Error in merchants report graph',
                      error: mongo_err
                    }, null)
                  } else {
                    var dates = []
                    var counts = []
                    var amount = []
                    var inc = 0
                    async.eachSeries(mongo_res, function (i, next) {
                      if (dates.length == 0) {
                        day = new Date(i.trans_date)
                        day.setHours(0, 0, 0, 0)
                        dates[inc] = day.getTime()
                        counts[inc] = 1
                        amount[inc] = i.trans_amount
                        next()
                      } else {
                        async.eachOfSeries(dates, function (val, j, next_val) {
                          day = new Date(i.trans_date)
                          day.setHours(0, 0, 0, 0)
                          if (val == day.getTime()) {
                            counts[j] += 1
                            amount[j] += i.trans_amount
                            next()
                          } else {
                            next_val()
                          }
                        }, function () {
                          inc++
                          day.setHours(0, 0, 0, 0)
                          dates[inc] = day.getTime()
                          counts[inc] = 1
                          amount[inc] = i.trans_amount
                          next()
                        })
                      }
                    }, function () {
                      callback(null, {
                        dates: dates,
                        counts: counts,
                        amount: amount
                      })
                    })
                  }
                }).sort({
                  trans_date: 1
                })
              },
              function (callback) {
                var fromDay = new Date()
                fromDay.setHours(0, 0, 0, 0)
                fromDay.setDate(fromDay.getDate() - api_request.body.days)
                Transactions.find({
                  merchant_id: id,
                  transaction_date: {
                    $gte: fromDay.getTime()
                  }
                },
                function (mongo_err, mongo_res) {
                  if (mongo_err) {
                    callback({
                      error: 'Error in users report graph',
                      error_message: mongo_res
                    }, null)
                  } else {
                    var dates = []
                    var counts = []
                    var inc = 0
                    async.eachSeries(mongo_res, function (i, next) {
                      if (dates.length == 0) {
                        day = new Date(i.transaction_date)
                        day.setHours(0, 0, 0, 0)
                        dates[inc] = day / 1
                        counts[inc] = 1
                        next()
                      } else {
                        async.eachOfSeries(dates, function (val, j, next_val) {
                          day = new Date(i.transaction_date)
                          day.setHours(0, 0, 0, 0)
                          if (val == day / 1) {
                            counts[j] += 1
                            next()
                          } else {
                            next_val()
                          }
                        }, function () {
                          inc++
                          day.setHours(0, 0, 0, 0)
                          dates[inc] = day / 1
                          counts[inc] = 1
                          next()
                        })
                      }
                    }, function () {
                      callback(null, {
                        dates: dates,
                        counts: counts
                      })
                    })
                  }
                }).sort({
                  transaction_date: 1
                })
              }
            ],
            function (err, results) {
              if (err) {
                return Response.error(err)
              } else if (results) {
                Array.prototype.unique = function () {
                  var a = this.concat()
                  for (var i = 0; i < a.length; ++i) {
                    for (var j = i + 1; j < a.length; ++j) {
                      if (a[i] === a[j]) { a.splice(j--, 1) }
                    }
                  }
                  return a
                }
                const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Novr', 'Dec']
                var total_days = results[0].dates.concat(results[1].dates).unique()
                total_days = total_days.sort()
                var user_dates = []

                var transactions_dates = []
                var users_count = []

                var transactions_count = []
                var labels = []
                var transactions_amount = []

                for (let i = 0; i < total_days.length; i++) {
                  var day = new Date(total_days[i])
                  labels.push(months[day.getMonth()] + ' ' + day.getDate())
                  if ((results[0].dates.indexOf(total_days[i]) != -1)) {
                    users_count.push(results[0].counts[results[0].dates.indexOf(total_days[i])])
                    transactions_amount.push(results[0].amount[results[0].dates.indexOf(total_days[i])])
                  } else {
                    transactions_amount.push(0)
                    users_count.push(0)
                  }
                  if ((results[1].dates.indexOf(total_days[i]) != -1)) {
                    transactions_count.push(results[1].counts[results[1].dates.indexOf(total_days[i])])
                  } else {
                    transactions_count.push(0)
                  }
                }
                merchant_reports = []
                for (var i in labels) {
                  if (i != 'unique') {
                    merchant_reports.push({
                      date: labels[i],
                      total_users: users_count[i],
                      total_transactions: transactions_count[i],
                      total_trans_amount: Number(transactions_amount[i].toFixed(2))
                    })
                  }
                }
                console.log('responceee', merchant_reports)
                var merRespObj = {}
                merRespObj.is_admin_verified = true
                merRespObj.merchant_reports = merchant_reports
                if (merchant_reports.length == 0) {
                  merRespObj.message = 'No Reports Found'
                }
                return Response.success('DASHBOARD-GRAPH-REPORT-BY-DAYS-FETCHED', merRespObj)
              }
            })
          } else {
            async.parallel([
              /* Last five transactions */
              function (callback) {
                Transactions.find({
                  merchant_id: id
                }, {
                  _id: true,
                  mobile_no: true,
                  reference_id: true,
                  trans_amount: true,
                  amount: true,
                  transaction_date: true,
                  trans_details: true,
                  status: true,
                  refund_status: true,
                  merchant_commission: true
                }, function (err_trans_find, TransactionData) {
                  if (err_trans_find) {
                    callback({
                      error_message: 'MONGO-ERROR-IN-LAST-TRANSACTION',
                      error: err_trans_find
                    })
                  } else {
                    SettingsModel.findOne({}, {
                      refund_period: true
                    }, function (settings_err, settings) {
                      TransactionData = JSON.parse(JSON.stringify(TransactionData))
                      const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Novr', 'Dec']
                      var currDate = Date.now()
                      async.eachSeries(TransactionData, function (i, next) {
                        // console.log("i---",i);
                        var trans_date = new Date(i.transaction_date)
                        let transStatus = i.status
                        let mStatus = i.m_status
                        transaction_date = new Date(i.transaction_date)
                        i.status = config.trans_status[i.status]
                        i.transaction_date = 'Transaction done on ' + transaction_date.getDate() + ' ' + months[transaction_date.getMonth()] + ' ' + transaction_date.getFullYear()
                        i['original_amount'] = Number(i.amount.toFixed(2))
                        i['merchant_commission_amount'] = Number((((i.amount ? i.amount : 0) * i.merchant_commission) / 100).toFixed(2))
                        if (i.trans_details) {
                          for (let j = 0; j < i.trans_details.length; j++) {
                            if (i.trans_details[j].status == 4) {
                              let refund_date = new Date(i.trans_details[j].date)
                              let refund_dt = refund_date.getDate() + ' ' + months[refund_date.getMonth()] + ' ' + refund_date.getFullYear()
                              i['trans_msg'] = 'Refunded on ' + refund_dt
                              break
                            } else if (i.trans_details[j].m_status == 8 || i.trans_details[j].m_status == 9) {
                              let claimed_date = new Date(i.trans_details[j].date)
                              let claimed_dt = claimed_date.getDate() + ' ' + months[claimed_date.getMonth()] + ' ' + claimed_date.getFullYear()
                              i['trans_msg'] = 'Claimed on ' + claimed_dt
                              break
                            } else i['trans_msg'] = 'RS. ' + i.original_amount + ' - RS.' + i.merchant_commission_amount + ' (W2M charges)'
                          }
                        } else i['trans_msg'] = 'RS. ' + i.original_amount + ' - RS.' + i.merchant_commission_amount + ' (W2M charges)'
                        i.amount -= i.merchant_commission_amount
                        i.amount = Number(i.amount.toFixed(2))
                        delete i.merchant_commission
                        delete i.trans_details
                        var refundExpriryDate = trans_date.setHours(24 * settings.refund_period)
                        if (merchatObj.merchant_status == 1 || merchatObj.merchant_status == 2) {
                          if (refundExpriryDate > currDate) {
                            if (transStatus == 1 || mStatus == 6) {
                              i.refund_status = 1
                              next()
                            } else {
                              i.refund_status = 0
                              next()
                            }
                          } else {
                            i.refund_status = 0
                            next()
                          }
                        } else {
                          i.refund_status = 0
                          next()
                        }
                      }, function (err) {
                        return callback(null, TransactionData)
                      })
                    })
                  }
                }).sort({
                  transaction_date: -1
                }).limit(5)
              },
              function (callback) {
                MerchantModel.findOne({
                  _id: id
                },
                function (mongo_err, mongo_res) {
                  if (mongo_err) {
                    callback({
                      error_message: 'MONGO-ERROR-IN-MERCHANTS-AMOUNT',
                      error: mongo_err
                    }, null)
                  } else if (mongo_res) {
                    console.log(mongo_res.bank_details, mongo_res.pan_details)
                    let details_msg
                    let show_update_profile = true
                    let bank_details = mongo_res.bank_details ? mongo_res.bank_details : 0
                    let pan_details = mongo_res.pan_details ? mongo_res.pan_details : 0

                    if (bank_details == 0 && (pan_details == 0 || pan_details == 1)) { details_msg = 'Dear Merchant, your bank details and pan details are not submitted. Please submit ASAP for verification' }
                    if (bank_details == 0 && pan_details == 2) { details_msg = 'Dear Merchant, your bank details are not submitted. Please submit ASAP for verification.' }
                    if (bank_details == 0 && pan_details == 3) { details_msg = 'Dear Merchant, your bank details are not submitted. Please submit ASAP for verification.' }

                    if (bank_details == 1 && (pan_details == 0 || pan_details == 1)) { details_msg = 'Dear Merchant, your pan details are not submitted. Please submit ASAP for verification.' }
                    if (bank_details == 1 && pan_details == 2) {
                      details_msg = 'Dear Merchant, your bank details and pan details are under verification. Please have patience untill verified'
                      show_update_profile = false
                    }
                    if (bank_details == 1 && pan_details == 3) {
                      details_msg = 'Dear Merchant, your bank details are under verification. Please have patience untill verified'
                      show_update_profile = false
                    }

                    if (bank_details == 2 && (pan_details == 0 || pan_details == 1)) { details_msg = 'Dear Merchant, your pan details are not submitted. Please submit ASAP for verification.' }
                    if (bank_details == 2 && pan_details == 2) {
                      details_msg = 'Dear Merchant, your pan details are under verification. Please have patience untill verified'
                      show_update_profile = false
                    }
                    if (bank_details == 2 && pan_details == 3) {
                      show_update_profile = false
                    }

                    pending_amt = mongo_res.pending_amt ? mongo_res.pending_amt : 0
                    approved_amt = mongo_res.approved_amt ? mongo_res.approved_amt : 0
                    claimed_amt = mongo_res.claimed_amt ? mongo_res.claimed_amt : 0,

                    callback(null, {
                      show_update_profile: show_update_profile,
                      details_msg: details_msg,
                      total_claimed_amt: claimed_amt,
                      total_balance_amt: pending_amt + approved_amt
                    })
                  }
                })
              }
            ],
            function (err, results) {
              if (err) {
                return Response.error(err, 500)
              } else if (results) {
                var dashBoardObj = {}
                dashBoardObj.is_admin_verified = true
                dashBoardObj.last_transactions = results[0]
                if (results[0].length == 0) {
                  dashBoardObj.message = 'No Transactions Found'
                }
                dashBoardObj.details_msg = results[1].details_msg
                dashBoardObj.show_update_profile = results[1].show_update_profile
                dashBoardObj.total_claimed = Number((results[1].total_claimed_amt).toFixed(2))
                dashBoardObj.total_balance = Number((results[1].total_balance_amt).toFixed(2))
                return Response.success('DASHBOARD-DATA-SUCCESSFULLY-FETCHED', dashBoardObj)
              }
            })
          }
        } else {
          MerchantModel.findOne({
            _id: id
          },
          function (mongo_err, mongo_res) {
            if (mongo_err) {
              return Response.error('MONGO-ERROR-IN-MERCHANTS-AMOUNT', 'ADMIN FALSE MONGO ERROR', null)
            } else if (mongo_res) {
              let details_msg
              let show_update_profile = true
              let bank_details = mongo_res.bank_details ? mongo_res.bank_details : 0
              let pan_details = mongo_res.pan_details ? mongo_res.pan_details : 0

              if (bank_details == 0 && (pan_details == 0 || pan_details == 1)) { details_msg = 'Dear Merchant, your bank details and pan details are not submitted. Please submit ASAP for verification' }
              if (bank_details == 0 && pan_details == 2) { details_msg = 'Dear Merchant, your bank details are not submitted. Please submit ASAP for verification.' }
              if (bank_details == 0 && pan_details == 3) { details_msg = 'Dear Merchant, your bank details are not submitted. Please submit ASAP for verification.' }

              if (bank_details == 1 && (pan_details == 0 || pan_details == 1)) { details_msg = 'Dear Merchant, your pan details are not submitted. Please submit ASAP for verification.' }
              if (bank_details == 1 && pan_details == 2) {
                details_msg = 'Dear Merchant, your bank details and pan details are under verification. Please have patience untill verified'
                show_update_profile = false
              }
              if (bank_details == 1 && pan_details == 3) {
                details_msg = 'Dear Merchant, your bank details are under verification. Please have patience untill verified'
                show_update_profile = false
              }

              if (bank_details == 2 && (pan_details == 0 || pan_details == 1)) { details_msg = 'Dear Merchant, your pan details are not submitted. Please submit ASAP for verification.' }
              if (bank_details == 2 && pan_details == 2) {
                details_msg = 'Dear Merchant, your pan details are under verification. Please have patience untill verified'
                show_update_profile = false
              }
              if (bank_details == 2 && pan_details == 3) {
                show_update_profile = false
              }

              return Response.success('Mechant Dash board data Success', {
                'is_admin_verified': false,
                'details_msg': details_msg,
                'show_update_profile': show_update_profile,
                'message': 'No Transactions Found'
              })
            } else {
              return Response.error('MERCHANT NOT FOUND', 'MERNOTEXIST', null)
            }
          })
        }
      } else {
        return Response.error('error while checking admin verified status for merchant', 'ADMINVERIFYCHECK', 500, null)
      }
    })
    //   return Response.error(user_details_messages, user_details_error_code, user_details.code);
    // return Response.success("Successfully get dashboard data", dashboard_data);
  } catch (exception) {
    Print.exception(exception + '')
    Logs.exception(exception + '', default_error_code, default_error)
    return Response.error(default_error, default_error_code, 500, null)
  }
}
