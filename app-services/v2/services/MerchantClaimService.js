var MerchantClaimService = module.exports
var async = require('async')
const mongoose = require('mongoose')
const MerchantProfileService = require('./MerchantProfileService')

var moment = require('moment')
/* Requiring models */
var MerchantClaimModel = require('../models/MerchantClaimsModel')
var MerchantsModel = require('../models/MerchantModel')
/* requiring utils */
var ResponseUtils = require('./../utils/ResponseUtils')
let this_file_name = 'RefundService'

MerchantClaimService.claimNow = function (api_request, api_response) {
  default_error = 'Error while merchants claim'
  default_error_code = 'CLAIM-ERROR'
  let this_function_name = 'MerchantClaimService.claimNow'
  let Response = new ResponseUtils.response(api_response)
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  try {
    MerchantClaimModel.findOne({
      merchant_id: api_request.user._id,
      claim_status: 0
    }, function (MerchantFindErr, MerchantFindRes) {
      if (MerchantFindErr) return Response.error('MongoError : Merchant find error', MerchantFindErr, 500)
      else if (MerchantFindRes) {
        return Response.success("Dear, Merchant you're last claimed amount is pending please be patient and try again", '', 500)
      } else {
        Claim = new MerchantClaimModel({
          merchant_id: api_request.user._id,
          amount: api_request.body.amount.toFixed(2),
          request_date: Date.now(),
          claim_status: 0
        })
        Claim.save(function (ClaimErr, ClaimSaved) {
          if (ClaimErr) return Response.error('MongoError : MerchantsClaim save error', ClaimErr, 500)
          else return Response.success('Successfully claimed')
        })
      }
    })
  } catch (exception) {
    Print.exception(exception + '')
    Logs.exception(exception + '', default_error_code, default_error)
    return Response.error(default_error, default_error_code, 500, null)
  }
}

MerchantClaimService.getClaimHistory = function (merchant_id, skip, limit, callback) {
  var limits = {}

  var findObj = {}
  let this_function_name = 'MerchantClaimService.getClaimHistory'
  // console.log("api_request",api_request.query);
  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Response = new ResponseUtils.response(callback)
  let default_error = 'Error while getting claims history list'
  let default_error_code = 'GET CLAIM HISTORY ERROR'
  try {
    limits = {
      skip: skip,
      limit: limit
    }
    findObj.merchant_id = merchant_id
    var total_claim_amount, claim_history_data
    // console.log("findObj", findObj);
    async.parallel([
      function (parallelCallback) {
        MerchantsModel.findById({
          _id: merchant_id
        }, {
          claimed_amt: 1,
          _id: 0
        }, function (find_err, findResult) {
          if (find_err) {
            console.log('Claim amount find error', find_err)
            Logs.error(find_err + '', 'ERROR CLAIM AMOUNT FOUND', 'CLAIM AMOUNT DATA ERROR')
            parallelCallback(find_err, null)
          } else {
            total_claim_amount = findResult.claimed_amt
            console.log(total_claim_amount)
            parallelCallback(null, '1_done')
          }
        })
      },
      function (parallelCallback) {
        MerchantClaimModel.find(findObj, {
          request_date: 1,
          ref_id: 1,
          amount: 1,
          _id: 0
        }, limits, function (err_claim_history_find, claimHistoryData) {
          if (err_claim_history_find) {
            console.log('CLAIM HISTORY FIND ERROR', err_claim_history_find)
            Logs.error(err_claim_history_find + '', 'ERROR CLAIM HISTORY FOUND', 'CLAIM HISTORY DATA ERROR')
            parallelCallback(err_claim_history_find, null)
          } else {
            claim_history_data = claimHistoryData
            parallelCallback(null, '2_done')
          }
        })
        // console.log('claimHistoryData ', claimHistoryData);
      }
    ], function (err, results) {
      if (err) {
        return callback(err, null, null)
      } else {
        console.log(results)
        return callback(null, claim_history_data, total_claim_amount)
      }
    })

    // MerchantClaimModel.find(findObj, {
    //   request_date: 1,
    //   ref_id: 1,
    //   amount: 1,
    //   _id: 0
    // }, limits, function (err_claim_history_find, claimHistoryData) {
    //   if (err_claim_history_find) {
    //     console.log("CLAIM HISTORY FIND ERROR", err_claim_history_find);
    //     Logs.error(err_claim_history_find + '', "ERROR CLAIM HISTORY FOUND", "CLAIM HISTORY DATA ERROR")
    //     return callback(err_claim_history_find, null);
    //   }

    //   // console.log('claimHistoryData ', claimHistoryData);
    //   return callback(null, claimHistoryData);
    // });
  } catch (exception) {
    Print.exception(exception + '')
    Logs.exception(exception + '', new Error(), default_error_code, default_error)
    return Response.error(default_error, default_error_code, 500)
  }
}

MerchantClaimService.getClaimHistoryCount = function (api_request, api_response) {
  console.log('we are at claim history counts')
  let this_function_name = 'MerchantClaimService.getClaimHistoryCount'
  // console.log("api_request",api_request.user)
  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Response = new ResponseUtils.response(api_response)
  let default_error = 'Error while getting claim history count'
  let default_error_code = 'GET CLAIM HISTORY  ERROR'
  try {
    if (api_request.user && api_request.user._id) {
      let merchant_id = mongoose.Types.ObjectId(api_request.user._id)
      let responseClaimHistoryObj = {}
      MerchantProfileService.getMerchantDetails(merchant_id, (merResp, merchatObj) => {
        if (merResp, merchatObj) {
          if (merchatObj.is_admin_verified) {
            MerchantClaimModel.countDocuments({
              'merchant_id': merchant_id
            }, function (err_claim_history_count_find, claimHistoryCount) {
              if (err_claim_history_count_find) {
                console.log('CLAIM HISTORY COUNT ERROR', err_claim_history_count_find)
                Logs.error(err_claim_history_count_find + '', 'ERROR CLAIM HISTORY COUNT FOUND', 'CLAIM HISTORY COUNT FIND ERROR')
              } else if (claimHistoryCount <= 0) {
                responseClaimHistoryObj.no_more_pages = true
                responseClaimHistoryObj.is_admin_verified = true
                responseClaimHistoryObj.total_claimed_amount = 0
                return Response.success('Success fetching claimHistory', responseClaimHistoryObj)
              } else {
                if (api_request.body.hasOwnProperty('page')) {
                  let page = Number(api_request.body.page)
                  let limit = 10
                  let totalCount = Number(claimHistoryCount)
                  let skip = limit * (page - 1)
                  module.exports.getClaimHistory(merchant_id, skip, limit, function (claim_history_find_error, claim_history_result, total_claimed_amount) {
                    if (claim_history_result && total_claimed_amount) {
                      claim_history_result = JSON.parse(JSON.stringify(claim_history_result))
                      async.eachSeries(claim_history_result, function (single_claim_history, single_claim_history_callback) {
                        single_claim_history.request_date = 'Claimed On : ' + moment(single_claim_history.request_date).format('MMMM DD YYYY')
                        single_claim_history.amount = 'Claimed Amount : ' + single_claim_history.amount.toFixed(2)
                        if (single_claim_history.ref_id) {
                          single_claim_history.ref_id = 'Reference Id : ' + single_claim_history.ref_id
                        }
                        single_claim_history_callback()
                      }, function (success) {
                        if ((page * limit) < totalCount) {
                          responseClaimHistoryObj.no_more_pages = false
                          responseClaimHistoryObj.is_admin_verified = true
                          responseClaimHistoryObj.total_claimed_amount = total_claimed_amount
                          responseClaimHistoryObj.claim_history = claim_history_result
                          return Response.success('Success fetching claimHistory', responseClaimHistoryObj)
                        } else {
                          responseClaimHistoryObj.no_more_pages = true
                          responseClaimHistoryObj.total_claimed_amount = total_claimed_amount
                          responseClaimHistoryObj.is_admin_verified = true
                          responseClaimHistoryObj.claim_history = claim_history_result
                          return Response.success('Success fetching claimHistory', responseClaimHistoryObj)
                        }
                      })
                    } else if (claim_history_find_error) {
                      console.log('TRANSACIONS FIND ERROR', claim_history_find_error)
                      Logs.error(claim_history_find_error + '', 'ERROR CLAIM HISTORY FOUND', 'CLAIM HISTORY DATA ERROR')
                    } else {
                      responseClaimHistoryObj.no_more_pages = true
                      responseClaimHistoryObj.is_admin_verified = true
                      responseClaimHistoryObj.claim_history = claim_history_result
                      responseClaimHistoryObj.total_claimed_amount = 0
                      responseClaimHistoryObj.message = 'No Claims History Found'
                      return Response.success('Success fetching claimHistory', responseClaimHistoryObj)
                    }
                  })
                } else {
                  responseClaimHistoryObj.no_more_pages = true
                  responseClaimHistoryObj.is_admin_verified = true
                  responseClaimHistoryObj.total_claimed_amount = 0
                  esponseClaimHistoryObj.message = 'No Claims History Found'
                  return Response.success('Success fetcing claimHistory', responseClaimHistoryObj)
                }
              }
            })
          } else {
            return Response.success('Merchant claim history data Success', {
              'is_admin_verified': false
            })
          }
        } else {
          return Response.error('error while checking admin verified status for merchant', 'ADMINVERIFYCHECK', 500, null)
        }
      })
    } else {
      Logs.error('Error! no user token data', default_error_code, default_error)
      return Response.error('Error! no user token data', 'DBERROR', 400)
    }
  } catch (exception) {
    Print.exception(exception + '')
    Logs.exception(exception + '', new Error(), default_error_code, default_error)
    return Response.error(default_error, default_error_code, 500)
  }
}
