var UserService = module.exports
const AppConfig = require('config')
const JWT = require('jsonwebtoken')
const moment = require('moment')
const mongoose = require('mongoose')

/* requiring models */
const userDeviceMOdel = require('./../models/UserDevicesModel')
const MerchantOTPModel = require('../models/MerchantOTPModel')
const MerchantModel = require('../models/MerchantModel')
const UserDetailsModel = require('./../models/UserDetailsModel')
const AuthenticateInformationModel = require('../models/AuthenticateInformationModel')

/* requiring utils */
const GeneralUtils = require('./../utils/GeneralUtils')
const NotificationUtils = require('./../utils/NotificationUtils')
const ResponseUtils = require('./../utils/ResponseUtils')
const Logs = new ResponseUtils.LogsCommon()
const Print = new ResponseUtils.PrintCommon()

const this_file_name = './UserService.js'

var banks = ['Allahabad Bank', 'Andhra Bank', 'Axis Bank', 'Bank of Bahrain and Kuwait', 'Bank of Baroda - Corporate Banking', 'Bank of Baroda - Retail Banking', 'Bank of India', 'Bank of Maharashtra', 'Canara Bank', 'Central Bank of India', 'City Union Bank', 'Corporation Bank', 'Deutsche Bank', 'Development Credit Bank', 'Dhanlaxmi Bank', 'Federal Bank', 'ICICI Bank', 'IDBI Bank', 'Indian Bank', 'Indian Overseas Bank', 'IndusInd Bank', 'ING Vysya Bank', 'Jammu and Kashmir Bank', 'Karnataka Bank Ltd', 'Karur Vysya Bank', 'Kotak Bank', 'Laxmi Vilas Bank', 'Oriental Bank of Commerce', 'Punjab National Bank - Corporate Banking', 'Punjab National Bank - Retail Banking', 'Punjab & Sind Bank', 'Shamrao Vitthal Co-operative Bank', 'South Indian Bank', 'State Bank of Bikaner & Jaipur', 'State Bank of Hyderabad', 'State Bank of India', 'State Bank of Mysore', 'State Bank of Patiala', 'State Bank of Travancore', 'Syndicate Bank', 'Tamilnad Mercantile Bank Ltd.', 'UCO Bank', 'Union Bank of India', 'United Bank of India', 'Vijaya Bank', 'Yes Bank Ltd']

UserService.sendOTPToMerchant = function (api_request, api_response) {
  var sendOtpStartTime = Date.now()
  console.log('Reqest Came For Send Otp at Time----->', sendOtpStartTime)
  let Response = new ResponseUtils.response(api_response)

  try {
    let mobile = Number(api_request.body.mobile)
    if (!mobile) {
      console.log('No Mobile found in request', mobile)
      return Response.error('No Mobile number sent.', 'BAD-PARAMS', 400)
    } else {
      status_data = {}
      SendOTP(mobile, false, (send_otp_error, send_otp_code, send_otp_message, send_otp_data) => {
        var sendOtpEndTime = Date.now()
        console.log('Responce for Send Otp at Time----->', sendOtpEndTime)

        console.log('Time Difference for Send Otp  Time----->', sendOtpStartTime - sendOtpEndTime)
        if (send_otp_error) {
          return Response.error(send_otp_message, send_otp_code, 500, send_otp_data)
        }
        // status_data.otp = send_otp_data
        return Response.success('Successfully sent otp.', status_data)
      })
    }
  } catch (exception) {
    Print.exception(exception + '', exception)
    Logs.exception('Exception occurred.', 'EXCEPTION', exception + '', new Error())
    return Response.error('Internal Server Error', 'EXCEPTION', 500)
  }
}

UserService.verifyMerchantOTP = function (api_request, api_response) {
  console.log('api_request', api_request.body)
  let Response = new ResponseUtils.response(api_response)

  let mobile_number = api_request.body.mobile_number
  let authenticate_details = api_request.body
  authenticate_details.ip = GeneralUtils.getIP(api_request)

  try {
    if (!api_request.body.mobile || !api_request.body.otp) {
      console.error('Mobile (OR) OTP not sent.')
      return Response.error('Mobile (OR) OTP not sent.', 'BAD-PARAMS', 400)
    }

    let mobile_filter = {}
    mobile_filter.mobile = api_request.body.mobile
    let saveObj = {}
    let deviceDetailsObj = {}
    let displayFilter = {}
    displayFilter.secret_key = 0
    displayFilter.web_access_token = 0
    displayFilter.web_secret_key = 0
    displayFilter.web_domains = 0
    displayFilter.merchant_status = 0
    displayFilter.token_validity = 0
    saveObj.mobile = api_request.body.mobile
    saveObj.register_date = Date.now()
    saveObj.source = api_request.body.source
    saveObj.pn_id = api_request.body.pn_id
    deviceDetailsObj = api_request.body.device_details
    if (deviceDetailsObj != undefined) {
      deviceDetailsObj.user_type = 'merchant'
    }
    VerifyOtp(api_request.body.mobile, api_request.body.otp, false, (otp_verify_error, verify_otp_code, verify_otp_message) => {
      if (otp_verify_error) {
        UserService.AuthenticateInformationStore(null, 'login', mobile_number, 'failure', verify_otp_code, authenticate_details, () => { })
        return Response.error(verify_otp_message, verify_otp_code, 500)
      }

      MerchantModel.findOne(mobile_filter, displayFilter, (merchant_find_error, merchant_data) => {
        if (merchant_find_error) {
          UserService.AuthenticateInformationStore(null, 'login', mobile_number, 'failure', 'DB-ERROR', authenticate_details, () => { })
          console.log('Error while finding the merchant details in mobile login.', merchant_find_error)
          return Response.error('Internal Server Error.', 'DB-ERROR', 500)
        } else if (!merchant_data) {
          new MerchantModel(saveObj).save((merchant_error, merchantObj) => {
            if (merchant_error) {
              console.log('MERCHANT SAVE ERROR', merchant_error)
              Logs.error(merchant_error + '', 'ERRO MERCHANT SAVE', 'MERCHANT SAVE ERROR')
            } else {
              let merchant = {}
              merchantObj = JSON.parse(JSON.stringify(merchantObj))

              /* Token consists of _id,mobile,regster_date */
              merchant._id = merchantObj._id
              merchant.mobile = merchantObj.mobile
              merchant.register_date = merchantObj.register_date
              let token = JWT.sign(merchant, AppConfig.jwtsecret)
              merchant.is_profile_verified = merchantObj.is_profile_verified
              merchant.banks = banks

              let return_data = {}
              return_data.merchant_data = merchant
              return_data.merchant_token = token
              return_data.type = 'login'
              MerchantModel.findOneAndUpdate({
                mobile: merchant.mobile
              }, {
                $set: {
                  mobile_token_id: token
                }
              }, {
                new: true
              }, function (token_update_failed, token_update_success) {
                if (token_update_failed) return Response.error('Internal Server Error.', 'DB-ERROR', 500)
                else {
                  UserService.AuthenticateInformationStore(merchant._id, 'login', mobile_number, 'success', 'SUCCESS', authenticate_details, () => { })
                  if (deviceDetailsObj != undefined) {
                    let merchant_id = mongoose.Types.ObjectId(merchant._id)
                    let merchantFindOj = {}
                    merchantFindOj.merchant_id = merchant_id
                    merchantFindOj.ggl_adv_id = deviceDetailsObj.ggl_adv_id
                    console.log('deviceDetailsObj', deviceDetailsObj)
                    userDeviceMOdel.findOneAndUpdate(merchantFindOj, { $set: deviceDetailsObj }, { upsert: true }, (errDevice, deviceObj) => {
                      if (errDevice) {
                        return Response.error(errDevice, 'devicefind error', 500)
                      } else {
                        return Response.success('Successfully done login.', return_data)
                      }
                    })
                  } else {
                    return Response.success('Successfully done login.', return_data)
                  }
                }
              })
            }
          })
        } else {
          let merchant = {}
          merchant_data = JSON.parse(JSON.stringify(merchant_data))
          merchant_data.banks = banks
          /* Token consists of _id,mobile,regster_date */
          merchant._id = merchant_data._id
          merchant.mobile = merchant_data.mobile
          merchant.register_date = merchant_data.register_date
          let token = JWT.sign(merchant, AppConfig.jwtsecret)

          merchant.mobile_token_id = token
          merchant.login_status = 'login'
          let return_data = {}
          return_data.merchant_data = merchant_data
          return_data.merchant_token = token
          return_data.type = 'login'

          MerchantModel.findOneAndUpdate({
            mobile: merchant.mobile
          }, {
            $set: {
              mobile_token_id: token
            }
          }, function (token_update_failed, token_update_success) {
            console.log(token_update_failed)
            if (token_update_failed) return Response.error('Internal Server Error.', 'DB-ERROR', 500)
            else {
              UserService.AuthenticateInformationStore(merchant._id, 'login', mobile_number, 'success', 'SUCCESS', authenticate_details, () => { })
              if (deviceDetailsObj != undefined) {
                let merchantFindOj = {}
                let merchant_id = mongoose.Types.ObjectId(merchant._id)
                merchantFindOj.merchant_id = merchant_id
                merchantFindOj.ggl_adv_id = deviceDetailsObj.ggl_adv_id
                console.log('deviceDetailsObj', deviceDetailsObj)
                userDeviceMOdel.findOneAndUpdate(merchantFindOj, { $set: deviceDetailsObj }, { upsert: true }, (errDevice, deviceObj) => {
                  if (errDevice) {
                    return Response.error(errDevice, 'devicefind error', 500)
                  } else {
                    return Response.success('Successfully done login.', return_data)
                  }
                })
              } else {
                return Response.success('Successfully done login.', return_data)
              }
            }
          })
        }
      })
    })
  } catch (exception) {
    Print.exception(exception + '', exception)
    Logs.exception('Exception occurred.', 'EXCEPTION', exception + '', exception)
    return Response.error('Internal Server Error', 'EXCEPTION', 500)
  }
}

var SendOTP = function (mobile, skip, callback) {
  if (skip) return callback(null)

  let otp_filter = {}
  otp_filter.mobile_number = mobile

  let otp_number = GeneralUtils.randomNumber(1000, 9999)

  let OTPData = new MerchantOTPModel()
  OTPData.mobile_number = mobile
  OTPData.otp = otp_number
  OTPData.otp_date = Date.now()

  MerchantOTPModel.findOne(otp_filter, (otp_find_error, otp_data) => {
    if (otp_find_error) {
      console.error('Error while finding the otp data.', otp_find_error)
      return callback(true, 'Internal server error.', 'DB-ERROR', null)
    }

    if (otp_data) {
      let otp_send_diff = GeneralUtils.unixTimeDiff(otp_data.otp_date, Date.now(), 'minutes')
      let otp_send_diff_seconds = GeneralUtils.unixTimeDiff(otp_data.otp_date, Date.now(), 'seconds')

      let is_same_day = GeneralUtils.isUnixSameDate(Date.now(), otp_data.otp_date, 1000, 1000)
      // console.log(otp_send_diff_seconds, otp_data.otp_date, Date.now());
      if (otp_send_diff_seconds < 30) {
        let error_data = {
          limit: 30
        }
        return callback(true, 'OTP-MULTIPLE-REQUESTS', 'Please wait for 30 seconds.', error_data)
      }

      if (is_same_day && otp_data.otp_count > 100) {
        let data = {}
        data.limit = 100
        return callback(true, 'OTP-COUNT-EXCEEDS', 'Otp count exceeds', data)
      }
      otp_data.otp_count = is_same_day ? otp_data.otp_count + 1 : 1
      // console.log("otp diff :: ", otp_send_diff);
      if (otp_send_diff_seconds >= 30 || otp_data.otp_verified) {
        otp_data.otp_date = Date.now()
        otp_data.otp = otp_number
        otp_data.otp_verified = false
      } else {
        otp_number = otp_data.otp
      }
    }

    let configuration = {
      mobile: mobile,
      replace: {
        code: otp_number
      }
    }

    NotificationUtils.sms('sms_mobile_verification', configuration, function (code_send_error, code_success) {
      if (code_send_error) {
        console.error('Error while sending verification code.', code_send_error)
        // return Response.error("Internal Server Error.", "INS-ERROR", null);
      }

      if (otp_data) {
        console.log('otp update data :: ', otp_data)
        otp_data.save((otp_save_error, otp_data) => {
          if (otp_save_error) {
            console.log('Error while updating the otp details.', otp_save_error)
            return callback(true, 'OTP-SAVE-ERROR', 'Internal Server Error.', null)
          }

          callback(null, null, null, otp_number)
        })
      }

      if (!otp_data) {
        console.log('otp save data :: ', OTPData)
        OTPData.save((otp_save_error, otp_data) => {
          if (otp_save_error) {
            console.log('Error while saving the otp details.', otp_save_error)
            return callback(true, 'OTP-SAVE-ERROR', 'Internal Server Error.', null)
          }

          callback(null, null, null, otp_number)
        })
      }
    })
  })
}

var VerifyOtp = function (mobile, otp, skip, callback) {
  console.log('mobile', mobile)
  if (mobile == '9550092677' && otp == 9999) {
    return callback(null, null, null, null)
  }
  if (skip) {
    return callback()
  }

  if (!otp || !mobile) {
    return callback(true, 'BAD-PARAMS', 'Mobile (OR) OTP not sent.', null)
  }

  let mobile_filter = {
    mobile_number: mobile,
    otp: otp
  }

  MerchantOTPModel.findOne(mobile_filter, (otp_find_error, otp_data) => {
    if (otp_find_error) {
      console.error('Error while finding mobile otp.', otp_find_error)
      return callback(true, 'DB-ERROR', 'Internal Server Error.', null)
    }

    if (!otp_data || !otp_data.otp) {
      console.log('No otp data found.', mobile, otp)
      return callback(true, 'INVALID', 'Invalid OTP.', null)
    }

    let otp_time = GeneralUtils.unixTimeDiff(otp_data.otp_date, Date.now(), 'minutes')

    if (otp_time > 2) {
      console.log('Mobile expires before ' + (otp_time - 2) + 'minutes')
      return callback(true, 'OTP-EXPIRY', 'Otp has been expired.', null)
    }

    otp_data.otp_verified = true
    // otp_data.otp = 0;

    otp_data.save((otp_date_save_error, otp_updated_data) => {
      if (otp_date_save_error) {
        Print.error(otp_date_save_error, new Error())
        return callback(true, 'DB-ERROR', 'Internal Server Error at otp details update.', null)
      }
      return callback(null, null, null, null)
    })
  })
}

/* Ramu */
/* User credit details */
/* gives credit due, credit limit, credit date and due_date  */
UserService.creditDetails = function (user_id, callback) {
  let this_function_name = 'UserService.creditDetails'

  /* defining logs */
  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  let Print = new ResponseUtils.print(this_file_name, this_function_name)

  /* defining errors */
  let default_error = 'Error while getting credit details for dashboard.'
  let default_error_code = 'ERRORWHILEGETTINGCREDITDETAILS'

  try {
    /* preparing find filter */
    let find_filter = {}
    find_filter = {
      _id: user_id
    }

    /* preparing projection fileds */
    let projection_fields = {}
    projection_fields = {
      credit_due: 1,
      credit_limit: 1,
      credit_available: 1,
      due_date: 1
    }

    /* getting user details */
    UserService.getUserDetails(find_filter, projection_fields, function (error, error_code, message, data) {
      if (error) {
        return callback(error, error_code, message, data)
      }

      data = JSON.parse(JSON.stringify(data))

      if (data) {
        if (!data.due_date) {
          data.due_date = null
          data.due_date_string = ''
          data.due_days_left = null
        } else {
          console.log('came to due date present....')
          data.due_date_string = moment(data.due_date).format('DD,MMMM YYYY')
          if (GeneralUtils.unixTimeDiff(Date.now(), data.due_date, 'days') > 0) {
            data.due_days_left = GeneralUtils.unixTimeDiff(Date.now(), data.due_date, 'days') + ' days left'
          } else {
            data.due_days_left = ''
          }
        }
        if (!data.credit_due) data.credit_due = 0
      }

      return callback(error, error_code, message, data)
    })
  } catch (exception) {
    Print.exception(exception + '')
    Logs.exception(exception + '', default_error_code, default_error)

    return callback(exception + '', default_error_code.default_error)
  }
}

/* Ramu */
/* Get one user details based on mobile number */
UserService.getUserDetails = function (find_filter, projection_fields, callback) {
  let this_function_name = 'UserService.creditDetails'

  /* defining errors */
  let default_error = 'Error while getting user details.'
  let default_error_code = 'ERRORWHILEGETTINGUSERDETAILS'

  /* defining logs */
  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)
  let Print = new ResponseUtils.print(this_file_name, this_function_name)

  try {
    /* getting data from database */
    UserDetailsModel.findOne(find_filter, projection_fields, function (error, data) {
      if (error) {
        Print.error(error + '')
        Logs.error(error + '', default_error_code, default_error)
        return callback(error, 'DB-ERROR', 'Error while getting user details', null)
      }
      if (!data) {
        return callback(true, 'USER-NOT-FOUND', 'User details not found. Please login.', {
          code: 404
        })
      }

      return callback(null, null, null, data)
    })
  } catch (exception) {
    Print.exception(exception + '')
    Logs.exception(exception + '', default_error_code, default_error)

    return callback(exception, default_error_code, 'Error while getting user details', data)
  }
}

UserService.sendOTP = function (api_request, api_response) {
  let mobile_number = api_request.body.mobile
  let Response = new ResponseUtils.response(api_response)
  if (!mobile_number) {
    return Response.error('No Mobile Number found in request.', 'BAD-PARAMS', 400)
  }

  SendOTP(mobile_number, false, (otp_error, otp_status_code, otp_status_message, otp_data) => {
    if (otp_error) {
      return Response.error(otp_message, otp_status_code, 500)
    }

    return Response.success('Successfully sent otp to ' + mobile_number + '.')
  })
}

UserService.verifyOTP = function (api_request, api_response) {
  let mobile_number = api_request.body.mobile
  let otp = api_request.body.otp

  let Response = new ResponseUtils.response(api_response)
  if (!mobile_number || !otp) {
    return Response.error('No Mobile Number (OR) OTP found in request.', 'BAD-PARAMS', 400)
  }

  VerifyOtp(mobile_number, otp, false, (otp_error, otp_status_code, otp_status_message, otp_data) => {
    if (otp_error) {
      return Response.error(otp_message, otp_status_code, 500)
    }

    return Response.success('Successfully sent otp to ' + mobile_number + '.')
  })
}

/** Rakesh */
/** Logout user */
UserService.logout = function (api_request, api_response) {
  let Response = new ResponseUtils.response(api_response)

  api_request.user.mobile_token_id = null
  api_request.user.login_status = 'logout'
  api_request.user.save((logout_error, logout_data) => {
    if (logout_error) {
      return Response.error('Internal Server Error.', 'DB-ERROR', 500)
    }
    return Response.success('Successfully logged out.')
  })
}

/** Rakesh */
/* store authenticate information */
UserService.AuthenticateInformationStore = function (user_id, type, mobile, status, status_code, authenticate_details, callback) {
  console.log('authentication details in log :: ', authenticate_details)
  let details = {}
  details.ip = authenticate_details.ip
  details.type = type
  details.status = status
  details.status_code = status_code
  details.source = authenticate_details.source
  details.device_details = authenticate_details.device_details
  details.merchant_id = user_id
  details.mobile_number = mobile

  /* storing authenticate information of user */

  let AuthenticateInformation = new AuthenticateInformationModel(details)

  AuthenticateInformation.save((auth_store_error, auth_data) => {
    if (auth_store_error) {
      Print.error(auth_store_error, new Error())
      Logs.error('Error while storing the authentication details.', 'AUTH-DB-ERROR', null, new Error())
      return callback(auth_store_error, 'DB-ERROR', 'Error while saving authentication details.', null)
    }
    callback(null, null, null, auth_data)
  })
}
