'use strict'

var RequestUtils = module.exports

RequestUtils.MakeRequestOptions = function (url, method, data, body_type, headers) {
  let options = {}
  let param_data = data || {}
  options.url = url
  options.json = true
  options.method = method
  options.headers = headers || {
    'content-type': 'application/json'
  }
  if (method == 'get' || method == 'GET') {
    options.qs = param_data
  } else if (method == 'post' || method == 'POST') {
    options[body_type] = param_data
  }
  return options
}
