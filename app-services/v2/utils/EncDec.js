'use strict'
var crypto = require('crypto')
// var AES = require('./lib/aes');
var key = 'truelittle@#$'
key = crypto.createHash('sha1').update(key, 'utf-8').digest()
key = key.slice(0, 16)

var ResponseUtils = require('./../utils/ResponseUtils')

module.exports = {
  decrypt: function (data) {
    return (decryption(data, key))
  },
  encrypt: function (data) {
    return (encryption(data, key))
  }
}

function encryption (data, key) {
  var iv = ''
  var clearEncoding = 'utf8'
  var cipherEncoding = 'base64'
  var cipherChunks = []

  var cipher = crypto.createCipheriv('aes-128-ecb', key, iv)
  cipher.setAutoPadding(true)

  cipherChunks.push(cipher.update(data, clearEncoding, cipherEncoding))
  cipherChunks.push(cipher.final(cipherEncoding))

  return cipherChunks.join('')
}

// Data is ready to decrypt your strings, key is your key
function decryption (data, key) {
  var iv = ''
  var clearEncoding = 'utf8'
  var cipherEncoding = 'base64'
  var cipherChunks = []
  var decipher = crypto.createDecipheriv('aes-128-ecb', key, iv)
  decipher.setAutoPadding(true)

  cipherChunks.push(decipher.update(data, cipherEncoding, clearEncoding))
  cipherChunks.push(decipher.final(clearEncoding))

  return cipherChunks.join('')
}

module.exports.base64DecReq = function (api_request, api_response, next_service) {
  // console.log("request:: ", api_request.query);
  let Response = new ResponseUtils.response(api_response)
  let body_keys
  if (Object.keys(api_request.query).length) {
    body_keys = Object.keys(api_request.query)
  } else {
    body_keys = Object.keys(api_request.body)
  }
  try {
    if (Object.keys(api_request.query).length) {
      api_request.query = JSON.parse(Buffer.from(body_keys[0], 'base64').toString())
    } else {
      api_request.body = JSON.parse(Buffer.from(body_keys[0], 'base64').toString())
    }
    // console.log("at encoding",api_request.body);
  } catch (error) {
    console.error(error)
  }
  next_service()
}
