const aes_node = module.exports
var CryptoJS = require('crypto-js')

var KEY_SIZE = 16
var iv = '83963b0fedccb4ce' // CryptoJS.lib.WordArray.random(8).toString();

// Encryption using AES CBC (128-bits)
aes_node.encrypt = function (plaintext, key) {
  try {
    key = (key.length > KEY_SIZE) ? key.substr(0, KEY_SIZE) : key
    var encrypted = CryptoJS.AES.encrypt(
      plaintext,
      CryptoJS.enc.Utf8.parse(key), {
        mode: CryptoJS.mode.CBC,
        iv: CryptoJS.enc.Utf8.parse(iv),
        // PKCS#7 with 8-byte block size
        padding: CryptoJS.pad.Pkcs7
      }
    )
    return encrypted.ciphertext.toString(CryptoJS.enc.Base64)
  } catch (error) {
    console.log('Encryption exception in encrypt(): ' + error.message)
    return null
  }
}

// Decryption using AES CBC (128-bits)
aes_node.decrypt = function (ciphertext, key) {
  try {
    key = (key.length > KEY_SIZE) ? key.substr(0, KEY_SIZE) : key
    var decrypted = CryptoJS.AES.decrypt(
      ciphertext,
      CryptoJS.enc.Utf8.parse(key), {
        mode: CryptoJS.mode.CBC,
        iv: CryptoJS.enc.Utf8.parse(iv),
        // PKCS#7 with 8-byte block size
        padding: CryptoJS.pad.Pkcs7
      }
    )
    return decrypted.toString(CryptoJS.enc.Utf8)
  } catch (error) {
    console.log('Encryption exception in decrypt(): ' + error.message)
    return null
  }
}

/** ************************************** FOR TEST **********************************************/

// var enc = aes_node.encrypt('hai helloooo', 'thisisasamplepassphraseforencoding');
// console.log(enc);
// var dec = aes_node.decrypt(enc, 'thisisasamplepassphraseforencoding');
// console.log(dec);
