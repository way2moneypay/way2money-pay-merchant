var RequestUtils = require('./../utils/RequestUtils')
var ResponseUtils = require('./../utils/ResponseUtils')
var GateWaysConfig = require('config')['GateWaysConfig']
var UserDetailsModel = require('./../models/UserDetailsModel')
// var ResponseUtils = require("./../utils/ResponseUtils");

var Request = require('request')

var GateWayService = module.exports
var qs = require('querystring')
var http = require('https')

GateWayService.InstaMojoCreatePayment = function (purpose, amount, mobile, buyer_name, email, skip, callback) {
  if (skip) return callback()
  let headers = GateWaysConfig.InstaMojo.keys
  let url = GateWaysConfig.InstaMojo.urls.create_payment

  let payload = {
    purpose: purpose,
    amount: amount,
    phone: mobile,
    buyer_name: buyer_name,
    redirect_url: GateWaysConfig.InstaMojo.redirect_url.create_payment,
    webhook: GateWaysConfig.InstaMojo.webhook_urls.create_payment,
    send_email: true,
    send_sms: true,
    email: email,
    allow_repeated_payments: false
  }

  let RequestOptions = RequestUtils.MakeRequestOptions(url, 'post', payload, 'form', headers)
  Request(RequestOptions, (error, response, body) => {
    if (error) {
      console.log('Error while doing payment :: ', error)
      return callback(true, 'INS-PAYMENT-ERROR', 'Internal Server Error.', null)
    }
    return callback(null, null, null, body)
  })
}

GateWayService.InstaMojoCreatePaymentRedirect = function () {

}

GateWayService.InstaMojoCreatePaymentWebhook = function () {

}

GateWayService.InstaMojoRefund = function (payment_id, type, body, callback) {
  if (skip) return callback()
  let headers = GateWaysConfig.InstaMojo.keys
  let url = GateWaysConfig.InstaMojo.urls.refund_payment

  let payload = {
    payment_id: payment_id,
    type: type,
    body: body
  }

  let RequestOptions = RequestUtils.MakeRequestOptions(url, 'post', payload, 'form', headers)
  Request(RequestOptions, (error, response, body) => {
    console.log(body)
  })
}

GateWayService.InstaMojoCreatePaymentRequest = function (api_request, api_response) {
  let Response = new ResponseUtils.response(api_response)
  let user_id = api_request.user._id

  let user_filter = {
    _id: user_id
  }

  UserDetailsModel.findOne(user_filter, (user_find_error, user_data) => {
    if (user_find_error) {
      console.log('Error while finding user details in InstaMojoCreatePaymentRequest.', user_find_error)
      return Response.error('Internal Server Error.', 'DB-ERROR', 500)
    }

    if (!user_data) {
      return Response.error('No user details found.', 'USER-NOT-FOUND', 404)
    }

    let purpose = 'WAY2MONEY DUE AMOUNT PAYMENT'
    let mobile = Number(user_data.mobile_number)
    let email = user_data.email
    let buyer_name = user_data.full_name
    let amount = user_data.credit_due

    GateWayService.InstaMojoCreatePayment(purpose, amount, mobile, buyer_name, email, false, (error, code, message, data) => {
      if (error) {
        return Response.error(message, status, 500)
      }

      return Response.success('Successfully created payment. Find url in data.', data.long_url)
    })
  })
}

GateWayService.InstaMojoCreateRefundRequest = function (api_request, api_response) {
  let purpose = api_request.body.payment_id
  let mobile = api_request.body.type
  let email = api_request.body.body

  GateWayService.InstaMojoRefund(purpose, amount, mobile, buyer_name, email, false, () => {

  })
}

GateWayService.InstaMojoDomainCheck = function (api_request, api_response) {
  let this_function_name = 'GateWayService.InstaMojoDomainCheck'
  let Print = new ResponseUtils.print(__filename, this_function_name)
  let Logs = new ResponseUtils.Logs(__filename, this_function_name)

  try {
    var options = {
      'method': 'POST',
      'hostname': 'www.instamojo.com',
      'port': null,
      'path': '/oauth2/token/',
      'headers': {
        'content-type': 'application/x-www-form-urlencoded',
        'cache-control': 'no-cache'
      }
    }

    var req = http.request(options, function (res) {
      var chunks = []

      res.on('data', function (chunk) {
        chunks.push(chunk)
      })

      res.on('end', function () {
        var body = Buffer.concat(chunks)
        console.log('InstaMojo response ::: ', body.toString())
        let access_token = 'production' + JSON.parse(body.toString()).access_token
        api_response.send(access_token)
      })
    })

    req.write(qs.stringify({
      grant_type: 'client_credentials',
      client_id: GateWaysConfig['InstaMojo']['android-credentials']['client-id'],
      client_secret: GateWaysConfig['InstaMojo']['android-credentials']['client-secret']
    }))
    req.end()
  } catch (exception) {
    Print.exception(exception + '')
    Logs.exception(exception + '', 'EXCEPTION', 'Exception while instal mojo.', new Error())
    api_response.status(200).send('some exception occurred.')
  }
}

GateWayService.InstaMojoDomainCheckIOS = function (api_request, api_response) {
  let this_function_name = 'GateWayService.InstaMojoDomainCheck'
  let Print = new ResponseUtils.print(__filename, this_function_name)
  let Logs = new ResponseUtils.Logs(__filename, this_function_name)

  try {
    var options = {
      'method': 'POST',
      'hostname': 'www.instamojo.com',
      'port': null,
      'path': '/oauth2/token/',
      'headers': {
        'content-type': 'application/x-www-form-urlencoded',
        'cache-control': 'no-cache'
      }
    }

    var req = http.request(options, function (res) {
      var chunks = []

      res.on('data', function (chunk) {
        chunks.push(chunk)
      })

      res.on('end', function () {
        var body = Buffer.concat(chunks)
        console.log('InstaMojo response ::: ', body.toString())
        let access_token = 'production' + JSON.parse(body.toString()).access_token
        api_response.send(access_token)
      })
    })

    req.write(qs.stringify({
      grant_type: 'client_credentials',
      client_id: GateWaysConfig['InstaMojo']['ios-credentials']['client-id'],
      client_secret: GateWaysConfig['InstaMojo']['ios-credentials']['client-secret']
    }))
    req.end()
  } catch (exception) {
    Print.exception(exception + '')
    Logs.exception(exception + '', 'EXCEPTION', 'Exception while instal mojo.', new Error())
    api_response.status(200).send('some exception occurred.')
  }
}
