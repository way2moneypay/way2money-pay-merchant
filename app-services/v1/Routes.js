'use strict'
var DashboardService = require('./services/DashboardService')
var AuthenticateUtils = require('./utils/AuthenticateUtils')
var EncDec = require('./utils/EncDec')
var paymentSerivce = require('../v1/services/PaymentService')
var MerchantProfileService = require('../v1/services/MerchantProfileService')
var TransactionService = require('../v1/services/TransactionService')
var QrCodeGenerator = require('./services/QrCodeGenerator')
var RefundService = require('../../restapp/services/RefundService')
var UserService = require('./services/UserService')
var MerchantClaimService = require('./services/MerchantClaimService')
var paymentStatusCheck = require('./utils/PaymentStatusCheck')
var BASE_URL = '/mobile/v1/merchant-'

module.exports = function (app) {
  app.post(BASE_URL + 'generate-otp', EncDec.base64DecReq, UserService.sendOTPToMerchant)
  app.post(BASE_URL + 'otp-verify', EncDec.base64DecReq, UserService.verifyMerchantOTP)
  app.post(BASE_URL + 'register', EncDec.base64DecReq, AuthenticateUtils.userAuthentication, MerchantProfileService.register)
  app.post(BASE_URL + 'dashboard', EncDec.base64DecReq, AuthenticateUtils.userAuthentication, DashboardService.getUserDashboardDetails)
  app.post(BASE_URL + 'transactions', EncDec.base64DecReq, AuthenticateUtils.userAuthentication, TransactionService.getTransactionsCount)
  app.post(BASE_URL + 'profile', EncDec.base64DecReq, AuthenticateUtils.userAuthentication, MerchantProfileService.getDetails)
  app.post(BASE_URL + 'update-logo', EncDec.base64DecReq, AuthenticateUtils.userAuthentication, MerchantProfileService.uploadDoc)
  app.post(BASE_URL + 'update', EncDec.base64DecReq, AuthenticateUtils.userAuthentication, MerchantProfileService.updateMerchant)
  app.post(BASE_URL + 'check-refund', EncDec.base64DecReq, AuthenticateUtils.userAuthentication, RefundService.checkRefund)
  app.post(BASE_URL + 'refund', EncDec.base64DecReq, AuthenticateUtils.userAuthentication, RefundService.refund)
  app.post(BASE_URL + 'generate-qrcode', EncDec.base64DecReq, AuthenticateUtils.userAuthentication, QrCodeGenerator.generateQrCode)
  app.post(BASE_URL + 'payment', paymentStatusCheck.checkLivePaymentStatus, EncDec.base64DecReq, paymentSerivce.checkPayment)
  app.post(BASE_URL + 'claim', EncDec.base64DecReq, AuthenticateUtils.userAuthentication, MerchantClaimService.claimNow)
  app.post(BASE_URL + 'claims-history', EncDec.base64DecReq, AuthenticateUtils.userAuthentication, MerchantClaimService.getClaimHistoryCount)
  app.post(BASE_URL + 'details', EncDec.base64DecReq, AuthenticateUtils.userAuthentication, TransactionService.getMerchantDetails)
  app.post(BASE_URL + 'logout', EncDec.base64DecReq, AuthenticateUtils.userAuthentication, UserService.logout)
  // app.post("/mobile/v1/merchant/updateTransactionDetails", EncDec.base64DecReq, AuthenticateUtils.userAuthentication, merchantServices.updateTransactionAmount);
  // app.post("/mobile/v1/merchant/edit", EncDec.base64DecReq, AuthenticateUtils.userAuthentication, MerchantProfileService.getMerchantDetails);
}
