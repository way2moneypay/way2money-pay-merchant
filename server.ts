// These are important and needed before anything else
import 'zone.js/dist/zone-node';
import 'reflect-metadata';
import { enableProdMode } from '@angular/core';
import * as express from 'express';
import { join } from 'path';
import * as compression from 'compression';
import * as robots from 'robots.txt';
import * as sitemap from 'sitemap';
import * as serveStatic from 'serve-static';

// Faster server renders w/ Prod mode (dev mode never needed)
enableProdMode();

import { environment } from './src/environments/environment';

// Express server
const app = express();
app.use(compression());

const cache_options = {
  dotfiles: 'ignore',
  etag: true,
  maxAge: '2d',
  extensions: ['js', 'css', 'jpg', 'png', 'jpeg'],
  index: false,
  redirect: false,
  fallthrough: false,
  immutable: true,
  lastModified: true
}
// console.log(environment);
// console.log(process.env.NODE_ENV);
var PORT = 4000;
if (process.env.NODE_ENV === 'staging') {
  PORT = 8624;
} else if (process.env.NODE_ENV === 'production') {
  PORT = 8702;
}
const DIST_FOLDER = join(process.cwd(), 'dist');

// * NOTE :: leave this as require() since this file is built Dynamically from webpack
const { AppServerModuleNgFactory, LAZY_MODULE_MAP } = require('./dist/server/main.bundle');

// Express Engine
import { ngExpressEngine } from '@nguniversal/express-engine';
// Import module map for lazy loading
import { provideModuleMap } from '@nguniversal/module-map-ngfactory-loader';

app.engine('html', ngExpressEngine({
  bootstrap: AppServerModuleNgFactory,
  providers: [
    provideModuleMap(LAZY_MODULE_MAP)
  ]
}));

app.set('view engine', 'html');
app.set('views', join(DIST_FOLDER, 'browser'));

// TODO: implement data requests securely
app.get('/api/*', (req, res) => {
  res.status(404).send('data requests are not supported');
});

console.log('__dirname========' + __dirname);
app.use(robots('robots.txt'));

const sm = sitemap.createSitemap({
  hostname: 'https://www.way2money.com',
  cacheTime: 600000, // 600 sec - cache purge period 
  urls: [{
    url: '/personal-loan',
    changefreq: 'daily',
    priority: 1
  }, {
    url: '/emi-calculator',
    changefreq: 'daily',
    priority: 1
  },
  {
    url: '/about-us',
    changefreq: 'monthly',
    priority: 1
  },
  {
    url: '/tips',
    changefreq: 'monthly',
    priority: 1
  },
  {
    url: '/faqs',
    changefreq: 'monthly',
    priority: 1
  }]
});

app.get('/sitemap.xml', (req, res) => {
  sm.toXML(function (err, xml) {
    if (err) {
      return res.status(500).end();
    }
    res.header('Content-Type', 'application/xml');
    res.send(xml);
  });
});

app.get('/googlefa37efdc4a9d84ca.html', (req, res) => {
  res.render('./googlefa37efdc4a9d84ca.html', { req })
});

// Server static files from /browser
if (process.env.NODE_ENV !== 'development') {
  app.get('*.*', serveStatic(join(DIST_FOLDER, 'browser'), cache_options));
  app.get('*.*', serveStatic(join(DIST_FOLDER, 'node_modules'), cache_options));
} else {
  app.get('*.*', serveStatic(join(DIST_FOLDER, 'browser')));
}


// All regular routes use the Universal engine
app.get('*', (req, res) => {
  res.render(join(DIST_FOLDER, 'browser', 'index.html'), { req });
});

// Start up the Node server
app.listen(PORT, () => {
  console.log(`Node server listening on http://localhost:${PORT}`);
});

process.on('uncaughtException', function (err) {
  console.error((new Date).toUTCString() + ' uncaughtException:', err.message)
  console.error(err);
});

module.exports = app;

