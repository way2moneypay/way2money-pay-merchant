import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import {CookieService} from 'ngx-cookie-service';
@Component({
  selector: 'app-verification',
  templateUrl: './verification.component.html',
  styleUrls: ['./verification.component.css']
})
export class VerificationComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router,private cookieService:CookieService) { }

  message: string;
  exp_date: string;
  update_profile:string;
  ngOnInit() {
    this.message = this.route.snapshot.queryParamMap.get('message');
    this.update_profile = this.route.snapshot.queryParamMap.get('update_profile');
  }

  onContinue(){
    if(this.message){
      if(this.cookieService.get('token') && this.cookieService.get('name')){
        if(this.update_profile){
        return this.router.navigate(['/app/users/profile']);
        }
        return this.router.navigate(['/app/dashboard']);
      }else{
        return this.router.navigate(['/login']);
      }
    }
  }
}
