import { Component, OnInit } from '@angular/core';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
    pathRoute: string;
}
export const ROUTES: RouteInfo[] = [
    { path: 'dashboard', title: 'Dashboard', pathRoute: '/app/dashboard', icon: 'pe-7s-graph', class: '' },
    { path: 'transactions', title: 'Transactions', pathRoute: '/app/transactions', icon: 'pe-7s-user', class: '' },
    { path: 'transactions/paid-history', title: 'Paid History', pathRoute: '/app/transactions/paid-history', icon: 'pe-7s-note2', class: '' },
    { path: 'users/profile', title: 'User Profile', pathRoute: '/app/users/profile', icon: 'pe-7s-note2', class: '' }
];

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
    menuItems: any[];

    constructor() { }

    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
        console.log(this.menuItems);
    }
    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };
}
