import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MerchantProfileComponent } from './merchant-profile.component';

const routes: Routes = [
    { path: '', component: MerchantProfileComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MerchantProfileRoutingModule { }
