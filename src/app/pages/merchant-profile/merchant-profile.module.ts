import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MerchantProfileRoutingModule } from './merchant-profile-routing.module';
import { MerchantProfileComponent } from './merchant-profile.component';

@NgModule({
  imports: [
    CommonModule,
    MerchantProfileRoutingModule,
    FormsModule
  ],
  declarations: [MerchantProfileComponent]
})
export class MerchantProfileModule { }
