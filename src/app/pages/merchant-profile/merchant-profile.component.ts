import { Location, isPlatformBrowser, DOCUMENT, isPlatformServer } from '@angular/common';
import { Router, ActivatedRoute, } from '@angular/router';
import { Component, OnInit, ViewContainerRef, Inject, PLATFORM_ID } from '@angular/core';
import { RestServicesService } from '../../shared/rest-services/rest-services.service';
import { environment } from '../../../environments/environment';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-merchant-profile',
  templateUrl: './merchant-profile.component.html',
  styleUrls: ['./merchant-profile.component.scss']
})
export class MerchantProfileComponent implements OnInit {
  thirdStep: boolean = false;
  secondStep: boolean = false;
  firstStep: boolean = true;
  is_admin_verified: boolean = false;
  merchantDetails: any = {};
  min: any;
  max: any;
  commission: any;
  regMerchantForm = {};
  regMerchantResponse: any = {};
  regCommissions = [];
  length = 0;
  numbers = [];
  singleCommission = {
    min_amount: 0,
    max_amount: 0,
    commission_rate: 0
  };
  merchantId;
  routeData;
  getMerchantApi;
  updateMerchant: any = {};
  cpage: any;
  cpageNum: any;
  namePattern = "^[a-zA-Z ]{2,30}$";
  emailPattern = "^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$";
  mobilePattern = "^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$";
  panPattern = "[A-Z]{5}[0-9]{4}[A-Z]{1}";
  tinPattern = "[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[0-9]{1}[Z][A-Z0-9]{1}";
  logoPath = environment.images_cdn;
  public vcr: ViewContainerRef;
  banks = ["Allahabad Bank", "Andhra Bank", "Axis Bank", "Bank of Bahrain and Kuwait", "Bank of Baroda - Corporate Banking", "Bank of Baroda - Retail Banking", "Bank of India", "Bank of Maharashtra", "Canara Bank", "Central Bank of India", "City Union Bank", "Corporation Bank", "Deutsche Bank", "Development Credit Bank", "Dhanlaxmi Bank", "Federal Bank", "ICICI Bank", "IDBI Bank", "Indian Bank", "Indian Overseas Bank", "IndusInd Bank", "ING Vysya Bank", "Jammu and Kashmir Bank", "Karnataka Bank Ltd", "Karur Vysya Bank", "Kotak Bank", "Laxmi Vilas Bank", "Oriental Bank of Commerce", "Punjab National Bank - Corporate Banking", "Punjab National Bank - Retail Banking", "Punjab & Sind Bank", "Shamrao Vitthal Co-operative Bank", "South Indian Bank", "State Bank of Bikaner & Jaipur", "State Bank of Hyderabad", "State Bank of India", "State Bank of Mysore", "State Bank of Patiala", "State Bank of Travancore", "Syndicate Bank", "Tamilnad Mercantile Bank Ltd.", "UCO Bank", "Union Bank of India", "United Bank of India", "Vijaya Bank", "Yes Bank Ltd", "Other"];


  constructor(private dataService: RestServicesService, @Inject(DOCUMENT) private document: Document, private route: ActivatedRoute,
    private router: Router, private _location: Location, public toastr: ToastsManager, vcr: ViewContainerRef, @Inject(PLATFORM_ID) public platformId: Object) {
    this.vcr = vcr;
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    isPlatformBrowser(this.platformId);
    if (isPlatformBrowser(this.platformId)) {
      this.document.body.classList.remove('modal-open');
      if (this.document.querySelector('.modal-backdrop')) {
        this.document.querySelector('.modal-backdrop').remove();
        this.document.querySelector('.modal-backdrop').remove();
      }
    }
    // this.addCommissionField();
    this.merchantId = this.route.snapshot.params.id;
    this.getMerchantDetails();
  }
  getMerchantDetails() {
    this.dataService.getAuth('api/merchant/getDetails').subscribe((result) => {
      if (result && result.status_code == 200) {
        this.updateMerchant = result.data;
        if (this.updateMerchant.bank_name == undefined || this.updateMerchant.bank_name === "" || this.updateMerchant.bank_name == null) {
          this.updateMerchant['bank_name'] = 'State Bank of India';
        }
        this.is_admin_verified = result.data.is_admin_verified;
      } else {
        this.updateMerchant = {};
        this.updateMerchant['bank_name'] = 'State Bank of India';
        this.is_admin_verified = false;
      }
    });
  }
  getKeys(data) {
    return Object.keys(data);
  }
  onSubmit(step) {
    // this.updateCommissionSubmit = false;
    // this.updateMerchant['regCommissions'] = this.updateCommissionsArray;
    // console.log('submit ');
    if(this.merchantId) {
      this.updateMerchant['type'] = 1;
    } else {
      this.updateMerchant['type'] = 0;
    }
    if (step == 1 || step == 2) {
      this.dataService.postAuth('api/merchant/register/' + step, this.updateMerchant).subscribe((response) => {
        if (response && response.status_code == 200) {
          this.regMerchantResponse = response;
          if (this.firstStep == true) {
            this.firstStep = false;
            this.secondStep = true;
            this.thirdStep = false;
          } else if (this.secondStep == true) {
            this.firstStep = false;
            this.secondStep = false;
            this.thirdStep = true;
          } else {
            this.firstStep = true;
            this.secondStep = false;
            this.thirdStep = false;
            this.router.navigate(['/app/dashboard']);
          }
        } else {
          this.regMerchantResponse = response;
        }
      });
    } else {
      this.router.navigate(['/app/dashboard']);
    }
  }
  onSkip() {
    this.firstStep = false;
    this.secondStep = false;
    this.thirdStep = true;
  }
  onBack(step) {
    if (step == 2) {
      this.firstStep = true;
      this.secondStep = false;
      this.thirdStep = false;
    } else {
      this.firstStep = false;
      this.secondStep = true;
      this.thirdStep = false;
    }
  }
  onUpdate() {
    // this.updateCommissionSubmit = false;
    // this.updateMerchant['regCommissionsUpdate'] = this.updateCommissionsArrayUpdate;
    // this.updateMerchant['regCommissions'] = this.updateCommissionsArray;
    this.dataService.postAuth('api/merchant/update', this.updateMerchant).subscribe((response) => {
      if (response && response.status_code == 200) {
        this.regMerchantResponse = response;
        this.router.navigate(['/app/dashboard']);
      } else {
        this.regMerchantResponse = response;
      }
    });
  }
  uploadLogo(files: FileList, image_type) {
    const valid_types = ['application/pdf', 'application/vnd.ms-excel', 'image/png', 'image/x-png', 'image/jpeg', 'image/jpg'];

    const url = 'api/uploadDoc';
    const file = files.item(0);
    if (valid_types.indexOf(file.type) === -1) {
      return this.toastr.warning('This file type is not allowed');
    } else {
      const file_key = 'file';
      const params: any = {};
      params.file_name = this.updateMerchant._id;
      params.image_type = image_type;
      params.file_extension = file.name.substr(file.name.lastIndexOf('.'), file.name.length);
      // params.old_file = this.updateMerchant['logo'];
      const progress = false;
      if (file != null && file !== undefined && file.name != null && file.name !== undefined) {
        this.dataService.uploadAuth(url, file, file_key, params, progress).subscribe(result => {
          if (result) {
            // console.log("result", result);
            // this.ngOnInit();
            // this.updateMerchant['logo'] = result.file_name;
            // 'file_name_logo' + params.file_extension + '?q=' + Math.random();
            this.updateMerchant[image_type] = this.logoPath + this.updateMerchant._id + '_' + image_type + params.file_extension + '?q=' + Math.random();
            // console.log("this------>",this.updateMerchant[image_type]);
          }
        });
      }
    }

  }
  downloadQR() {
    this.dataService.downloadImg('api/merchant-download-qrcode')
  }
}
