import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { NavbarModule } from './../shared/navbar/navbar.module';
import { FooterModule } from './../shared/footer/footer.module';
import { SidebarModule } from './sidebar/sidebar.module';
import { TopbarModule } from "./topbar/topbar.module";
import { PaidHistoryComponent } from './paid-history/paid-history.component';

@NgModule({
  imports: [
    CommonModule,
    PagesRoutingModule,
    NavbarModule,
    FooterModule,
    SidebarModule,
    TopbarModule,
    FormsModule
  ],
  declarations: [PagesComponent, PaidHistoryComponent]
})
export class PagesModule { }
