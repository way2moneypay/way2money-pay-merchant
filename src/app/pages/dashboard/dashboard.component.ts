import * as Chart from "chart.js";
import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { DOCUMENT } from "@angular/common";
import { DatePipe } from '@angular/common';
import { RestServicesService } from './../../shared/rest-services/rest-services.service';
import { Router, ActivatedRoute } from '@angular/router';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { Meta, Title } from "@angular/platform-browser";
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ViewContainerRef } from '@angular/core';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [DatePipe]
})
export class DashboardComponent implements OnInit {
  merchantDetails: any = {};

  barChart: any;
  dashboarData: any = {};
  days_select = "Last 30 days";
  is_admin_verified = false;
  claimFailed = false;
  claimRemarks = null;
  constructor(private dataService: RestServicesService, @Inject(PLATFORM_ID) public platformId: Object, meta: Meta, title: Title, @Inject(DOCUMENT) private document: Document,
  ) { }

  ngOnInit() {
    if (isPlatformBrowser(this.platformId)) {
      this.document.body.classList.remove('modal-open');
      if (this.document.querySelector('.modal-backdrop')) {
        this.document.querySelector('.modal-backdrop').remove();
        this.document.querySelector('.modal-backdrop').remove();
      }
    }
    this.dataService.getAuth('api/merchant/getDetails').subscribe((result) => {
      // console.log("result", result);
      if (result && result.status_code == 200) {
        this.merchantDetails = result.data;
        this.is_admin_verified = result.data.is_admin_verified;
        if(this.is_admin_verified) {
          this.dataService.getAuth("api/dashboard-data").subscribe((response) => {
            if (response.status_code == 200) {
              this.dashboarData = response.data;
              // this.graphReport(this.dashboarData.graph_report)
            } else if (response.status_code == 400) {
              console.log('DASHBOARDERROR')
            }
          });
          this.dataService.getAuth("api/dashboard-chartdata").subscribe((response) => {
            if (response.status_code == 200) {
              // this.dashboarData = response.data;
              this.graphReport(response.data.graph_report)
            } else if (response.status_code == 400) {
              console.log('DASHBOARDERROR')
            }
          });
        }
      } else {
        console.log('DASHBOARDERROR ELSE');
      }
    });
  }
  graphReport(data) {
    this.barChart = new Chart('barChart', {
      type: 'bar',
      data: {
        labels: data.labels,
        datasets: [{
          label: "Users",
          data: data.users_count,
          backgroundColor: 'rgb(255, 127, 127,0.5)',
          borderColor: 'rgb(255, 127, 127,1)'
        }, {
          label: 'Transactions',
          data: data.transactions_count,
          backgroundColor: 'rgb(121, 252, 237,0.5)',
          borderColor: 'rgb(121, 252, 237,1)'
        }]
      },
      options: {
        title: {
          text: "User's Transactions / Amount / ",
          display: true
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }
  filter(form) {
    this.dataService.getAuth("api/dashboard-chartdata/" + form.value.days).subscribe((response) => {
      if (response.status_code == 200) {
        this.graphReport(response.data.graph_report)
      } else if (response.status_code == 400) {
        console.log('DASHBOARDERROR')
      }
    })
  }
  claim(amount) {
    this.claimFailed = false;
    this.dataService.postAuth("api/merchant-claim", { amount: amount }).subscribe((response) => {
      if (response.status_code == 200) {
        this.claimFailed = true;
        this.claimRemarks = response.message
      } else if (response.status_code == 500) {
        this.claimFailed = true;
        this.claimRemarks = response.message
      }
    })
  }
}
