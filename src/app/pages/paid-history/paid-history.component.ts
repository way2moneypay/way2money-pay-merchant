import { Component, OnInit } from '@angular/core';
import { RestServicesService } from '../../shared/rest-services/rest-services.service';

@Component({
  selector: 'app-paid-history',
  templateUrl: './paid-history.component.html',
  styleUrls: ['./paid-history.component.scss']
})
export class PaidHistoryComponent implements OnInit {
  
  constructor(private dataService: RestServicesService) { }

  ngOnInit() {
  }

}
