import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from "./pages.component";
import {PaidHistoryComponent} from './paid-history/paid-history.component'
import {isProfileVerified} from '../shared/guard/auth.guard'

const routes: Routes = [
  {
    path: '', children: [
      { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule'},
      { path: 'transactions/:cpage', loadChildren: './transactions/transactions.module#TransactionsModule' },
      // { path: 'users', loadChildren: './user/user.module#UserModule' },
      { path: 'claim-history', component: PaidHistoryComponent},
      { path: 'merchant-profile', loadChildren: './merchant-profile/merchant-profile.module#MerchantProfileModule' },
      { path: 'merchant-profile/:id', loadChildren: './merchant-profile/merchant-profile.module#MerchantProfileModule'},
      { path: 'reports', loadChildren: './reports/reports.module#ReportsModule'}
    ], component: PagesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
