import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TopbarComponent } from './topbar.component';

@NgModule({
  imports: [
    RouterModule, CommonModule
  ],
  declarations: [TopbarComponent],
  exports: [TopbarComponent]
})
export class TopbarModule { }
