import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { RestServicesService } from '../../shared/rest-services/rest-services.service';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {
  token: string;
  user: string;
  isLoginSHOW: boolean = false;
  isLogoutSHOW: boolean = false;
  merchantDetails: any;
  isMenuItemsShow: boolean = false;
  // is_admin_verified: boolean = false;
  // is_profile_verified: boolean = false;
  // is_nothing_verified: boolean = false;
  constructor(private cookieService: CookieService, private router: Router,private dataService:RestServicesService) { }

  ngOnInit() {
    // this.user = this.cookieService.get('mobile');
    this.token = this.cookieService.get('merchant_token');
    if (this.token) {
      // this.getMerchantDetails();
      this.isLoginSHOW = false;
      this.isLogoutSHOW = true;
    } else {
      this.isLoginSHOW = true;
    }
  }
  // getMerchantDetails (){
  //   this.dataService.getAuth('api/merchant/getDetails').subscribe((result) => {
  //     if (result && result.status_code == 200) {
  //       if(result.data.is_profile_verified && result.data.is_admin_verified) {
  //         this.is_admin_verified = true;
  //       } else if (result.data.is_profile_verified){
  //         this.is_profile_verified = true;
  //       } else {
  //         this.is_nothing_verified = true;
  //       }
  //     }
  //   });
  // }
  logout() {
    console.log("LOGOUT CALLED");
    // this.cookieService.delete('user', "/");
    this.cookieService.delete('merchant_token', "/");
    // this.cookieService.delete('mobile', "/");
    this.cookieService.deleteAll();
    this.isLoginSHOW = true;
    this.router.navigate(['']);
  }
}
