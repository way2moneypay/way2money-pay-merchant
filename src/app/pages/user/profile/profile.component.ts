import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { RestServicesService } from './../../../shared/rest-services/rest-services.service';
import { CookieService } from 'ngx-cookie-service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Observable } from 'rxjs/Rx';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  constructor(private restservice: RestServicesService, private cookieService: CookieService, public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }
  profile: any = {};
  change_pwd: any = {};
  new_mobile: number;
  new_mobile_update: Boolean = false;
  showOtpBox: Boolean = false;
  showChangePwd: Boolean = false;
  showChangeEmail: Boolean = false;
  isEmailSucces: any = false;
  validation_pwd_error: any = false;
  new_email_update: String;
  profileError: String;
  profileVerifyError: String;
  mailVerifyErr: String;
  pswdErr: String;
  // otp_details: any = {};
  paramData: any = {};


  otp_verification_time: any;
  count_down: any;
  otp_count = 4;
  otp: any;
  otp_details_verify: any = [
    {
      type: "number",
      maxlength: 1
    },
    {
      type: "number",
      maxlength: 1
    },
    {
      type: "number",
      maxlength: 1
    },
    {
      type: "number",
      maxlength: 1
    }
  ];



  ngOnInit() {
    // this.profile.name = "Madhu";
    // this.profile.email = "madd@gmail.com";
    // this.profile.password = "m&d";
    // this.profile.mobile = "7893549123";
    this.restservice.getAuth('api/profile').subscribe(response => {
      console.log(response);
      if (response.type === 'SUCCESS') {
        this.profile = response.data;
        // this.toastr.success(response.message);
      } else {
        this.toastr.error(response.message);
      }
    })
  }

  isEdit() {
    if (!this.new_mobile_update) {
      this.otp_verification_time = null;
    }

    if (this.otp_verification_time && this.otp_verification_time > 0 && (!this.new_mobile_update || this.showOtpBox)) {
      this.showOtpBox = false;
      this.profileError = '';
      this.new_mobile_update = true;
      return;
    }


    this.new_mobile_update = !this.new_mobile_update;
    if (this.new_mobile_update && !this.otp_verification_time) {
      this.new_mobile = this.profile.mobile_number;
      this.showOtpBox = false;
      // this.otp_details = {};
    }

    console.log('new mobile is ', this.new_mobile);
  }

  makeCounter() {
    this.otp_verification_time = 60;
    this.count_down = Observable.timer(0, 1000)
      .take(this.otp_verification_time)
      .map((i) => {
        this.otp_verification_time--;
      }).subscribe((counter) => { });
  }

  onResendOTP() {
    this.onSendSMSOTP();
  }

  onSendSMSOTP() {
    // isPlatformBrowser(this.platformId)
    if (this.new_mobile === this.profile.mobile_number) {
      this.profileError = 'Try To update with new mobile';
      return;
    }
    // if (this.new_mobile !== this.profile.mobile) {
    // console.log('send otp mobile is ', this.profile.dob);
    console.log('mobile is ', this.new_mobile);
    let paramData: any = {};
    this.paramData.mobile_number = this.new_mobile;
    // this.paramData.token = this.cookieService.get('token');
    this.restservice.postAuth('api/profile-send-otp',
      this.paramData).subscribe(response => {
        console.log(response);
        if (response.type === 'SUCCESS') {
          this.showOtpBox = true;
          this.otp = null;
          // this.verify_status = response.data.is_mobile_verified;
          this.toastr.success(response.message);
          // this.verify_status = response.data.is_mobile_verified;
          this.makeCounter();
        } else {
          this.toastr.error(response.message);
          this.profileError = response.message;
        }
      });
    // }
  }

  setshowChangePwd(flag) {
    this.showChangePwd = flag;
    if (!flag) {
      this.change_pwd = {};
    }
  }
  setUpdate() {
    console.log(this.profile);
  }

  setshowEmail(flag) {
    this.showChangeEmail = flag;
  }



  setChangePWD() {
    console.log(this.change_pwd);


    if (this.change_pwd.current_pwd && this.change_pwd.new_pwd && this.change_pwd.conform_pwd) {
      if (this.change_pwd.new_pwd === this.change_pwd.conform_pwd) {
        let paramData: any = {};
        this.paramData = this.change_pwd;

        this.restservice.postAuth('api/change-password', this.change_pwd).subscribe((response) => {
          if (response.type === 'SUCCESS') {
            console.log(response);
            this.toastr.success(response.message)
            this.setshowChangePwd(false);
          } else {
            this.pswdErr = response.message;
            this.toastr.error(response.message);
          }
        })
      } else {
        this.toastr.error('Passwords did not match');
        console.log('Passwords did not match');
        this.pswdErr = 'Passwords did not match';
      }
    } else {
      this.toastr.error('Please provide all informations to change password');
      console.log('Please provide all informations to change password');
      this.pswdErr = 'Please provide all informations to change password';
      this.validation_pwd_error = true;
    }
  }

  setChangeEMAIL() {

    console.log(this.new_email_update);
    if (this.new_email_update && this.isEmailSucces) {
      if (this.new_email_update === this.profile.email) {
        return this.toastr.error('Email Did not changed');
      }
      const paramData: any = {};
      paramData.email = this.new_email_update;

      this.restservice.postAuth('api/change-profile-email', paramData).subscribe((response) => {
        if (response.type === 'SUCCESS') {
          console.log(response);
          this.toastr.success(response.message);
          this.setshowEmail(false);
        } else {
          this.toastr.error(response.message);
          console.log(response.message);
        }
      });
    }
  }

  setProfileUpdate() {
    if (!this.profile.name || !this.profile.email || !this.profile.mobile_number) {
      console.log('Please Fill All Profile Fields');
    } else {
      this.restservice.postAuth('api/update-profile', this.profile).subscribe(response => {
        console.log(response);
        if (response.type === 'SUCCESS') {
          this.toastr.success(response.message);
          this.profile = response.data;
        } else {
          this.toastr.error(response.message);
          console.log(response.message);
        }
      });
    }
  }

  onOTPVerify() {
    // if (isPlatformBrowser(this.platformId)) {
    // console.log('otp is ', this.otp);
    // const otp = this.otp_details.otp_field_one +
    //   this.otp_details.otp_field_two +
    //   this.otp_details.otp_field_three +
    //   this.otp_details.otp_field_four;

    console.log(this.otp);
    if (!this.otp) {
      return this.profileVerifyError = 'Please enter OTP.';
    }
    if (this.otp.toString().length < 4) {
      return this.profileVerifyError = 'OTP should be 4 digits.';
    }
    this.paramData.otp = this.otp;
    this.paramData.mobile = this.new_mobile;
    // this.paramData.token = this.cookieService.get('token');
    this.restservice.postAuth('api/profile-verify-otp',
      this.paramData).subscribe((response) => {
        console.log(response);
        if (response.type === 'SUCCESS') {
          this.showOtpBox = false;
          this.new_mobile_update = false;
          this.profile.mobile_number = response.data.mobile_number;
          this.otp = "";
          // console.log('mobile verified is ', response.data.is_mobile_verified);
          // this.verify_status = response.data.is_mobile_verified;
          this.toastr.success(response.message);
        } else if (response.status === 'VERIFYOTPEXPIRE') {
          // this.send_again_otp = true;
          this.showOtpBox = false;
          this.new_mobile_update = true;
          this.toastr.error(response.message);
          this.otp = "";
        } else {
          this.otp = "";
          this.profileVerifyError = response.message;
        }
      });
  }

  onVerifyEmail() {
    if (this.profile.email) {
      const paramData: any = {};
      paramData.email = this.profile.email;
      this.restservice.postAuth('api/email-send', paramData).subscribe((response) => {
        if (response.type === 'SUCCESS') {
          console.log(response);
          this.toastr.success(response.message);
        } else {
          this.toastr.error(response.message);
          console.log(response);
        }
      })
    }

  }
  isEmailVerify() {
    if (!this.new_email_update) {
      this.mailVerifyErr = 'Email required *';
      return;
    }
    let paramData: any = {};
    paramData.email = this.new_email_update;
    this.restservice.post('api/verify-email', paramData).subscribe(response => {

      this.mailVerifyErr = response.message;
      this.isEmailSucces = response.data.verification;
    });
  }

  // }
}
