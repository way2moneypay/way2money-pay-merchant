import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import { UserRoutingModule } from './user-routing.module';
import { ProfileComponent } from './profile/profile.component';
import { FormFieldsModule } from './../../shared/modules/form-fields/form-fields.module';

@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    FormsModule,
    FormFieldsModule
  ],
  declarations: [ProfileComponent]
})
export class UserModule { }
