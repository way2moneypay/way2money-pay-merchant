import { Component, OnInit } from '@angular/core';
import { RestServicesService } from './../../shared/rest-services/rest-services.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { PagerService } from '../../shared/pagination/pager.service';
@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss'],
  providers: [PagerService]
})
export class TransactionsComponent implements OnInit {
  merchant_details: any = {};
  fDate: Date = new Date();
  toDate: Date = new Date();
  settingsFrom = {
    bigBanner: true,
    timePicker: false,
    format: 'yyyy-MM-dd',
    defaultOpen: false,
    placeholder: 'From Date'
  }
  settingsTo = {
    bigBanner: true,
    timePicker: false,
    format: 'yyyy-MM-dd',
    defaultOpen: false,
    placeholder: 'To Date'
  }

  countStatus: any = false;
  transactionDetails: any;
  private allItems: any[];
  pager: any = {};
  cpage: any;
  pagedItems: any[];
  currentPage: number = 1;
  errMsg = false;
  pageLength: any;
  searchString = "";

  searchVariable = "mobile";
  showData = false;
  searchString2: any;
  searchStatus: any = false;
  paginationStatus: any = false;
  userTransactionsApi: any = 0;
  updateTransactionResponse: any = {};
  redirectUrl: String;
  obj: any;
  responseStatus: any = false;
  refundButtonShowStatus: any = false;
  resetButtonShowStatus: any = false;
  navigationSubscription;
  checkRefund: any;
  checkRefundStatus = false;
  refundStatus = 0;
  refundRemarks: any;
  refundButtonStatus;

  constructor(private dataService: RestServicesService, private pagerService: PagerService, private route: ActivatedRoute, private router: Router, private modalService: NgbModal) {
    this.navigationSubscription = router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.currentPage = this.route.snapshot.params.cpage;
        if (this.currentPage < 1 || this.currentPage == undefined) {
          this.currentPage = 1;
        }
        this.getMerchantDetails();
        if (this.searchStatus) {
          this.setSearchPage(this.currentPage);
        } else {
          this.getCount();
        }
      }
    });
  }
  ngOnInit() {

  }
  ngOnDestroy() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  getMerchantDetails() {
    this.dataService.getAuth('api/merchant/getDetails').subscribe((result) => {
      // console.log("result", result);
      if (result.status_code == 200) {
        this.merchant_details = result.data.merchant;
      }
    })
  }

  getCount() {
    this.userTransactionsApi = 'api/transactionsCount';
    if (!this.countStatus) {
      this.dataService.getAuthParams(this.userTransactionsApi, {}).subscribe(result => {
        if (result.data) {
          this.pageLength = parseInt(result.data);
          this.countStatus = true;
          this.setTransactionPage(this.currentPage);
        } else {
          this.errMsg = true;
          this.pageLength = 0;
        }
      })
    } else {
      this.setTransactionPage(this.currentPage);
    }
  }
  getTransactions(skip, limit) {
    this.obj = {
      skip: skip,
      limit: limit,
    }
    this.dataService.getAuthParams("api/transactions", this.obj).subscribe(result => {
      if (result.status_code === 200) {
        this.pagedItems = result.data;
        // console.log("Pagesd :: ", this.pagedItems);
        this.showData = this.pagedItems.length > 0 ? true : false;
      } else {
        this.showData = false;
        this.errMsg = true;
      }
      this.resetButtonShowStatus = false;
    }, error => {
      // console.log('Error getting transactions ', error);
    })
  }
  setPage(page) {
    this.currentPage = page;
    this.cpage = this.router.routerState.snapshot.url;
    this.cpage = '/app/transactions/' + page;
    this.router.navigate([this.cpage]);

  }
  setTransactionPage(page) {
    if (this.pageLength) {
      if (this.pageLength > 10) {
        this.paginationStatus = true;
        this.pager = this.pagerService.getPager(Number(this.pageLength), Number(page), 10);
        if (this.currentPage > this.pager.totalPages) {
          this.currentPage = this.pager.totalPages;
        }
        this.getTransactions(this.pager.startIndex, this.pager.endIndex + 1 - this.pager.startIndex);
      } else {
        this.currentPage = 1;
        this.getTransactions(0, this.pageLength);
        this.paginationStatus = false;
      }
    } else {
      this.errMsg = true;
      this.paginationStatus = false;
    }

  }
  setSearchPage(page) {
    this.currentPage = page;
    if (this.pageLength) {
      if (this.pageLength > 10) {
        this.paginationStatus = true;
        this.pager = this.pagerService.getPager(Number(this.pageLength), Number(page), 10);
        if (this.currentPage > this.pager.totalPages) {
          this.currentPage = this.pager.totalPages;
        }
        this.pagedItems = this.transactionDetails.slice(this.pager.startIndex, this.pager.endIndex + 1);
      } else {
        this.currentPage = 1;
        this.pagedItems = this.transactionDetails;
        this.paginationStatus = false;

      }
    } else {
      this.errMsg = true;
    }
  }

  search() {
    this.responseStatus = true;
    var data = {}, array = [];
    // if (this.searchVariable == "date") {
    // }
    this.fDate = new Date(this.fDate);
    this.toDate = new Date(this.toDate);
    data["type"] = "transaction_date";
    data["fromDate"] = this.fDate.setHours(0, 0, 0, 0);
    data["toDate"] = this.toDate.setHours(23, 59, 59, 59);
    if (this.searchVariable == "mobile" && this.searchString) {
      data["mobile_no"] = this.searchString;
    }
    if (this.searchVariable == "status" && this.searchString) {
      data["transaction_status"] = this.searchString;
    }
    if (this.searchVariable == "m_id" && this.searchString) {
      data["merchant_id"] = this.searchString;
    }
    if (this.searchVariable == "r_id" && this.searchString) {
      data["reference_id"] = this.searchString;
    }
    // console.log(data);
    array[0] = 1;
    array.push(data);
    this.dataService
      .getAuthParams("api/transactions/", data)
      .subscribe(result => {
        // this.responseStatus = false;
        if (result.status_code === 200) {
          // console.log("search result is", result);
          // console.log("result search", result);
          this.transactionDetails = result.data;
          this.countStatus = false;
          this.pageLength = this.transactionDetails.length;
          this.showData = result.data.length > 0 ? true : false;
          this.errMsg = false;
          this.searchStatus = true;
          this.setSearchPage(1);
        } else {
          this.errMsg = true;
          this.showData = false;
          this.searchStatus = false;
        }

      });
  }
  reset() {
    this.resetButtonShowStatus = true;
    this.searchString = "";
    this.searchString2 = "";
    this.getCount();
    this.errMsg = false;
    this.searchStatus = false;
  }
  reFundProcess(transaction_id) {
    this.checkRefundStatus = false;
    this.refundStatus = 0;
    this.dataService.postAuth('api/merchant-check-refund', { transaction_id: transaction_id }).subscribe(result => {
      this.checkRefund = result.data
      this.checkRefund['transaction_id'] = transaction_id
      this.checkRefundStatus = true;
    })
  }
  refund() {
    this.refundButtonShowStatus = true;
    this.refundStatus = 0;
    this.dataService.postAuth('api/merchant-refund', { refund: this.checkRefund }).subscribe(result => {
      // console.log(result)
      if (result.status_code == 200) {
        if (this.searchStatus) {
          this.search();
        } else {
          this.getCount()
        }
        this.refundStatus = 1
        this.checkRefundStatus = false;
      } else if (result.status_code == 500) {
        this.checkRefundStatus = false;
        this.refundRemarks = result.message;
        this.refundStatus = 2
      }
      this.refundButtonShowStatus = false;
    })
  }
}
