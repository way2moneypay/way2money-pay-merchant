import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { TransactionsRoutingModule } from './transactions-routing.module';
import { TransactionsComponent } from './transactions.component';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';

@NgModule({
  imports: [
    CommonModule,
    TransactionsRoutingModule,
    NgbModule,
    FormsModule,
    AngularDateTimePickerModule
    
  ],
  declarations: [TransactionsComponent]
})
export class TransactionsModule { }
