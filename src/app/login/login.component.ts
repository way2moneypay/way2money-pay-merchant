import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RestServicesService } from './../shared/rest-services/rest-services.service';
import { CookieService } from 'ngx-cookie-service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  confirm_password: any;
  isResetPassword: boolean = false;
  isForgot: boolean = false;

  constructor(
    private dataService: RestServicesService,
    private cookieService: CookieService,
    private activated: ActivatedRoute,
    private router: Router,
    public toastr: ToastsManager, vcr: ViewContainerRef
  ) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  email: any;
  otp: any;
  isLogin: boolean = true;
  isRegister: boolean = false;
  isOTPSHOW: boolean = false;
  paramsData: any = {};
  fname: any;
  lname: any;
  mobile: any;
  password: any;
  user: any;
  token: any;
  ngOnInit() {
    this.toastr.success("I AM LOGIN");
    this.user = this.cookieService.get('name');
    this.token = this.cookieService.get('token');
    if (this.user && this.token) {
      this.router.navigate(['/app/dashboard']);
    }
    console.log("TOKEN NAME", this.user, this.token);
  }
  /* LOGIN FUNCTION */
  onClickLogin() {

    this.paramsData.mobile = this.mobile;
    this.paramsData.password = this.password;
    console.log("LOGIN PARAMS DATA", this.paramsData)
    this.dataService.post('api/login', this.paramsData).subscribe((login_response) => {
      console.log("LOGIN RESPONSE", login_response);

      if (login_response.status === "SUCCESS") {
        if (login_response.data && login_response.data.temp_password) {
          this.isLogin = false;
          this.isForgot = false;
          this.isRegister = false;
          this.isResetPassword = true;
        } else {
          this.cookieService.set('name', login_response.data.user_details.name, new Date(Date.now() + (86400000 * 7)), '/', );
          this.cookieService.set('mobile', login_response.data.user_details.mobile_number, new Date(Date.now() + (86400000 * 7)), '/', );
          this.cookieService.set('token', login_response.data.token, new Date(Date.now() + (86400000 * 7)), '/', );
          this.router.navigate(['/app/dashboard']);
        }
      }
      else if (login_response.status === "INVAU") {
        alert("Invalid password");
        console.log("INVALID PASSWORD");
      } else {
        alert("Please register first");
        console.log("NOT EXIST NUMBER");
        this.isLogin = false;
        this.isRegister = true;
      }

    })
  }
  onClickBack() {
    this.isOTPSHOW = false;
    this.isLogin = false;
    this.isRegister = true;
    this.isResetPassword = false;

  }
  onChangeRegister() {
    this.isForgot = false;
    this.isOTPSHOW = false;
    this.isLogin = false;
    this.isRegister = true;
    this.isResetPassword = false;

  }

  /* REGISTER FUNCTION */
  onClickNext() {
    this.isLogin = false;
    this.isForgot = false;
    this.isRegister = false;
    this.isOTPSHOW = true;
    this.paramsData = {};
    this.paramsData.mobile = this.mobile;
    this.dataService.post('api/registrationOTP', this.paramsData).subscribe((otp_response) => {
      console.log("OTP RESPOENSE", otp_response);
    })
  }
  isLoginChange() {
    this.isForgot = false;
    this.isOTPSHOW = false;
    this.isLogin = true;
    this.isRegister = false;
    this.isResetPassword = false;

  }
  onClickRegister() {
    this.paramsData.fname = this.fname;
    this.paramsData.lname = this.lname;
    this.paramsData.mobile = this.mobile;
    this.paramsData.password = this.password;
    this.paramsData.otp = this.otp;
    this.paramsData.register_date = Date.now();
    this.paramsData.email = this.email;
    console.log("PARAMS DATA", this.paramsData);
    this.dataService.post('api/register', this.paramsData).subscribe((register_response) => {
      console.log("REGISTER RESPONSE", register_response);
      if (register_response.data.user_details) {
        this.cookieService.set('name', register_response.data.user_details.name, new Date(Date.now() + (86400000 * 7)), '/', );
        this.cookieService.set('mobile', register_response.data.user_details.mobile_number, new Date(Date.now() + (86400000 * 7)), '/', );
        this.cookieService.set('token', register_response.data.token, new Date(Date.now() + (86400000 * 7)), '/', );
        this.router.navigate(['/app/dashboard']);
      } else {
        if (register_response.status === "MOBILALREDY") {
          this.router.navigate(['']);
        }
      }
    })


  }
  onClickChangePassword() {
    this.isLogin = false;
    this.isRegister = false;
    this.isOTPSHOW = false;
    this.isForgot = true;
    this.isResetPassword = false;
  }
  /* forgot password */
  onClickForgot() {
    this.paramsData = {};
    this.paramsData.mobile = this.mobile;
    this.dataService.post('api/forgotpassword', this.paramsData).subscribe((forgot_response) => {
      console.log("forgot RESPONSE", forgot_response);
      if (forgot_response.status_code === 200 && forgot_response.type === "SUCCESS") {
        alert("TEMPORARY PASSWORD HAS BEEN SENT TO YOUR MOBILE PLEASE LOGGIN WITH THAT");
        this.isForgot = false;
        this.isRegister = false;
        this.isForgot = false;
        this.isResetPassword = false;
        this.isLogin = true;
      } else {
        alert("MOBILE NUMBER IS NOT REGISTERED");
        this.isForgot = false;
        this.isRegister = true;
        this.isForgot = false;
        this.isResetPassword = false;
        this.isLogin = false;
      }
    })
  }
  /* Update Password */
  onClickUpdatePassword() {
    this.paramsData = {};
    this.paramsData.mobile = this.mobile;
    this.paramsData.password = this.password;
    this.paramsData.cpassword = this.confirm_password;
    if (this.password === this.confirm_password) {
      this.dataService.post("api/resetPassword", this.paramsData).subscribe((Response) => {
        console.log("UPDATE RESPONSE", Response);
        if (Response.type === "SUCCESS" && Response.data) {
          this.cookieService.set('name', Response.data.name, new Date(Date.now() + (86400000 * 7)), '/', );
          this.cookieService.set('token', Response.data.token, new Date(Date.now() + (86400000 * 7)), '/', );
          this.router.navigate(['/app/dashboard']);
        } else {
          console.log("NO DATA FOUND OF RECORD FOR U{DATING PASSWORD");
        }
      })
    } else {
      console.log("PASSWORD NOT MATCHED");
    }
  }
}
