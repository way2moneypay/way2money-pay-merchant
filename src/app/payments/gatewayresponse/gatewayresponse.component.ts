import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { RestServicesService } from './../../shared/rest-services/rest-services.service';

@Component({
  selector: 'app-gatewayresponse',
  templateUrl: './gatewayresponse.component.html',
  styleUrls: ['./gatewayresponse.component.scss']
})
export class GatewayresponseComponent implements OnInit {

  paymentId: String;
  paymentRequestId: String;
  // status:any;
  payment_response: any = {};
  response_message: String;
  constructor(private route: ActivatedRoute,
    private router: Router,
    private cookieService: CookieService,
    private restService: RestServicesService
  ) { }


  ngOnInit() {

    this.paymentRequestId = this.route.snapshot.queryParamMap.get('payment_request_id');
    this.paymentId = this.route.snapshot.queryParamMap.get('payment_id');
    // this.status = this.route.snapshot.queryParamMap.get('status');

    if (this.paymentRequestId && this.paymentId) {
      let paramData: any = {};
      paramData.payment_id = this.paymentId;
      paramData.payment_request_id = this.paymentRequestId;
      if (this.cookieService.get('token') && this.cookieService.get('name')) {
        this.restService.postAuth('payments/insta-mojo/payment-redirect', paramData).subscribe(response => {
          console.log('-------payment response',response);
          if (response.type === 'SUCCESS') {
            this.payment_response = response.data;
            this.setStatus(response.status);
            return;
          }

          this.response_message = 'Invalid payment url';
          console.log("Invalid authentication");
        });
      }
    }else{
      this.response_message = 'Invalid payment url';
      console.log('Invalid callback url and Invalid Autontication.');
    }
  }


  setStatus(status) {
    if (status === 'PENDING') {
      this.response_message = 'Payment still in process';
    }
    else if (status === 'FAILURE') {
      this.response_message = 'Payment Failed';
    }else if(status === 'SUCCESS'){
      this.response_message = 'Payment Success!';
    }
  }

  sendRoute() {
    return this.router.navigate(['/app/transactions']);
  }
}
