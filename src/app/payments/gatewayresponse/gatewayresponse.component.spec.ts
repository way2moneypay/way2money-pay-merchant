import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GatewayresponseComponent } from './gatewayresponse.component';

describe('GatewayresponseComponent', () => {
  let component: GatewayresponseComponent;
  let fixture: ComponentFixture<GatewayresponseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GatewayresponseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GatewayresponseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
