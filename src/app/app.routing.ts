import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { IsLogin,isProfileVerified } from "./shared/guard/auth.guard";
import { VerificationComponent } from './verification/verification.component';
import { GatewayresponseComponent } from './payments/gatewayresponse/gatewayresponse.component';

const routes: Routes = [
  { path: '', loadChildren: './home/home.module#HomeModule' },
  { path: 'app', loadChildren: './pages/pages.module#PagesModule', canActivate: [IsLogin] },
  { path: 'login', loadChildren: './login/login.module#LoginModule' },
  { path: 'verification', component: VerificationComponent },
  { path: 'payments/insta-mojo/response', component: GatewayresponseComponent }
];


@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes),

  ],
  exports: [
  ],
})
export class AppRoutingModule { }
