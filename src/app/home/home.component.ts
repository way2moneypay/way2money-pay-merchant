import { Component, OnInit, ViewContainerRef, Inject, PLATFORM_ID, ViewChild, ElementRef } from '@angular/core';
import { DOCUMENT } from "@angular/common";
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RestServicesService } from './../shared/rest-services/rest-services.service';
import { CookieService } from 'ngx-cookie-service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { Meta, Title } from "@angular/platform-browser";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  isTemporary: boolean = false;
  @ViewChild('closeBtn') closeBtn: ElementRef;
  @ViewChild('closeBtn2') closeBtn2: ElementRef;
  contactus: any = {};
  confirm_password: any;
  isResetPassword: boolean = false;
  isForgot: boolean = false;
  showResendButton: boolean = false;
  counter = 25;
  timer: boolean = false;
  isHome: any = "footer-home";
  mobile_number: any = {};
  merchantDetails: any = {};
  constructor(
    private dataService: RestServicesService,
    private cookieService: CookieService,
    private activated: ActivatedRoute,
    private router: Router,
    public toastr: ToastsManager, vcr: ViewContainerRef,
    @Inject(DOCUMENT) private document: Document,
    @Inject(PLATFORM_ID) public platformId: Object, meta: Meta, title: Title
  ) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  email: any;
  otp: any;
  countDown;
  isPasswordSHOW: boolean = false;
  isLogin: boolean = true;
  isRegister: boolean = false;
  isOTPSHOW: boolean = false;
  paramsData: any = {};
  fname: any;
  lname: any;
  mobile: any;
  password: any;
  user: any;
  token: any;
  loginPop: any = true;
  mobile_error: any;
  showSentMessage: boolean = false;
  ngOnInit() {
    window.scrollTo(0, 0);
    this.token = this.cookieService.get('merchant_token');
    if (this.token && this.merchantDetails.is_profile_verified) {
      this.router.navigate(['/app/dashboard']);
    }else if(this.token){
      this.router.navigate(['/app/merchant-profile']);
    }else{
      this.router.navigate(['']);
    }
  }
  getMerchantDetails (){
    this.dataService.getAuth('api/merchant/getDetails').subscribe((result) => {
      // console.log("result", result);
      if (result && result.status_code == 200) {
        this.merchantDetails = result.data;
        this.ngOnInit();
      } else {
        this.merchantDetails = {};
      }
    });
  }
  loginClicked() {
    this.isLogin = true;
  }
  generateOTP() {
    this.paramsData = {};
    this.paramsData.mobile = this.mobile;
    if(this.otp){
      this.otp=null;
    }
    this.counter = 30;
    this.countDown = Observable.timer(0, 1000)
      .take(this.counter)
      .map(() => --this.counter);
    this.dataService.post('api/registrationOTP', this.paramsData).subscribe((otp_response) => {
      // console.log("otp_response", otp_response);
      if (otp_response.status == "SUCCESS" && otp_response.status_code == 200) {
        this.isOTPSHOW = true;
        this.showSentMessage = true;
        this.showResendButton = false;
        setTimeout(() => {
          this.timer = false;
          this.showResendButton = true;
          this.showSentMessage = false;
        }, 30000);
        this.timer = true;
      } else {
        this.toastr.error(otp_response.message)
      }
    });
  }
  onClickLogin() {
    this.paramsData = {};
    this.paramsData.mobile = this.mobile;
    this.paramsData.otp = this.otp;
    this.dataService.post('api/login', this.paramsData).subscribe((login_response) => {
      // console.log("login_response", login_response);
      if (login_response.status == "SUCCESS" && login_response.status_code == 200) {
        // this.cookieService.set('name', login_response.data.merchant_details.name, new Date(Date.now() + (86400000 * 7)), '/', );
        // this.cookieService.set('mobile', login_response.data.merchant_details.mobile_number, new Date(Date.now() + (86400000 * 7)), '/', );
        this.cookieService.set('merchant_token', login_response.data.merchant_token, new Date(Date.now() + (86400000 * 7)), '/', );
        this.getMerchantDetails();
      } else {
        this.toastr.error(login_response.message)
      }
    });

  }
  modelClose(){
   this.mobile=null;
   this.isOTPSHOW=false;
  }
}
