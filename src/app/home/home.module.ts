import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgbModule, NgbPagination } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { HttpModule } from '@angular/http';
import { FormFieldsModule } from './../shared/modules/form-fields/form-fields.module';
import { AboutUsComponent } from './about-us/about-us.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { TermsComponent } from './terms/terms.component';
import { FaqsComponent } from './faqs/faqs.component';
import { StaticHeaderComponent } from './static-header/static-header.component';
import { FooterModule } from './../shared/footer/footer.module';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        HomeRoutingModule,
        FormsModule,
        NgbModule.forRoot(),
        FormFieldsModule,
        FooterModule
    ],
    declarations: [
        HomeComponent,
        AboutUsComponent,
        PrivacyComponent,
        TermsComponent,
        FaqsComponent,
        StaticHeaderComponent
    ]
})
export class HomeModule { }
