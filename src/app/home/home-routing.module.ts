import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from "./home.component";
import { AboutUsComponent } from "./about-us/about-us.component";
import { TermsComponent } from "./terms/terms.component";
import { PrivacyComponent } from "./privacy/privacy.component";
import { FaqsComponent } from "./faqs/faqs.component";


const routes: Routes = [
  {
    path: '', children: [
      { path: '', component: HomeComponent },
      { path: 'about-us', component: AboutUsComponent },
      { path: 'terms', component: TermsComponent },
      { path: 'faqs', component: FaqsComponent },
      { path: 'privacy', component: PrivacyComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
