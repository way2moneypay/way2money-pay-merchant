import { Directive, Input, Output, ElementRef, SimpleChanges, HostListener } from '@angular/core';
import { ValueTransformer } from '@angular/compiler/src/util';

@Directive({
  selector: '[InputText]'
})
export class InputTextDirective {
  @Input() Min: any;
  @Input() minLength: any;
  @Input() Max: any;
  @Input() InputType: any;
  @Input() addCommas: Boolean;
  @Input() changeKeyUp: Boolean;
  @Input() capitalize: Boolean;
  @Input() maxLength: any;
  @Input() trimSpaces: any;

  constructor(
    private element: ElementRef
  ) {
    this.functionality(null);
  }

  commafy(num) {
    var str = num.toString().split('.');
    if (str[0].length >= 5) {
      str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');
    }
    if (str[1] && str[1].length >= 5) {
      str[1] = str[1].replace(/(\d{3})/g, '$1 ');
    }
    return str.join('.');
  }

  removeComma(string) {
    string = string.replace(/,/g, '');
    return string;
  }

  replaceValue(regex: RegExp) {
    let value = this.element.nativeElement.value;
    if (regex.test(value)) {
      // console.log(regex.test(value))
      this.element.nativeElement.value = value.replace(regex, "");
      this.element.nativeElement.dispatchEvent(new Event("input"));
    }
  }

  makeInputType() {
    let pattern: RegExp;
    let value = this.element.nativeElement.value;
    if (this.InputType == "number-nlz") {
      this.replaceValue(/^0+/g);
      this.replaceValue(/[^0-9,]+/g);
    }

    if (this.InputType == "number") {
      this.replaceValue(/[^0-9,]+/g);
    }
    if (this.InputType == "number-ncm") {
      this.replaceValue(/[^0-9]+/g);
    }

    if (this.InputType == "string") {
      this.replaceValue(/[^a-zA-Z ]+/g)
    }

    if (this.InputType == "word") {
      this.replaceValue(/[^a-zA-Z0-9]+/g)
    }

  }

  makeOnKeyUpEvents(event: KeyboardEvent) {

    let value = this.element.nativeElement.value;

    if (this.maxLength < value.length) {
      this.element.nativeElement.value = value.substr(0, this.maxLength);
      this.element.nativeElement.dispatchEvent(new Event("input"));
    }
    if (this.minLength > value.length) {
      this.element.nativeElement.value = value.substr(0, this.minLength);
      this.element.nativeElement.dispatchEvent(new Event("input"));
    }

    if (this.addCommas) {
      this.element.nativeElement.value = this.commafy(this.removeComma(this.element.nativeElement.value));
      this.element.nativeElement.dispatchEvent(new Event("input"));
    }

    if (this.InputType) {
      this.makeInputType();
    }

    if (this.capitalize) {
      this.element.nativeElement.value = this.element.nativeElement.value.toUpperCase();
      this.element.nativeElement.dispatchEvent(new Event("input"));
    }

    if (/^\s+$/.test(value) && this.trimSpaces) {
      this.element.nativeElement.value = "";
      this.element.nativeElement.dispatchEvent(new Event("input"));
    }


  }

  functionality(event: KeyboardEvent) {

    let ignore_keys = [17, 37, 38, 39, 40, 8];
    let ctrl_ignore_keys = [65, 97, 67, 99];

    if (event && event.ctrlKey && ctrl_ignore_keys.indexOf(event.keyCode) != -1) {
      return;
    }

    if (event && ignore_keys.indexOf(event.keyCode) != -1) {
      return;
    }

    if (this.changeKeyUp) this.makeOnKeyUpEvents(event);
  }

  @HostListener('change', ['$event']) onChange(event: KeyboardEvent) {
    this.functionality(event);
  }

  @HostListener('input', ['$event']) onInput(event: KeyboardEvent) {
    this.functionality(event);
  }

  @HostListener('keyup', ['$event']) onKeyUp(event: KeyboardEvent) {
    this.functionality(event);
  }

  // @HostListener('keypress', ['$event']) onKeyPress(event: KeyboardEvent) {
  //   this.functionality(event);
  // }


  // @HostListener('keydown', ['$event']) onKeyDown(event: KeyboardEvent) {
  //   this.functionality(event);
  // }

}
