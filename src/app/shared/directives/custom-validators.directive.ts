import { Directive, Input } from "@angular/core";
import { NG_VALIDATORS, FormControl, Validator } from '@angular/forms';

function removeComma(string) {
    string = string.replace(/,/g, '');
    return string;
}

@Directive({
    selector: '[minValue][ngModel]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: MinValueValidator, multi: true, deps: []}
    ]
})
export class MinValueValidator implements Validator {
    @Input() minValue: number;

    validate(c: FormControl) {
        let value = Number(c.value);
        return (value < this.minValue) ? {
            "minValue": {
                actualValue: value,
                requiredValue: this.minValue
            }
        } : null;
    }
}

@Directive({
    selector: '[maxValue][ngModel]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: MaxValueValidator, multi: true, deps: [] }
    ]
})
export class MaxValueValidator implements Validator {
    @Input() maxValue: number;

    validate(c: FormControl) {
        let value = Number(c.value);
        
        return (value > this.maxValue) ? {
            "maxValue": {
                actualValue: value,
                requiredValue: this.maxValue
            }
        } : null;
    }
}

@Directive({
    selector: '[rangeValue][ngModel]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: RangeValueValidator, multi: true, deps: [] }
    ]
})
export class RangeValueValidator implements Validator {
    @Input() minValue: number;
    @Input() maxValue: number;


    validate(c: FormControl) {
        let value = Number(c.value);
        
        return (value > this.maxValue || value < this.minValue) ? {
            "rangeValue": {
                actualValue: value,
                requiredValue: this.minValue + "-" + this.maxValue
            }
        } : null;
    }
}