import { Component, OnInit, ElementRef } from '@angular/core';
import { ROUTES } from './../../pages/sidebar/sidebar.component';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
@Component({
    // moduleId: module.id,
    selector: 'navbar-cmp',
    templateUrl: 'navbar.component.html'
})

export class NavbarComponent implements OnInit {
    private listTitles: any[];
    private toggleButton: any;
    private sidebarVisible: boolean;

    constructor(private element: ElementRef, private router: Router, private cookieService: CookieService, ) {
        this.sidebarVisible = false;
    }
    isLoginSHOW: boolean = false;

    user: String;
    token: String;
    ngOnInit() {
        console.log("RECHED HERE NAVBAR");
        this.user = this.cookieService.get('name');
        this.token = this.cookieService.get('token');
        console.log("TOKEN AND ANME", this.user, this.token);
        if (this.user || this.token) {
            // alert("user")
            console.log("THIs person is already logged in");
            this.isLoginSHOW = false;

        } else {
            // alert("no user")
            this.isLoginSHOW = true;
            console.log("First time login ");

        }
        this.listTitles = ROUTES.filter(listTitle => listTitle);
        const navbar: HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
    }
    sidebarOpen() {
        const toggleButton = this.toggleButton;
        const body = document.getElementsByTagName('body')[0];
        setTimeout(function () {
            toggleButton.classList.add('toggled');
        }, 500);
        body.classList.add('nav-open');

        this.sidebarVisible = true;
    };
    sidebarClose() {
        const body = document.getElementsByTagName('body')[0];
        this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        body.classList.remove('nav-open');
    };
    sidebarToggle() {
        // const toggleButton = this.toggleButton;
        // const body = document.getElementsByTagName('body')[0];
        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        } else {
            this.sidebarClose();
        }
    };

    getTitle() {
        let menuItems = ROUTES.filter(menuItem => menuItem);
        for (var index = 0; index < menuItems.length; ++index) {
            if (menuItems[index]['pathRoute'] == location.pathname) {
                return menuItems[index]['title'];
            }
        }
        return 'Dashboard';
    }
    logout() {
        console.log("LOGOUT IN NAVBAR");

        this.cookieService.delete('user', "/");
        this.cookieService.delete('token', "/");
        this.cookieService.delete('mobile', "/");
        this.cookieService.deleteAll();
        this.isLoginSHOW = true;
        this.router.navigate(['']);
    }
    login() {
        console.log("LOGGING IN NAVBAR");
        this.router.navigate(['/login']);
    }
}
