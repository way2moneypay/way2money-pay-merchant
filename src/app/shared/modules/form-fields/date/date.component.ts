import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.css']
})
export class DateComponent implements OnInit {
  @Input() value: any;
  @Input() minDate: any;
  @Input() maxDate: any;
  @Input() formYear: any;
  @Input() minYear: number;
  @Input() maxYear: number;
  @Input() isDisabled: boolean;

  @Output() valueChange: EventEmitter<any> = new EventEmitter();


  month: any;
  day: any;
  year: any;

  days: any = [];
  month_numbers: any = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
  month_names: any = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  years: any = [];

  constructor() {

  }

  days_data: any = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

  getRange(min, max, type) {
    let range = [];
    for (var index = min; index <= max; ++index) {
      let value = index;
      if (type == "str") {
        value = value < 10 ? "0" + value : value + "";
      }
      range.push(value);
    }
    return range;
  }


  checkLeapYear(year) {
    return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
  }



  prepareMonths(year) {
    if (this.maxDate && this.maxDate.year && this.maxDate.year == year) {
      this.month_names = this.month_names.slice(0, this.maxDate.month);
      this.month_numbers = this.month_numbers.slice(0, this.maxDate.month);
    }
  }


  prepareDates(year, month) {
    if (this.checkLeapYear(year) && month == 2) {

      this.days = this.getRange(1, 29, "str");
      return;
    }

    this.days = this.getRange(1, this.days_data[month - 1], "str");
  }

  prepareYears() {
    let min_year = new Date().getFullYear() - 100;
    let max_year = new Date().getFullYear() + 100;

    if (this.minDate && this.minDate.year) min_year = this.minDate.year;
    if (this.maxDate && this.maxDate.year) max_year = this.maxDate.year;
    // console.log(this.minDate);
    this.years = this.getRange(min_year, max_year, "number");
  }

  changeDate() {
    if (this.value && this.value.year && this.value.month && this.value.day) {
      this.valueChange.emit(this.value);
    }
  }

  ngOnInit() {
    if (!this.value.day) this.value.day = "";
    if (!this.value.month) this.value.month = "";
    if (!this.value.year) this.value.year = "";
    this.prepareDates(new Date().getFullYear(), new Date().getMonth() + 1);
    this.prepareYears();
  }

}
