import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';

@Component({
  selector: 'field-error',
  templateUrl: './form-error.component.html',
  styleUrls: ['./form-error.component.css']
})
export class FormErrorComponent implements OnInit {
  @Input() type: String;
  @Input() field: any;
  @Input() fieldType: String;
  @Input() fieldName: String;
  @Input() validationError: any;
  @Input() PatternMessage: String;
  @Input() className: String;
  constructor() {
    console.log(this.field)
  }

  ngOnInit() {
  }

}
