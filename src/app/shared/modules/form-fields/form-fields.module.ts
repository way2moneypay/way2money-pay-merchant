import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DateComponent } from './date/date.component';
import { AutoCompleteComponent } from './auto-complete/auto-complete.component';
import { ClickOutsideModule } from 'ng4-click-outside';
import { InputTextDirective } from './../../directives/input-text.directive';
import { FormErrorComponent } from './form-error/form-error.component'
import { MinValueValidator, MaxValueValidator, RangeValueValidator } from "./../../directives/custom-validators.directive";
import { PartitionFieldComponent } from './partition-field/partition-field.component';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ClickOutsideModule
  ],
  declarations: [
    DateComponent, AutoCompleteComponent, InputTextDirective, FormErrorComponent,
    MinValueValidator, MaxValueValidator, RangeValueValidator, PartitionFieldComponent

  ],
  exports: [
    DateComponent, AutoCompleteComponent,
    InputTextDirective, FormErrorComponent, MinValueValidator,
    MaxValueValidator, RangeValueValidator, PartitionFieldComponent
  ]
})
export class FormFieldsModule { }
