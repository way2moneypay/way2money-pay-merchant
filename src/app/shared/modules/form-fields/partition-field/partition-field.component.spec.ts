import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartitionFieldComponent } from './partition-field.component';

describe('PartitionFieldComponent', () => {
  let component: PartitionFieldComponent;
  let fixture: ComponentFixture<PartitionFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartitionFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartitionFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
