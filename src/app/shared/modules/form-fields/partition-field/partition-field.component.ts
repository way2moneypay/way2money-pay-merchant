import { Component, OnInit, Input, Output, EventEmitter, PLATFORM_ID, Inject } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { DOCUMENT } from '@angular/platform-browser';

@Component({
  selector: 'partition-field',
  templateUrl: './partition-field.component.html',
  styleUrls: ['./partition-field.component.css']
})
export class PartitionFieldComponent implements OnInit {
  @Input() valueDetails: any;
  @Input() valueClass: string;
  @Input() partitionCount: number;
  @Input() value: string;

  @Output() valueChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() inputChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() inputClick: EventEmitter<any> = new EventEmitter<any>();
  inputData: any = [];
  back_spaces: number = 0;
  constructor(@Inject(DOCUMENT) private document: Document,
    @Inject(PLATFORM_ID) private platformId: string) { }

  ngOnInit() {
    console.log(this.valueDetails);
  }

  ngAfterViewInit() {
    if (isPlatformBrowser(this.platformId)) {
      let element: any = this.document.getElementsByClassName('partition')[0];
      element.focus();
    }
  }

  clickEvent() {
    this.inputClick.emit();
  }

  getRange(min, max) {
    let results = [];
    for (var index = min; index <= max; ++index) {
      results.push(index);
    }
    return results;
  }

  ngOnChanges(changes) {
    if (changes.value && this.value != undefined && this.value != null) {
      this.inputData[0] = this.value[0];
      this.inputData[1] = this.value[1];
      this.inputData[2] = this.value[2];
      this.inputData[3] = this.value[3];
    }
  }

  getType(value) {
    let number_regex: RegExp = /\d+/
    if (number_regex.test(value)) return "number";
  }

  onKeyUp(event: KeyboardEvent, event_type, input_value) {

    if (!event.srcElement && !event.target) return;
    console.log(input_value)


    let keyCode = event.keyCode || event.which || event.charCode;
    let type = this.getType(input_value);

    let value = this.inputData.join('');
    console.log(event.which, event.keyCode)
    if (event_type == "number") Number(value);
    if (type == event_type || keyCode == 8) this.valueChange.emit(value)


    let element: any = event.srcElement || event.target;
    let next: any = element.nextSibling;
    let prev: any = element.previousSibling;

    if (keyCode == 37 || keyCode == 8) {
      if (prev && prev.focus) prev.focus(); return;
    }

    if (keyCode == 39 || type == event_type) {
      if (next && next.focus) next.focus();
    }


  };
}
