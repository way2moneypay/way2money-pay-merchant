import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CookieService } from "ngx-cookie-service";
import { RestServicesService } from "./../rest-services/rest-services.service";

@Injectable()
export class IsLogin implements CanActivate {

    constructor(private router: Router, private cookieService: CookieService) {

    }
    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        // return true;

        if (!this.cookieService.get('merchant_token')) {
            this.router.navigate(['']);
        }
        return true;
    }
}
@Injectable()
export class isProfileVerified implements CanActivate {
    isMenuItemsShow: any;
    is_profile_verified: boolean = false;
    is_admin_verified: boolean = false;

    constructor(private router: Router, private cookieService: CookieService, private dataService: RestServicesService) {

    }
    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        // return true;
        this.dataService.getAuth("api/merchant/getDetails").subscribe((result) => {
            // console.log("result----------->", result);
            if (result && result.status_code == 200) {
                this.is_profile_verified = result.data.is_profile_verified;
                this.is_admin_verified = result.data.is_admin_verified;
                // this.ngOnInit();
                // console.log("isMenuItemsShow", this.isMenuItemsShow);
                if (this.cookieService.get('merchant_token') && this.is_profile_verified && this.is_admin_verified) {
                    if (this.router.routerState.snapshot.url == "/app/merchant-profile") {
                        this.router.navigate(['/app/merchant-profile/1']);
                    } else {
                        return true;
                    }
                } else if (this.cookieService.get('merchant_token') && this.is_profile_verified) {
                    if (this.router.routerState.snapshot.url == "/app/merchant-profile") {
                        this.router.navigate(['/app/merchant-profile/1']);
                    } else if (this.router.routerState.snapshot.url == "/app/merchant-profile/1") {
                        return true;
                    } else {
                        this.router.navigate(['/app/dashboard']);
                    }
                } else if (this.cookieService.get('merchant_token') && !this.is_profile_verified) {
                    this.router.navigate(['/app/merchant-profile']);
                } else if (!this.cookieService.get('merchant_token')) {
                    this.router.navigate(['']);
                } else {
                    return false;
                }
            } else {
                if (this.cookieService.get('merchant_token')) {
                    this.router.navigate(['/app/merchant-profile']);
                } else if (!this.cookieService.get('merchant_token')) {
                    this.router.navigate(['']);
                } else {
                    return false;
                }
            }
        });
        return true;
    }
}



@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router) { }

    canActivate() {

        const now = new Date().getTime();
        const f_expiry = localStorage.getItem('f_expiry');
        if (f_expiry != null && +f_expiry < now) {
            if (localStorage.getItem('name') != null) {
                localStorage.removeItem('name');
            }
            if (localStorage.getItem('_wf_token') != null) {
                localStorage.removeItem('_wf_token');
            }
            localStorage.removeItem('f_expiry');
            this.router.navigate(['/login']);
            return false;
        }

        if (localStorage.getItem('name') && localStorage.getItem('_wf_token') && localStorage.getItem('f_expiry')) {
            return true;
        }

        this.router.navigate(['/login']);
        return false;

    }
}
