import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, ResponseContentType } from '@angular/http';
import { environment } from './../../../environments/environment';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { HttpRequest, HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
@Injectable()
export class RestServicesService {

  apiurl: string = environment.apiurl;

  constructor(private http: Http, private cookieService: CookieService, private router: Router, private httpclient: HttpClient) { }

  post(url, body) {
    console.log(this.apiurl + url);
    return this.http.post(this.apiurl + url, body)
      .map(res => res.json());
  }

  get(url) {
    return this.http.get(this.apiurl + url)
      .map(res =>
        res.json()
      );
  }
  downloadImg(url) {
    const headers = new Headers();
    const token = this.cookieService.get('merchant_token')
    headers.append('x-access-token', token);
    const opts = new RequestOptions();
    opts.headers = headers;
    opts.responseType = ResponseContentType.Blob

    return this.http
      .get(this.apiurl + url, opts).map(res => {
        return {
          filename: 'QRImage.png',
          data: res.blob()
        };
      })
      .subscribe(res => {
        var url = window.URL.createObjectURL(res.data);
        var a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = url;
        a.download = res.filename;
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove(); // remove the element
      }, error => {
        console.log('download error:', JSON.stringify(error));
      }, () => {
      });

  }
  getAuth(url) {
    const headers = new Headers();
    const token = this.cookieService.get('merchant_token')
    headers.append('x-access-token', token);
    const opts = new RequestOptions();
    opts.headers = headers;
    return this.http.get(this.apiurl + url, opts)
      .map(res => {
        return this.expireToken(res, false);
      });
  }

  getAuthParams(url, params) {
    console.log(url, params)
    const headers = new Headers();
    // const token = localStorage.getItem('p_au_token');
    const token = this.cookieService.get('merchant_token')

    headers.append('x-access-token', token);
    const opts = new RequestOptions();
    opts.headers = headers;
    opts.params = params;

    return this.http.get(this.apiurl + url, opts)
      .map(res => {
        return this.expireToken(res, false);
      });
  }
  getAuthParamsNew(url, params) {
    const headers = new Headers();
    // const token = localStorage.getItem('p_au_token');
    const token = this.cookieService.get('merchant_token')

    headers.append('x-access-token', token);
    const opts = new RequestOptions();
    opts.headers = headers;
    opts.params = params;

    return this.http.get(this.apiurl + url, opts)
      .map(res => {
        return this.expireToken(res, true);
      });
  }

  getParams(url, params) {
    const opts = new RequestOptions();
    opts.params = params;
    return this.http.get(this.apiurl + url, opts)
      .map(res => res.json());
  }

  postAuth(url, body) {
    const headers = new Headers();
    // const token = localStorage.getItem('p_au_token');
    const token = this.cookieService.get('merchant_token')

    headers.append('x-access-token', token);
    const opts = new RequestOptions();
    opts.headers = headers;

    return this.http.post(this.apiurl + url, body, opts)
      .map(res => {
        return this.expireToken(res, false);
      });
  }

  expireToken(res, notjson) {
    if ('' + res.json().status === 'AUTHTOKENERR') {
      if (localStorage.getItem('p_name') != null) {
        localStorage.removeItem('p_name');
      }
      if (localStorage.getItem('p_au_token') != null) {
        localStorage.removeItem('p_au_token');
      }
      if (localStorage.getItem('p_profile_pic')) {
        localStorage.removeItem('p_profile_pic');
      }
      localStorage.removeItem('p_expiry');
      return this.router.navigate(['/']);
    } else {
      if (notjson) {
        return res;
      } else {
        return res.json();
      }
    }
  }

  prepareParams(data) {
    let params = new HttpParams();
    let param_keys = Object.keys(data);
    for (var index = 0; index < param_keys.length; ++index) {
      let key = param_keys[index];
      let value = data[key];
      params = params.append(key, value);
    }

    return params;
  }

  upload(url, file, file_key, data, progress) {
    // const token = localStorage.getItem('_wf_token');
    const token = this.cookieService.get('merchant_token')

    let form_data = new FormData();
    form_data.append(file_key, file);

    let params = this.prepareParams(data);

    let request_options: any = {};
    progress ? request_options.reportProgress = true : "no progress";
    request_options.params = params;

    let request = new HttpRequest("POST", this.apiurl + url, form_data, request_options);

    return this.httpclient.request(request);
  }


  uploadAuth(url, file, file_key, data, progress) {
    // const token = localStorage.getItem('_wf_token');
    const token = this.cookieService.get('merchant_token')

    const headers = new HttpHeaders().set('x-access-token', token);

    let form_data = new FormData();
    form_data.append(file_key, file);

    let params = data ? this.prepareParams(data) : {};

    let request_options: any = {};
    progress ? request_options.reportProgress = true : "no progress";
    request_options.params = params;
    request_options.headers = headers;

    let request = new HttpRequest("POST", this.apiurl + url, form_data, request_options);
    console.log('request :: ', request);
    return this.httpclient.request(request);
  }

}
