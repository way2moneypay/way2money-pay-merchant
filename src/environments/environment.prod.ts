export const environment = {
  production: true,
  apiurl: 'https://business.way2money.com/',
  images_cdn: 'https://business.way2money.com/'
};
